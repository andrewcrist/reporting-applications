import pandas as pd
import numpy as np
import os
import glob
import re
from random import shuffle
from collections import Counter
import warnings
import xlsxwriter
from functools import reduce
from statistics import mean
import math
import sys
import itertools
import difflib
import logging

def ask_user():
    check = str(input("Would you like section headers? (y/n): ")).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_user()
    except Exception as error:
        print("Please enter valid inputs")
        print(error)
        return ask_user()
        
def ppl_parser(df):
    # Create dataframe of respondents
    ppl = dict()
    cols = ['ResponseID', 'UniqueIdentifier', 'FirstName', 'LastName', 'MiddleName', 'Capacity', 'Audit', 'Compensation', 'NomGov', 'Title']
    cols = ['UniqueIdentifier', 'FirstName', 'LastName', 'MiddleName', 'Capacity', 'Audit', 'Compensation', 'NomGov', 'Title']
    for k, v in df.items():
        if k in cols:
            ppl[k] = v

    ppl_df = pd.DataFrame.from_dict(ppl)
    # if (list(ppl_df.columns)[0] == '\ufeffUniqueIdentifier') | ((list(ppl_df.columns)[0] == '\ufeffResponseID')):
    #     newFirstCol = re.sub('\ufeff', '', list(ppl_df.columns)[0])
    #     ppl_df = ppl_df.rename(columns={list(ppl_df.columns)[0]: newFirstCol})

    # if 'ResponseID' in ppl_df.columns:
    #     ppl_df.set_index('ResponseID', inplace=True)
    # elif  'UniqueIdentifier' in ppl_df.columns:
    #     ppl_df.set_index('UniqueIdentifier', inplace=True)
    if  'UniqueIdentifier' in ppl_df.columns:
        ppl_df.set_index('UniqueIdentifier', inplace=True)
    # ppl_df['test'] = [2,3,4,5,6,7]
    # ppl_df.set_index('test', inplace=True)
    # print(ppl_df.index)
    if None in set(ppl_df['Capacity'].tolist()) or '' in set(ppl_df['Capacity'].tolist()):
        logging.warning('Null values in Capacity column.\nppl_df will not be sorted.')
        return ppl_df

    orderedCapacities = ['Both', 'Director', 'Officer']
    df_list = []
    for i in orderedCapacities:
        d = ppl_df[ppl_df['Capacity']==i]
        d.sort_values(['LastName', 'FirstName'], inplace=True)
        df_list.append(d)
    ppl_df = pd.concat(df_list)

    return ppl_df

def parse_raw_fsr(dictionary):
    raw = pd.DataFrame.from_dict(dictionary)
    print('\nRaw Shape: ', raw.shape)

    # if (list(raw.columns)[0] == '\ufeffUniqueIdentifier') | ((list(raw.columns)[0] == '\ufeffResponseID')):
    #     newFirstCol = re.sub('\ufeff', '', list(raw.columns)[0])
    #     raw = raw.rename(columns={list(raw.columns)[0]: newFirstCol})

    # if 'ResponseID' in raw.columns:
    #     raw.set_index('ResponseID', inplace=True)
    # elif  'UniqueIdentifier' in raw.columns:
    #     raw.set_index('UniqueIdentifier', inplace=True)
    if  'UniqueIdentifier' in raw.columns:
        raw.set_index('UniqueIdentifier', inplace=True)
    columns = raw.columns

    # Identify where the data begins and ends.
    # Identify what kind of survey style is in use
    re_alpha_first_col = re.compile('^(?!.*\(Continued\))[A-Za-z\s\t]*(Part)?[\t\s]*(A)[\s\t]?[\.:—-]')
    alpha_firstCols = list(filter(re_alpha_first_col.match, columns))
    re_roman_first_col = re.compile('^(?!.*\(Continued\))[A-Za-z\s\t]*(Part)?[\t\s]*(I)[\s\t]?[\.:—-]')
    roman_firstCols = list(filter(re_roman_first_col.match, columns))
    re_arabic_first_col = re.compile('^(?!.*\(Continued\))[A-Za-z\s\t]*(Part)?[\t\s]*(1)[\s\t]?[\.:—-]')
    arabic_firstCols = list(filter(re_arabic_first_col.match, columns))
    # Some reports have data that proceed the first instance of section symbology
    # Example: GENERAL INFORMATION
    # To present
    re_multipleWords_first_col = re.compile('\w+[\s\t]+\w+')
    multipleWords = list(filter(re_multipleWords_first_col.match, columns))
    re_one_word_ends_with_colon = re.compile('\w+:')
    oneWordColon = list(filter(re_one_word_ends_with_colon.match, columns))
    # Sometimes more than one style will be found
    first_col_list = []
    symbols = []
    if alpha_firstCols:
        alpha_firstCol = alpha_firstCols[0]
        alpha_symbol = re_alpha_first_col.match(alpha_firstCol).group(2)
        first_col_list.append(alpha_firstCol)
        symbols.append(alpha_symbol)
    if roman_firstCols:
        roman_firstCol = roman_firstCols[0]
        roman_symbol = re_roman_first_col.match(roman_firstCol).group(2)
        first_col_list.append(roman_firstCol)
        symbols.append(roman_symbol)
    if arabic_firstCols:
        arabic_firstCol = arabic_firstCols[0]
        arabic_symbol = re_arabic_first_col.match(arabic_firstCol).group(2)
        first_col_list.append(arabic_firstCol)
        symbols.append(arabic_symbol)
    if multipleWords:
        multipleWords_first_col = multipleWords[0]
        first_col_list.append(multipleWords_first_col)
        symbols.append('')
    if oneWordColon:
        oneWordFirstColumn = oneWordColon[0]
        first_col_list.append(oneWordFirstColumn)
        symbols.append('w')

    if len(symbols)==0:
        logging.warning('No symbology found!')

    # print('\nAll Possible First Column Symbols Found: ', symbols)
    # print('Length of first_col_list=', len(first_col_list))
    # print('\nVariable: first_col_list:')
    # for col in first_col_list:
    #     print('\n')
    #     print(col)
    # print('\nEnd Col List\n------------------\n\n')

    # It's important to note here that, above, multipleWords is tested for last.
    # Because multipleWords is appended to first_col_list last, the first item in
    # first_col_list shouldn't be ''.

    # If there are reports using no symbology style, then the loop below will
    # need to be changed, and the last condition below resulting in:
    # logging.warning('\nNO SYMBOL!\n')
    # will need to be swapped out for handling of word column symobology
    for fc, sym in zip(first_col_list, symbols):
        if fc == first_col_list[0]:
            first_col_number = raw.columns.get_loc(fc)
            first_col_name = fc
            if sym != '' and sym != 'w':
                symbol = sym
        else:
            if raw.columns.get_loc(fc) < first_col_number:
                first_col_number = raw.columns.get_loc(fc)
                first_col_name = fc
                if sym != '' and sym != 'w':
                    symbol = sym
            else:
                continue

    # print('\nFirst Column Name:', first_col_name)
    # print('\nSymbol', symbol)
    # print('\n')
    # print('\n----------Questions:\n')


    # Trim down to the raw data
    data = raw.iloc[:, first_col_number:]
    questions_df = pd.DataFrame({'question_raw': data.columns})
    header_symbol_list = []
    questions = []
    header_list = []
    re_scrubbed_continued = re.compile('.*\(continued\)[\t\s]*(.*)', re.IGNORECASE)
    # re_headers only works in conjunction with each version of re_sections
    re_headers = re.compile('(.*?)\s\s')
    re_no_space_header_for_arabic = re.compile('(.*?\d+[\.:]?)')
    # re_test = re.compile('(.*?\d+[\s\t]?[\.:—–][\s\t]?.*?)(\s\s|(?<!:))')

    # Do the work of identifying
    for i, value in zip(questions_df.index, questions_df['question_raw']):
        if symbol == 'A':
            # section_name_with_number_and_content_between = re.compile('(?=(^|^[\t\s]*|^Part[\s\t]+)(^[A-Z]\.\s+[/A-Z0-9\(\)"\'\s\t\.-]+)[\t\s]*(_<)?[A-Z]?[a-z]+.*1\.\s+.*')
            re_sections = re.compile('^(?!.*\(Continued\))[\t\s]*(Part[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([A-Z]{1,2})[\s\t]?[\.:—–][\s\t]?(.*)')
            section_groups = re_sections.match(value)
            # print('\nvalue:', value)
            scrubbed_continued = re_scrubbed_continued.match(value)
            if section_groups is not None:
                # print('\nvalue:', value)
                # print(section_groups.groups())
                if section_groups.group(3) not in set(header_symbol_list):
                    header_symbol_list.append(section_groups.group(3))
                    header = re_headers.match(value)
                    if header is not None:
                        header_list.append(header.group(1))
                    else:
                        logging.warning('No formatted header found')
                        header_list.append(None)
                else:
                    header_symbol_list.append(None)
                    header_list.append(None)
                # print('\nvalue:', value)
                # print(section_groups.groups())
                questions.append(section_groups.group(4))
            elif scrubbed_continued is not None:
                # print('\n')
                # print(scrubbed_continued.group(0))
                # print(scrubbed_continued.group(1))
                questions.append(scrubbed_continued.group(1))
                header_symbol_list.append(None)
                header_list.append(None)
            else:
                header_symbol_list.append(None)
                questions.append(value)
                header_list.append(None)

                # print(section_groups.groups())
            # subsection = re.compile('')
    #         section_name = section_name_with_number_and_content_between.match(value)
    #         if section_name is None:
    #             section_name_with_number = re.compile('(^[A-Z]\.\s+[A-Z0-9\(\)/"\'\s\t\.-]+)\s+1\.\s+.*')
    #             section_name = section_name_with_number.match(value)
    #         if section_name is not None:
    # #                 print('\nNumbered. Section: ', section_name.group(1))
    #             section_name_with_question_numbered = re.compile('.*(1\..*)$')
    #             question = section_name_with_question_numbered.match(value)
    # #                 print('Question: ', question.group(1))
    #         else:
    #             section_name_with_no_number = re.compile('(^[A-Z]\.\s+.*)[A-Z]?[a-z]+.*')
    #             section_name = section_name_with_no_number.match(value)
    #             if section_name is not None:
    # #                     print('\nNo Number. Section: ', section_name.group(1))
    #                 question_with_no_number = re.compile('.*([A-Z][a-z].*)$')
    #                 question = question_with_no_number.match(c[1])
    # #                     print('Question: ', question.group(1))
    #             else:
    #                 logging.warning('Houston, we have a problem')
        elif symbol == 'I':
            # re_sections = re.compile('^(?!.*\((?i:continued)\))[\t\s]*((?i:part)[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([IVX]+)[\s\t]?[\.:—-](.*)')
            re_sections = re.compile('^(?!.*\((?i:continued)\))[\t\s]*((?i:part)[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([IVX]+)[\s\t]?[\.:—–][\s\t]?(.*)')
            section_groups = re_sections.match(value)
            scrubbed_continued = re_scrubbed_continued.match(value)
            if section_groups is not None:
                # print('\nvalue:', value)
                # print(section_groups.groups())
                if section_groups.group(3) not in set(header_symbol_list):
                    header_symbol_list.append(section_groups.group(3))
                    header = re_headers.match(value)
                    if header is not None:
                        header_list.append(header.group(1))
                    else:
                        logging.warning('No formatted header found')
                        header_list.append(None)
                else:
                    header_symbol_list.append(None)
                    header_list.append(None)
                questions.append(section_groups.group(4))
            elif scrubbed_continued is not None:
                # print('scrubbed_continued is not None')
                # print(scrubbed_continued.group(0))
                # print(scrubbed_continued.group(1))
                questions.append(scrubbed_continued.group(1))
                header_symbol_list.append(None)
                header_list.append(None)
            else:
                header_symbol_list.append(None)
                questions.append(value)
                header_list.append(None)
                # re_question = re.compile('^(.*)(\b[\(\)\d]+[\.:\s\t]|\b[\(\)ivx]+[\.:\s\t]|\b[\(\)a-zA-Z]+[\.:\s\t])(.*)')
                # re_question = re.compile('^(.*)([\s\t]*[\(\)\d]+)[\.:\s\t](.*)')
                # question = re_question.match(value)
                # if question is not None:
                    # print(question.groups())
                    # header_symbol_list.append(None)
                    # questions.append(question.group(3))
                # else:
                    # print(question.groups())
        elif symbol == '1':
            re_sections = re.compile('^(?!.*\(Continued\))[\t\s]*(Part[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([\d]+)[\s\t]?[\.:—–][\s\t]?(?!\d)(.*)')
            section_groups = re_sections.match(value)
            scrubbed_continued = re_scrubbed_continued.match(value)
            if section_groups is not None:
                # print('\nvalue:', value)
                # print(section_groups.groups())
                if section_groups.group(3) not in set(header_symbol_list):
                    header_symbol_list.append(section_groups.group(3))
                    header = re_headers.match(value)
                    # header = re_test.match(value)
                    no_space_header = re_no_space_header_for_arabic.match(value)
                    if header is not None:
                        header_list.append(header.group(1))
                    elif no_space_header is not None:
                        header_list.append(no_space_header.group(1))
                    else:
                        # logging.warning('No formatted header found!\nThere may be a formatted header in the question:\n')
                        # print('\nNo formatted header found!\nThere may be a formatted header in the question:')
                        # print(value)
                        header_list.append(None)
                else:
                    header_symbol_list.append(None)
                    header_list.append(None)
                # print('\nvalue:', value)
                # print(section_groups.groups())
                questions.append(section_groups.group(4))
            elif scrubbed_continued is not None:
                # print('\n')
                # print(scrubbed_continued.group(0))
                # print(scrubbed_continued.group(1))
                questions.append(scrubbed_continued.group(1))
                header_symbol_list.append(None)
                header_list.append(None)
            else:
                header_symbol_list.append(None)
                questions.append(value)
                header_list.append(None)
                # re_question = re.compile('^(.*)(\b[\(\)\d]+[\.:\s\t]|\b[\(\)ivx]+[\.:\s\t]|\b[\(\)a-zA-Z]+[\.:\s\t])(.*)')
                # re_question = re.compile('^(.*)([\s\t]*[\(\)\d]+)[\.:\s\t](.*)')
                # question = re_question.match(value)
                # if question is not None:
                    # print(question.groups())
                    # header_symbol_list.append(None)
                    # questions.append(question.group(3))
                # else:
                    # print('ERROR!')
                    # print(question.groups())

        else:
            logging.warning('\nNO SYMBOL STYLE FOUND!\n')

    questions_df['header_symbol'] = header_symbol_list
    questions_df['question'] = questions
    questions_df['header'] = header_list


    cert_col = re.compile('(?!.*blank_column_)(?=.*(CERTIFICATION|ACKNOWLEDGEMENT|ACKNOWLEDGE))(?!.*__\d+$)(.*?)_(.*)_([\w]+)?', re.IGNORECASE)
    cert_section_list = []
    for i, raw_question in zip(questions_df.index, questions_df['question_raw']):
        cert_match = cert_col.match(raw_question)
        if cert_match is not None and cert_match.group(2) not in cert_section_list:
            # print('\ngroup2',cert_match.group(2))
            cert_section_list.append(cert_match.group(2))

            questions_df.at[i, 'header_symbol'] = 'cert'
            questions_df.at[i, 'header'] = cert_match.group(2)
            questions_df.at[i, 'question'] = cert_match.group(3)

            # print('\nstart header')
            # print(cert_match.groups())
        elif cert_match is not None:
            questions_df.at[i, 'question'] = cert_match.group(3)
            # print('\nnot beginning header')
            # print(cert_match.groups())
        else:
            pass
    # print(questions_df)
    # if symbol == 'A':
    #     for row in row['header_symbol'].tolist():
    #         if len(row) > 1:
    #             remainder = ord(list(row)[-1].lower()) - 96
    #             for preceding in list(row)[:-1]:
    #                 number_of_moduli += (ord(preceding) - 96) * 26
    #             section = number_of_moduli + remainder

        # questions_df['question'] = questions_df.apply(lambda row: ord(row['header_symbol'].lower()) - 96)
    # questions_df['section']
    # print(questions_df)
    # print('-------------------------\n\n')

    # Table question data
    dict_of_dfs = {}

    # print('SHAPE', data.shape, data.columns)

    # Check that the last instance of row/column data is retrieved
    ##########################
    # matrix comp re
    # re_row_multiValued = re.compile('(.*?)/RowHeader(\d+):+(.*?);(?=[^;]*?(/RowHeader)\d+|$)')
    ##########################
    re_detectsEverythingExceptForResponseCasewsemiw = re.compile('(.*?[^\s\t]/[^\s\t].*?:):*(.*?);(?=([^\s\t]|$))')
    re_row_multiValued = re.compile('(.*?)(/(RowHeader)\d+:+)(.*?);(?=[^;]*?(/RowHeader)\d+|$)')
    re_col_multiValued = re.compile('ColumnHeader\d+/(.*?:):?(?=([^:]*?)(;ColumnHeader\d+/|;$))')
    # re_unlabeled_multiValued_original = re.compile('(.*?/.*?):+(.*?);')
    # re_unlabeled_multiValued_usefulInDetectingListsOfRelativeInformation = re.compile('([^\s]*?/[^\s]*?):+(.*?);(?=[^\s;]*?/[^\s]*?:+|$)')
    type_flag = ''
    for q, i in zip(questions_df['question_raw'], questions_df.index):
        if (data[q].apply(lambda x: bool(re_row_multiValued.match(x))).any()) | (data[q].apply(lambda x: bool(re_col_multiValued.match(x))).any()) | (data[q].apply(lambda x: bool(re_detectsEverythingExceptForResponseCasewsemiw.match(x))).any()):
            # print(q, i)
            question_dict = {}
            # Iterate through the identified column's rows
            for ind, row in zip(data.index, data[q]):
                # print(ind, row=='')
                if re_row_multiValued.match(row) is not None:
                    multi = re_row_multiValued.findall(row)
                    type_flag = 'row'
                elif re_col_multiValued.match(row) is not None:
                    multi = re_col_multiValued.findall(row)
                    type_flag = 'col'
                elif re_detectsEverythingExceptForResponseCasewsemiw.match(row) is not None:
                    multi = re_detectsEverythingExceptForResponseCasewsemiw.findall(row)
                    type_flag = 'boundMatrix'
                elif row=='':
                    continue
                else:
                    print(row)
                    logging.warning('\n\n--------\nERROR\nCould not distinguish between col and row multiheaders')

                # For each respondent that has matrix responses, populate a dataframe
                # that contains questions, responses, and 'group' values.
                # A group value refers to a grouping of questions. Groups of questions
                # often repeat. We will want to examine these groupings, so we'll label
                # them here.
                ###########################
                # Matrix comp df
                # individual_df = pd.DataFrame(columns=['question', 'value', 'group'])
                ###########################
                individual_df = pd.DataFrame(columns=['question', 'value'])
                for ii, m in enumerate(multi):
                    if type_flag == 'row':
                        individual_df.loc[ii] = [m[0], m[3]]
                    elif type_flag == 'col':
                        individual_df.loc[ii] = [m[0], m[1]]
                    elif type_flag == 'boundMatrix':
                        individual_df.loc[ii] = [m[0], m[1]]
                    else:
                        print('ERROR')
                nbr_of_unique_questions = individual_df['question'].nunique()
                if type_flag == 'row':
                    if (individual_df['question'].nunique() != len(individual_df['question'])) and (set(individual_df.iloc[-nbr_of_unique_questions:]['value'].tolist()).issubset([None, ''])):
                        # print('\n', ind)
                        # print(individual_df['question'].nunique() != len(individual_df['question']))
                        # print(individual_df['question'].nunique())
                        # print(individual_df)
                        while (set(individual_df.iloc[-individual_df['question'].nunique():]['value'].tolist()).issubset(['', None])) and (not individual_df.empty):
                            individual_df.drop(individual_df.tail(individual_df['question'].nunique()).index,inplace=True)
                elif type_flag == 'col':
                    # print(individual_df)
                    # individual_df.drop(individual_df.tail(individual_df['question'].nunique()).index,inplace=True)
                    # print(individual_df['value'].tolist()[-individual_df['question'].nunique():])
                    # if individual_df['question'].tolist()[-individual_df['question'].nunique():] == None:
                        # print('here!')
                    pass
                elif type_flag == 'boundMatrix':
                    pass
                else:
                    print('ERROR')
                # print('\n', ind)
                # print('\nIndividual DF: ', individual_df)
                # print('Number of Unique Questions', individual_df['question'].nunique())
                # print('Unique Questions: ', individual_df['question'].unique())
                question_dict[ind] = individual_df
            if type_flag == 'row':
                question_dict['meta_data'] = ['rowTypeData']
            elif type_flag == 'col':
                question_dict['meta_data'] = ['columnTypeData']
            elif type_flag == 'boundMatrix':
                question_dict['meta_data'] = ['boundMatrix']
            else:
                logging.warning('type_flag error')
            # for i in question_dict:
            #     print('\n', i)
            dict_of_dfs[i] = question_dict
    return data, questions_df, dict_of_dfs, symbol
