import pandas as pd
import numpy as np
import os
import glob
import re
from random import shuffle
from collections import Counter
import warnings
import xlsxwriter
from functools import reduce
from statistics import mean
import math
import sys
import itertools
import difflib
import logging
from collections import OrderedDict
import csv

class fsr_opener:
    def __init__(self, file):
        print('\nOpening FSR...\n', file)
        self.file = file
    def parse(self):
        try:
            columns = []
            with open(os.path.join(app.config['UPLOAD_FOLDER'], self.file), encoding = "utf8", mode='r') as f:
                reader = csv.reader(f)
                for row in reader:
                    if columns:
                        row = [re.sub(r'\xa0', ' ', i) for i in row]
                        row = [re.sub('%%', '', i) for i in row]
                        row = [re.sub('\ufeff', '', i) for i in row]
                        for i, value in enumerate(row):
                            columns[i].append(value)
                    else:
                        row = [re.sub(r'\xa0', ' ', i) for i in row]
                        row = [re.sub('%%', '', i) for i in row]
                        row = [re.sub('\ufeff', '', i) for i in row]
                        # first row
                        blank_column_count = 1
                        duplicate_column_count = 1
                        duplicate_tracker = []
                        for i, value in enumerate(row):
                            if value == '':
                                emptyColName = 'blank_column_{}'.format(blank_column_count)
                                row[i] = emptyColName
                                blank_column_count += 1
                                duplicate_tracker.append(emptyColName)
                            elif value in duplicate_tracker:
                                row[i] = value + '__{}'.format(duplicate_column_count)
                                duplicate_column_count += 1
                                duplicate_tracker.append(row[i])
                            else:
                                duplicate_tracker.append(value)
                                pass
                        columns = [[value] for value in row]
            as_dict = OrderedDict((subl[0], subl[1:]) for subl in columns)
            # print(as_dict.keys())
            return as_dict
        except:
            columns = []
            with open(self.file, encoding = "ISO-8859-1", mode='r') as f:
                reader = csv.reader(f)
                for row in reader:
                    if columns:
                        row = [re.sub(r'\xa0', ' ', i) for i in row]
                        row = [re.sub('%%', '', i) for i in row]
                        row = [re.sub('\ufeff', '', i) for i in row]
                        for i, value in enumerate(row):
                            columns[i].append(value)
                    else:
                        row = [re.sub(r'\xa0', ' ', i) for i in row]
                        row = [re.sub('%%', '', i) for i in row]
                        row = [re.sub('\ufeff', '', i) for i in row]
                        # first row
                        blank_column_count = 1
                        duplicate_column_count = 1
                        duplicate_tracker = []
                        for i, value in enumerate(row):
                            if value == '':
                                emptyColName = 'blank_column_{}'.format(blank_column_count)
                                row[i] = emptyColName
                                blank_column_count += 1
                                duplicate_tracker.append(emptyColName)
                            elif value in duplicate_tracker:
                                row[i] = value + '__{}'.format(duplicate_column_count)
                                duplicate_column_count += 1
                                duplicate_tracker.append(row[i])
                            else:
                                duplicate_tracker.append(value)
                                pass
                        columns = [[value] for value in row]

            as_dict = OrderedDict((subl[0], subl[1:]) for subl in columns)
        return as_dict