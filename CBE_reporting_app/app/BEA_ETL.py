import pandas as pd
import numpy as np
import os
import glob
import re
from random import shuffle
from collections import Counter
import warnings
import xlsxwriter
from functools import reduce
from statistics import mean
import math
import sys
import itertools
import difflib
import logging
import time
from pandas import ExcelWriter
from decimal import Decimal, ROUND_HALF_UP


def BEA_ETL(df, ppl_df, report_type, file_name):
    # print('\n\n', file_name, report_type)
    questions_df = pd.DataFrame({'question_raw': df.columns})
    # if 'Aptevo' in file_name:
    #     print(questions_df)
    survey = pd.DataFrame(columns = ['type', 'respondent_id', 'question_id', 'response', 'section', 'sub_section', 'number'])

    questions = []
    header_list = []

    search = []
    for values in questions_df['question_raw']:
        new_question = re.sub(r'__\d+$', '', values)
        search.append(new_question)
    questions_df['question'] = search

    re_continued_section = re.compile('(?!.*[\s\t]+1\.).*\(Continued\).*?[\s\t]+(\d+\.)[\s\t]+(.*)')
    re_continued_assessment = re.compile('(?!.*_[^a]\.)(?=.*[\s\t]+1\.).*\(Continued\).*[\s\t]+([IVX]+\..*)[\s\t]+(1\.)[\s\t]+(.*)')
    re_continued = re.compile('.*\(Continued\)')

    re_section_match = re.compile(r'(?!.*_[^a]\.)^.*?[\s\t]*([IVX]+\..*?)[\s\t]+(\d+\.)?[\s\t]+(.*)')
    re_question_number = re.compile('.*?(\d+\.).+')

    re_skills_matrix = re.compile('.*skills\s+matrix.*', re.IGNORECASE)
    re_attributes_matrix = re.compile('.*attributes\s+matrix.*', re.IGNORECASE)
    re_alpha_subsection = re.compile(':\s*_[a-z]\.')
    # re_rank_subsection = re.compile('[\d]\..*_[A-Z].*_$')
    re_rank_subsection = re.compile('.*_[A-Z].*_.*')
    committee_critique = re.compile(r'^_.*committee.*_$', re.IGNORECASE)

    sections = []
    numbers = []
    questions = []
    types = []
    sub_sections = []

    not_applicable_count = 0
    not_observed_count = 0

    for question, header in zip(questions_df['question'], df.columns):
        # print(file_name, report_type, df[header])
        if df[header].dtype == 'object':
            if df[header].str.contains('^[1-9]|10$').any():
                d = df[header].value_counts()
                try:
                    not_applicable_count += d['n/a']
                except:
                    pass
                try:
                    not_observed_count += d['n/o']
                except:
                    pass

                df[header].replace({'n/a': np.nan}, inplace=True)
                df[header].replace({'n/o': np.nan}, inplace=True)

        if df[header].isnull().all():
             data = df[header].astype(str)
             df[header] = data
        elif df[header].str.isdigit().all():
            data = pd.to_numeric(df[header])
            df[header] = data
        else:
            data = df[header].astype(str)
            df[header] = data

        continued_match = re_continued.match(question)
        section_match = re_section_match.match(question)
        if continued_match is not None:
            if re_continued_section.match(question) is not None:
                section = sections[-1]
                try:
                    number = re_question_number.match(question).group(1)
                except:
                    number = ''
                try:
                    formatted_question = re_continued_section.match(question).group(2)
                except:
                    formatted_question = '!__!'

            elif re_continued_assessment.match(question) is not None:
                try:
                    section = re_continued_assessment.match(question).group(1)
                except:
                    section = '!__!'
                try:
                    number = re_question_number.match(question).group(1)
                except:
                    number = '!__!'
                try:
                    formatted_question = re_continued_assessment.match(question).group(3)
                except:
                    formatted_question = '!__!'

            else:
                section = '!__!'
                number = '!__!'
                formatted_question = '!__!'
                # print('question:\n', question, '\nIs improperly formatted. Please ensure that the question number and the period following it, are surrounded by spaces.')
                # return 1

            if re_skills_matrix.match(question) is not None:
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'skills_matrix'

            elif re_attributes_matrix.match(question) is not None:
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'attributes_matrix'

            elif (any(isinstance(answer, str) for answer in data)) | (all(data.isna())):
                sub_section = ''
                type = 'long_form'

            elif all(str(item).lower() in ['yes', 'no'] for item in data.tolist()):
                sub_section = ''
                type = 'yes/no'

            elif re.search(':_[\s\t]*[a-z]\.', question) is not None:
                try:
                    sub_section = re.search(':.*_[\s\t]*([a-z]\..*)_', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'matrix'

            elif re_rank_subsection.match(question) is not None:
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'rank'

            elif committee_critique.match(question):
                # question = re.search('_(.*)_', question).group(1)
                type = 'committee_critique'
                sub_section = ''

            else:
                type = 'likert'
                sub_section = ''

        elif section_match is not None:
            try:
                section = section_match.group(1)
            except:
                section = '!__!'
            try:
                number = re_question_number.match(question).group(1)
            except:
                number = '!__!'
            try:
                if re.search('.*_[A-Z].*_.*', question) is None and re.search(':.*_[a-z]\.', question) is None:
                    formatted_question = section_match.group(3)
                elif re_skills_matrix.match(question) is not None:
                    try:
                        formatted_question = re.search('(.*)_.*_$', header).group(1)
                    except:
                        formatted_question = '!__!'
                else:
                    # print(question)
                    formatted_question = re.search('^.*[\s\t]+(\d+\.)?[\s\t]+(.*:).*_', question).group(1)
            except:
                formatted_question = '!__!'

            if re_skills_matrix.match(question) is not None:
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'skills_matrix'

            elif re_attributes_matrix.match(question) is not None:
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'attributes_matrix'

            elif (any(isinstance(answer, str) for answer in data)) | (all(data.isna())):
                sub_section = ''
                type = 'long_form'

            elif all(str(item).lower() in ['yes', 'no'] for item in data.tolist()):
                sub_section = ''
                type = 'yes/no'

            elif re.search(':.*_[\s\t]*[a-z]\.', question) is not None:
                try:
                    sub_section = re.search(':.*_[\s\t]*([a-z]\..*)_', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'matrix'

            elif re_rank_subsection.match(question) is not None:
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section
                type = 'rank'

            elif committee_critique.match(question):
                try:
                    question = re.search('_(.*)_', question).group(1)
                except:
                    question = '!__!'
                type = 'committee_critique'
                sub_section = ''

            else:
                type = 'likert'
                sub_section = ''
        else:
            try:
                section = sections[-1]
            except:
                section = '!__!'

            if re_skills_matrix.match(question) is not None or (types[-1] == 'skills_matrix' and '_a.' not in re.search('_(.*)_$', question).group(1)):
                try:
                    # No greedy search needed. The line below finds the first instance of \d
                    number = re_question_number.match(question).group(1)
                except:
                    number = '!__!'
                try:
                    formatted_question = re.search('(.*)_.*_$', header).group(1)
                except:
                    formatted_question = '!__!'
                try:
                    skills_match = re.search('_(.*)_$', header).group(1)
                except:
                    skills_match = '!__!'
                type = 'skills_matrix'
                sub_section = skills_match

            elif re_attributes_matrix.match(question) is not None or (types[-1] == 'attributes_matrix' and '_a.' not in re.search('_(.*)_$', question).group(1)):
                try:
                    # No greedy search needed. The line below finds the first instance of \d
                    number = re_question_number.match(question).group(1)
                except:
                    number = '!__!'
                try:
                    formatted_question = re.search('(.*)_.*_$', header).group(1)
                except:
                    formatted_question = '!__!'
                try:
                    skills_match = re.search('_(.*)_$', header).group(1)
                except:
                    skills_match = '!__!'
                type = 'attributes_matrix'
                sub_section = skills_match

            # Yes/No Questions
            elif all(str(item).lower() in ['yes', 'no'] for item in data.tolist()):
                try:
                    # No greedy search needed. The line below finds the first instance of \d
                    number = re_question_number.match(question).group(1)
                except:
                    number = '!__!'
                try:
                    formatted_question = re.search('^.*?\d+\.[\s\t]*(.*)$', question).group(1)
                except:
                    formatted_question = '!__!'
                type = 'yes/no'
                sub_section = ''

            # Long form questions
            elif (any(isinstance(answer, str) for answer in data)) | (all(data.isna())):
                # If unnnumbered, just grab the whole string as the question value
                try:
                    if (re.search('^(?!.*\d\.[^$])(.*)$', question) is not None):
                        formatted_question = re.search('(.*)', question).group(0)
                        number = ''
                    else:
                        try:
                            # No greedy search needed. The line below finds the first instance of \d
                            number = re_question_number.match(question).group(1)
                        except:
                            number = '!__!'
                        # No greedy search necessary
                        formatted_question = re.search('\d+\.[\s\t]*(.*)$', question).group(1)
                except:
                    formatted_question = '!__!'
                type = 'long_form'
                sub_section = ''

            # Matrix questions
            # elif re_alpha_subsection.match(question) is not None or re.search(':_\s*[a-z]\.', question) is not None:
            elif re.search(':[\s\t]*_[\s\t]*[a-z]\.', question) is not None:
                try:
                    # No greedy search needed. The line below finds the first instance of \d
                    number = re_question_number.match(question).group(1)
                except:
                    number = '!__!'
                try:
                    formatted_question = re.search('\d+\.\s(.*:)[\s\t]*_[\s\t]*[a-z]\.[\s\t]*(.*)_', question).group(1)
                    if formatted_question is None:
                        formatted_question = re.search('\d+\.\s(.*:).*_[\s\t]*[a-z]\.[\s\t]*(.*)_', question).group(1)
                except:
                    formatted_question = '!__!'
                try:
                    sub_section = re.search(':[\s\t]*_[\s\t]*([a-z]\..*)_', question).group(1)
                    if sub_section is None:
                        sub_section = re.search(':.*_[\s\t]*([a-z]\..*)_', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'matrix'

            # Check if it's a ranked choice question
            # Ranked choice questions must contain a substring with a capital letter that follows an underscore and the entire string must end in an underscore
            elif re_rank_subsection.match(question) is not None:
                try:
                    # No greedy search needed. The line below finds the first instance of \d
                    number = re_question_number.match(question).group(1)
                except:
                    number = '!__!'
                try:
                    formatted_question = re.search('\d+\.[\s\t]*(.*:)', question).group(1)
                except:
                    formatted_question = '!__!'
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'rank'

            elif committee_critique.match(question):
                try:
                    # No greedy search needed. The line below finds the first instance of \d
                    number = re_question_number.match(question).group(1)
                except:
                    number = '!__!'
                try:
                    formatted_question = re.search('_(.*)_', question).group(1)
                except:
                    formatted_question = '!__!'
                type = 'committee_critique'
                sub_section = ''
            else:
                try:
                    # No greedy search needed. The line below finds the first instance of \d
                    number = re_question_number.match(question).group(1)
                except:
                    number = '!__!'
                try:
                    formatted_question = re.search('\d+\.[\s\t]*(.*)', question).group(1)
                except:
                    formatted_question = '!__!'
                type = 'likert'
                sub_section = ''

            # tup = get_question_and_question_type(data, question)
            # type, formatted_question, number, sub_section = tup[0], tup[1], tup[2], tup[3]

        # if 'Boston' in file_name:
        #     print(formatted_question)

        sections.append(section)
        numbers.append(number)
        questions.append(formatted_question)
        types.append(type)
        sub_sections.append(sub_section)

    questions_df['section'] = sections
    questions_df['number'] = numbers
    questions_df['question'] = questions
    questions_df['type'] = types
    questions_df['subsection'] = sub_sections

    # print(questions_df)
    # questions_df.to_excel(writer, '{}_{}'.format(file_name[:20], report_type[:9]))
    # worksheet = writer.sheets['{}_{}'.format(file_name[:20], report_type[:9])]
    # for idx, col in enumerate(questions_df.columns, start=1):
    #     worksheet.set_column(idx, idx, 50)

    # print('not applicable: ', not_applicable_count)
    # print('not observed: ', not_observed_count)
    return questions_df, df, not_applicable_count, not_observed_count
