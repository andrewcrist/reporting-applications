import pandas as pd
import numpy as np
import os
import glob
import re
from random import shuffle
from collections import Counter
import warnings
import xlsxwriter
from functools import reduce
from statistics import mean
import math
import sys
import itertools
import difflib
import logging
import time
from pandas import ExcelWriter
from decimal import Decimal, ROUND_HALF_UP

def BEA_PPL_PARSER(df):
    # Create dataframe of respondents
    ppl = dict()
    cols = ['UniqueIdentifier', 'FirstName', 'LastName', 'MiddleName', 'Capacity', 'Audit', 'Compensation', 'NomGov', 'HR', 'Risk', 'Trust', 'hsse', 'Regulatory', 'Compliance', 'Corporate', 'Title']
    for k, v in df.items():
        if k in cols:
            ppl[k] = v

    ppl_df = pd.DataFrame.from_dict(ppl)

    if  'UniqueIdentifier' in ppl_df.columns:
        ppl_df.set_index('UniqueIdentifier', inplace=True)
    return ppl_df
