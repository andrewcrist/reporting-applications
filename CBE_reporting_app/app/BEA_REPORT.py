import pandas as pd
import numpy as np
import os
import glob
import re
from random import shuffle
from collections import Counter
import warnings
import xlsxwriter
from functools import reduce
from statistics import mean
import math
import sys
import itertools
import difflib
import logging
import time
from pandas import ExcelWriter
from decimal import Decimal, ROUND_HALF_UP

class BEA:
    '''
    --Class variables--
        self.workbook: workbook for radar chart, hilo, aggregate section analysis, and bar chart,
        self.reportname: name of the excel Workbook,
        self.df: raw csv input,
        self.file: input CSV file,
        self.rows_of_survey_object: rows of multidimensional dataframe,
        self.shuffled_rows_of_survey_object: shuffled rows of multidimensional dataframe,
        self.number_of_respondents: number of respondents, i.e. the number of people who took the survey,
        self.survey_object: multidimensional dataframe containing all information about every question
        self.report_sheet: formatted sheet for final output
        self.averages: dictionary of averages for each section
        self.five_point_scale: boolean flag indicating whether the survey is based on a 5 point likert scale.

    --Class Methods--
        read_file: drop extraneous columns. Initialize workbook, reportname, and number_of_respondents.
        create_multi_index_rows: creating a multiindexed dataframe requires an explicit index. An iterative
            process requires that each row is assigned an index before the rows are concatenated.
            This method creates these indexed rows for shuffled and unshuffled Responses
        ToArabic: Roman numerals are difficult to sort. ToArabic and check_valid methods are a robust
            method to convert from roman numerals to arabic numerals for the purpose of sorting.
        check_valid: helper function for ToArabic.
        parse_columns: this method does all the heavy lifting in terms of the logic necesssary to parse
            columns with a sparse or spotty multiindex.
        statistics: this function takes the ouput of parse_columns and create_multiindex. It calculates
            averages for different sections, string formatting, formats report_sheet, and creates the survey_object
        heat_map: creates the heat map for the report. Currently, this heat map outputs to its own workbook.
            The reason for this is that the to_excel method automatically merges columns which is extremely
            convienent. If this method is to be outputted using self.workbook, this merging and formatting
            will need to be done using xlsxwriter.
        format_heat_map: column values need a specific formatting. This function formats every column
        radar_chart: creates the radar chart, i.e. "web".
        bar_chart: creates the bar chart.
        hi_lo: creates hilo sheet.
        aggregated_section_analysis: calculates the individual statistics for the aggregated section analysis sheet.
        eSigma: calculates the eSigma score, i.e. the sum of the averages of all sections and the average of
            the averages of each question divided by 2.
            This function assings a value to self.average_total_rating_given--necessary for the aggregated_section_analysis
        writeXLSX: calls self.workbook.close().
'''

    def __init__(self, file_name, report_type, df, questions_df, ppl_df, not_applicable_count, not_observed_count, app, randomize = True):
        self.file_name = file_name
        self.report_type = report_type
        self.df = df
        self.questions_df = questions_df
        self.ppl_df = ppl_df
        self.not_applicable_count = not_applicable_count
        self.not_observed_count = not_observed_count
        self.app = app

        self.randomize = randomize

        self.rows_of_survey_object = []
        self.shuffled_rows_of_survey_object = []
        self.skill_matrix_dfs = {}
        self.five_point_scale = False

        # print(file_name, report_type)
        # try:
        self.bea_handler()
        # except:
        #     print('\nCould not produce: ', self.report_name)

    def bea_handler(self):
        # Initialize workbook
        self.report_name = re.search('^(.*).csv', self.file_name).group(1)   
        self.output_filename = '{}_{}.xlsx'.format(self.report_name, self.report_type)

        self.workbook = xlsxwriter.Workbook(os.path.join(self.app.config['UPLOAD_FOLDER'], self.output_filename), {'nan_inf_to_errors': True})
        # print('here', os.path.join('\uploads\', self.output_filename))
        self.number_of_respondents = len(self.df)
        self.overview()
        self.statistics()
        if self.report_type == 'BEA':
            self.radar_chart()
        self.bar_chart()
        self.aggregated_section_analysis()
        self.hi_lo()
        self.heat_map()
        if self.skill_matrix_dfs:
            for tup in self.skill_matrix_dfs:
                self.skills_matrix(tup)

        self.workbook.close()

    def create_multi_index_rows(self, section, number, subsection, qtype, question, answers):
        # question = re.sub(r'\.\.\d+$', r'.', question)
        index = pd.MultiIndex.from_arrays([[section], [number], [subsection], [qtype], [question]], \
                                          names = ['section', 'number', 'subsection', 'question_type', 'question'])
        formatted_reportsheet_row = pd.DataFrame([tuple(answers)], index = index, columns = np.arange(1, len(self.df)+1))

        self.rows_of_survey_object.append(formatted_reportsheet_row)
        shuffle(answers)
        shuffled_row= pd.DataFrame([tuple(answers)], index = index, columns = np.arange(1, len(self.df)+1))
        self.shuffled_rows_of_survey_object.append(shuffled_row)

    def ToArabic(self, roman):
        roman = self.check_valid(roman)
        keys = ['IV', 'IX', 'XL', 'XC', 'CD', 'CM', 'I', 'V', 'X', 'L', 'C', 'D', 'M']
        to_arabic = {'IV': '4', 'IX': '9', 'XL': '40', 'XC': '90', 'CD': '400', 'CM': '900',
                'I': '1', 'V': '5', 'X': '10', 'L': '50', 'C': '100', 'D': '500', 'M': '1000'}
        for key in keys:
            if key in roman:
                roman = roman.replace(key, ' {}'.format(to_arabic.get(key)))
        arabic = sum(int(num) for num in roman.split())
        return arabic

    def check_valid(self, roman):
        roman = roman.upper()
        invalid = ['IIII', 'VV', 'XXXX', 'LL', 'CCCC', 'DD', 'MMMM']
        if any(sub in roman for sub in invalid):
            raise ValueError('Numerus invalidus est: {}\n.The number is an invalid Roman Numeral'.format(roman))
        return roman

    def format_heat_map(self, row):
        # print(self.report_name, self.report_type, self.survey_object)
        # print(row)
        row['section'] = re.search(r'\b([IVX]+)\.?', row['section']).group(1)
        row['number'] += row['subsection']
        # remove spaces from likert questions
        match_likert = re.search('^(\d+)\.$', row['number'])
        if match_likert is not None:
            row['number'] = match_likert.group(1)
        # match everything except 'a' so we can format correctly
        match_b_z = re.search('^\d+\.(?![a])([a-z])', row['number'])
        if match_b_z is not None:
            row['number'] = match_b_z.group(1)
        # pair 'a' with the corresponding question number without a period
        match_a = re.search('^(\d+)\.(a)\.', row['number'])
        if match_a is not None:
            row['number'] = match_a.group(1) + match_a.group(2)
        return row

    def radar_chart(self):
        worksheet = self.workbook.add_worksheet('Radar Chart')

        web = self.survey_object.copy()
        web = web[web['question_type'].isin(['likert', 'matrix'])]
        del web['subsection']
        del web['question_type']
        del web['question']
        web.reset_index(inplace=True, drop=True)
        colList = web.columns.tolist()
        for c in colList:
            try:
                web[c].replace({'': 0}, inplace=True)
            except:
                pass
        web.loc[:, '1':] = web.loc[:, '1':].apply(pd.to_numeric)
        sums = web.iloc[:, 2:].sum(axis=0)
        counts = web.iloc[:, 2:].apply(pd.value_counts)
        if self.five_point_scale == False:
            new_index = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            counts = counts.reindex(new_index, fill_value=0)
            ten_counts = counts.loc[10]
            data = pd.concat([ten_counts, sums], axis=1, sort=True)
            data.reset_index(inplace=True)
            data.columns = ['director', 'Number of Times a Director Rated a Question at a 10', 'Individual Director Total Rating (Scaled=Total/10)']
            data.iloc[:, 2] = data.iloc[:, 2]/10
        else:
            new_index = [1, 2, 3, 4, 5]
            counts = counts.reindex(new_index, fill_value=0)
            five_counts = counts.loc[5]
            data = pd.concat([five_counts, sums], axis=1, sort=True)
            data.reset_index(inplace=True)
            data.columns = ['director', 'Number of Times a Director Rated a Question at a 5', 'Individual Director Total Rating (Scaled=Total/5)']
            data.iloc[:, 2] = data.iloc[:, 2]/5

        # Global font
        fmt = self.workbook.add_format({"font_name": "Garamond"})
        bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': 1})


        data = data.sort_values('director', ascending=True)

        worksheet.write_row('A1', data.columns, bold_fmt)
        worksheet.write_column('A2', data['director'], fmt)
        worksheet.write_column('B2', data.iloc[:, 1], fmt)
        worksheet.write_column('C2', data.iloc[:, 2], fmt)

        # Create a new radar chart.
        chart1 = self.workbook.add_chart({'type': 'radar'})

        chart1.add_series({
            'name':       ['Radar Chart', 0, 1],
            'categories': ['Radar Chart', 1, 0, len(data), 0],
            'values':     ['Radar Chart', 1, 1, len(data), 1],
            'line': {'color': '#2E682B'}
        })

        chart1.add_series({
            'name':       ['Radar Chart', 0, 2],
            'categories': ['Radar Chart', 1, 0, len(data), 0],
            'values':     ['Radar Chart', 1, 2, len(data), 2],
            'line': {'color': '#5E7BAD'}
        })

        chart1.set_legend({'position': 'bottom',
                            'font': {'name': "Garamond", 'size': 14, 'bold': False}
                            })
        # Add a chart title and some axis labels.
        chart1.set_title ({'name': 'Number of Directors = {}'.format(len(data)),
                            'name_font': {'name': "Garamond", 'size': 14, 'bold': False},
                            'position': 'bottom',
                            'overlay': True,
                            'layout': {
                                'x': 0.35,
                                'y': 0.85,
                            }
                        })

        chart1.set_x_axis({'name': 'Test number', 'num_font':  {'name': 'Garamond'}})
        chart1.set_y_axis({'name': 'Sample length (mm)'})
        worksheet.insert_chart('D2', chart1, {'x_offset': 25, 'y_offset': 10, 'x_scale': 1.5, 'y_scale': 2})

    def skills_matrix_row_builder(self, section, number, skillArea, firstName, lastName, answers):
        firstName = firstName.tolist()
        lastName = lastName.tolist()
        listOfFirstIndex = []
        for f, l in zip(firstName, lastName):
            joined = f + ' ' + l
            first = joined.split()
            letters = [word[0] for word in first]
            ind = ''.join(letters)
            listOfFirstIndex.append(ind)
        uniqueID = list(np.arange(1, len(listOfFirstIndex) + 1))
        arrays = [uniqueID, listOfFirstIndex]
        mi = pd.MultiIndex.from_arrays(arrays,names=['uniqueID', 'initials'])
        if (section, number) not in self.skill_matrix_dfs:
            self.skill_matrix_dfs[(section, number)] = []
            # print(section, number, skillArea)
            # print('\n', mi)
            self.skill_matrix_dfs[(section, number)].append(pd.DataFrame(answers.tolist(), columns=[skillArea], index=mi))
        else:
            self.skill_matrix_dfs[(section, number)].append(pd.DataFrame(answers.tolist(), columns=[skillArea], index=mi))
        # self.skill_matrix_rows.append(pd.DataFrame([answers.tolist()], columns=listOfColumns, index=[skillArea]))

    def skills_matrix(self, tup):
        # print('\n\n')
        # print('\n', tup)
        # print(self.skill_matrix_dfs[tup])
        skills_matrix = self.skill_matrix_dfs[tup][0].join(self.skill_matrix_dfs[tup][1:])

        skills_matrix.loc[(len(skills_matrix)+1, 'board'), :] = skills_matrix.apply(lambda row: float(str(Decimal(mean(row)).quantize(Decimal('1e-2'), ROUND_HALF_UP))), axis = 0)
        # writer = pd.ExcelWriter('skills.xlsx')
        # skills_matrix.to_excel(writer)
        # writer.save()
        # skills_matrix.index.name = 'Skill Areas'
        # skills_matrix.reset_index(inplace=True)
        # skills_matrix.sort_values('Board Average', ascending = False, inplace=True)
        # print(skills_matrix.loc[18, 'board'])
        # print(skills_matrix)
        skills_matrix = skills_matrix.sort_values((len(skills_matrix), 'board'), axis = 1, ascending=False)
        # print(skills_matrix)
        # Move 'Board Average' to the beginning of the list, remove it from the end and delete 'index'
        # columnReorder = ['Skill Areas', 'Board Average']
        # print('\n\nskills_matrix.columns[1:-1]', skills_matrix.columns[1:-1])
        # columnReorder.extend([i for i in skills_matrix.columns[1:-1]])

        # print('column reorder', columnReorder)

        # skills_matrix = skills_matrix[columnReorder]
        self.write_df(skills_matrix, 'Skills Matrix_{}_{}'.format(tup[0][:12], tup[1][:3]))

    def overview(self):
        # Identify the columns of data in an iterative manner.
        columns = list(self.df.columns)
        # Format rows one at a time and append them to a list
        formatted_reportsheet_rows = []

        # Need the ability to set and reset values for ranked choice questions
        # the process is iterative, so it is necessary to count ahead as well as behind
        # rankFlag is True as long as the question is a ranked question.
        rankFlag = False
        lengthOfRankedChoiceSection = 0
        yAxisCount = 1
        sectionNames = []
        firstColumnFound = False
        continuedSectionFlag = False

        section_set = set()

        for index, row in self.questions_df.iterrows():
            # Check for a skills matrix section
            # if re.search('Skills\sMatrix', column) is not None:
            #     skillsMatch = re.search('_(.*)_$', column)
            #     # print(self.report_name, self.report_type, skillsMatch, column, self.df[column])
            #     self.skills_matrix_row_builder(skillsMatch.group(1), self.ppl_df['FirstName'], self.ppl_df['LastName'], self.df[column])
            #     continue

            # Funnel skills_matrix questions out of the report sheet
            if row['type'] == 'skills_matrix':
                # print(self.file_name, self.report_type)
                self.skills_matrix_row_builder(row['section'], row['number'], row['subsection'], self.ppl_df['FirstName'], self.ppl_df['LastName'], self.df[row['question_raw']])
                continue

            elif row['type'] == 'attribute_matrix':
                # print(self.file_name, self.report_type)
                self.skills_matrix_row_builder(row['section'], row['number'], row['subsection'], self.ppl_df['FirstName'], self.ppl_df['LastName'], self.df[row['question_raw']])
                continue
            # Create an average row to appear above section header row if the
            # section does not just consist of long_form question types
            # print(row['section'], section_set, row['section'] in section_set)
            if row['section'] not in section_set:
                section_set.add(row['section'])
                new_section_flag = True

                if not set(self.questions_df[self.questions_df['section'] == row['section']]['type']).issubset(set(['long_form', 'yes/no'])):
                    # print(row['section'])
                    # print(set(self.questions_df[self.questions_df['section'] == row['section']]['type']))
                    formatted_reportsheet_rows.append((None, None, None, None, None) + tuple([None]*(len(self.df[row['question_raw']])-1)) + ('Average', 'average_value'))
            else:
                new_section_flag = False

            # Long form questions
            if row['type'] == 'long_form':
                # If long_form question types are unnumbered
                if row['number'] == '' or row['number'] == np.nan:
                    if new_section_flag:
                        long_form_title_row = (row['section'], row['number'], row['question'], None, None)
                    else:
                        long_form_title_row = (None, None, row['question'], None, None)

                    answers = self.df[row['question_raw']].tolist()
                    answers = [str(answer) for answer in answers]

                    # If the question is unnumbered, only display it if it is answered
                    if len(set(answers)) == 1:
                        continue
                    else:
                        #
                        self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())
                        #
                        formatted_reportsheet_rows.append(long_form_title_row)
                        if self.randomize == True:
                            shuffle(answers)
                        for optional in answers:
                            if optional != 'nan':
                                formatted_reportsheet_rows.append((None, None, optional))
                        continue

                # If it's a numbered, optional question that needs displayed
                elif row['number'] != '' and row['number'] != np.nan:
                    if new_section_flag:
                        long_form_title_row = (row['section'], row['number'], row['question'], None, None)
                    else:
                        long_form_title_row = (None, row['number'], row['question'], None, None)
                    answers = self.df[row['question_raw']].tolist()
                    #
                    self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())
                    #
                    formatted_reportsheet_rows.append(long_form_title_row)
                    if self.randomize == True:
                        shuffle(answers)

                    if set(answers) == set(['nan']) | set(answers) == set([np.nan]):
                        formatted_reportsheet_rows.append((None, None, "no answers given"))
                        continue
                    else:
                        for optional in answers:
                            if optional != 'nan':
                                formatted_reportsheet_rows.append((None, None, optional))
                        continue
                else:
                    print('long_form error in: ', self.file_name, self.report_type)


            # Yes/No Questions
            elif row['type'] == 'yes/no':
                if new_section_flag:
                    yes_no_title_row = (row['section'], row['number'], row['question'], None, None)
                else:
                    yes_no_title_row = (None, row['number'], row['question'], None, None)

                yesNoCheck = self.df[row['question_raw']].tolist()
                yesNoCheck = [answer.lower() for answer in yesNoCheck]
                #
                self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())
                #
                yesNoCheck = dict(Counter(yesNoCheck))

                formatted_reportsheet_rows.append(yes_no_title_row)
                formatted_reportsheet_rows.append((None, None, 'Yes', yesNoCheck.get('yes', 0)))
                formatted_reportsheet_rows.append((None, None, 'No', yesNoCheck.get('no', 0)))
                continue
            # Determine if question has multiple parts
            elif row['type'] == 'matrix':
                # Reset Flags
                rankFlag = False
                yAxisCount = 1

                if any(self.df[row['question_raw']].isna()):
                    answers = self.df[row['question_raw']].replace({np.nan: None})
                    answers.fillna('N/A', inplace=True)
                    answers = answers.tolist()
                else:
                    answers = self.df[row['question_raw']].tolist()

                if self.randomize ==True:
                    shuffle(answers)

                try:
                    subsection_identifier = re.search('^([a-z]\.)', row['subsection']).group(1)
                except:
                    subsection_identifier = '!__!'
                try:
                    subsection_value = re.search('^[a-z]\.[\s\t]*(.*)', row['subsection']).group(1)
                except:
                    subsection_value = '!__!'

                if re.search('(^[a-z]\.)', row['subsection']).group(1) != 'a.':
                    self.create_multi_index_rows(row['section'], row['number'], subsection_identifier, row['type'], row['question'] + ' ' + subsection_value, self.df[row['question_raw']].tolist())

                    formatted_reportsheet_row = (None, None, subsection_identifier, subsection_value) + tuple(answers) + (str(Decimal(np.mean(self.df[row['question_raw']].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    formatted_reportsheet_rows.append(formatted_reportsheet_row)
                    continue

                else:
                    if new_section_flag:
                        matrix_title_row = (row['section'], row['number'], row['question'], None, None)
                    else:
                        matrix_title_row = (None, row['number'], row['question'], None, None)
                    self.create_multi_index_rows(row['section'], row['number'], subsection_identifier, row['type'], row['question'] + ' ' + subsection_value, self.df[row['question_raw']].tolist())
                    # self.create_multi_index_rows(sectionNames[-1], number, re.search(r':_([a-z]\.)', c).group(1), 'matrix', re.search('\d+\.\s(.*:)_[a-z]\.\s(.*)_', c).group(1) + ' ' + re.search('\d+\.\s(.*:)_[a-z]\.\s(.*)_', c).group(2), self.df[c].tolist())

                    # if new_section_flag:
                    #     formatted_reportsheet_row (row['section'], row['number'], row['question'], row['subsection']) + tuple(answers) + ("{0:.2f}".format(round(np.mean(self.df[row['question_raw']].dropna()),2)),)
                    # else:
                    formatted_reportsheet_row = (None, None, subsection_identifier, subsection_value) + tuple(answers) + (str(Decimal(np.mean(self.df[row['question_raw']].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    # formatted_reportsheet_rows.append(subsectionHeader)
                    formatted_reportsheet_rows.append(matrix_title_row)
                    formatted_reportsheet_rows.append(formatted_reportsheet_row)
                    continue

            # Check if it's a ranked choice question
            # Ranked choice questions must contain a substring with a capital letter that follows an underscore and the entire string must end in an underscore
            elif row['type'] == 'rank':
                # Ranked questions have no sequential identification.
                # "Flip a switch" to indicate that the iterative process has encountered a ranked Choice question
                # We will switch the rankFlag value to False when the loop encounters a different type of question
                if rankFlag == False:
                    # We know that, if rankFlag is false, this is the first question.
                    # Create and format the rank choice section's titles
                    if new_section_flag:
                        rank_title_row = (row['section'], row['number'], row['question'], None, None)
                    else:
                        rank_title_row = (None, row['number'], row['question'], None, None)

                    xAxisTitle = (None, None, None, None, "Number of Responses")

                    # Iterate ahead in order to get length of the section
                    # beginningIndex = columns.index(column)
                    # endingIndex = beginningIndex
                    # numberCheck = number
                    # while numberCheck == number:
                    #     endingIndex += 1
                    #     try:
                    #         numberCheck = re.search('(\d+\.)', columns[endingIndex]).group(0)
                    #     except:
                    #         break
                    # lengthOfRankedChoiceSection = endingIndex - beginningIndex
                    lengthOfRankedChoiceSection = len(self.questions_df[(self.questions_df['section']==row['section']) & (self.questions_df['number'] == row['number'])])
                    xAxis = (None, None, 'Rank:', None) + tuple(np.arange(1, lengthOfRankedChoiceSection+1, 1))

                    formatted_reportsheet_rows.append(rank_title_row)
                    formatted_reportsheet_rows.append(xAxisTitle)
                    formatted_reportsheet_rows.append(xAxis)

                # Count up the respective rankings, add them to list, and insert 0s where necessary
                dictonaryOfCounts = dict(Counter(self.df[row['question_raw']].tolist()))
                listOfOrderedCounts = list()
                for value in np.arange(1, lengthOfRankedChoiceSection+1, 1):
                    listOfOrderedCounts.append(dictonaryOfCounts.get(value, 0))

                formatted_reportsheet_row = (None, None, yAxisCount, row['subsection']) + tuple(listOfOrderedCounts)
                formatted_reportsheet_rows.append(formatted_reportsheet_row)
                # Ranked questions have a number of responses based on the number of choices for the whole section

                self.create_multi_index_rows(row['section'], row['number'], yAxisCount, row['type'], row['question'] + ' ' + row['subsection'], self.df[row['question_raw']].tolist())
                #
                rankFlag = True
                yAxisCount+=1
                continue

            # The simple instance of a rating question
            else:
                if any(self.df[row['question_raw']].isna()):
                    answers = self.df[row['question_raw']].replace({np.nan: None})
                    answers.fillna('N/A', inplace=True)
                    answers = answers.tolist()
                else:
                    answers = self.df[row['question_raw']].tolist()

                rankFlag = False
                yAxisCount = 1
                if new_section_flag:
                    # print(str(Decimal(np.mean(self.df[row['question_raw']].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    if self.randomize:
                        shuffle(answers)
                    formatted_reportsheet_row = (row['section'], row['number'], row['question'], None) + tuple(answers) + (str(Decimal(np.mean(self.df[row['question_raw']].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    formatted_reportsheet_rows.append(formatted_reportsheet_row)

                    self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())
                    #

                else:
                    if self.randomize:
                        shuffle(answers)

                    # if all(isinstance(answer, (int, float)) for answer in answers):

                    formatted_reportsheet_row = (None, row['number'], row['question'], None) + tuple(answers) + (str(Decimal(np.mean(self.df[row['question_raw']].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    formatted_reportsheet_rows.append(formatted_reportsheet_row)
                    #
                    self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())
                    #
                    continue

        self.report_sheet = pd.DataFrame(formatted_reportsheet_rows)

        # writer = pd.ExcelWriter('report_sheet.xlsx')
        # self.report_sheet.to_excel(writer)
        # writer.save()

        # self.write_df(self.report_sheet, 'Report')
            # self.sectionNames = sectionNames
            # if len(self.skill_matrix_dfs) > 1:
                # self.skills_matrix()
            # if self.committee_critique_begin is not None:
                # self.committee_critiques()

    def committee_critiques(self):
        # print(self.committee_critique_section)
        numberCounter = 1
        rows = []
        for c in self.committee_critique_section.columns:
            try:
                if pd.to_numeric(self.committee_critique_section[c]).all():
                    self.committee_critique_section[c] = pd.to_numeric(self.committee_critique_section[c])
            except:
                pass

            # if (any(isinstance(answer, str) for answer in self.committee_critique_section[c])) | (all(self.committee_critique_section[c].isna())):
                # print('long_form', c)
                # if (re.search(r'(^[^\d]+$)', c) is not None):
                #     question = re.search(r'(.*)', c_sub).group(0)
                #     titleRow = (None, None, question, None, None)
                # else:
                #     question = re.search(r'\d+\.\s(.*)$', c).group(1)
                #     titleRow = (None, number, question, None, None)
                #
                # # If the question is unnumbered, only display it if it is answered
                # if (re.search(r'(^[^\d]+$)', c_sub) is not None):
                #     question = re.search(r'(.*)', c_sub).group(0)
                #     answers = self.df[c].tolist()
                #     answers = [str(answer) for answer in answers]
                #     if len(set(answers)) == 1:
                #         continue
                #     else:
                #         #
                #         self.create_multi_index_rows(sectionNames[-1], number, '', 'long_form', question, self.df[c].tolist())
                #         #
                #         formatted_reportsheet_rows.append(titleRow)
                #         shuffle(answers)
                #         for optional in answers:
                #             if optional != 'nan':
                #                 formatted_reportsheet_rows.append((None, None, optional))
                #         continue
                #
                # # If it's a numbered, optional question that needs displayed
                # elif (re.search(r'\d+', c_sub) is not None):
                #         question = re.search(r'\d+\.\s(.*)$', c).group(1)
                #         answers = self.df[c].tolist()
                #         #
                #         self.create_multi_index_rows(sectionNames[-1], number, '', 'long_form', question, self.df[c].tolist())
                #         #
                #         formatted_reportsheet_rows.append(titleRow)
                #         shuffle(answers)
                #         answers = [str(optional) for optional in answers]
                #         if set(answers) == set(['nan']) | set(answers) == set([np.nan]):
                #             formatted_reportsheet_rows.append((None, None, "no answers given"))
                #             continue
                #         else:
                #             for optional in answers:
                #                 if optional != 'nan':
                #                     formatted_reportsheet_rows.append((None, None, optional))
                #             continue

            # if any(self.df[c].isna()):
            #     answers = self.df[c].replace({np.nan: None})
            #     answers.fillna('N/A', inplace=True)
            # else:
            #     answers = self.df[c].tolist()
            # answers = self.committee_critique_section[c].tolist()
            # shuffle(answers)
            # if all(isinstance(answer, (int, float)) for answer in answers):
            #     print('likert', c)
            #     formatted_reportsheet_row = (fullTitle, number, re.search('\d+\.\s(.*)', c).group(1), None) + tuple(answers) + ("{0:.2f}".format(round(np.mean(self.df[c].dropna()),2)),)
            # else:
            #     formatted_reportsheet_row (fullTitle, number, re.search('\d+\.\s(.*)', c).group(1), None) + tuple(answers)
            # formatted_reportsheet_rows.append(formatted_reportsheet_row)
            #
            # self.create_multi_index_rows(sectionNames[-1], number, '', 'likert', re.search('^.*\d+\.\s(.*)$', c).group(1), self.df[c].tolist())
            #
        # formatted_reportsheet_rows.append(('Committee Review'))

    def statistics(self):
        df = pd.concat(self.rows_of_survey_object)
        df_shuffled = pd.concat(self.shuffled_rows_of_survey_object)

        df.reset_index(inplace=True)
        df_shuffled.reset_index(inplace=True)

        # Calculate Averages for each section
        df.columns = [str(i) for i in df.columns]
        df_shuffled.columns = [str(i) for i in df.columns]

        #
        df[df['question_type'].isin(['likert', 'matrix'])].loc[:, '1':] = df[df['question_type'].isin(['likert', 'matrix'])].loc[:, '1':].apply(pd.to_numeric)
        #

        numericAnswers = df[df['question_type'].isin(['likert', 'matrix'])]
        # Check if the report is based on a 5 point scale
        # if numericAnswers.loc[:,'1':].max(numeric_only=True) <= 5:
        if max(numericAnswers.loc[:,'1':].max()) <=5:
            self.five_point_scale = True

        averages = {}
        for s in df['section'].unique():
            try:
                sub_frame = numericAnswers[numericAnswers['section']==s]
                totalNumberOfNullReponses = sub_frame.isna().sum().sum()
                sums = []
                # print(sub_frame)
                for index, row in sub_frame.loc[:, '1':].iterrows():
                    sums.append(row.sum())
                total = sum(sums)
                totalNumberOfResponses = len(sums)*len(sub_frame.loc[:, '1':].columns)
                if totalNumberOfResponses == 0:
                    continue
                average = total/(totalNumberOfResponses-totalNumberOfNullReponses)
                # Create Dictionary of Averages for each Section
                str(Decimal(average).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                averages[s.replace(u'\xa0', u' ')] = str(Decimal(average).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                pass

        self.report_sheet.columns = [str(i) for i in self.report_sheet.columns]

        sectionIndices = self.report_sheet.index[self.report_sheet[self.report_sheet.columns[-1]] == 'average_value']
        sectionIndices = [i + 1 for i in sectionIndices]
        self.row_count_of_sections = sectionIndices
        for i in sectionIndices:
            section = self.report_sheet.iloc[i, 0]
            section = section.replace(u'\xa0', u' ')
            self.report_sheet.iloc[i-1, [-1]] = averages.get(section)
        df['subsection'] = df['subsection'].astype(str)
        # df['section'] = df['section'].str.replace(u'\xa0', u' ')
        df_shuffled['subsection'] = df_shuffled['subsection'].astype(str)

        # Save members averages and survey_object
        self.averages = averages

        self.survey_object = df.copy()
        self.shuffled_survey_object = df_shuffled.copy()

        self.sectionMinsAndMaxes = {}
        for section in self.survey_object['section'].unique():
            sub_frame = self.survey_object[(self.survey_object['section']==section) & (self.survey_object['question_type'].isin(['likert', 'matrix']))]
            sub_frame.loc[:,'1':] = sub_frame.loc[:,'1':].apply(pd.to_numeric)
            if sub_frame.empty:
                continue
            try:
                self.sectionMinsAndMaxes[section.replace(u'\xa0', u' ')] = {'min': int(min(sub_frame.loc[:,'1':].min())), 'max': int(max(sub_frame.loc[:, '1':].max()))}
            except:
                print('This error is being thrown most likely because a question has been assigned an incorrect question type.\nSee the dataframe below to debug:\n')
                print(sub_frame)
                pass

        # write dataframe to existing Workbook
        self.write_df(self.report_sheet, 'Report')
        self.write_df(self.survey_object, 'Survey Object')

    def statistics2(self):
        numeric_questions_df = questions_df[questions_df['type'].isin(['likert', 'matrix'])]
        numeric_values = self.df[numeric_questions_df['question_raw'].tolist()]
        if numeric_values.max().max() <=5:
            self.five_point_scale = True

        averages = {}
        for s in self.questions_df['section'].unique():
            try:
                sub_questions_df = numeric_questions_df[numeric_questions_df['section'] == s]
                sub_frame = self.df[sub_questions_df['question_raw']]
                totalNumberOfNullReponses = sub_frame.isna().sum().sum()
                sums = []
                # print(sub_frame)
                # for index, row in sub_frame.loc[:, '1':].iterrows():
                #     sums.append(row.sum())

                for col in sub_frame:
                    sums.append(sum(sub_frame[col]))
                total = sum(sums)

                totalNumberOfResponses = len(sums)*len(sub_frame)
                if totalNumberOfResponses == 0:
                    continue
                average = total/(totalNumberOfResponses-totalNumberOfNullReponses)
                # Create Dictionary of Averages for each Section
                averages[s.replace(u'\xa0', u' ')] = str(Decimal(average).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                pass

        self.report_sheet.columns = [str(i) for i in self.report_sheet.columns]

        sectionIndices = self.report_sheet.index[self.report_sheet[self.report_sheet.columns[-1]] == 'average_value']
        sectionIndices = [i + 1 for i in sectionIndices]
        self.row_count_of_sections = sectionIndices
        for i in sectionIndices:
            section = self.report_sheet.iloc[i, 0]
            # section = section.replace(u'\xa0', u' ')
            self.report_sheet.iloc[i-1, [-1]] = averages.get(section)

        # df['subsection'] = df['subsection'].astype(str)
        # df['section'] = df['section'].str.replace(u'\xa0', u' ')
        # df_shuffled['subsection'] = df_shuffled['subsection'].astype(str)

        # Save members averages and survey_object
        self.averages = averages
        # self.survey_object = df.copy()
        # self.shuffled_survey_object = df_shuffled.copy()

        self.sectionMinsAndMaxes = {}
        for section in self.questions_df['section'].unique():
            columns = self.questions_df[(self.questions_df['section'] == section) & (self.questions_df['type'].isin(['likert', 'matrix']))]['question_raw']

            # sub_frame = self.df[(self.survey_object['section']==section) & (self.survey_object['question_type'].isin(['likert', 'matrix']))]
            sub_frame = self.df[columns]
            # sub_frame.loc[:,'1':] = sub_frame.loc[:,'1':].apply(pd.to_numeric)
            if sub_frame.empty:
                continue
            try:
                self.sectionMinsAndMaxes[section] = {'min': int(np.nanmin(sub_frame.values)), 'max': int(np.nanmax(sub_frame.values))}
            except:
                print('This error is being thrown most likely because a question has been assigned an incorrect question type.\nSee the dataframe below to debug:\n')
                print(sub_frame)
                pass

        # write dataframe to existing Workbook
        # self.write_df(self.report_sheet, 'Report')
        # self.write_df(self.survey_object, 'Survey Object')

    def heat_map(self):
        worksheet = self.workbook.add_worksheet('Heat Map')
        if self.randomize:
            hm = self.shuffled_survey_object.copy()
        else:
            hm = self.survey_object.copy()

        hm = hm[hm['question_type'].isin(['likert', 'matrix'])]
        hm_copy = hm.copy()
        hm = hm.apply(lambda row: self.format_heat_map(row), axis = 1)
        del hm['subsection']
        del hm['question_type']
        del hm['question']
        hm.set_index(['section', 'number'], inplace=True)
        # Get the size of each section in order to merge cells
        indexSize = hm.groupby(level=0).size()
        # hm = hm.sample(frac=1, axis=1)
        hm = hm.T

        # Add a header format.
        header_format1 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'bg_color': '#BE6E28', 'font_size': 20, 'center_across': True, 'border': 2})
        header_format2 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 14, 'center_across': True})
        # Add fill formats
        green_format = self.workbook.add_format({'bg_color': '#008000', 'font_color': '#008000'})
        yellow_format = self.workbook.add_format({'bg_color': '#FFFF00', 'font_color': '#FFFF00'})
        red_format = self.workbook.add_format({'bg_color': '#FF0000', 'font_color': '#FF0000'})
        white_format = self.workbook.add_format({'bg_color': '#FFFFFF', 'font_color': '#FFFFFF'})

        # Conditional formatting for colors
        start_row = 3
        end_row   = start_row + hm.shape[0] - 1
        start_col = 1
        end_col   = start_col + hm.shape[1] - 1

        if self.five_point_scale == False:
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
                {'type': 'cell',
                 'criteria': 'between',
                 'minimum': '1',
                 'maximum': '3',
                 'format': red_format})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
                {'type': 'cell',
                 'criteria': 'between',
                 'minimum': '4',
                 'maximum': '7',
                 'format': yellow_format})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
                {'type': 'cell',
                 'criteria': '>',
                 'value': '7',
                 'format': green_format})
        else:
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
             'criteria': 'between',
             'minimum': '1',
             'maximum': '2',
             'format': red_format})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
             'criteria': '==',
             'value': '3',
             'format': yellow_format})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
             'criteria': '>',
             'value': '3',
             'format': green_format})

        worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
             'criteria': '==',
             'value': '0',
             'format': white_format})

        # Write the column headers with correct format.
        header_format1 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'bg_color': '#BE6E28', 'font_size': 20, 'center_across': True, 'border': 2})
        header_format2 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 14, 'center_across': True})
        header_format2_leftBorder = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 14, 'center_across': True, 'left': 2})
        header_format2_rightBorder = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 14, 'center_across': True, 'right': 2})

        indexSize = pd.DataFrame({'section':indexSize.index, 'length':indexSize.values})
        # Ensure correct ordering
        indexSize['arabic'] = indexSize.apply(lambda x: self.ToArabic(x['section']), axis = 1)
        indexSize.sort_values('arabic', inplace=True)
        indexSize.reset_index(drop=True, inplace=True)
        indexSize.set_index('section', inplace=True)

        # Y Axis: Number of directors
        directors = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 18, 'center_across': True})
        worksheet.write_column(3, 0, np.arange(1, self.number_of_respondents+1, 1), directors)

        # Section Headers
        col = 1
        for ind in indexSize.index:
            if indexSize.loc[ind]['length'] > 1:
                worksheet.merge_range(1, col, 1, col+indexSize.loc[ind]['length'] - 1, ind, header_format1)
                col += indexSize.loc[ind]['length']
            elif indexSize.loc[ind]['length'] == 1:
                worksheet.write(1, col, ind, header_format1)
                col += indexSize.loc[ind]['length']

        # Create a dataframe that contains the ending column count for each section.
        indexSizeTotals = indexSize.copy()
        indexSizeTotals.reset_index(inplace=True)
        for i in range(1, len(indexSize)):
            indexSizeTotals.loc[i, 'length'] += indexSizeTotals.loc[i-1, 'length']

        worksheet.write(2, 1, hm.columns.values[0][1], header_format2_leftBorder)

        # Question Number Headers
        col = 2
        for value in hm.columns.values[1:]:
            if col == indexSizeTotals.loc[indexSizeTotals['section'] == value[0], 'length'].item():
                worksheet.write(2, col, value[1], header_format2_rightBorder)
            else:
                worksheet.write(2, col, value[1], header_format2)
            col += 1

        # Global font
        fmt = self.workbook.add_format({"font_name": "Garamond"})

        # Adjust row and column widths
        worksheet.set_column(1, 250, 3, fmt)
        worksheet.set_default_row(20.2)
        worksheet.set_row(1, 26.2, fmt)
        worksheet.set_row(2, 18, fmt)
        worksheet.set_row(0, 3, fmt )
        worksheet.set_row(self.number_of_respondents+4, 3, fmt)
        worksheet.set_column(int(end_col)+1, int(end_col)+1, .3, fmt)

        top_thick_border = self.workbook.add_format({'top': 2})
        bottom_thick_border = self.workbook.add_format({'bottom': 2})
        left_thick_border = self.workbook.add_format({'left': 2})
        right_thick_border = self.workbook.add_format({'right': 2})
        thick = self.workbook.add_format({'top':2, 'bottom': 2, 'left': 2, 'right': 2})

        cell = self.workbook.add_format({'border': 1, "font_name": "Garamond"})
        cell_leftBorder = self.workbook.add_format({'left': 2, 'bottom': 1, 'top': 1, 'right': 1})
        cell_rightBorder = self.workbook.add_format({'right': 2, 'top': 1, 'bottom': 1, 'left': 1})

        hm.fillna(0, inplace=True)

        # Write the colored response cells
        worksheet.write_column(3, 1, hm.iloc[:, 0], cell_leftBorder)
        col = 2
        for colName in hm.columns[1:]:
            if col == indexSizeTotals.loc[indexSizeTotals['section'] == colName[0], 'length'].item():
                worksheet.write_column(3, col, hm.iloc[:, col - 1], cell_rightBorder)
            else:
                worksheet.write_column(3, col, hm.iloc[:, col - 1], cell)
            col += 1

        # Format the bottom row containing averages
        startColumnMerge = 1
        endColumnMerge = 0
        for ind, size, avg in zip(indexSize.index, indexSize['length'], self.averages.values()):
            if indexSize.loc[ind]['length'] > 1:
                endColumnMerge += size
                worksheet.merge_range(len(hm)+3, startColumnMerge, len(hm)+3, endColumnMerge, avg, header_format1)
                startColumnMerge = endColumnMerge + 1
            elif indexSize.loc[ind]['length'] == 1:
                endColumnMerge += size
                worksheet.write(len(hm)+3, startColumnMerge, avg, header_format1)
                startColumnMerge = endColumnMerge + 1

        # bottom row height
        worksheet.set_row(hm.shape[0] + 3, 26.2, fmt)
        return

    def write_df(self, df, sheetName):
        worksheet = self.workbook.add_worksheet(sheetName)
        df.fillna('', inplace=True)
        colNum = 0

        if sheetName == 'Survey Object':
            worksheet.write_row(0, 0, df.columns)
            for col in df.columns:
                worksheet.write_column(1, colNum, df[col])
                colNum += 1

        elif 'Skills Matrix' in sheetName:
            fmt = self.workbook.add_format({"font_name": "Garamond"})
            left_top_fmt = self.workbook.add_format({"font_name": "Garamond", 'left': 1, 'top': 1})
            top_bold_fmt_left_right = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1, 'left': 1, 'right': 1})
            top_bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1})
            top_right_bold_center = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1, 'right': 1})
            left_bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'left': 1, 'bottom': 1})
            right_bottom_bold_center_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'right': 1, 'bottom': 1})
            left_fmt = self.workbook.add_format({"font_name": "Garamond", 'left': 1})
            right_fmt = self.workbook.add_format({"font_name": "Garamond", 'right': 1})
            bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True})
            center_bold_fmt_left_right = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1, 'bottom': 1})
            center_bold_fmt_bottom = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'bottom': 1})
            center_bold = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1})
            center_bold_top = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1, 'top': 1})

            worksheet.write(1, 1, '', left_top_fmt)
            worksheet.write(1, 2, 'Board', top_bold_fmt_left_right)

            number_of_skills_matrix_respondents = df.shape[0] - 1
            number_of_skills_matrix_questions = df.shape[1]
            worksheet.merge_range(1, 3, 1, 2 + number_of_skills_matrix_respondents, 'Director Self-Ratings', top_right_bold_center)
            worksheet.write(2, 1, 'Skill Areas:', left_bold_fmt)
            worksheet.write(2, 2, 'Average', center_bold_fmt_left_right)
            initials = list(df.index.get_level_values('initials'))
            initials.remove('board')
            worksheet.write_row(2, 3, initials[:-1], center_bold_fmt_bottom)
            worksheet.write(2, 2+len(initials), initials[-1], right_bottom_bold_center_fmt)

            df.reset_index(inplace=True)

            colNum += 1
            start_row = 3
            end_row   = start_row + number_of_skills_matrix_questions
            start_col = 1
            end_col   = start_col + number_of_skills_matrix_respondents + 1

            ten_format = self.workbook.add_format({'bg_color': '#63be7b'})
            nine_format = self.workbook.add_format({'bg_color': '#86c97e'})
            eight_format = self.workbook.add_format({'bg_color': '#a9d27f'})
            seven_format = self.workbook.add_format({'bg_color': '#ccdd82'})
            six_format = self.workbook.add_format({'bg_color': '#eee683'})
            five_format = self.workbook.add_format({'bg_color': '#fedd81'})
            four_format = self.workbook.add_format({'bg_color': '#fcbf7b'})
            three_format = self.workbook.add_format({'bg_color': '#fba276'})
            two_format = self.workbook.add_format({'bg_color': '#f98570'})
            one_format = self.workbook.add_format({'bg_color': '#f8696b'})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '9.01',
            'maximum': '10',
            'format': ten_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '8.01',
            'maximum': '9',
            'format': nine_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '7.01',
            'maximum': '8',
            'format': eight_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '6.01',
            'maximum': '7',
            'format': seven_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '5.01',
            'maximum': '6',
            'format': six_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '4.01',
            'maximum': '5',
            'format': five_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '3.01',
            'maximum': '4',
            'format': four_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '2.01',
            'maximum': '3',
            'format': three_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '1.01',
            'maximum': '2',
            'format': two_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '0.01',
            'maximum': '1',
            'format': one_format})

            worksheet.write_column(3, 1, df.columns[2:], left_fmt)
            worksheet.write_column(3, 2, df.iloc[number_of_skills_matrix_respondents, 2:], center_bold)
            colNum += 2
            for i, u in zip(initials, df['uniqueID'].tolist()):
                value = df.loc[(df['uniqueID']==u) & (df['initials']==i)].values.flatten().tolist()
                value.remove(i)
                value.remove(u)
                if i == initials[-1]:
                    worksheet.write_column(3, colNum, value, right_fmt)
                else:
                    worksheet.write_column(3, colNum, value, fmt)
                colNum += 1

            average_of_averages = df.loc[(df['initials']=='board')].values.flatten().tolist()
            average_of_averages = average_of_averages[2:]
            average_of_averages = np.mean(average_of_averages)

            bold_align_right_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'right', 'left': 1, 'top': 1})
            worksheet.write(number_of_skills_matrix_questions + 3, 1, 'Overall Average:', bold_align_right_fmt)
            worksheet.write(number_of_skills_matrix_questions+3, 2, str(Decimal(average_of_averages).quantize(Decimal('1e-2'), ROUND_HALF_UP)), center_bold_top)

            top_bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1})
            top_right_bold_center_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'right': 1, 'top': 1})

            del df['uniqueID']
            del df['initials']
            df.drop(df.tail(1).index,inplace=True)

            colNum = 3

            df['respondentAverage'] = df.apply(lambda row: row.mean(), axis=1)
            # writer = pd.ExcelWriter('skills.xlsx')
            # df.to_excel(writer)
            # writer.save()
            for i, r in df.iterrows():
                if i == len(df) - 1:
                    worksheet.write(number_of_skills_matrix_questions+3, colNum, str(Decimal(r['respondentAverage'].mean()).quantize(Decimal('1e-2'), ROUND_HALF_UP)), top_right_bold_center_fmt)
                else:
                    worksheet.write(number_of_skills_matrix_questions+3, colNum, str(Decimal(r['respondentAverage'].mean()).quantize(Decimal('1e-2'), ROUND_HALF_UP)), top_bold_fmt)
                colNum += 1

            blank_left_bottom = self.workbook.add_format({"font_name": "Garamond", 'left': 1, 'bottom': 1})
            blank_left_right_bottom = self.workbook.add_format({"font_name": "Garamond", 'left': 1, 'bottom': 1, 'right': 1})
            blank_bottom_right_bold = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'bottom': 1, 'right': 1})
            worksheet.write(number_of_skills_matrix_questions + 4, 1, '', blank_left_bottom)
            worksheet.write(number_of_skills_matrix_questions + 4, 2, '', blank_left_right_bottom)
            worksheet.merge_range(number_of_skills_matrix_questions + 4, 3, number_of_skills_matrix_questions + 4, 2 + number_of_skills_matrix_respondents, 'Total Expertise Average by Directors', blank_bottom_right_bold)

        elif sheetName == 'Report':
            fmt = self.workbook.add_format({'font_name': 'Garamond',
                                                    'font_size': 12})
            fmt_center = self.workbook.add_format({'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True})
            right_border = self.workbook.add_format({'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True,
                                                    'right': 1})
            # Identify the columns we want to center_across, and where we want the
            # right border line to go
            str_columns = [str(x) for x in range(4, 5 + self.number_of_respondents)]
            # print(df)
            for col in df.columns:
                if col == str(self.number_of_respondents + 3):
                    # print(df[col])
                    worksheet.write_column(0, colNum, df[col], right_border)
                    colNum += 1
                elif col in str_columns:
                    worksheet.write_column(0, colNum, df[col], fmt_center)
                    colNum += 1
                else:
                    worksheet.write_column(0, colNum, df[col], fmt)
                    colNum += 1

            # Get min and max average rating for each question in a section,
            # Get the lowest and highest absolute response in each section
            sectionMinsAndMaxes = {}
            questionAveragesBySectionMinsAndMaxes = {}
            for section in self.questions_df['section'].unique():
                questions_sub_frame = self.questions_df[(self.questions_df['section']==section) & (self.questions_df['type'].isin(['likert', 'matrix']))]
                raw_questions_list = questions_sub_frame['question_raw'].tolist()
                sub_frame = self.df[raw_questions_list]
                # sub_frame.loc[:,'1':] = sub_frame.loc[:,'1':].apply(pd.to_numeric)
                if sub_frame.empty:
                    continue
                try:
                    sectionMinsAndMaxes[section] = {'min': int(np.nanmin(sub_frame.values)), 'max': int(np.nanmax(sub_frame.values))}
                except:
                    print('This error is being thrown most likely because a question has been assigned an incorrect question type.\nSee the dataframe below to debug:\n')
                    print(sub_frame)
                    pass
                means = []
                # for index, row in sub_frame.loc[:, '1':].iterrows():
                #     means.append(row.mean())
                for col in sub_frame:
                    means.append(np.mean(sub_frame[col]))


                questionAveragesBySectionMinsAndMaxes[section.replace(u'\xa0', u' ')] = {'min': str(Decimal(min(means)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), 'max': str(Decimal(max(means)).quantize(Decimal('1e-2'), ROUND_HALF_UP))}
            report_format = self.workbook.add_format({'bold': True,
                                                    'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True,
                                                    'bottom': 1,
                                                    'right': 1})
            underline = self.workbook.add_format({'bold': True,
                                                    'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True,
                                                    'bottom': 1})

            for rowIndex, section in zip(self.row_count_of_sections, self.survey_object['section'].unique()):
                sub_frame = self.survey_object[(self.survey_object['section']==section) & (self.survey_object['question_type'].isin(['likert', 'matrix']))]
                sub_frame.loc[:,'1':] = sub_frame.loc[:,'1':].apply(pd.to_numeric)
                if sub_frame.empty:
                    continue
                worksheet.merge_range(rowIndex - 1, 4, rowIndex - 1, 3 + self.number_of_respondents, 'Individual Ratings', report_format)
                worksheet.write(rowIndex - 1, 4 + self.number_of_respondents, 'Average', underline)
                formattedText = r'The average rating for this section is {}. The average rating for each question in the section ranged from {} to {}. Individual Board member ratings ranged from {} to {}.'

                worksheet.write(rowIndex - 1, 5 + self.number_of_respondents, formattedText.format(self.averages.get(section),
                    questionAveragesBySectionMinsAndMaxes.get(section).get('min'),
                    questionAveragesBySectionMinsAndMaxes.get(section).get('max'),
                    sectionMinsAndMaxes.get(section).get('min'),
                    sectionMinsAndMaxes.get(section).get('max')), fmt)

    def bar_chart(self):
        chart = self.survey_object
        chart = chart[chart['question_type'].isin(['likert', 'matrix'])]
        chart.loc[:,'1':] = chart.loc[:,'1':].apply(pd.to_numeric)
        worksheet = self.workbook.add_worksheet('Bar Chart')

        del chart['number']
        del chart['subsection']
        del chart['question_type']
        del chart['question']

        min_by_user = chart.groupby('section').min()
        min_by_user = min_by_user.min(axis=1)
        min_by_user = pd.DataFrame(min_by_user)
        min_by_user.reset_index(inplace=True)
        min_by_user.columns = ['Section', 'Low']
        max_by_user = chart.groupby('section').max()
        max_by_user = max_by_user.max(axis=1)
        max_by_user = pd.DataFrame(max_by_user)
        max_by_user.reset_index(inplace=True)
        max_by_user.columns = ['Section', 'High']

        chart = pd.DataFrame(list(self.averages.items()), columns=['Section', 'Averages'])
        chart = chart[chart['Averages']!='nan']

        chart['Section'] = chart.apply(lambda row: row['Section'].replace(u'\xa0', u' '), axis=1)
        chart['Averages'] = chart['Averages'].astype(float)
        min_by_user['Section'] = min_by_user.apply(lambda row: row['Section'].replace(u'\xa0', u' '), axis=1)
        max_by_user['Section'] = max_by_user.apply(lambda row: row['Section'].replace(u'\xa0', u' '), axis=1)
        chart = chart.merge(min_by_user, how='outer', on='Section')
        chart = chart.merge(max_by_user, how='outer', on='Section')

        text = 'Range of Ratings'
        chart = chart.reindex(index=chart.index[::-1])

        # Global font
        fmt = self.workbook.add_format({"font_name": "Garamond"})
        bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': 1})

        worksheet.write_row('A2', chart.columns, bold_fmt)
        worksheet.write_column('A3', chart['Section'], bold_fmt)
        worksheet.write_column('B3', chart['Averages'], bold_fmt)
        worksheet.write_column('C3', chart['Low'], bold_fmt)
        worksheet.write_column('D3', chart['High'], bold_fmt)

        worksheet.write(0, 2, text)
        worksheet.merge_range('C1:D1', text, fmt)

        chart1 = self.workbook.add_chart({'type': 'bar', 'name_font': {'font_name': 'Garamond', 'bold': True}})

        if self.five_point_scale == False:
            chart1.set_x_axis({'min': 1.00, 'max': 10.00, 'major_unit': 1, 'num_font':  {'name': 'Garamond'}})
            # chart1.set_y_axis({'num_font':  {'name': 'Garamond'}})
        else:
            chart1.set_x_axis({'min': 1.00, 'max': 5.00, 'major_unit': 1, 'num_font':  {'name': 'Garamond'}})
            # chart1.set_y_axis({'num_font':  {'name': 'Garamond'}})
        chart1.set_y_axis({'major_gridlines': {
            'visible': True,
            'line': {'width': 1},
            # 'name_font': {'font_name': 'Garamond', 'bold': True}
        }})
        chart1.add_series({
            'name': ['Bar Chart', 0, 2],
            'categories': ['Bar Chart', 2, 0, len(chart)+1, 0],
            'values': ['Bar Chart', 2, 1, len(chart)+1, 1],
            'fill': {'color': '#BE6E28'},
            'data_labels': {'value': True, 'position': 'inside_end'},
            'gap': 50,
            # 'name_font': {'font_name': 'Garamond', 'bold': True}
        })

        worksheet.insert_chart('G2', chart1, {'x_offset': 25, 'y_offset': 10, 'x_scale': 1.5, 'y_scale': 2})

    def hi_lo(self):
        worksheet = self.workbook.add_worksheet('Hi-Lo')
        hilo = self.survey_object

        hilo = hilo[hilo['question_type'].isin(['likert', 'matrix'])]
        pd.options.mode.chained_assignment = None
        raw_answers = hilo.loc[:,'1':]
        raw_answers = raw_answers.apply(pd.to_numeric)
        hilo['question_average'] = raw_answers.apply(lambda row: row.mean(), axis=1)
        hi = hilo.nlargest(5, 'question_average', keep='all')
        lo = hilo.nsmallest(5, 'question_average', keep='all')

        hi['question_average'] = hi.apply(lambda row: str(Decimal(row['question_average']).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)
        lo['question_average'] = lo.apply(lambda row: str(Decimal(row['question_average']).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)
        hi_header = self.workbook.add_format({"font_name": "Garamond",
                                         'bold': 2,
                                         'bg_color': '#008000',
                                         'font_color': '#FFFFFF',
                                         'align': 'center',
                                         'border': 1,
                                         'border_color': '#FFFFFF'})
        hi_header_left_align = self.workbook.add_format({"font_name": "Garamond",
                                         'bold': 1,
                                         'bg_color': '#008000',
                                         'font_color': '#FFFFFF',
                                         'align': 'left',
                                         'border': 1,
                                         'border_color': '#FFFFFF'})
        cell_fmt = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#F3F3F3',
                                           'align': 'left',
                                           'border': 1,
                                           'border_color': '#FFFFFF'})

        worksheet.merge_range('B2:E2', 'Highest Rated Questions', hi_header)
        worksheet.write('B3', 'Rating', hi_header)
        worksheet.write('C3', 'Section', hi_header)
        worksheet.merge_range('D3:E3', 'Question', hi_header_left_align)

        hi['formatted_number'] = hi['number'] + hi['subsection']

        worksheet.write_column('B4', hi['question_average'], cell_fmt)
        worksheet.write_column('C4', hi['section'], cell_fmt)
        hi['formatted_number'] = hi['formatted_number'].str.replace(r'\.', r'')
        hi['formatted_number'] += '.'
        worksheet.write_column('D4', hi['formatted_number'], cell_fmt)
        worksheet.write_column('E4', hi['question'], cell_fmt)

        lo_header = self.workbook.add_format({"font_name": "Garamond",
                                         'bold': 1,
                                         'bg_color': '#FFFF00',
                                         'align': 'center',
                                         'border': 1,
                                         'border_color': '#FFFFFF'})
        lo_header_left_align = self.workbook.add_format({"font_name": "Garamond",
                                         'bold': 1,
                                         'bg_color': '#FFFF00',
                                         'align': 'left',
                                         'border': 1,
                                         'border_color': '#FFFFFF'})

        worksheet.merge_range('B{0}:E{0}'.format(int(4 + len(hi))), 'Lowest Rated Questions', lo_header)
        worksheet.write('B{}'.format(int(5 + len(hi))), 'Rating', lo_header)
        worksheet.write('C{}'.format(int(5 + len(hi))), 'Section', lo_header)
        worksheet.merge_range('D{0}:E{0}'.format(int(5 + len(hi))), 'Question', lo_header_left_align)

        lo['formatted_number'] = lo['number'] + lo['subsection']

        worksheet.write_column('B{}'.format(int(6 + len(hi))), lo['question_average'], cell_fmt)
        worksheet.write_column('C{}'.format(int(6 + len(hi))), lo['section'], cell_fmt)
        lo['formatted_number'] = lo['formatted_number'].str.replace(r'\.', r'')
        lo['formatted_number'] += '.'
        worksheet.write_column('D{}'.format(int(6 + len(hi))), lo['formatted_number'], cell_fmt)
        worksheet.write_column('E{}'.format(int(6 + len(hi))), lo['question'], cell_fmt)
        return

    def eSigma(self):
        eS = pd.DataFrame(list(self.averages.items()), columns=['Section', 'Averages'])
        eS['Section'] = eS.apply(lambda row: row['Section'].replace(u'\xa0', u' '), axis=1)
        eS['Averages'] = eS['Averages'].astype(float)
        average_of_section_averages = eS['Averages'].mean()
        qa = self.survey_object.copy()
        qa = qa[qa['question_type'].isin(['likert', 'matrix'])]
        qa = qa.loc[:,'1':].apply(pd.to_numeric)
        qa['average'] = qa.loc[:,'1':].apply(lambda row: row.mean(), axis=1)
        average_of_question_averages = qa['average'].mean()
        # This is a convient place to calculate part of the section analysis
        self.average_total_rating_given = qa['average'].sum()
        #
        eSigma = (average_of_question_averages + average_of_section_averages) / 2
        return str(Decimal(eSigma).quantize(Decimal('1e-2'), ROUND_HALF_UP))

    def aggregated_section_analysis(self):
        worksheet = self.workbook.add_worksheet('Section Analysis')

        sec = self.survey_object.copy()

        header = self.workbook.add_format({"font_name": "Garamond",
                                         'bg_color': '#c4590f',
                                         'font_color': '#FFFFFF',
                                         'align': 'center'})
        header_left_align = self.workbook.add_format({"font_name": "Garamond",
                                         'bg_color': '#c4590f',
                                         'font_color': '#FFFFFF',
                                         'align': 'left',
                                         'bottom': 1,
                                         'left': 1,
                                         'top': 1,
                                         'border_color': '#c4590f'})
        cell_fmt1 = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#fbe4d5',
                                           'align': 'left',
                                           'bottom': 1,
                                           'left': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt2 = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#FFFFFF',
                                           'align': 'left',
                                           'bottom': 1,
                                           'left': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt1_centered = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#fbe4d5',
                                           'align': 'center',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt2_centered = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#FFFFFF',
                                           'align': 'center',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt1_right = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#fbe4d5',
                                           'align': 'right',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt2_right = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#FFFFFF',
                                           'align': 'right',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_bold = self.workbook.add_format({"font_name": "Garamond",
                                           'align': 'right',
                                           'bold': 1})
        cell_bold_left = self.workbook.add_format({"font_name": "Garamond",
                                           'align': 'left',
                                           'border': 1,
                                           'bold': 1,
                                           'border_color': '#f5b083'})
        orange_border_orange_bg = self.workbook.add_format({'border': 1,
                                                'border_color': '#f5b083',
                                                "font_name": "Garamond",
                                                'bg_color': '#fbe4d5',
                                                'align': 'center'
                                                })
        orange_border_white_bg = self.workbook.add_format({'border': 1,
                                                'border_color': '#f5b083',
                                                "font_name": "Garamond",
                                                'bg_color': '#FFFFFF',
                                                'align': 'center'
                                                })
        orange_border = self.workbook.add_format({'border': 1, 'border_color': '#f5b083'})

        worksheet.write('A1', 'Aggregated Section Analysis', header_left_align)
        worksheet.write('B1', 'Average', header)

        averages = pd.DataFrame(list(self.averages.items()), columns=['Section', 'Averages'])
        averages = averages[averages['Averages']!='nan']
        averages['Section'] = averages.apply(lambda row: row['Section'].replace(u'\xa0', u' '), axis=1)
        currentRow = 2
        for index, row in averages.iterrows():
            if index % 2 == 0:
                worksheet.write(r'A{}'.format(index+2), row['Section'], cell_fmt1)
                worksheet.write(r'B{}'.format(index+2), str(row['Averages']), cell_fmt1_centered)
            else:
                worksheet.write(r'A{}'.format(index+2), row['Section'], cell_fmt2)
                worksheet.write(r'B{}'.format(index+2), str(row['Averages']), cell_fmt2_centered)
            currentRow += 1

        eSigma_value = self.eSigma()

        currentRow += 2
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'Evaluation Statistics', header_left_align)
        currentRow += 1

        # Assessment Statistics
        worksheet.write('A{}'.format(currentRow), 'Number of rated questions', cell_fmt1)
        worksheet.write('B{}'.format(currentRow), len(sec[sec['question_type'].isin(['likert', 'matrix'])]), cell_fmt1_right)
        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Number of open-ended questions', cell_fmt2)
        worksheet.write('B{}'.format(currentRow), len(sec[sec['question_type'].isin(['long_form'])]), cell_fmt2_right)
        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Number of Board members surveyed', cell_fmt1)
        worksheet.write('B{}'.format(currentRow), self.number_of_respondents, cell_fmt1_right)
        currentRow += 1

        worksheet.write('A{}'.format(currentRow), 'Percentage participation', cell_fmt2)
        # likert_matrix_questions = self.questions_df[self.questions_df['type'].isin(['likert', 'matrix'])]
        # print(self.df[likert_matrix_questions['question_raw'].tolist()].apply(pd.value_counts))
        raw_answers = sec[sec['question_type'].isin(['likert', 'matrix'])]
        raw_answers = raw_answers.loc[:,'1':]
        try:
            number_of_empty_strings = raw_answers.apply(pd.value_counts).loc[''].sum().sum()
        except:
            number_of_empty_strings = 0

        number_of_non_responses = raw_answers.isnull().sum().sum()
        number_of_non_responses += number_of_empty_strings
        len_index_times_len_columns = len(raw_answers.index) * len(raw_answers.columns)
        number_of_questions_answered = len_index_times_len_columns - number_of_non_responses
        percent_participation = (len_index_times_len_columns - number_of_non_responses) / len_index_times_len_columns

        worksheet.write('B{}'.format(currentRow), str(Decimal(percent_participation).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Total number of responses', cell_fmt1)
        worksheet.write('B{}'.format(currentRow), len_index_times_len_columns - number_of_non_responses, cell_fmt1_right)
        currentRow += 1
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'Question analysis by Board member response:', cell_bold_left)
        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Maximum possible total rating', cell_fmt1)
        if self.five_point_scale == False:
            worksheet.write('B{}'.format(currentRow), len(sec[sec['question_type'].isin(['likert', 'matrix'])])*10, cell_fmt1_right)
        else:
            worksheet.write('B{}'.format(currentRow), len(sec[sec['question_type'].isin(['likert', 'matrix'])])*5, cell_fmt1_right)
        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Average total rating given', cell_fmt2)
        worksheet.write('B{}'.format(currentRow), str(Decimal(self.average_total_rating_given).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Highest total rating given', cell_fmt1)
        check = 0
        raw_answers = raw_answers.apply(pd.to_numeric)
        for c in raw_answers.columns:
            if raw_answers[c].sum()>check:
                check = raw_answers[c].sum()
        worksheet.write('B{}'.format(currentRow), check, cell_fmt1_right)
        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Lowest total rating given', cell_fmt2)
        check = len(sec[sec['question_type'].isin(['likert', 'matrix'])])*10
        for c in raw_answers.columns:
            if raw_answers[c].sum()<check:
                check = raw_answers[c].sum()
        worksheet.write('B{}'.format(currentRow), check, cell_fmt2_right)
        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Maximum possible average', cell_fmt1)
        if self.five_point_scale == False:
            worksheet.write('B{}'.format(currentRow), '10.00', cell_fmt1_right)
        else:
            worksheet.write('B{}'.format(currentRow), '5.00', cell_fmt1_right)
        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Highest rating average given', cell_fmt2)
        check = 0
        for c in raw_answers.columns:
            if raw_answers[c].mean()>check:
                check = raw_answers[c].mean()
        worksheet.write('B{}'.format(currentRow), str(Decimal(check).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Lowest rating average given', cell_fmt1)
        check = 10
        for c in raw_answers.columns:
            if raw_answers[c].mean()<check:
                check = raw_answers[c].mean()
        worksheet.write('B{}'.format(currentRow), str(Decimal(check).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
        currentRow += 1
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'Group response analysis:', cell_bold_left)

        # This section needs try, except clauses for the case that value counts returns a zero count for whole range of answers
        # Create one series from the raw answers to use value_counts function
        series_of_all_answers = pd.Series(dtype='float')
        for c in raw_answers.columns:
            series_of_all_answers = series_of_all_answers.append(raw_answers[c], ignore_index=True)
        counts = raw_answers.apply(pd.value_counts)
        counts.fillna(0, inplace=True)
        if self.five_point_scale == False:
            new_index = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        else:
            new_index = [1, 2, 3, 4, 5]
        counts = counts.reindex(new_index, fill_value=0)
        currentRow += 1
        if self.five_point_scale == False:
            worksheet.write('A{}'.format(currentRow), 'Percentage of questions receiving a rating of 10', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), str(Decimal((counts.loc[10].sum()/number_of_questions_answered)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percentage of questions receiving a rating of 8 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), str(Decimal(((counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())/number_of_questions_answered)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percentage of questions receiving a rating of 7 or below', cell_fmt1)

            seven_or_below = number_of_questions_answered - (counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())
            worksheet.write('B{}'.format(currentRow), str(Decimal((seven_or_below/number_of_questions_answered)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percentage of questions receiving a rating of 3 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), str(Decimal(((counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())/number_of_questions_answered)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            currentRow += 1
            if self.not_observed_count or self.not_applicable_count:
                x = cell_fmt2
                cell_fmt2 = cell_fmt1
                cell_fmt1 = x
                x = cell_fmt2_right
                cell_fmt2_right = cell_fmt1_right
                cell_fmt1_right = x
                worksheet.write('A{}'.format(currentRow), 'Percentage of questions receiving a rating of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), str(Decimal(((self.not_observed_count + self.not_applicable_count)/(number_of_questions_answered + self.not_observed_count + self.not_applicable_count))).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
                currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of total responses with a rating of 10', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), counts.loc[10].sum(), cell_fmt1_right)
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of total responses with a rating of 8 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum()), cell_fmt2_right)
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of total responses with a rating of 7 or below', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), seven_or_below, cell_fmt1_right)
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of total responses with a rating of 3 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum()), cell_fmt2_right)
            if self.not_observed_count or self.not_applicable_count:
                worksheet.write('A{}'.format(currentRow), 'Number of questions receiving a rating of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), str(int(self.not_observed_count + self.not_applicable_count)), cell_fmt2_right)
                currentRow += 1
        else:
            worksheet.write('A{}'.format(currentRow), 'Percentage of questions receiving a rating of 5', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), str(Decimal((counts.loc[5].sum()/number_of_questions_answered)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percentage of questions receiving a rating of 4 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), str(Decimal(((counts.loc[4].sum() + counts.loc[5].sum())/number_of_questions_answered)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percentage of questions receiving a rating of 3 or below', cell_fmt1)
            three_or_below = number_of_questions_answered - (counts.loc[4].sum() + counts.loc[5].sum())
            worksheet.write('B{}'.format(currentRow), str(Decimal((three_or_below/number_of_questions_answered)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percentage of questions receiving a rating of 2 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), str(Decimal(((counts.loc[1].sum() + counts.loc[2].sum())/number_of_questions_answered)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            currentRow += 1
            if self.not_observed_count or self.not_applicable_count:
                x = cell_fmt2
                cell_fmt2 = cell_fmt1
                cell_fmt1 = x
                x = cell_fmt2_right
                cell_fmt2_right = cell_fmt1_right
                cell_fmt1_right = x
                worksheet.write('A{}'.format(currentRow), 'Percentage of questions receiving a rating of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), str(Decimal(((self.not_observed_count + self.not_applicable_count)/(number_of_questions_answered + self.not_observed_count + self.not_applicable_count))).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
                currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of total responses receiving a rating of 5', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), counts.loc[5].sum(), cell_fmt1_right)
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of total responses receiving a rating of 4 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[4].sum() + counts.loc[5].sum()), cell_fmt2_right)
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of total responses receiving a rating of 3 or below', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), three_or_below, cell_fmt1_right)
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of total responses receiving a rating of 2 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[1].sum() + counts.loc[2].sum()), cell_fmt2_right)
            if self.not_observed_count or self.not_applicable_count:
                worksheet.write('A{}'.format(currentRow), 'Number of questions receiving a rating of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), str(int(self.not_observed_count + self.not_applicable_count)), cell_fmt2_right)
                currentRow += 1

        currentRow += 2
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'e∑igma = {}'.format(eSigma_value), orange_border_white_bg)
