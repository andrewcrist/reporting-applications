from flask import Flask, redirect, url_for, request, render_template, make_response, send_from_directory, send_file, after_this_request
from werkzeug.utils import secure_filename
from FSR_OPENER import fsr_opener
from FSR_ETL import parse_raw_fsr, ppl_parser, ask_user
from FSR import FSR

from BEA_OPENER import BEA_OPENER
from BEA_PPL_PARSER import BEA_PPL_PARSER
from BEA_SPLITTER import BEA_SPLITTER
from BEA_ETL import BEA_ETL
from BEA_REPORT import BEA

import pandas as pd
import numpy as np
import os
import glob
import re
from random import shuffle
from collections import Counter
import warnings
import xlsxwriter
from functools import reduce
from statistics import mean
import math
import openpyxl
import sys
import itertools
import difflib
import logging
import time
import re
from decimal import Decimal, ROUND_HALF_UP
import zipfile

app = Flask(__name__, static_folder='static', template_folder='templates')
UPLOAD_FOLDER = 'uploads\\'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
if not os.path.exists(UPLOAD_FOLDER):
    os.makedirs(UPLOAD_FOLDER)

@app.route('/')
def home():
  return render_template('index.html')

@app.route('/bea_uploader', methods=['GET', 'POST'])
def bea_uploader():
   if request.method == 'POST':
        # check if the post request has the file part
        if 'bea_export' not in request.files:
            print('no file')
            return redirect(request.url)
        bea_file = request.files['bea_export']
        # if user does not select file, browser also
        # submit a empty part without filename
        if bea_file.filename == '':
            print('no filename')
            return redirect(request.url)
        else:
            file_name = secure_filename(bea_file.filename)
            bea_file.save(os.path.join(app.config['UPLOAD_FOLDER'], file_name))
            randomize_bea = request.form['randomize_bea']
            bea = run_bea(file_name, randomize_bea)

            report_name = re.search('^(.*).csv', file_name).group(1)
            # zipf = zipfile.ZipFile(r'/uploads/' + '{}.zip'.format(report_name),'w', zipfile.ZIP_DEFLATED)
            zipf = zipfile.ZipFile(os.path.join(app.config['UPLOAD_FOLDER'], '{}.zip'.format(report_name)),'w', zipfile.ZIP_DEFLATED)

            for report in bea:
                zipf.write(os.path.join(app.config['UPLOAD_FOLDER'], report.output_filename))
            zipf.close()

            for report in bea:
                os.remove(os.path.join(app.config['UPLOAD_FOLDER'], report.output_filename))
            os.remove(os.path.join(app.config['UPLOAD_FOLDER'], file_name))
            # request_zip(filename, bea)
            # for b in bea:
            #     print(b.output_filename)
            #     return redirect('/downloadfile/'+ b.output_filename)
            # bea_output_filename = bea.output_filename
            # print(bea_output_filename)
            return redirect('/downloadfile/'+ '{}.zip'.format(report_name))

   return render_template('bea.html')

@app.route('/fsr_uploader', methods=['GET', 'POST'])
def fsr_uploader():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'modified' not in request.files or 'plain' not in request.files:
            return redirect(request.url)
        modified = request.files['modified']
        plain = request.files['plain']
        create_section_headers = request.form['create_section_headers']
        create_modified_instance = request.form['create_modified_instance']
        # print('section headers', section_headers)
        # print('modified instance', modified_instance)

        # if user does not select file, browser also
        # submit a empty part without filename
        if modified.filename == '' or plain.filename == '':
            return redirect(request.url)
        else:
            modified_filename = secure_filename(modified.filename)
            plain_filename = secure_filename(plain.filename)

            modified.save(os.path.join(app.config['UPLOAD_FOLDER'], modified_filename))
            plain.save(os.path.join(app.config['UPLOAD_FOLDER'], plain_filename))
        
            fsr = run_fsr(plain_filename, modified_filename, create_section_headers, create_modified_instance)
            print(fsr.output_filename)
            fsr_output_filename = fsr.output_filename
            return redirect('/downloadfile/'+ fsr_output_filename)
    return render_template('fsr.html')

def run_fsr(plain_filename, modified_filename, create_section_headers, create_modified_instance):
    plain_path = os.path.join(app.config['UPLOAD_FOLDER'], plain_filename)
    modified_path = os.path.join(app.config['UPLOAD_FOLDER'], modified_filename)

    pdf_raw = fsr_opener(file = plain_path).parse()
    mdf_raw = fsr_opener(file = modified_path).parse()

    mdf, mquestions_df, mdict_of_dfs, symbol  = parse_raw_fsr(mdf_raw)
    pdf, pquestions_df, pdict_of_dfs, symbol = parse_raw_fsr(pdf_raw)

    ppl_df = ppl_parser(pdf_raw)
    print_headers = create_section_headers

    fsr = FSR(pdf, mdf, ppl_df, mquestions_df, pdict_of_dfs, mdict_of_dfs, print_headers, app, modified_filename, create_modified_instance)    
    
    return fsr

def run_bea(bea_filename, randomize_bea):
    bea_export_path = os.path.join(app.config['UPLOAD_FOLDER'], bea_filename)

    raw_data_dict = BEA_OPENER(file = bea_export_path)
    dict_of_dfs = BEA_SPLITTER(bea_export_path, raw_data_dict)

    meta_section = dict_of_dfs.get('meta')
    ppl_df = BEA_PPL_PARSER(meta_section)  

    not_app_or_obs = {}
    ETLd_question_dfs = {}
    ETLd_dfs = {}

    for report_type in dict_of_dfs:
            if report_type != 'meta':
                report_to_run = dict_of_dfs.get(report_type)
                questions_df, transformed_df, not_applicable_count, not_observed_count = BEA_ETL(report_to_run, ppl_df, report_type, bea_export_path)
                ETLd_question_dfs[report_type] = questions_df
                ETLd_dfs[report_type] = transformed_df
                not_app_or_obs[report_type] = {}
                not_app_or_obs[report_type]['not_observed_count'] = not_observed_count
                not_app_or_obs[report_type]['not_applicable_count'] = not_applicable_count

    bea_reports = []
    for report_type in dict_of_dfs:
        if report_type != 'meta':
            print('Creating {} report for {}'.format(report_type, bea_filename))
            report_to_run = ETLd_dfs.get(report_type)
            questions_df = ETLd_question_dfs.get(report_type)
            bea = BEA(bea_filename, report_type, report_to_run, questions_df, ppl_df, not_app_or_obs[report_type]['not_applicable_count'], not_app_or_obs[report_type]['not_observed_count'], app, randomize = randomize_bea)
            bea_reports.append(bea)
    return bea_reports    

# Download API
@app.route("/downloadfile/<filename>", methods = ['GET'])
def download_file(filename):
    # @after_this_request
    # def remove_file(response):
    #     # try:
    #     os.remove(os.getcwd() + r'\uploads\{}'.format(filename))
    #     file_handle = open(os.getcwd() + r'\uploads\{}'.format(filename), 'r')
    #     file_handle.close()
    #     # except Exception as error:
    #         # app.logger.error("Error removing or closing downloaded file handle", error)
    #     return response
            
    print('\n\nhere' , filename)
    print('filepath: ', UPLOAD_FOLDER + filename)
    print(os.getcwd())
    print('\n')


    return send_file(os.getcwd() + r'\uploads\{}'.format(filename), as_attachment=True, attachment_filename='')
    

# @app.route('/download-zip')
# def request_zip(file_name, list_of_reports):
#     report_name = re.search('^(.*).csv', file_name).group(1)
#     # zipf = zipfile.ZipFile(r'/uploads/' + '{}.zip'.format(report_name),'w', zipfile.ZIP_DEFLATED)
#     zipf = zipfile.ZipFile(os.path.join(app.config['UPLOAD_FOLDER'], 'Name.zip'),'w', zipfile.ZIP_DEFLATED)

#     for report in list_of_reports:
#         zipf.write(os.path.join(app.config['UPLOAD_FOLDER'], report.output_filename))
#     zipf.close()

#     result = send_file(os.getcwd() + r'\uploads\Name.zip',mimetype = 'zip',attachment_filename= 'Name.zip',as_attachment = True)
#     for report in list_of_reports:
#         os.remove(os.path.join(app.config['UPLOAD_FOLDER'], report.output_filename))   
#     return result


if __name__ == '__main__':
  app.run()
  
