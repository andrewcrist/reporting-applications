from jinja2 import Template, Environment, PackageLoader, select_autoescape, FileSystemLoader
import datetime
import win32com.client as win32
from PIL import ImageGrab
import os
import time
import sys
import glob
import pandas as pd
from weasyprint import HTML, CSS
from decimal import Decimal, ROUND_HALF_UP
import pandas as pd
import numpy as np
from collections import Counter
import re
from random import shuffle

# sys.path.insert(1, r'C:\Users\Drew\Nasdaq_Classes_and_Fns')
# from ppl_parser import ppl_parser
# from csv_opener import csv_opener
#
# from bea_etl import bea_splitter, bea_etl
# from bea_report import bea_report

from library.ppl_parser import ppl_parser
from library.csv_opener import csv_opener

from library.bea_etl import bea_splitter, bea_etl
from library.bea_report import bea_report

import warnings
warnings.filterwarnings("ignore", 'This pattern has match groups')

def ask_create_section(report_type, section):
    check = str(input("Would you like to create a {} for report type: {}? (y/n): ".format(section, report_type))).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_create_section(report_type, section)
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_create_section(report_type, section)

def BEA_templater(file_name, member_variable_query, report_type, df, questions_df, ppl_df, skills_response, swot=True, highlights_actionpoints=True, randomize = True, relationship_web=True, CGPG=True):

    client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file_name)

    if report_type != 'BEA' and not skills_response:
        swot = False
        highlights_actionpoints = False
        relationship_web = False
        if report_type == 'NomGov':
            report_title = 'Nominating and Governance Committee Report'
            intro_title = 'Nominating and Governance Committee'
        else:
            report_title = '{} Committee Report'.format(report_type)
            intro_title = '{} Committee'.format(report_type)
        board_or_committee = 'committee'
        show_GCFR_method = False
        show_flag_legend = False

        eval_overview_html = '''
        <div style="text-align: justify;">
          The {} of {} completed a self-evaluation. All responses and comments presented throughout this report in all graphics and reporting features are anonymized and randomized to promote candid feedback and adhere to best practices in corporate governance. The goal of this report is to provide the committee with an understanding  of how the directors view the committee’s effectiveness, highlight areas of strength and areas for improvement, promote positive committee dynamics, and provide information to help improve the committee’s overall performance and effectiveness.
        </div>'''.format(intro_title, client_name.group(1))

    if report_type == 'BEA' and not skills_response:
        relationship_web = ask_create_web()
        highlights_actionpoints = ask_create_highlights_actionpoints()
        swot = ask_create_swot()
        show_GCFR_method = ask_show_GCFR_method(report_type)
        show_flag_legend = ask_show_flag_legend(report_type)

        # relationship_web = True
        # highlights_actionpoints = True
        # swot = True
        # show_GCFR_method = True
        # show_flag_legend = True

        report_title = 'Board Evaluation Report'
        intro_title = 'Board of Directors'
        board_or_committee = 'Board'

        eval_overview_html = '''
        <div style="text-align: justify;">
          The {} of {} completed a self-evaluation. All responses and comments presented throughout this report in all graphics and reporting features are anonymized and randomized to promote candid feedback and adhere to best practices in corporate governance. The goal of this report is to provide the Board with an understanding of how the directors view the Board’s effectiveness, highlight areas of strength and areas for improvement, promote positive Board dynamics, and provide information to help improve the Board’s overall performance.
        </div>'''.format(intro_title, client_name.group(1))
    else:
        relationship_web = False

    if not skills_response:
        CGPG = ask_create_cgpg(report_type)
        hilo_section_bool = ask_create_section(report_type, 'Hi-Lo Section')

        # CGPG = True
        # hilo_section_bool = True

    if skills_response:
        relationship_web = False
        highlights_actionpoints = False
        swot = False
        show_GCFR_method = False
        show_flag_legend = False
        report_title = 'Skills Matrix'
        question_column_width = 35
        intro_title = 'Board of Directors'
        board_or_committee = 'Board'

    if not skills_response:
        question_column_width = set_question_column_width(report_type)
        # question_column_width = 35

        if member_variable_query['eval_stats_list_of_dicts'][1].get('Number of Board members surveyed') == 0:
            del member_variable_query['eval_stats_list_of_dicts'][1]

    now = datetime.datetime.now()
    formatted_date = str(now.strftime("%B")) + ' ' + str(now.day) + ', ' + str(now.year)


    file_loader =FileSystemLoader('templates')
    env = Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)
    template = env.get_template('bea_report_template.html')

    skills_matrix_present = 'skills_matrix' in questions_df['type'].unique()

    # print(skills_matrix_present)
    # print('\n\nswot: ', swot, '\nhighlights: ', highlights_actionpoints)
# 'client_logo': r'images/client_logo.png'
    jinja_input_dict = {'title': 'Nasdaq Board Evaluation', 'nasdaq_logo': r'images/nasdaq_logo.png', 'CGPG': CGPG, 'report_title': report_title,
         'year': str(now.year), 'formatted_date': formatted_date, 'skills_matrix_present': skills_matrix_present,
         'questions_df': questions_df, 'swot': swot, 'highlights_actionpoints': highlights_actionpoints, 'relationship_web': relationship_web,
         'swot_s': r'images/swot_s.png', 'swot_w': r'images/swot_w.png', 'swot_o': r'images/swot_o.png', 'swot_t': r'images/swot_t.png',
         'scale_image': r'images/scale.png', 'green_flag': r'images/green_flag.png', 'yellow_flag': r'images/yellow_flag.png',
          'red_flag': r'images/red_flag.png', 'question_column_width': question_column_width, 'show_GCFR_method': show_GCFR_method,
          'show_flag_legend': show_flag_legend, 'intro_title': intro_title, 'board_or_committee': board_or_committee, 'hilo_section_bool': hilo_section_bool}

    if report_type == 'BEA' and relationship_web:
        jinja_input_dict['radar_chart_image'] = r'charts\radar_chart.png'
    if CGPG:
        jinja_input_dict['cgpg_bar_chart'] = r'charts\cgpg_bar_chart.png'

    # Get Assessment sections labels and section names
    section_names_and_labels = {}
    label_re = re.compile(r'^[\s\t]*([IVX]+\.)[\s\t]*(.*)')
    for section in questions_df['section'].unique().tolist():
        label = label_re.match(section)
        section_names_and_labels[label.group(1)] = label.group(2)
    jinja_input_dict['section_names_and_labels'] = section_names_and_labels

    # Control which sections are in the table of contents
    jinja_input_dict['table_of_contents_titles'] = ['#heat_map_title', '#hilo_title', '#CGPG_method_title', '#CG_flag_title', '#statistical_analysis_title']

    optional_sections = []
    if CGPG:
        optional_sections.append('#CGPG_title')
    if relationship_web:
        optional_sections.append('#radar_title')
    if member_variable_query.get('skills_matrix_average') is not None:
        optional_sections.append('#skills_matrix_title')
    if swot:
        optional_sections.append('#swot_title')
    if highlights_actionpoints:
        optional_sections.append('#highlights_actionpts_title')

    # print(optional_sections)
    # print(jinja_input_dict['table_of_contents_titles'])

    if optional_sections:
        if '#CGPG_title' in optional_sections or '#radar_title' in optional_sections:
            index = 1
        else:
            index = 2
        flag = False
        for optional_section in optional_sections:
            if optional_section in ['#skills_matrix_title', '#swot_title', '#highlights_actionpts_title'] and ('#CGPG_title' in optional_sections or '#radar_title' in optional_sections) and not flag:
                flag = True
                index += 1
            # if optional_section == '#radar_title':
            #     if not
            #     jinja_input_dict['table_of_contents_titles'].insert(1, optional_section)
            # else:
            jinja_input_dict['table_of_contents_titles'].insert(index, optional_section)
            index+=1

    # print(jinja_input_dict['table_of_contents_titles'])
    # print('optional sections: ', optional_sections)
    # print('table of contents: ', jinja_input_dict['table_of_contents_titles'])

    jinja_input_dict['number_of_sections'] = len(questions_df['section'].unique().tolist())
    jinja_input_dict['report_type'] = report_type
    jinja_input_dict['client_name'] = client_name.group(1)
    jinja_input_dict['eval_overview_html'] = eval_overview_html

    for item in member_variable_query.keys():
        jinja_input_dict['{}'.format(item)] = member_variable_query[item]

    output = template.render(jinja_input_dict)
    # with open("test.html", "w") as fh:
    #     fh.write(output)

    # print(member_variable_query['data_dict'])
    HTML(string=output, base_url='.').write_pdf('output\\{}_{}.pdf'.format(client_name.group(1), report_type), stylesheets=[CSS(r'static\bea_template_style.css')])

def ask_create_cgpg(report_type):
    check = str(input("Would you like to create a corporate governance priority graph for report type: {}? (y/n): ".format(report_type))).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_create_cgpg(report_type)
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_create_cgpg(report_type)

def ask_show_GCFR_method(report_type):
    check = str(input("Would you like to display the Corporate Goverance Flag Report methodology section for report type: {}? (y/n): ".format(report_type))).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_show_GCFR_method(report_type)
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_show_GCFR_method(report_type)

def ask_show_flag_legend(report_type):
    check = str(input("Would you like to display the Corporate Goverance Flag Report legend for report type: {}? (y/n): ".format(report_type))).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_show_flag_legend(report_type)
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_show_flag_legend(report_type)

def ask_use_questionsdf_in_directory(report_type):
    check = str(input("Would you like to use the {}_questions_df file in the project directory? (y/n): ".format(report_type))).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_use_questionsdf_in_directory(report_type)
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_use_questionsdf_in_directory(report_type)

def ask_create_web():
    check = str(input("Would you like to create a corporate relationship web graph? (y/n): ")).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_create_web()
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_create_web()

def ask_randomize_report(report_type):
    check = str(input("Would you like to randomize report: {}? (y/n): ".format(report_type))).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_randomize_report()
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_randomize_report(report_type)

def set_question_column_width(report_type):
    check = str(input("Would you like to set a column width for report type: {}? (y/n): ".format(report_type))).lower().strip()
    try:
        if check[0] == 'y':
            percentage = int(input('> '))
            if len(str(percentage)) == 2:
                return percentage
            else:
                print('Invalid Input')
                return set_question_column_width(report_type)
        elif check[0] == 'n':
            return 35
        else:
            print('Invalid Input')
            return set_question_column_width(report_type)
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return set_question_column_width(report_type)

def ask_create_highlights_actionpoints():
    check = str(input("Would you like to create a highlights and actionpoints page? (y/n): ")).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_create_highlights_actionpoints()
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_create_highlights_actionpoints()

def ask_create_swot():
    check = str(input("Would you like to create a swot page? (y/n): ")).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_create_swot()
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_create_swot()

def join_qdf_and_rawdata_into_smoothed_df(qdf, tdf, file, report_type, anonymize):

    ############################################################################
    # ETL
    df = pd.DataFrame(columns=['client_name', 'report_type', 'section', 'number', 'question_raw', 'question', 'sub_section', 'type', 'average', 'response', 'respondent_id', 'question_mean'])
    try:
        client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file).group(1)
    except:
        client_name = None

    if anonymize ==True:
        director_encoder = dict()
        director_count = 1

    for s in qdf['section'].unique():
        for q in qdf[qdf['section']==s]['question_raw']:
            for i in tdf[q].index:
                # if (tdf.loc[i,q]==np.nan or (tdf.loc[i,q]=='nan')):
                #     print('here!', i, q)
                #     response = None
                # else:
                #     response = tdf.loc[i,q]
                if anonymize == False:
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item(), 'response': tdf.loc[i,q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}
                else:
                    sub_q = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item()
                    if sub_q not in ['', '!__!']:
                        if director_encoder.get(sub_q) is None:
                            director_encoder[sub_q] = 'Director {}'.format(director_count)
                            sub_q = 'Director {}'.format(director_count)
                            director_count += 1
                        else:
                            sub_q = director_encoder.get(sub_q)
                    else:
                        pass
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section': sub_q, 'response': tdf.loc[i,q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}

                df = df.append(row, ignore_index=True)

    question_means = []
    for section in df['section'].unique():
        for number in df[(df['section']==section)]['number'].unique():
            try:
                mean_by_question = df[(df['section']==section) & (df['number']==number)]['response'].mean()
                mean_by_question = "{0:.2f}%".format(Decimal('{}'.format(mean_by_question)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                mean_by_question = None
            mean_column_addition = [mean_by_question]*len(df[(df['section']==section) & (df['number']==number)]['response'])
            question_means.extend(mean_column_addition)

    df['question_mean'] = question_means
    df.to_excel('benchmark_data\\SMOOTHED_{}_{}.xlsx'.format(report_type, client_name))
    return df

def create_bea_jinja_dict(qdf, tdf, randomize):
    data_dict = dict()
    rankFlag = False
    for s in qdf['section'].unique():
        data_dict[s] = {}
        for q in qdf[qdf['section']==s]['question_raw']:
            if qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() == 'skills_matrix':
                del data_dict[s]
                break

            data_dict[s][q] = {}
            data_dict[s][q]['type'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item()
            data_dict[s][q]['number'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item()
            data_dict[s][q]['question'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item()
            data_dict[s][q]['sub_section'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item()
            if qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['matrix', 'likert']:
                rankFlag = False
                answers = tdf[q].replace({np.nan: None})
                answers.fillna('N/A', inplace=True)
                answers = answers.tolist()
                answers = [int(x) if x != 'N/A' else x for x in answers]
                if randomize:
                    shuffle(answers)
                data_dict[s][q]['answers'] = answers
                data_dict[s][q]['average'] = str(Decimal(np.mean(tdf[q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            elif qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['long_form', 'optional_long_form']:
                rankFlag = False
                answers = tdf[q].dropna().tolist()
                answers = [str(a) for a in answers if a.lower() not in ['nan', '#name?', None,  'no comment', 'none', np.nan, '']]
                if not answers:
                    answers = [None]
                if randomize:
                    shuffle(answers)
                data_dict[s][q]['answers'] = answers
            elif qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['rank']:
                if rankFlag == False:
                    rankFlag = True
                    count = 1
                    data_dict[s][q]['count'] = count
                    lengthOfRankedChoiceSection = len(qdf[(qdf['section']==s) & (qdf['number']==data_dict[s][q]['number'])])
                    data_dict[s][q]['lengthOfRankedChoiceSection'] = lengthOfRankedChoiceSection
                    data_dict[s][q]['ranks'] = list(np.arange(1, lengthOfRankedChoiceSection+1, 1))
                    data_dict[s][q]['first'] = True

                    dictonaryOfCounts = dict(Counter(tdf[q].tolist()))
                    listOfOrderedCounts = list()
                    for value in np.arange(1, lengthOfRankedChoiceSection+1, 1):
                        listOfOrderedCounts.append(dictonaryOfCounts.get(value, 0))
                    data_dict[s][q]['answers'] = listOfOrderedCounts
                else:
                    rankFlag = True
                    count += 1
                    data_dict[s][q]['count'] = count
                    data_dict[s][q]['first'] = False

                    dictonaryOfCounts = dict(Counter(tdf[q].tolist()))
                    listOfOrderedCounts = list()
                    for value in np.arange(1, lengthOfRankedChoiceSection+1, 1):
                        listOfOrderedCounts.append(dictonaryOfCounts.get(value, 0))
                    data_dict[s][q]['answers'] = listOfOrderedCounts

            elif qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['yes/no']:
                answers = tdf[q].dropna().tolist()
                yesNoCheck = [answer.lower() for answer in answers]
                yesNoCheck = dict(Counter(yesNoCheck))
                data_dict[s][q]['answers'] = yesNoCheck

    # count = 0
    # for s in data_dict:
    #     print('\n\nSection Name:  ', s)
    #     for q in data_dict[s]:
    #         print('Raw Question:  ', q)
    #         try:
    #             print('Question Average:  ', data_dict[s][q]['average'])
    #         except:
    #             pass
    #         print('Number: ', data_dict[s][q]['number'])
    #         print('Type:  ', data_dict[s][q]['type'])
    #         print('Question: ', data_dict[s][q]['question'])
    #         print('Sub:  ', data_dict[s][q]['sub_section'])
    #         print('Answers:  ', data_dict[s][q]['answers'])
    #         print('\n')
    #         count+=1
    #         if count == 14:
    #             break
    return data_dict

if __name__ == "__main__":
    start_time = time.time()
    print(sys.version)
    print('\n------------------------------------------\n')

    cwd = os.getcwd()

    extension = 'csv'
    file = glob.glob("*.{}".format(extension))[0]

    print('\n-----------------\nProccessing BEA File...')
    print('File Name: ', file, '\n\n')

    raw_data_dict = csv_opener(file)
    dict_of_dfs = bea_splitter(file, raw_data_dict)
    try:
        meta_section = dict_of_dfs.get('meta')
    except:
        pass

    ppl_df = ppl_parser(meta_section, sort=False)

    not_app_or_obs = {}
    ETLd_question_dfs = {}
    ETLd_dfs = {}
    data_dicts = {}
    smoothed_dfs = {}

    for report_type in dict_of_dfs:
        if report_type != 'meta':
            data_dicts[report_type] = dict()
            report_to_run = dict_of_dfs.get(report_type)
            questions_df, transformed_df, na_no_null_dict = bea_etl(report_to_run, ppl_df, report_type, file, True)
            # print(na_no_null_dict)
            # response = ask_use_questionsdf_in_directory(report_type)
            response = True
            if response:
                if os.path.isfile('{}_questions_df.xlsx'.format(report_type)):
                    q_df = pd.read_excel('{}_questions_df.xlsx'.format(report_type), index=False, converters={'number': str})
                    ETLd_question_dfs[report_type] = q_df
                    questions_df = q_df
                else:
                    print('\n\nAborting... Question data frame file "{}_questions_df.xlsx" not found in directory\n'.format(report_type))
                    sys.exit()
            else:
                ETLd_question_dfs[report_type] = questions_df


            ETLd_dfs[report_type] = transformed_df
            not_app_or_obs[report_type] = {}
            not_app_or_obs[report_type]['na_no_null_dict'] = na_no_null_dict

            jinja_data_dict = create_bea_jinja_dict(questions_df, transformed_df, True)
            data_dicts[report_type]['randomized'] = jinja_data_dict

            smoothed_df = join_qdf_and_rawdata_into_smoothed_df(questions_df, transformed_df, file, report_type, False)
            smoothed_dfs[report_type] = smoothed_df



    if set(questions_df['type']).issubset(set(['skills_matrix', ''])):
        skills_response = True
    else:
        skills_response = False

    # print('skills response: ', skills_response)
    for report_type in dict_of_dfs:
        if report_type != 'meta':
            # na_present = bool([a for a in not_app_or_obs[report_type]['na_no_null_dict'].values() if bool(a)])
            print('\n\n-----------------------------------\nCreating {} report for {}'.format(report_type, file))


            randomize_bea_bool = ask_randomize_report(report_type)
            # randomize_bea_bool = False


            report_to_run = ETLd_dfs.get(report_type)
            questions_df = ETLd_question_dfs.get(report_type)
            BEA = bea_report(file, report_type, report_to_run, questions_df, ppl_df, not_app_or_obs[report_type]['na_no_null_dict'], skills_response, randomize = randomize_bea_bool)
            member_variable_query = dict()

            if BEA.na_in_heat_map:
                member_variable_query['na_in_heat_map'] = True
            else:
                member_variable_query['na_in_heat_map'] = False

            try:
                member_variable_query['skills_matrix_average'] = BEA.skills_matrix_average
                member_variable_query['number_of_skills_matrix_questions'] = BEA.number_of_skills_matrix_questions
            except:
                pass
            member_variable_query['five_point_scale'] = BEA.five_point_scale
            member_variable_query['skills_response'] = skills_response
            try:
                member_variable_query['skills_matrix_df'] = BEA.skills_matrix_export
                member_variable_query['skills_matrix_df_width'] = np.arange(1, BEA.skills_matrix_export.shape[1] + 1, 1)
            except:
                pass

            if not skills_response:
                member_variable_query['eSigma_value'] = BEA.eSigma_value
                member_variable_query['section_data'] = BEA.section_data
                member_variable_query['questions_df'] = BEA.questions_df
                if randomize_bea_bool:
                    member_variable_query['jinja_for_loop_dict'] = data_dicts[report_type]['randomized']
                else:
                    member_variable_query['jinja_for_loop_dict'] = create_bea_jinja_dict(questions_df, report_to_run, False)
                member_variable_query['number_of_respondents'] = BEA.number_of_respondents
                member_variable_query['section_averages'] = BEA.averages
                member_variable_query['eval_stats_list_of_dicts'] = BEA.eval_stats_list_of_dicts
                member_variable_query['eval_stats_list_of_dicts_last_item'] = tuple((list(BEA.eval_stats_list_of_dicts[-1].keys())[0], BEA.eval_stats_list_of_dicts[-1].get(list(BEA.eval_stats_list_of_dicts[-1].keys())[0])))
                member_variable_query['section_avg_list'] = BEA.section_avg_list
                member_variable_query['section_avg_list_last_item'] = tuple((list(BEA.section_avg_list[-1].keys())[0], BEA.section_avg_list[-1].get(list(BEA.section_avg_list[-1].keys())[0])))
                member_variable_query['jinja_hi'] = BEA.jinja_hi
                member_variable_query['jinja_lo'] = BEA.jinja_lo
                # member_variable_query['heat_map_export'] = BEA.heat_map_export
                member_variable_query['hm_section_length_counts'] = BEA.hm_section_length_counts
                member_variable_query['hm_numbers_borders'] = BEA.hm_numbers_borders
                # member_variable_query['heat_map_index'] = BEA.heat_map_index
                member_variable_query['heat_map_averages'] = BEA.heat_map_averages
                # member_variable_query['hm_values_borders'] = BEA.hm_values_borders
                member_variable_query['hm_rows_directions'] = BEA.hm_rows_directions
                member_variable_query['percentage_size'] = BEA.percentage_size
                # print('percentage_size: ', member_variable_query['percentage_size'])

                member_variable_query['header_font_size'] = 11
                if BEA.percentage_size<13 and BEA.percentage_size>10:
                    member_variable_query['header_font_size'] = 10
                elif BEA.percentage_size<=10:
                    member_variable_query['header_font_size'] = 9

                member_variable_query['number_font_size'] = 10
                if BEA.percentage_size<13 and BEA.percentage_size>10:
                    member_variable_query['number_font_size'] = 8
                elif BEA.percentage_size<=10:
                    member_variable_query['number_font_size'] = 7

                member_variable_query['hm_cell_height'] = hm_cell_height = 10
                if BEA.number_of_respondents < 16 and BEA.number_of_respondents >= 10:
                    hm_cell_height = 8
                elif BEA.number_of_respondents < 20 and BEA.number_of_respondents >= 16:
                    hm_cell_height = 6
                elif BEA.number_of_respondents >= 20:
                    hm_cell_height = 5
                member_variable_query['hm_cell_height'] = hm_cell_height

            else:
                member_variable_query['eSigma_value'] = None
                member_variable_query['section_data'] = None
                member_variable_query['questions_df'] = None
                if randomize_bea_bool:
                    member_variable_query['jinja_for_loop_dict'] = data_dicts[report_type]['randomized']
                else:
                    member_variable_query['jinja_for_loop_dict'] = create_bea_jinja_dict(questions_df, report_to_run, False)
                member_variable_query['number_of_respondents'] = None
                member_variable_query['section_averages'] = None
                member_variable_query['eval_stats_list_of_dicts'] = None
                member_variable_query['eval_stats_list_of_dicts_last_item'] = None
                member_variable_query['section_avg_list'] = None
                member_variable_query['section_avg_list_last_item'] = None
                member_variable_query['jinja_hi'] = None
                member_variable_query['jinja_lo'] = None

            BEA_templater(file, member_variable_query, report_type, report_to_run, questions_df, ppl_df, skills_response, randomize = randomize_bea_bool)


    #####################################
    print('\n\n--- Run Time ---')
    print('--- %s seconds ---' % (time.time() - start_time))
