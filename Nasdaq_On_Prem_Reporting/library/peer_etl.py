import pandas as pd
import numpy as np
import os
import glob
import re
from random import shuffle
from collections import Counter
import warnings
import xlsxwriter
from functools import reduce
from statistics import mean
import math
import openpyxl
import sys
import itertools
import difflib
import logging
import time
from pandas import ExcelWriter
from decimal import Decimal, ROUND_HALF_UP, ROUND_HALF_EVEN
from itertools import cycle
from fuzzywuzzy import fuzz

def peer_etl(df, ppl_df, report_type, file_name, write_questions_df, year=None):
    # df.to_excel('entrydata.xlsx')
    # ppl_df.to_excel('ppl_df.xlsx')
    questions_df = pd.DataFrame({'question_raw': df.columns})
    survey = pd.DataFrame(columns = ['type', 'respondent_id', 'question_id', 'response', 'section', 'sub_section' 'sub_section_value', 'sub_section_number', 'number'])

    questions = []
    header_list = []

    search = []
    for values in questions_df['question_raw']:
        new_question = re.sub(r'__\d+$', '', values)
        search.append(new_question)
    questions_df['question'] = search

    re_continued_section = re.compile('(?!.*[\s\t]+1\.).*\(Continued\).*?[\s\t]+(\d+\.)[\s\t]+(.*?)_?')
    re_continued_assessment = re.compile('(?!.*_[a]\.)(?=.*[\s\t]+1\.).*\(Continued\).*[\s\t]+([IVX]+\..*)[\s\t]+(1\.)[\s\t]+(.*)')
    re_continued = re.compile('.*\(Continued\)')
    re_continued_section_and_assessment = re.compile('.*\(Continued\).*\(Continued\).*?[\s\t]+(\d+\.)[\s\t]+(.*)')

    re_section_match = re.compile('^.*?[\s\t]*([IVX]+\..*?)[\s\t]+(\d+\.)?[\s\t]+(.*)')
    re_question_number = re.compile('.*?(\d+\.).+')

    re_skills_matrix = re.compile('.*Skills\s+Matrix.*')
    re_attributes_matrix = re.compile('.*Attributes\s+Matrix.*')

    re_individuals_subsection = re.compile('.*_[A-Z].*_.*')
    committee_critique = re.compile(r'^_.*committee.*_$', re.IGNORECASE)

    sections = []
    numbers = []
    questions = []
    types = []
    sub_sections = []
    sub_section_values = []
    sub_section_numbers = []
    ppl_ids = []

    empty_cell_count = 0

    not_observed_dict = dict()
    not_applicable_dict = dict()
    empty_cell_dict = dict()

    unnumbered_question_indexer = 1
    ids = df.index.tolist()

    for question, header in zip(questions_df['question'], df.columns):
        # print('\n', id, question)
        # print(file_name, report_type, df[header])
        if df[header].dtype == 'object':
            if df[header].str.contains('^[1-9]|10$').any():
                d = df[header].value_counts(dropna=False)
                try:
                    not_applicable_dict[question]=d['n/a']
                except:
                    pass
                try:
                    not_observed_dict[question]= d['n/o']
                except:
                    pass
                try:
                    empty_cell_count += df[header].isna().sum()
                    if df[header].isna().sum():
                        empty_cell_dict[question]= df[header].isna().sum()
                except:
                    pass

                df[header].replace({'n/a': np.nan}, inplace=True)
                df[header].replace({'n/o': np.nan}, inplace=True)
        if df[header].isnull().all():
             data = df[header].astype(str)
             df[header] = data
        elif df[header].str.isdigit().all():
            data = pd.to_numeric(df[header])
            df[header] = data
        else:
            data = df[header].astype(str)
            df[header] = data
        continued_match = re_continued.match(question)
        section_match = re_section_match.match(question)

        if continued_match is not None:

            ########################
            if re_continued_section_and_assessment.match(question) is not None:
                # print('1.', question)
                section = sections[-1]
                try:
                    number = re_question_number.match(question).group(1)
                except:
                    try:
                        number = numbers[-1]
                    except:
                        number = '!__!'
                try:
                    formatted_question = re.search('(?!.*_$).*\(Continued\).*\(Continued\).*?[\s\t]+(\d+\.)[\s\t]+(.*)', question).group(2)
                except:
                    try:
                        formatted_question = re.search('.*\(Continued\).*\(Continued\).*?[\s\t]+(\d+\.)[\s\t]+(.*?)_.*_', question).group(2)
                    except:
                        formatted_question = '!__!'
                try:
                    sub_section = re.search('.*?_(.*)_', question).group(1)
                except:
                    sub_section = ''
            ########################


            elif re_continued_section.match(question) is not None:
                # print('2.', question)
                section = sections[-1]
                try:
                    number = re_question_number.match(question).group(1)
                except:
                    try:
                        number = numbers[-1]
                    except:
                        number = '!__!'
                try:
                    re_continued_section_named = re.compile('(?!.*[\s\t]+1\.).*\(Continued\).*?[\s\t]+(\d+\.)[\s\t]+(.*?)_')
                    formatted_question = re_continued_section_named.match(question).group(2)
                except:
                    try:
                        formatted_question = re_continued_section.match(question).group(2)
                    except:
                        formatted_question = '!__!'
                try:
                    sub_section = re.search('.*?_(.*)_', question).group(1)
                except:
                    sub_section = ''

            elif re_continued_assessment.match(question) is not None:
                # print(question)
                try:
                    section = re_continued_assessment.match(question).group(1)
                except:
                    section = '!__!'
                try:
                    number = re_question_number.match(question).group(1)
                except:
                    try:
                        number = numbers[-1]
                    except:
                        number = '!__!'
                try:
                    formatted_question = re.search('(?!.*_$).*\(Continued\).*?[\s\t]+(\d+\.)[\s\t]+(.*)', question).group(2)
                except:
                    try:
                        formatted_question = re.search('.*\(Continued\).*?[\s\t]+(\d+\.)[\s\t]+(.*?)_.*_', question).group(2)
                    except:
                        formatted_question = '!__!'
                try:
                    sub_section = re.search('.*?_(.*)_', question).group(1)
                except:
                    sub_section = ''

            else:
                section = '!__!'
                number = '!__!'
                formatted_question = '!__!'
                try:
                    sub_section = re.search('.*?_(.*)_', question).group(1)
                except:
                    sub_section = ''
                # print('question:\n', question, '\nIs improperly formatted. Please ensure that the question number and the period following it, are surrounded by spaces.')
                # return 1

            if re_skills_matrix.match(question) is not None:
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'skills_matrix'

            elif re_attributes_matrix.match(question) is not None:
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'attributes_matrix'

            # elif all(str(item).lower() in ['yes', 'no', 'nan'] for item in data.tolist()) and data.tolist().count('nan')<2:
            elif all(str(item).lower() in ['yes', 'no'] for item in data.tolist()):
                sub_section = ''
                type = 'yes/no'

            elif (any(isinstance(answer, str) for answer in data)) | (all(data.isna())):
                try:
                    sub_section = re.search('.*?_(.*)_', question).group(1)
                except:
                    sub_section = ''
                type = 'long_form'


            elif re.search(':_[\s\t]*[a-z]\.', question) is not None:
                try:
                    sub_section = re.search(':.*_[\s\t]*([a-z]\..*)_', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'matrix'

            elif re_individuals_subsection.match(question) is not None:
                try:
                    sub_section = re.search('.*?_(.*)_', question).group(1)
                except:
                    sub_section = ''
                type = 'likert'

            elif committee_critique.match(question):
                # question = re.search('_(.*)_', question).group(1)
                type = 'committee_critique'
                sub_section = ''

            else:
                type = 'likert'
                try:
                    sub_section = re.search('.*?_(.*)_', question).group(1)
                except:
                    sub_section = ''

        elif section_match is not None:
            # print(data.tolist())
            # print(all(str(item).lower() in ['yes', 'no', 'nan'] for item in data.tolist()))
            # print(data.tolist().count('nan'))
            try:
                section = section_match.group(1)
            except:
                section = '!__!'
            try:
                number = re_question_number.match(question).group(1)
            except:
                try:
                    number = numbers[-1]
                except:
                    number = '!__!'
            try:
                if re.search('.*_[A-Z].*_.*', question) is None and re.search(':.*_[a-z]\.', question) is None:
                    formatted_question = section_match.group(3)
                elif re_skills_matrix.match(question) is not None:
                    try:
                        formatted_question = re.search('(.*)_.*_$', header).group(1)
                    except:
                        formatted_question = '!__!'
                else:
                    formatted_question = re.search('^.*[\s\t]+(\d+\.)?[\s\t]+(.*?)_', question).group(2)
            except:
                formatted_question = '!__!'

            if re_skills_matrix.match(question) is not None:
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'skills_matrix'

            elif re_attributes_matrix.match(question) is not None:
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'attributes_matrix'

            # elif all(str(item).lower() in ['yes', 'no', 'nan'] for item in data.tolist()) and data.tolist().count('nan')<2:
            elif all(str(item).lower() in ['yes', 'no'] for item in data.tolist()):
                sub_section = ''
                type = 'yes/no'

            elif (any(isinstance(answer, str) for answer in data)) | (all(data.isna())):
                try:
                    sub_section = re.search('.*?_(.*)_', question).group(1)
                except:
                    sub_section = ''
                type = 'long_form'

            elif re.search(':.*_[\s\t]*[a-z]\.', question) is not None:
                try:
                    sub_section = re.search(':.*_[\s\t]*([a-z]\..*)_', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'matrix'

            elif re_individuals_subsection.match(question) is not None:
                try:
                    sub_section = re.search('.*?_(.*)_', question).group(1)
                except:
                    sub_section = ''
                type = 'likert'

            elif committee_critique.match(question):
                try:
                    question = re.search('_(.*)_', question).group(1)
                except:
                    question = '!__!'
                type = 'committee_critique'
                sub_section = ''

            else:
                type = 'likert'
                try:
                    sub_section = re.search('.*?_(.*)_', question).group(1)
                except:
                    sub_section = ''
        else:
            try:
                section = sections[-1]
            except:
                section = '!__!'

            try:
                # No greedy search needed. The line below finds the first instance of \d
                number = re_question_number.match(question).group(1)
            except:
                try:
                    number = numbers[-1]
                except:
                    number = '!__!'

            # print(question)
            # print('types', types)
            # print(re.search('_(.*)_\s*$', question).groups())
            if re_skills_matrix.match(question) is not None or (types[-1] == 'skills_matrix' and '_a.' not in re.search('_(.*)_\s*$', question).group(1)):
                try:
                    formatted_question = re.search('(.*)_.*_\s*$', header).group(1)
                except:
                    formatted_question = '!__!'
                try:
                    skills_match = re.search('_(.*)_\s*$', header).group(1)
                except:
                    skills_match = '!__!'
                type = 'skills_matrix'
                sub_section = skills_match

            elif re_attributes_matrix.match(question) is not None or (types[-1] == 'attributes_matrix' and '_a.' not in re.search('_(.*)_', question).group(1)):
                try:
                    formatted_question = re.search('(.*)_.*_$', header).group(1)
                except:
                    formatted_question = '!__!'
                try:
                    skills_match = re.search('_(.*)_$', header).group(1)
                except:
                    skills_match = '!__!'
                type = 'attributes_matrix'
                sub_section = skills_match

            # Yes/No Questions
            # elif all(str(item).lower() in ['yes', 'no', 'nan'] for item in data.tolist()) and data.tolist().count('nan')<2:
            elif all(str(item).lower() in ['yes', 'no'] for item in data.tolist()):
                try:
                    formatted_question = re.search('^.*?\d+\.[\s\t]*(.*)$', question).group(1)
                except:
                    formatted_question = '!__!'
                type = 'yes/no'
                sub_section = ''

            # Long form questions
            elif (any(isinstance(answer, str) for answer in data)) | (all(data.isna())):
                # If unnnumbered, just grab the whole string as the question value
                if (re.search('^(?!.*\d\.[^$])(.*)$', question) is not None):
                    # print('\n', unnumbered_question_indexer)
                    formatted_question = re.search('(.*)', question).group(0)
                    type = 'optional_long_form'

                    # We want to group unnumbered, optional questions that refer to
                    # numbered questions using a nomenclature that is different from
                    # those numbered questions. That way, the jinja_for_loop_dict
                    # can make a distinction between the two and format appropriately.

                    if '_unnumbered' in numbers[-1]:
                        # I'm not sure why I implemented the line directly below. What is needed in create_peer_composite_jinja_dict is unique numbers...
                        # see First Business Financial Peer.csv
                        # number = re.sub('_\d+$', '_{}'.format(unnumbered_question_indexer), numbers[-1])
                        number = 'question_{}_unnumbered'.format(unnumbered_question_indexer)

                        # print('here1: ', question)
                        # print('number: ', number)

                        unnumbered_question_indexer += 1
                    elif number != '!__!':
                        # print('number: ', number)

                        # I think the below line might be incorrect--see First Business Financial Peer.csv
                        # number = 'question_{}_unnumbered'.format(number)
                        number = 'question_{}_unnumbered'.format(unnumbered_question_indexer)

                        # print('here2: ', question)
                        # print('number: ', number)
                        # I think the below line might be incorrect--see First Business Financial Peer.csv, do i increment here?
                        unnumbered_question_indexer += 1
                    else:
                        number = 'question_empty_numbers_list_unnumbered{}'.format(unnumbered_question_indexer)
                        # print('here3: ', question)
                        # print('number: ', number)
                        unnumbered_question_indexer += 1
                else:
                    try:
                        formatted_question = re.search('\d+\.[\s\t]*(.*?)_.*_$', question).group(1)
                    except:
                        try:
                            # No greedy search necessary
                            formatted_question = re.search('\d+\.[\s\t]*(.*)$', question).group(1)
                        except:
                            formatted_question = '!__!'

                    type = 'long_form'
                try:
                    sub_section = re.search('.*?_(.*)_', question).group(1)
                except:
                    sub_section = ''


            # Matrix questions
            # elif re_alpha_subsection.match(question) is not None or re.search(':_\s*[a-z]\.', question) is not None:
            elif re.search(':[\s\t]*_[\s\t]*[a-z]\.', question) is not None:
                try:
                    formatted_question = re.search('\d+\.\s(.*:)[\s\t]*_[\s\t]*[a-z]\.[\s\t]*(.*)_', question).group(1)
                    if formatted_question is None:
                        formatted_question = re.search('\d+\.\s(.*:).*_[\s\t]*[a-z]\.[\s\t]*(.*)_', question).group(1)
                except:
                    formatted_question = '!__!'
                try:
                    sub_section = re.search(':[\s\t]*_[\s\t]*([a-z]\..*)_', question).group(1)
                    if sub_section is None:
                        sub_section = re.search(':.*_[\s\t]*([a-z]\..*)_', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'matrix'

            # elif re_individuals_subsection.match(question) is not None:
            #     try:
            #         formatted_question = re.search('\d+\.[\s\t]*(.*:)', question).group(1)
            #     except:
            #         formatted_question = '!__!'
            #     try:
            #         sub_section = re.search('_(.*)_$', question).group(1)
            #     except:
            #         sub_section = '!__!'
            #     type = 'likert'

            elif committee_critique.match(question):
                try:
                    formatted_question = re.search('_(.*)_', question).group(1)
                except:
                    formatted_question = '!__!'
                type = 'committee_critique'
                sub_section = ''
            else:
                try:
                    formatted_question = re.search('\d+\.[\s\t]*(.*?)_.*_', question).group(1)
                except:
                    formatted_question = '!__!'
                type = 'likert'
                try:
                    sub_section = re.search('.*?_(.*)_', question).group(1)
                except:
                    sub_section = ''

        if sub_section not in ['', '!__!']:
            matcher = {key: None for key in ids}
            for i in ids:
                Ratio = fuzz.ratio(sub_section.lower(),i.lower())
                matcher[i] = Ratio
                # print('\nFuzz matches\nsubsection: ', sub_section, '\nI:', i, '\nRatio: ', Ratio)
            name_match = max(matcher, key=matcher.get)
        else:
            name_match = ''

            # tup = get_question_and_question_type(data, question)
            # type, formatted_question, number, sub_section = tup[0], tup[1], tup[2], tup[3]

        re_alpha_subsection = re.compile('^\s*([a-z])\.')
        re_alpha_subsection_value = re.compile('^\s*([a-z]\.)?\s*(.*)')
        if sub_section != '':
            # print(sub_section)
            try:
                sub_section_number = re_alpha_subsection.match(sub_section).group(1)
            except:
                sub_section_number = None
            try:
                sub_section_value = re_alpha_subsection_value.match(sub_section).group(2)
            except:
                sub_section_value = None
        else:
            sub_section_number = None
            sub_section_value = None

        # print(question, number)
        # print(type(number))
        sub_section_numbers.append(sub_section_number)
        sub_section_values.append(sub_section_value)
        sections.append(section)
        numbers.append(str(number))
        questions.append(formatted_question)
        types.append(type)
        sub_sections.append(sub_section)
        ppl_ids.append(name_match)


    questions_df['sub_section_number'] = sub_section_numbers
    questions_df['sub_section_value'] = sub_section_values
    questions_df['section'] = sections
    questions_df['number'] = numbers
    questions_df['question'] = questions
    questions_df['type'] = types
    questions_df['sub_section'] = sub_sections
    questions_df['UniqueIdentifier'] = ppl_ids

    # print(questions_df['number'])

    # print(ppl_df)

    # print(df)
    df.set_index(ppl_df.index, inplace=True)
    if write_questions_df:
        if not year:
            try:
                questions_df.to_excel('{}_questions_df.xlsx'.format(report_type), index=False)
            except:
                pass
        else:
            try:
                questions_df.to_excel('{}_{}_questions_df.xlsx'.format(year, report_type), index=False)
            except:
                pass

    na_no_null_dict = {'na': not_applicable_dict, 'no': not_observed_dict, 'null': empty_cell_dict}

    return questions_df, df, na_no_null_dict


def peer_splitter(file_name, raw_data_dict):

    # print('\n', file_name)
    df = pd.DataFrame.from_dict(raw_data_dict)

    columns = list(df.columns)
    columns = [re.sub(r'\xa0', ' ', c) for c in columns]
    df.columns = columns

    workbook_args = []

    # Skill matrices
    skills_matrix = re.compile('.*skills\s+matrix.*', re.IGNORECASE)
    skills_matrices = list(filter(skills_matrix.match, columns))

    # Attribute matrices
    attribute_matrix = re.compile('.*attributes\s+matrix.*', re.IGNORECASE)
    attribute_matrices = list(filter(attribute_matrix.match, columns))

    # Start columns for BEA and committee sections
    # Changed ('(?!.*\([Cc]ontinued\))(?!.*[Cc]ommittee)(?=^I\.|.*[\s\t]+I\.)')
    # to ('(?!.*\([Cc]ontinued\))(?=^I\.|.*[\s\t]+I\.)')
    re_first_section = re.compile('(?!.*\([Cc]ontinued\))(?=^I\.|.*[\s\t]+I\.)')

    # Mistake made and then corrected: negative lookahead combined with negative
    # characterclass. Watch out for '_a.' vs '_b.' and others
    re_committee_sections = re.compile('(?!.*\([Cc]ontinued\))(?!.*:_a\.)(?!.*:.*_a\.)(?=.*[Cc]ommittee)(?=^I\.|.*[\s\t]+I\.)')

    first_section = list(filter(re_first_section.match, columns))
    committee_sections = list(filter(re_committee_sections.match, columns))
    # For now, override the committe section filter
    committee_sections = []

    # print(first_section)

    # Often, section headers will duplicate the name of the section across
    # multiple questions. We want to iterate backward and remove column numbers
    # from first_section and committee_sections where this duplication occurs
    # This way, there is only one value for the start column for BEA and committee reports
    if first_section:
        if len(first_section) > 1:
            column_numbers = [df.columns.get_loc(x) for x in first_section]
            first_section = [df.columns[min(column_numbers)]]
    # if 'aatrim' in file_name:
    #     print('\nfirst sections: ', first_section)

    # Iterate backwards. If the column count is one higher than the column count
    # of the value that precedes it, delete the current column from the list of
    # potential start columns.
    if committee_sections:
        for index, value in list(reversed(list(enumerate(committee_sections))))[:-1]:
            if df.columns.get_loc(committee_sections[index-1]) == df.columns.get_loc(value)-1:
                del committee_sections[index]

    # Create a dictionary with committee sections as keys and a list of booleans
    # as values indicating whether a row is null or contains Yes/yes
    committeeDictWithBoolValueKeys = {}
    if first_section and len(first_section) == 1:
        for col in df.columns.tolist()[:df.columns.get_loc(first_section[0])]:
            boolist = df[col].astype(str).str.contains(r'yes', flags=re.IGNORECASE, regex=True)
            if any(boolist):
                committeeDictWithBoolValueKeys[col] = boolist.tolist()
    elif committee_sections:
        for col in df.columns.tolist()[:df.columns.get_loc(committee_sections[0])]:
            boolist = df[col].astype(str).str.contains(r'yes', flags=re.IGNORECASE, regex=True)
            if any(boolist):
                committeeDictWithBoolValueKeys[col] = boolist.tolist()
    else:
        logging.warning('\nIt is not clear what column begins the first section of the survey.\nThe first column of the Board Excellence Assessment must contain "I."\nCommittee Sections Must also contain "I." and contain the word "Committee\nRevise the input file to try again."')
        return 1

    if committee_sections:
        committee_names_and_start_col = []
        for committee_start_column in committee_sections:
            # Here, we match committee indicator columns to the first column of a committee
            # section. The idea being, if a committee indicator column (e.g. "Audit") contains
            # values "Yes" or "yes", then the positions of those yes values will match non null
            # positions in the first column of a committee section.
            # Also, by deleting keys as they are discovered, we allow the possibility that
            # committees contain the same members, and as long as the committee survey data
            # appears in the input file in the same order that the committee indicator columns
            # do, then the committee reports will be labeled correctly

            beginColsBoolList = pd.to_numeric(df.iloc[:,df.columns.get_loc(committee_start_column)], errors='coerce').notnull().tolist()

            for k in list(committeeDictWithBoolValueKeys.keys()):
                if committeeDictWithBoolValueKeys[k] == beginColsBoolList:
                    committee_names_and_start_col.append((k, df.columns.get_loc(committee_start_column)))
                    del committeeDictWithBoolValueKeys[k]
                    break

        if len(committee_sections) > 1:
            for index, values in enumerate(committee_names_and_start_col[:-1]):
                committee_name, commitee_start_col_number = values
                workbook_args.append((committee_name, commitee_start_col_number, committee_names_and_start_col[index+1][1]))
            workbook_args.append((committee_names_and_start_col[-1][0], committee_names_and_start_col[-1][1], len(df.columns)))
        else:
            for committee_name, commitee_start_col_number in committee_names_and_start_col:
                workbook_args.append((committee_name, commitee_start_col_number, len(df.columns)-1))

    if first_section:
        # print('here', first_section)
        if not committee_sections:
            workbook_args.append(('Peer', df.columns.get_loc(first_section[0]), len(df.columns)))
        else:
            start_cols = []
            for committee in committee_sections:
                start_cols.append(df.columns.get_loc(committee))
            workbook_args.append(('Peer', df.columns.get_loc(first_section[0]), min(start_cols)))

    # print(workbook_args)

    # !!! NOTE !!!
    # There exists the possibility that if two committees contain the same members,
    # and the committee sections at the end of survey are in an order different from the
    # order that they appear in the meta data section, then the
    # committee report will be labeled incorrectly.

    # Additionally, if a member is present in a committee but does not take part in the
    # committee survey, that committee indicator column will not match the first column
    # of the committee section. Deletion of the yes row in the committee indicator column
    # will fix this.

    # Here, we split the dataframe into sections by column and add them to dict_of_raw_bea_and_committee_data
    # What trimmed_df.replace(r'^\s*$', np.nan, regex=True) allows us is removing any
    # amount of white space. Including the string: "''" from columns that should be int type

    dict_of_raw_bea_and_committee_data = {}
    for arg in workbook_args:
        # print('arg', arg)
        # print(arg)
        trimmed_df = df.copy()
        trimmed_df = trimmed_df.iloc[:,arg[1]:arg[2]]
        trimmed_df.set_index(df['UniqueIdentifier'], inplace= True)
        trimmed_df = trimmed_df.replace(r'^\s*$', np.nan, regex=True)
        trimmed_df = trimmed_df.dropna(how='all', axis=0)
        ########################
        # Debugging
        # if 'XYZ' in file_name:
            # print(trimmed_df.iloc[:,2])
            # print(trimmed_df.shape)
            # print('firstrow: ', trimmed_df.iloc[0,0], trimmed_df.iloc[0,1])
            # print(type(trimmed_df.iloc[0,:0]), type(trimmed_df.iloc[1,:0]))
            # print(isinstance(trimmed_df.iloc[0,0], str))
            # print(trimmed_df.columns[0])
        ########################
        dict_of_raw_bea_and_committee_data[arg[0]] = trimmed_df

    first_section_col_num = min(workbook_args, key = lambda t: t[1])[1]
    # print('first col num', first_section_col_num)
    dict_of_raw_bea_and_committee_data['meta'] = df.iloc[:,:first_section_col_num]

    # print(dict_of_raw_bea_and_committee_data, '\n\nreturn\n\n')

    return dict_of_raw_bea_and_committee_data
