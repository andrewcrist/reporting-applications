import pandas as pd
import numpy as np
import os
import glob
import re
from random import shuffle
from collections import Counter
import warnings
import xlsxwriter
from functools import reduce
from statistics import mean
import math
import openpyxl
import sys
import itertools
import difflib
import logging
import time
from pandas import ExcelWriter
from decimal import Decimal, ROUND_HALF_UP, ROUND_HALF_EVEN
from itertools import cycle
from fuzzywuzzy import fuzz

def peer_splitter(file_name, raw_data_dict):

    # print('\n', file_name)
    df = pd.DataFrame.from_dict(raw_data_dict)

    columns = list(df.columns)
    columns = [re.sub(r'\xa0', ' ', c) for c in columns]
    df.columns = columns

    workbook_args = []

    # Skill matrices
    skills_matrix = re.compile('.*skills\s+matrix.*', re.IGNORECASE)
    skills_matrices = list(filter(skills_matrix.match, columns))

    # Attribute matrices
    attribute_matrix = re.compile('.*attributes\s+matrix.*', re.IGNORECASE)
    attribute_matrices = list(filter(attribute_matrix.match, columns))

    # Start columns for BEA and committee sections
    # Changed ('(?!.*\([Cc]ontinued\))(?!.*[Cc]ommittee)(?=^I\.|.*[\s\t]+I\.)')
    # to ('(?!.*\([Cc]ontinued\))(?=^I\.|.*[\s\t]+I\.)')
    re_first_section = re.compile('(?!.*\([Cc]ontinued\))(?=^I\.|.*[\s\t]+I\.)')
    re_committee_sections = re.compile('(?!.*\([Cc]ontinued\))(?!.*:_[^a]\.)(?!.*:.*_[^a]\.)(?=.*[Cc]ommittee)(?=^I\.|.*[\s\t]+I\.)')

    first_section = list(filter(re_first_section.match, columns))
    committee_sections = list(filter(re_committee_sections.match, columns))
    # For now, override the committe section filter
    committee_sections = []

    # print(first_section)

    # Often, section headers will duplicate the name of the section across
    # multiple questions. We want to iterate backward and remove column numbers
    # from first_section and committee_sections where this duplication occurs
    # This way, there is only one value for the start column for BEA and committee reports
    if first_section:
        if len(first_section) > 1:
            column_numbers = [df.columns.get_loc(x) for x in first_section]
            first_section = [df.columns[min(column_numbers)]]
    # if 'aatrim' in file_name:
    #     print('\nfirst sections: ', first_section)

    # Iterate backwards. If the column count is one higher than the column count
    # of the value that precedes it, delete the current column from the list of
    # potential start columns.
    if committee_sections:
        for index, value in list(reversed(list(enumerate(committee_sections))))[:-1]:
            if df.columns.get_loc(committee_sections[index-1]) == df.columns.get_loc(value)-1:
                del committee_sections[index]

    # Create a dictionary with committee sections as keys and a list of booleans
    # as values indicating whether a row is null or contains Yes/yes
    committeeDictWithBoolValueKeys = {}
    if first_section and len(first_section) == 1:
        for col in df.columns.tolist()[:df.columns.get_loc(first_section[0])]:
            boolist = df[col].astype(str).str.contains(r'yes', flags=re.IGNORECASE, regex=True)
            if any(boolist):
                committeeDictWithBoolValueKeys[col] = boolist.tolist()
    elif committee_sections:
        for col in df.columns.tolist()[:df.columns.get_loc(committee_sections[0])]:
            boolist = df[col].astype(str).str.contains(r'yes', flags=re.IGNORECASE, regex=True)
            if any(boolist):
                committeeDictWithBoolValueKeys[col] = boolist.tolist()
    else:
        logging.warning('\nIt is not clear what column begins the first section of the survey.\nThe first column of the Board Excellence Assessment must contain "I."\nCommittee Sections Must also contain "I." and contain the word "Committee\nRevise the input file to try again."')
        return 1

    if committee_sections:
        committee_names_and_start_col = []
        for committee_start_column in committee_sections:
            # Here, we match committee indicator columns to the first column of a committee
            # section. The idea being, if a committee indicator column (e.g. "Audit") contains
            # values "Yes" or "yes", then the positions of those yes values will match non null
            # positions in the first column of a committee section.
            # Also, by deleting keys as they are discovered, we allow the possibility that
            # committees contain the same members, and as long as the committee survey data
            # appears in the input file in the same order that the committee indicator columns
            # do, then the committee reports will be labeled correctly

            beginColsBoolList = pd.to_numeric(df.iloc[:,df.columns.get_loc(committee_start_column)], errors='coerce').notnull().tolist()

            for k in list(committeeDictWithBoolValueKeys.keys()):
                if committeeDictWithBoolValueKeys[k] == beginColsBoolList:
                    committee_names_and_start_col.append((k, df.columns.get_loc(committee_start_column)))
                    del committeeDictWithBoolValueKeys[k]
                    break

        if len(committee_sections) > 1:
            for index, values in enumerate(committee_names_and_start_col[:-1]):
                committee_name, commitee_start_col_number = values
                workbook_args.append((committee_name, commitee_start_col_number, committee_names_and_start_col[index+1][1]))
            workbook_args.append((committee_names_and_start_col[-1][0], committee_names_and_start_col[-1][1], len(df.columns)))
        else:
            for committee_name, commitee_start_col_number in committee_names_and_start_col:
                workbook_args.append((committee_name, commitee_start_col_number, len(df.columns)-1))

    if first_section:
        # print('here', first_section)
        if not committee_sections:
            workbook_args.append(('Peer', df.columns.get_loc(first_section[0]), len(df.columns)))
        else:
            start_cols = []
            for committee in committee_sections:
                start_cols.append(df.columns.get_loc(committee))
            workbook_args.append(('Peer', df.columns.get_loc(first_section[0]), min(start_cols)))

    # print(workbook_args)

    # !!! NOTE !!!
    # There exists the possibility that if two committees contain the same members,
    # and the committee sections at the end of survey are in an order different from the
    # order that they appear in the meta data section, then the
    # committee report will be labeled incorrectly.

    # Additionally, if a member is present in a committee but does not take part in the
    # committee survey, that committee indicator column will not match the first column
    # of the committee section. Deletion of the yes row in the committee indicator column
    # will fix this.

    # Here, we split the dataframe into sections by column and add them to dict_of_raw_bea_and_committee_data
    # What trimmed_df.replace(r'^\s*$', np.nan, regex=True) allows us is removing any
    # amount of white space. Including the string: "''" from columns that should be int type

    dict_of_raw_bea_and_committee_data = {}
    for arg in workbook_args:
        # print('arg', arg)
        # print(arg)
        trimmed_df = df.copy()
        trimmed_df = trimmed_df.iloc[:,arg[1]:arg[2]]
        trimmed_df.set_index(df['UniqueIdentifier'], inplace= True)
        trimmed_df = trimmed_df.replace(r'^\s*$', np.nan, regex=True)
        trimmed_df = trimmed_df.dropna(how='all', axis=0)
        ########################
        # Debugging
        # if 'XYZ' in file_name:
            # print(trimmed_df.iloc[:,2])
            # print(trimmed_df.shape)
            # print('firstrow: ', trimmed_df.iloc[0,0], trimmed_df.iloc[0,1])
            # print(type(trimmed_df.iloc[0,:0]), type(trimmed_df.iloc[1,:0]))
            # print(isinstance(trimmed_df.iloc[0,0], str))
            # print(trimmed_df.columns[0])
        ########################
        dict_of_raw_bea_and_committee_data[arg[0]] = trimmed_df

    first_section_col_num = min(workbook_args, key = lambda t: t[1])[1]
    # print('first col num', first_section_col_num)
    dict_of_raw_bea_and_committee_data['meta'] = df.iloc[:,:first_section_col_num]

    # print(dict_of_raw_bea_and_committee_data, '\n\nreturn\n\n')

    return dict_of_raw_bea_and_committee_data
