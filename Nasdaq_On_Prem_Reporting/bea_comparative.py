from jinja2 import Template, Environment, PackageLoader, select_autoescape, FileSystemLoader
import datetime
import win32com.client as win32
from PIL import ImageGrab
import os
import time
import sys
import glob
import pandas as pd
from weasyprint import HTML, CSS
from decimal import Decimal, ROUND_HALF_UP
import pandas as pd
import numpy as np
from collections import Counter, OrderedDict
import re
import warnings
from random import shuffle

# sys.path.insert(1, r'C:\Users\Drew\Nasdaq_Classes_and_Fns')
# from ppl_parser import ppl_parser
# from csv_opener import csv_opener
#
# from bea_etl import bea_splitter, bea_etl
# from bea_report import bea_report

from library.ppl_parser import ppl_parser
from library.csv_opener import csv_opener

from library.bea_etl import bea_splitter, bea_etl
from library.bea_report import bea_report

def ask_create_section(report_type, section):
    check = str(input("Would you like to create a {} for report type: {}? (y/n): ".format(section, report_type))).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_create_section(report_type, section)
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_create_section(report_type, section)

def bea_comparative_templater(file_name, member_variable_query, report_type, df, questions_df, ppl_df, swot=True, highlights_actionpoints=True, randomize = True, number_of_comparisons=1, relationship_web=True):
    if report_type != 'BEA':
        swot = False
        highlights_actionpoints = False
        relationship_web = False
        if report_type == 'NomGov':
            report_title = 'Nominating and Governance Committee Report'
            intro_title = 'Nominating and Governance Committee'
        else:
            report_title = '{} Committee Report'.format(report_type)
            intro_title = '{} Committee'.format(report_type)
        board_or_committee = 'committee'

    if report_type == 'BEA':
        relationship_web = ask_create_web()
        highlights_actionpoints = ask_create_highlights_actionpoints()
        swot = ask_create_swot()
        report_title = 'Board Evaluation Report'
        intro_title = 'Board of Directors'
        board_or_committee = 'Board'

        # relationship_web = True
        # highlights_actionpoints = True
        # swot = True
    else:
        relationship_web = False

    CGPG = ask_create_cgpg(report_type)
    hilo_section_bool = ask_create_section(report_type, 'Hi-Lo Section')
    show_GCFR_method = ask_show_GCFR_method(report_type)
    show_flag_legend = ask_show_flag_legend(report_type)
    question_column_width = set_question_column_width(report_type)

    # CGPG = True
    # hilo_section_bool = True
    # show_GCFR_method = True
    # show_flag_legend = True
    # question_column_width = 35

    now = datetime.datetime.now()
    formatted_date = str(now.strftime("%B")) + ' ' + str(now.day) + ', ' + str(now.year)


    file_loader =FileSystemLoader('templates')
    env = Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)
    template = env.get_template('comparative_bea_report_template.html')

    skills_matrix_present = 'skills_matrix' in questions_df['type'].unique()

    jinja_input_dict = {'title': 'Nasdaq BEA Comparative Evaluation', 'nasdaq_logo': r'images/nasdaq_logo.png', 'CGPG': CGPG, 'report_title': report_title,
         'year': str(now.year), 'formatted_date': formatted_date, 'skills_matrix_present': skills_matrix_present,
         'questions_df': questions_df, 'swot': swot, 'highlights_actionpoints': highlights_actionpoints, 'number_of_comparisons': number_of_comparisons,
         'swot_s': r'images/swot_s.png', 'swot_w': r'images/swot_w.png', 'swot_o': r'images/swot_o.png', 'swot_t': r'images/swot_t.png', 'relationship_web': relationship_web,
         'scale_image': r'images/scale.png', 'green_flag': r'images/green_flag.png', 'yellow_flag': r'images/yellow_flag.png', 'red_flag': r'images/red_flag.png',
         'question_column_width': question_column_width, 'show_GCFR_method': show_GCFR_method, 'show_flag_legend': show_flag_legend, 'intro_title': intro_title,
         'board_or_committee': board_or_committee, 'hilo_section_bool': hilo_section_bool}

    if CGPG:
        jinja_input_dict['cgpg_bar_chart'] = r'charts\cgpg_bar_chart.png'
        jinja_input_dict['compared_cgpg_bar_charts'] = []
        for year in member_variable_query['compared_years']:
            jinja_input_dict['compared_cgpg_bar_charts'].append((year, r'charts\{}_cgpg_bar_chart.png'.format(year)))
    if relationship_web:
        jinja_input_dict['radar_chart_image'] = r'charts\radar_chart.png'


    # Get Assessment sections labels and section names
    section_names_and_labels = {}
    label_re = re.compile(r'^[\s\t]*([IVX]+\.)[\s\t]*(.*)')
    for section in questions_df['section'].unique().tolist():
        label = label_re.match(section)
        section_names_and_labels[label.group(1)] = label.group(2)
    jinja_input_dict['section_names_and_labels'] = section_names_and_labels

    # Control which sections are in the table of contents
    # jinja_input_dict['table_of_contents_titles'] = ['#heat_map_title', '#CGPG_title', '#hilo_title', '#CGPG_method_title', '#CG_flag_title', '#statistical_analysis_title']
    #
    # optional_sections = []
    # if member_variable_query.get('skills_matrix_average') is not None:
    #     optional_sections.append('#skills_matrix_title')
    # if relationship_web:
    #     optional_sections.append('#radar_title')
    # if swot:
    #     optional_sections.append('#swot_title')
    # if highlights_actionpoints:
    #     optional_sections.append('#highlights_actionpts_title')
    #
    # if optional_sections:
    #     index = 2
    #     for optional_section in optional_sections:
    #         if optional_section == '#radar_title':
    #             jinja_input_dict['table_of_contents_titles'].insert(2, optional_section)
    #         else:
    #             jinja_input_dict['table_of_contents_titles'].insert(index, optional_section)
    #             index+=1

    jinja_input_dict['table_of_contents_titles'] = ['#heat_map_title']
    if CGPG:
        jinja_input_dict['table_of_contents_titles'].append('#CGPG_title')
    if relationship_web:
        jinja_input_dict['table_of_contents_titles'].append('#radar_title')
    jinja_input_dict['table_of_contents_titles'].append('#hilo_title')
    if member_variable_query.get('skills_matrix_average') is not None:
        jinja_input_dict['table_of_contents_titles'].append('#skills_matrix_title')
    if swot:
        jinja_input_dict['table_of_contents_titles'].append('#swot_title')
    if highlights_actionpoints:
        jinja_input_dict['table_of_contents_titles'].append('#highlights_actionpts_title')
    jinja_input_dict['table_of_contents_titles'].append('#CGPG_method_title')
    jinja_input_dict['table_of_contents_titles'].append('#CG_flag_title')
    jinja_input_dict['table_of_contents_titles'].append('#statistical_analysis_title')


    # print("jinja_input_dict['table_of_contents_titles']: ", jinja_input_dict['table_of_contents_titles'])

    client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file_name)
    jinja_input_dict['client_name'] = client_name.group(1)

    jinja_input_dict['number_of_sections'] = len(questions_df['section'].unique().tolist())
    jinja_input_dict['number_of_long_form_questions'] = len(questions_df[questions_df['type']=='long_form'])
    jinja_input_dict['number_of_rated_questions'] = len(questions_df['question_raw']) - len(questions_df[questions_df['type']=='long_form']) - len(questions_df[questions_df['type']=='skill_matrix']) - len(questions_df[questions_df['type']=='attribute_matrix'])


    jinja_input_dict['report_type'] = report_type

    for item in member_variable_query.keys():
        jinja_input_dict['{}'.format(item)] = member_variable_query[item]

    output = template.render(jinja_input_dict)
    # with open("test.html", "w") as fh:
    #     fh.write(output)

    # print(member_variable_query['data_dict'])
    HTML(string=output, base_url='.').write_pdf('output\\{}_{}.pdf'.format(client_name.group(1), report_type), stylesheets=[CSS(r'static\bea_comparative_template_style.css')])

def ask_create_cgpg(report_type):
    check = str(input("Would you like to create a corporate governance priority graph for report type: {}? (y/n): ".format(report_type))).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_create_cgpg(report_type)
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_create_cgpg(report_type)

def ask_show_GCFR_method(report_type):
    check = str(input("Would you like to display the Corporate Goverance Flag Report methodology section for report type: {}? (y/n): ".format(report_type))).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_show_GCFR_method(report_type)
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_show_GCFR_method(report_type)

def ask_show_flag_legend(report_type):
    check = str(input("Would you like to display the Corporate Goverance Flag Report legend for report type: {}? (y/n): ".format(report_type))).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_show_flag_legend(report_type)
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_show_flag_legend(report_type)

def ask_create_web():
    check = str(input("\n\nWould you like to create a corporate relationship web graph? (y/n): ")).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_create_web()
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_create_web()

        set_question_column_width

def set_question_column_width(report_type):
    check = str(input("Would you like to set a column width for report type: {}? (y/n): ".format(report_type))).lower().strip()
    try:
        if check[0] == 'y':
            percentage = int(input('> '))
            if len(str(percentage)) == 2:
                return percentage
            else:
                print('Invalid Input')
                return set_question_column_width(report_type)
        elif check[0] == 'n':
            return 35
        else:
            print('Invalid Input')
            return set_question_column_width(report_type)
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return set_question_column_width(report_type)

def ask_create_highlights_actionpoints():
    check = str(input("Would you like to create a highlights and actionpoints page? (y/n): ")).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_create_highlights_actionpoints()
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_create_highlights_actionpoints()

def ask_create_swot():
    check = str(input("Would you like to create a swot page? (y/n): ")).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_create_swot()
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_create_swot()

def ask_use_questionsdf_in_directory(year, report_type):
    check = str(input("Would you like to use {}_{}_questions_df file in the project directory? (y/n): ".format(year, report_type))).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_use_questionsdf_in_directory(year, report_type)
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_use_questionsdf_in_directory(year, report_type)

def join_qdf_and_rawdata_into_smoothed_df(qdf, tdf, file, report_type, anonymize):

    ############################################################################
    # ETL
    df = pd.DataFrame(columns=['client_name', 'report_type', 'section', 'number', 'question_raw', 'question', 'sub_section', 'type', 'average', 'response', 'respondent_id', 'question_mean'])
    try:
        client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file).group(1)
    except:
        client_name = None

    if anonymize ==True:
        director_encoder = dict()
        director_count = 1

    for s in qdf['section'].unique():
        for q in qdf[qdf['section']==s]['question_raw']:
            for i in tdf[q].index:
                # if (tdf.loc[i,q]==np.nan or (tdf.loc[i,q]=='nan')):
                #     print('here!', i, q)
                #     response = None
                # else:
                #     response = tdf.loc[i,q]
                if anonymize == False:
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item(), 'response': tdf.loc[i,q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}
                else:
                    sub_q = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item()
                    if sub_q not in ['', '!__!']:
                        if director_encoder.get(sub_q) is None:
                            director_encoder[sub_q] = 'Director {}'.format(director_count)
                            sub_q = 'Director {}'.format(director_count)
                            director_count += 1
                        else:
                            sub_q = director_encoder.get(sub_q)
                    else:
                        pass
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section': sub_q, 'response': tdf.loc[i,q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}

                df = df.append(row, ignore_index=True)

    question_means = []
    for section in df['section'].unique():
        for number in df[(df['section']==section)]['number'].unique():
            try:
                mean_by_question = df[(df['section']==section) & (df['number']==number)]['response'].mean()
                mean_by_question = "{0:.2f}%".format(Decimal('{}'.format(mean_by_question)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                mean_by_question = None
            mean_column_addition = [mean_by_question]*len(df[(df['section']==section) & (df['number']==number)]['response'])
            question_means.extend(mean_column_addition)

    df['question_mean'] = question_means
    df.to_excel('benchmark_data\\SMOOTHED_{}_{}.xlsx'.format(report_type, client_name))
    return df

def ask_randomize_report(report_type):
    check = str(input("Would you like to randomize report: {}? (y/n): ".format(report_type))).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_randomize_report()
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_randomize_report(report_type)

def create_comparative_bea_jinja_dict(sorted_years):
    copied_dict = sorted_years.copy()
    comparative_year = copied_dict.pop(max(copied_dict.keys()), None)
    compared_years = copied_dict

    number_of_comparisons = len(list(copied_dict.keys()))
    print("Number of comparisons: ", number_of_comparisons)
    # print(sorted_years.get(max(sorted_years.keys())).keys())
    # for key in sorted_years.get(max(sorted_years.keys())).keys():
    #     print('\n\n', key, '\n\n', sorted_years.get(max(sorted_years.keys())).get(key))
    # print(sorted_years.get(max(sorted_years.keys())).get('BEA'))

    reports = dict()
    for report_type in comparative_year.get('report_types'):
        print('\nReport Type {} found in comparative year'.format(report_type))
        if report_type != 'meta':
            rankFlag = False
            randomize = ask_randomize_report(report_type)
            # randomize = False

            # populate report_types_to_compare with similar report types of compared_years
            report_types_to_compare = []
            year_numbers_compared = []
            for year in compared_years:
                try:
                    report_types_to_compare.append(compared_years[year]['report_types'][report_type])
                    year_numbers_compared.append(year)
                    print('found also in year: ', year)
                    # print(year, report_type, compared_years[year]['report_types'][report_type]['ETLd_df'].columns)
                except:
                    print('not found in year: ', year)
                    pass
            # print('\nMatching report types found: ', report_types_to_compare.keys())
            data_dict = dict()
            qdf = comparative_year['report_types'][report_type].get('questions_df')
            tdf = comparative_year['report_types'][report_type].get('ETLd_df')

            for s in qdf['section'].unique():
                # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
                #     print(qdf[qdf['section']==s])
                #     print(qdf[qdf['section']==s]['type'].unique().tolist())
                if qdf[qdf['section']==s]['type'].unique().tolist() == ['skills_matrix']:
                    continue
                data_dict[s] = {}
                for q in qdf[qdf['section']==s]['question_raw']:
                    data_dict[s][q] = {}
                    data_dict[s][q]['type'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item()
                    data_dict[s][q]['number'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item()
                    data_dict[s][q]['question'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item()
                    data_dict[s][q]['sub_section'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item()
                    if qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['matrix', 'likert']:
                        rankFlag = False
                        answers = tdf[q].replace({np.nan: None})
                        answers.fillna('N/A', inplace=True)
                        answers = answers.tolist()
                        answers = [int(x) if x != 'N/A' else x for x in answers]
                        if randomize:
                            shuffle(answers)
                        data_dict[s][q]['answers'] = answers
                        data_dict[s][q]['average'] = str(Decimal(np.mean(tdf[q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP))

                        if report_types_to_compare:
                            try:
                                comp = report_types_to_compare[0]
                                # print('\n\n', q)
                                # print([r.get('ETLd_df')[q] for r in report_types_to_compare])
                                data_dict[s][q]['previous_averages'] = [str(Decimal(np.mean(r.get('ETLd_df')[q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP)) for r in report_types_to_compare]
                            except:
                                data_dict[s][q]['previous_averages'] = 'N/A'
                            # print([str(Decimal(np.mean(r.get('ETLd_dfs')[q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP)) for r in report_types_to_compare])
                            # print(q in list(comp.get('ETLd_df').columns))
                    elif qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['long_form', 'optional_long_form']:
                        rankFlag = False
                        answers = tdf[q].dropna().tolist()
                        answers = [str(a) for a in answers if a.lower() not in ['nan', '#name?', None,  'no comment', 'none', np.nan, '']]
                        if not answers:
                            answers = [None]
                        if randomize:
                            shuffle(answers)
                        data_dict[s][q]['answers'] = answers
                    elif qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['rank']:
                        print(qdf[(qdf['section']==s)&(qdf['question_raw']==q)])
                        if rankFlag == False:
                            rankFlag = True
                            count = 1
                            data_dict[s][q]['count'] = count
                            lengthOfRankedChoiceSection = len(qdf[(qdf['section']==s) & (qdf['number']==data_dict[s][q]['number'])])
                            data_dict[s][q]['lengthOfRankedChoiceSection'] = lengthOfRankedChoiceSection
                            data_dict[s][q]['ranks'] = list(np.arange(1, lengthOfRankedChoiceSection+1, 1))
                            data_dict[s][q]['first'] = True

                            dictonaryOfCounts = dict(Counter(tdf[q].tolist()))
                            listOfOrderedCounts = list()
                            for value in np.arange(1, lengthOfRankedChoiceSection+1, 1):
                                listOfOrderedCounts.append(dictonaryOfCounts.get(value, 0))
                            data_dict[s][q]['answers'] = listOfOrderedCounts
                        else:
                            rankFlag = True
                            count += 1
                            data_dict[s][q]['count'] = count
                            data_dict[s][q]['first'] = False

                            dictonaryOfCounts = dict(Counter(tdf[q].tolist()))
                            listOfOrderedCounts = list()
                            for value in np.arange(1, lengthOfRankedChoiceSection+1, 1):
                                listOfOrderedCounts.append(dictonaryOfCounts.get(value, 0))
                            data_dict[s][q]['answers'] = listOfOrderedCounts

                    elif qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['yes/no']:
                        answers = tdf[q].dropna().tolist()
                        yesNoCheck = [answer.lower() for answer in answers]
                        yesNoCheck = dict(Counter(yesNoCheck))
                        data_dict[s][q]['answers'] = yesNoCheck

            reports[report_type] = {'data_dict': data_dict}
            reports[report_type]['number_of_comparisons'] = len(report_types_to_compare)
            reports[report_type]['compared_years'] = year_numbers_compared
            reports[report_type]['comparative_year'] = max(sorted_years.keys())
            reports[report_type]['randomized'] = randomize
            # print('compared_years: ', compared_years.keys())

    # count = 0
    # for s in data_dict:
    #     print('\n\nSection Name:  ', s)
    #     for q in data_dict[s]:
    #         print('Raw Question:  ', q)
    #         try:
    #             print('Question Average:  ', data_dict[s][q]['average'])
    #         except:
    #             pass
    #         print('Number: ', data_dict[s][q]['number'])
    #         print('Type:  ', data_dict[s][q]['type'])
    #         print('Question: ', data_dict[s][q]['question'])
    #         print('Sub:  ', data_dict[s][q]['sub_section'])
    #         print('Answers:  ', data_dict[s][q]['answers'])
    #         print('\n')
            # count+=1
            # if count == 4:
            #     break
    return reports

if __name__ == "__main__":
    start_time = time.time()
    print(sys.version)
    print('\n------------------------------------------\n')

    cwd = os.getcwd()

    extension = 'csv'
    files = glob.glob("*.{}".format(extension))
    years = {}
    for f in files:
        year = f[-8:-4]
        years[int(year)] = {'file': f}

    sorted_years = OrderedDict(sorted(years.items(), reverse=True))
    print('sorted_years: ', sorted_years)

    current_year = max(sorted_years.keys())

    current_year_file = sorted_years[current_year].get('file')

    for year in sorted_years.keys():
        sorted_years[year]['raw_data_dict'] = csv_opener(sorted_years.get(year).get('file'))

    for year in sorted_years.keys():
        sorted_years[year]['dict_of_dfs'] = bea_splitter(sorted_years.get(year).get('file'), sorted_years.get(year).get('raw_data_dict'))
        # Check to see if a proper split has been made across years and report types
        # print('\n\n', sorted_years[year]['dict_of_dfs'].keys())
    for year in sorted_years.keys():
        sorted_years[year]['ppl_df'] = ppl_parser(sorted_years.get(year).get('dict_of_dfs').get('meta'))

    for year in sorted_years.keys():
        # print('\n\nyear: ', year)
        for year2 in sorted_years.keys():
            # print('year2: ', year2)
            if set(sorted_years[year]['ppl_df'].index) != set(sorted_years[year2]['ppl_df'].index):
                warnings.warn('UniqueIdentifier columns do not match!')


    for year in sorted_years.keys():
        # print(year)
        sorted_years[year]['report_types'] = dict()
        for report_type in sorted_years.get(year).get('dict_of_dfs'):
            # print(report_type)
            if report_type != 'meta':
                sorted_years[year]['report_types'][report_type] = dict()
                sorted_years[year]['report_types'][report_type]['not_app_or_obs'] = {}
                sorted_years[year]['report_types'][report_type]['ETLd_df'] = {}
                sorted_years[year]['report_types'][report_type]['data_dict'] = {}
                sorted_years[year]['report_types'][report_type]['smoothed_df'] = {}

                report_to_run = sorted_years[year]['dict_of_dfs'].get(report_type)

                questions_df, transformed_df, na_no_null_dict = bea_etl(report_to_run, sorted_years.get(year).get('ppl_df'), report_type, sorted_years.get(year).get('file'), True, year=year)

                # response = ask_use_questionsdf_in_directory(year, report_type)
                response = True

                if response:
                    if os.path.isfile('{}_{}_questions_df.xlsx'.format(year, report_type)):
                        q_df = pd.read_excel('{}_{}_questions_df.xlsx'.format(year, report_type), index=False, converters={'number': str})
                        sorted_years[year]['report_types'][report_type]['questions_df'] = q_df
                        questions_df = q_df
                    else:
                        print('\n\nAborting... Question data frame file "{}_questions_df.xlsx" not found in directory\n'.format(report_type))
                        sys.exit()
                else:
                    sorted_years[year]['report_types'][report_type]['questions_df'] = questions_df

                sorted_years[year]['report_types'][report_type]['ETLd_df'] = transformed_df
                sorted_years[year]['report_types'][report_type]['na_no_null_dict'] = na_no_null_dict

                # jinja_data_dict = create_comparative_bea_jinja_dict(questions_df, transformed_df)
                # sorted_years[year][report_type]['data_dict'] = jinja_data_dict
                # print(jinja_data_dict)

    # print('sorted_years BEFORE: ', sorted_years.keys())
    reports = create_comparative_bea_jinja_dict(sorted_years)
    # print('sorted_years AFTER: ', sorted_years.keys())
    # randomize_bea_bool = True

    if set(questions_df['type']).issubset(set(['skills_matrix', ''])):
        skills_response = True
    else:
        skills_response = False

    # for k, v in reports.items():
    #     print('\n\n', k)
    #     print(reports[k])

    # print(sorted_years)

    comparative_year_number = max(sorted_years.keys())
    comparative_year = sorted_years.pop(max(sorted_years.keys()), None)

    # the reports dictionary has already married the appropriate committee reports
    # to the comparative year.
    for report_type in reports:
        # print('report_type: ', report_type)

        member_variable_query = dict()

        randomize_bea_bool = reports[report_type]['randomized']
        list_of_compared_bea_objects = []

        print('\nCreating {} report for {}'.format(report_type, comparative_year.get('file')))

        report_to_run = comparative_year['report_types'].get(report_type).get('ETLd_df')
        questions_df = comparative_year['report_types'].get(report_type).get('questions_df')
        BEA = bea_report(comparative_year.get('file'), report_type, report_to_run, questions_df, comparative_year.get('ppl_df'), comparative_year['report_types'].get(report_type).get('na_no_null_dict'), randomize = randomize_bea_bool)
        member_variable_query['compared_heat_map_export'] = BEA.heat_map_export
        member_variable_query['compared_hm_section_length_counts'] = BEA.hm_section_length_counts
        member_variable_query['compared_hm_numbers_borders'] = BEA.hm_numbers_borders
        member_variable_query['compared_heat_map_index'] = BEA.heat_map_index
        member_variable_query['compared_heat_map_averages'] = BEA.heat_map_averages
        member_variable_query['compared_hm_values_borders'] = BEA.hm_values_borders
        member_variable_query['compared_hm_rows_directions'] = BEA.hm_rows_directions
        member_variable_query['compared_hm_pixel_size_list'] = BEA.hm_pixel_size_list
        member_variable_query['compared_percentage_size'] = BEA.percentage_size

        member_variable_query['header_font_size'] = 11
        if BEA.percentage_size<13 and BEA.percentage_size>10:
            member_variable_query['header_font_size'] = 10
        elif BEA.percentage_size<=10:
            member_variable_query['header_font_size'] = 9

        member_variable_query['number_font_size'] = 10
        if BEA.percentage_size<13 and BEA.percentage_size>10:
            member_variable_query['number_font_size'] = 8
        elif BEA.percentage_size<=10:
            member_variable_query['number_font_size'] = 7

        member_variable_query['hm_cell_height'] = hm_cell_height = 10
        if BEA.number_of_respondents < 16 and BEA.number_of_respondents >= 10:
            hm_cell_height = 7
        elif BEA.number_of_respondents < 20 and BEA.number_of_respondents >= 16:
            hm_cell_height = 5
        elif BEA.number_of_respondents >= 20:
            hm_cell_height = 4
        member_variable_query['hm_cell_height'] = hm_cell_height

        try:
            member_variable_query['skills_matrix_df'] = BEA.skills_matrix_export
        except:
            pass


        esigmas = []
        for year in sorted_years:
            try:
                compared_year_report_to_run = sorted_years[year]['report_types'][report_type]['ETLd_df']
                compared_year_questions_df = sorted_years[year]['report_types'][report_type]['questions_df']

                b = bea_report(sorted_years.get(year).get('file'), report_type, compared_year_report_to_run, compared_year_questions_df, sorted_years.get(year).get('ppl_df'), sorted_years[year]['report_types'].get(report_type).get('na_no_null_dict'), randomize = randomize_bea_bool, year=year, comparative=False)
                member_variable_query['comparative_heat_map_export'] = b.heat_map_export
                member_variable_query['comparative_hm_section_length_counts'] = b.hm_section_length_counts
                member_variable_query['comparative_hm_numbers_borders'] = b.hm_numbers_borders
                member_variable_query['comparative_heat_map_index'] = b.heat_map_index
                member_variable_query['comparative_heat_map_averages'] = b.heat_map_averages
                member_variable_query['comparative_hm_values_borders'] = b.hm_values_borders
                member_variable_query['comparative_hm_rows_directions'] = b.hm_rows_directions
                member_variable_query['comparative_hm_pixel_size_list'] = b.hm_pixel_size_list
                member_variable_query['comparative_percentage_size'] = b.percentage_size

                list_of_compared_bea_objects.append(b)

                esigmas.append({year: b.eSigma_value})

                # print('\OK...')
                # print(b.hm_values_borders)
                # print(BEA.hm_values_borders)
                # print(len(b.hm_pixel_size_list), b.hm_pixel_size_list)
                # print(len(BEA.hm_pixel_size_list), BEA.hm_pixel_size_list)
                #
                #
                # print('\n')
                # print(len(member_variable_query['compared_hm_pixel_size_list']), member_variable_query['compared_hm_pixel_size_list'])
                # print('\n')
                # print(len(member_variable_query['comparative_hm_pixel_size_list']), member_variable_query['comparative_hm_pixel_size_list'])

            except:
                pass

        if not list_of_compared_bea_objects:
            print('\n\nNo applicable report types found for type {}!'.format(report_type))
            continue

        try:
            member_variable_query['skills_matrix_average'] = BEA.skills_matrix_average
            member_variable_query['number_of_skills_matrix_questions'] = BEA.number_of_skills_matrix_questions
            member_variable_query['skills_matrix_df_width'] = np.arange(1, BEA.skills_matrix_export.shape[1] + 1, 1)
        except:
            pass

        member_variable_query['eSigma_value'] = BEA.eSigma_value
        member_variable_query['previous_esigmas'] = esigmas
        member_variable_query['b_esigma'] = b.eSigma_value

        formattedText = 'The average rating for this section is {} ({} - {}). The average rating for each question in the section ranged from {} to {}. Individual ratings ranged from {} to {}.'
        equal_formattedText = 'The average rating for each question and this section is {} ({} - {}). Individual ratings ranged from {} to {}.'
        compared_section_data = dict()
        for section in BEA.averages:
            if BEA.questionAveragesBySectionMinsAndMaxes.get(section).get('max') == BEA.questionAveragesBySectionMinsAndMaxes.get(section).get('min') and (BEA.questionAveragesBySectionMinsAndMaxes.get(section).get('max') == BEA.averages.get(section)):
                compared_section_data[section] = equal_formattedText.format(BEA.averages.get(section), b.averages.get(section), list(sorted_years.keys())[0], BEA.sectionMinsAndMaxes.get(section).get('min'), BEA.sectionMinsAndMaxes.get(section).get('max'))
            else:
                compared_section_data[section] = formattedText.format(BEA.averages.get(section), b.averages.get(section), list(sorted_years.keys())[0],
                    BEA.questionAveragesBySectionMinsAndMaxes.get(section).get('min'),
                    BEA.questionAveragesBySectionMinsAndMaxes.get(section).get('max'),
                    BEA.sectionMinsAndMaxes.get(section).get('min'),
                    BEA.sectionMinsAndMaxes.get(section).get('max'))
        member_variable_query['compared_section_data'] = compared_section_data

        member_variable_query['questions_df'] = BEA.questions_df
        member_variable_query['jinja_for_loop_dict'] = reports[report_type]['data_dict']
        member_variable_query['number_of_respondents'] = BEA.number_of_respondents
        member_variable_query['section_averages'] = BEA.averages
        member_variable_query['five_point_scale'] = BEA.five_point_scale
        # print('Five Point Scale: ', BEA.five_point_scale)

        member_variable_query['jinja_hi'] = BEA.jinja_hi
        member_variable_query['jinja_lo'] = BEA.jinja_lo
        member_variable_query['number_of_comparisons'] = reports[report_type]['number_of_comparisons']

        ##################
        # Old Method. It's tied to eval_stats_list_of_dicts and sometimes
        # there will be 0s in the heatmap that do not count as N/As
        # na_present = False
        compared_year_na_present = False
        comparative_year_na_present = False

        eval_stats_list_of_dicts = BEA.eval_stats_list_of_dicts
        for category in eval_stats_list_of_dicts:
            category_key = list(category.keys())[0]
            if category_key in ['Percent of ratings of N/A', 'Number of ratings of N/A']:
                # na_present = True
                compared_year_na_present = True

        eval_stats_list_of_dicts = b.eval_stats_list_of_dicts
        for category in eval_stats_list_of_dicts:
            category_key = list(category.keys())[0]
            if category_key in ['Percent of ratings of N/A', 'Number of ratings of N/A']:
                # na_present = True
                comparative_year_na_present = True

        # print('na_present: ', na_present)
        # print('comparative_year_na_present: ', comparative_year_na_present)
        # print('compared_year_na_present: ', compared_year_na_present)
        ##################
        if b.na_in_heat_map or BEA.na_in_heat_map:
            member_variable_query['na_in_heat_map'] = True
        else:
            member_variable_query['na_in_heat_map'] = False
        ##################


        # member_variable_query['eval_stats_list_of_dicts'] = BEA.eval_stats_list_of_dicts
        # member_variable_query['eval_stats_list_of_dicts_last_item'] = tuple((list(BEA.eval_stats_list_of_dicts[-1].keys())[0], BEA.eval_stats_list_of_dicts[-1].get(list(BEA.eval_stats_list_of_dicts[-1].keys())[0])))
        eval_stats_list_of_dicts = BEA.eval_stats_list_of_dicts
        new_eval_list_of_dicts = list()
        add = False
        for category in eval_stats_list_of_dicts:
            category_key = list(category.keys())[0]
            if category_key in ['Question analysis by Board member response:', 'Group response analysis:']:
                new_eval_list_of_dicts.append({category_key: ['']*(reports[report_type]['number_of_comparisons']+1)})
                continue
            score = category.get(list(category.keys())[0], '')
            list_of_scores = list()
            list_of_scores.append(score)
            for item in list_of_compared_bea_objects:
                score_list = item.eval_stats_list_of_dicts
                for i in score_list:
                    for k, v in i.items():
                        if k == category_key:
                            list_of_scores.append(i.get(category_key, ''))
                        else:
                            pass
            # If there are n/a in the current year, but not last year's
            if (category_key in ['Percent of ratings of N/A', 'Number of ratings of N/A']) and (len(list_of_scores)==1):
                if category_key == 'Percent of ratings of N/A':
                    list_of_scores.append('0%')
                elif category_key == 'Number of ratings of N/A':
                    list_of_scores.append('0')

            if new_eval_list_of_dicts:
                # If there are n/a in last year but not the current year
                if comparative_year_na_present and not compared_year_na_present and (list(new_eval_list_of_dicts[-1].keys())[0] in ['Number of ratings of 3 or below', 'Number of ratings of 2 or below']):
                    for i in score_list:
                        for k, v in i.items():
                            if k == 'Number of ratings of N/A':
                                add1_list_of_scores = ['0', i.get(k)]
                                add1_category_key = 'Number of ratings of N/A'
                                add = True
                elif comparative_year_na_present and not compared_year_na_present and (list(new_eval_list_of_dicts[-1].keys())[0] in ['Percent of ratings of 2 or below', 'Percent of ratings of 3 or below']):
                    for i in score_list:
                        for k, v in i.items():
                            if k == 'Percent of ratings of N/A':
                                add1_list_of_scores = ['0%', i.get(k)]
                                add1_category_key = 'Percent of ratings of N/A'
                                add = True

            if add:
                new_eval_list_of_dicts.append({category_key: list_of_scores})
                new_eval_list_of_dicts.append({add1_category_key: add1_list_of_scores})
                add = False
            else:
                new_eval_list_of_dicts.append({category_key: list_of_scores})

        member_variable_query['eval_stats_list_of_dicts'] = new_eval_list_of_dicts

        member_variable_query['compared_years'] = reports[report_type]['compared_years']
        member_variable_query['comparative_year_number'] = comparative_year_number


        # member_variable_query['section_avg_list'] = BEA.section_avg_list
        section_avg_list = BEA.section_avg_list
        # member_variable_query['section_avg_list_last_item'] = tuple((list(BEA.section_avg_list[-1].keys())[0], BEA.section_avg_list[-1].get(list(BEA.section_avg_list[-1].keys())[0])))
        # section_avg_list_last_item = tuple((list(BEA.section_avg_list[-1].keys())[0], BEA.section_avg_list[-1].get(list(BEA.section_avg_list[-1].keys())[0])))
        # print('here: ', section_avg_list_last_item)
        new_section_avg_list = list()
        for section in section_avg_list:
            section_key = list(section.keys())[0]
            section_avg = section.get(list(section.keys())[0], '')
            # new_section_avg_list.append({section_key: []})
            list_of_section_averages = list()
            list_of_section_averages.append(section_avg)
            for item in list_of_compared_bea_objects:
                avg_list = item.section_avg_list
                for i in avg_list:
                    for k, v in i.items():
                        if k == section_key:
                            # if i == avg_list[-1]:
                            #     section_avg_list_last_item[1].append(i.get(section_key, ''))
                            # else:
                            list_of_section_averages.append(i.get(section_key, ''))

                        else:
                            pass
            new_section_avg_list.append({section_key: list_of_section_averages})


        # print('new_section_avg_list: ', new_section_avg_list)
        for section in new_section_avg_list:
            if len(section.get(list(section.keys())[0], '')) == 1:
                section.get(list(section.keys())[0], '').append('')
        # print('new_section_avg_list: ', new_section_avg_list)
        member_variable_query['section_avg_list'] = new_section_avg_list
        # member_variable_query['section_avg_list_last_item'] = section_avg_list_last_item

        bea_comparative_templater(comparative_year.get('file'), member_variable_query, report_type, report_to_run, questions_df, comparative_year.get('ppl_df'), randomize = randomize_bea_bool)


    #####################################
    print('\n\n--- Run Time ---')
    print('--- %s seconds ---' % (time.time() - start_time))
