from jinja2 import Template, Environment, PackageLoader, select_autoescape, FileSystemLoader
import datetime
import win32com.client as win32
from PIL import ImageGrab
import os
import time
import sys
import glob
import pandas as pd
from weasyprint import HTML, CSS
from decimal import Decimal, ROUND_HALF_UP
import pandas as pd
import numpy as np
from collections import Counter
import re
from collections import OrderedDict
from random import shuffle
import os.path
import warnings
import math

# sys.path.insert(1, r'C:\Users\Drew\Nasdaq_Classes_and_Fns')
# from ppl_parser import ppl_parser
# from csv_opener import csv_opener
#
# from peer_etl import peer_splitter, peer_etl
# from peer_reports import peer_composite_report, peer_individual_reports

from library.ppl_parser import ppl_parser
from library.csv_opener import csv_opener

from library.peer_etl import peer_splitter, peer_etl
from library.peer_reports import  peer_composite_report, peer_individual_reports


def comparative_peer_composite_templater(file_name, member_variable_query, report_type, df, questions_df, ppl_df, randomize = True, anonymized=False):
    if anonymized:
        question_column_width = set_question_column_width('Peer', 'Composite Anonymized')
    else:
        question_column_width = set_question_column_width('Peer', 'Composite Named')

    if anonymized:
        heat_map_section_bool = ask_create_section('Peer Composite Anonymized', 'Heat Map')
        section_analysis_bool = ask_create_section('Peer Composite Anonymized', 'Peer and Self-Evaluation Comparison Graph')
        esigma_bar_chart_bool = ask_create_section('Peer Composite Anonymized', 'eSigma Bar Chart')
        hilo_section_bool = ask_create_section('Peer Composite Anonymized', 'Hi-Lo Section')
        peer_self_report_bool = ask_create_section('Peer Composite Anonymized', 'Director Peer and Self-Evaluation Report')
        statistical_analysis_bool = ask_create_section('Peer Composite Anonymized', 'Statistical Analysis')


    else:
        heat_map_section_bool = ask_create_section('Peer Composite Named', 'Heat Map')
        section_analysis_bool = ask_create_section('Peer Composite Named', 'Peer and Self-Evaluation Comparison Graph')
        esigma_bar_chart_bool = ask_create_section('Peer Composite Named', 'eSigma Bar Chart')
        hilo_section_bool = ask_create_section('Peer Composite Named', 'Hi-Lo Section')
        peer_self_report_bool = ask_create_section('Peer Composite Named', 'Director Peer and Self-Evaluation Report')
        statistical_analysis_bool = ask_create_section('Peer Composite Named', 'Statistical Analysis')

    # question_column_width = 35
    # heat_map_section_bool = True
    # section_analysis_bool = True
    # esigma_bar_chart_bool = True
    # hilo_section_bool = True
    # peer_self_report_bool = True
    # statistical_analysis_bool = True

    now = datetime.datetime.now()
    formatted_date = str(now.strftime("%B")) + ' ' + str(now.day) + ', ' + str(now.year)

    file_loader =FileSystemLoader('templates')
    env = Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)
    template = env.get_template('comparative_peer_composite_report_template.html')

    skills_matrix_present = 'skills_matrix' in questions_df['type'].unique()

    # Get Assessment sections labels and section names
    section_names_and_labels = {}
    label_re = re.compile(r'^[\s\t]*([IVX]+\.)[\s\t]*(.*)')
    for section in questions_df['section'].unique().tolist():
        try:
            label = label_re.match(section)
            section_names_and_labels[label.group(1)] = label.group(2)
        except:
            section_names_and_labels[section] = section

    jinja_input_dict = {'nasdaq_logo': r'images/nasdaq_logo.png', 'year': str(now.year), 'formatted_date': formatted_date, 'skills_matrix_present': skills_matrix_present,
         'questions_df': questions_df, 'scale_image': r'images/scale.png', 'green_flag': r'images/green_flag.png', 'yellow_flag': r'images/yellow_flag.png', 'red_flag': r'images/red_flag.png',
         'question_column_width': question_column_width, 'heat_map_section_bool': heat_map_section_bool,
         'section_analysis_bool': section_analysis_bool, 'esigma_bar_chart_bool': esigma_bar_chart_bool, 'hilo_section_bool': hilo_section_bool,
         'peer_self_report_bool': peer_self_report_bool, 'statistical_analysis_bool': statistical_analysis_bool, 'anonymized': anonymized}

    jinja_input_dict['section_names_and_labels'] = section_names_and_labels

    # Control which sections are in the table of contents
    jinja_input_dict['table_of_contents_titles'] = ['#heat_map_title', '#section_analysis_title', '#eSigma_title_bar_chart', '#hilo_title', '#peer_and_self_report_title', '#statistical_analysis_title']

    optional_sections = []
    if member_variable_query.get('skills_matrix_df') is not None:
        optional_sections.append('#skills_matrix_title')

    if optional_sections:
        index = 2
        for optional_section in optional_sections:
            # print(optional_section)
            jinja_input_dict['table_of_contents_titles'].insert(index, optional_section)
            index+=1

    client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file_name)

    try:
        jinja_input_dict['client_name'] = client_name.group(1)
        jinja_input_dict['test'] = client_name.group(1)
    except:
        jinja_input_dict['client_name'] = 'insert_client_name_here'
        jinja_input_dict['test'] = 'insert_file_title_here'

    jinja_input_dict['number_of_sections'] = len(questions_df['section'].unique().tolist())
    jinja_input_dict['number_of_long_form_questions'] = len(questions_df[questions_df['type']=='long_form'])
    jinja_input_dict['number_of_rated_questions'] = len(questions_df['question_raw']) - len(questions_df[questions_df['type']=='long_form']) - len(questions_df[questions_df['type']=='skill_matrix']) - len(questions_df[questions_df['type']=='attribute_matrix'])

    jinja_input_dict['report_type'] = report_type

    if anonymized == False:
        jinja_input_dict['section_avg_bar_chart'] = r'charts\{}_section_avg_bar_chart.png'.format(member_variable_query.get('comparative_year_number'))
        jinja_input_dict['compared_section_avg_bar_chart'] = r'charts\{}_section_avg_bar_chart.png'.format(member_variable_query.get('compared_year_number'))

        jinja_input_dict['esigma_bar_chart'] = r'charts\{}_esigma_bar_chart.png'.format(member_variable_query.get('comparative_year_number'))
        jinja_input_dict['compared_esigma_bar_chart'] = r'charts\{}_esigma_bar_chart.png'.format(member_variable_query.get('compared_year_number'))
    else:
        jinja_input_dict['section_avg_bar_chart'] = r'charts\{}_anonymized_section_avg_bar_chart.png'.format(member_variable_query.get('comparative_year_number'))
        jinja_input_dict['compared_section_avg_bar_chart'] = r'charts\{}_anonymized_section_avg_bar_chart.png'.format(member_variable_query.get('compared_year_number'))

        jinja_input_dict['esigma_bar_chart'] = r'charts\{}_anonymized_esigma_bar_chart.png'.format(member_variable_query.get('comparative_year_number'))
        jinja_input_dict['compared_esigma_bar_chart'] = r'charts\{}_anonymized_esigma_bar_chart.png'.format(member_variable_query.get('compared_year_number'))



    for item in member_variable_query.keys():
        jinja_input_dict['{}'.format(item)] = member_variable_query[item]

    # for section in member_variable_query['jinja_for_loop_dict']:
    #     print('\n\n', section)
    #     for number in member_variable_query['jinja_for_loop_dict'][section]:
    #         print(number)
            # print(member_variable_query['jinja_for_loop_dict'][section][number])

    # print(member_variable_query['jinja_for_loop_dict'])

    output = template.render(jinja_input_dict)
    # with open("test.html", "w") as fh:
    #     fh.write(output)

    if anonymized == False:
        HTML(string=output, base_url='.').write_pdf('output\\{}_{}.pdf'.format(client_name.group(1), report_type), stylesheets=[CSS(r'static\peer_composite_template_style.css')])
    else:
        HTML(string=output, base_url='.').write_pdf('output\\{}_{}_Anonymized.pdf'.format(client_name.group(1), report_type), stylesheets=[CSS(r'static\peer_composite_template_style.css')])

def comparative_peer_individuals_templater(file_name, member_variable_query, report_type, df, questions_df, ppl_df, director_name, randomize = True):
    now = datetime.datetime.now()
    formatted_date = str(now.strftime("%B")) + ' ' + str(now.day) + ', ' + str(now.year)


    file_loader =FileSystemLoader('templates')
    env = Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)
    template = env.get_template('comparative_peer_individuals_report_template.html')

    skills_matrix_present = 'skills_matrix' in questions_df['type'].unique()

    jinja_input_dict = {'director_name': director_name, 'title': 'Peer Individual Report for {}'.format(director_name), 'nasdaq_logo': r'images/nasdaq_logo.png',
         'year': str(now.year), 'formatted_date': formatted_date, 'skills_matrix_present': skills_matrix_present,
         'questions_df': questions_df, 'scale_image': r'images/scale.png'}

    # Get Assessment sections labels and section names
    section_names_and_labels = {}
    label_re = re.compile(r'^[\s\t]*([IVX]+\.)[\s\t]*(.*)')
    for section in questions_df['section'].unique().tolist():
        label = label_re.match(section)
        section_names_and_labels[label.group(1)] = label.group(2)
    jinja_input_dict['section_names_and_labels'] = section_names_and_labels

    # Control which sections are in the table of contents
    jinja_input_dict['table_of_contents_titles'] = ['#heat_map_title', '#peer_self_comparison_graph_title', '#hilo_title', '#peer_and_self_report_title', '#statistical_analysis_title']

    optional_sections = []
    if member_variable_query.get('self_skills_present') is not None:
        optional_sections.append('#skills_matrix_title')

    # if not swot:
    #     optional_sections.append('#swot_title')
    # if highlights_actionpoints:
    #     optional_sections.append('#highlights_actionpts_title')

    if optional_sections:
        index = 2
        for optional_section in optional_sections:
            # print(optional_section)
            jinja_input_dict['table_of_contents_titles'].insert(index, optional_section)
            index+=1

    client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file_name)
    jinja_input_dict['client_name'] = client_name.group(1)

    jinja_input_dict['number_of_sections'] = len(questions_df['section'].unique().tolist())
    jinja_input_dict['number_of_long_form_questions'] = len(questions_df[questions_df['type']=='long_form'])
    jinja_input_dict['number_of_rated_questions'] = len(questions_df['question_raw']) - len(questions_df[questions_df['type']=='long_form']) - len(questions_df[questions_df['type']=='skill_matrix']) - len(questions_df[questions_df['type']=='attribute_matrix'])

    jinja_input_dict['report_type'] = report_type

    for item in member_variable_query.keys():
        jinja_input_dict['{}'.format(item)] = member_variable_query[item]


    if member_variable_query['peer_self_comparison_bool']:
        jinja_input_dict['section_avg_bar_chart'] = r'charts\{}_{}_peer_self.png'.format(member_variable_query.get('comparative_year_number'), director_name)
        jinja_input_dict['compared_section_avg_bar_chart'] = r'charts\{}_{}_peer_self.png'.format(member_variable_query.get('compared_year_number'), director_name)



    output = template.render(jinja_input_dict)
    # with open("test.html", "w") as fh:
    #     fh.write(output)
    HTML(string=output, base_url='.').write_pdf('output\\{}_{}_{}.pdf'.format(director_name, client_name.group(1), report_type), stylesheets=[CSS(r'static\peer_individuals_template_style.css')])

def ask_create_section(report_type, section):
    check = str(input("Would you like to create a {} for report type: {}? (y/n): ".format(section, report_type))).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_create_section(report_type, section)
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_create_section(report_type, section)

def ask_randomize_report(report_type):
    check = str(input("Would you like to randomize report: {}? (y/n): ".format(report_type))).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_randomize_report()
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_randomize_report(report_type)

def set_question_column_width(report_type, specific_type):
    check = str(input("Would you like to set a column width for report type: {} {}? (y/n): ".format(report_type, specific_type))).lower().strip()
    try:
        if check[0] == 'y':
            percentage = int(input('> '))
            if len(str(percentage)) == 2:
                return percentage
            else:
                print('Invalid Input')
                return set_question_column_width(report_type, specific_type)
        elif check[0] == 'n':
            return 25
        else:
            print('Invalid Input')
            return set_question_column_width(report_type, specific_type)
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return set_question_column_width(report_type, specific_type)

def create_comparative_peer_composite_jinja_dict(sorted_years, anonymize, randomize, no_self_ratings_bool, report_type='Peer'):
    copied_dict = sorted_years.copy()
    comparative_year = copied_dict.pop(max(copied_dict.keys()), None)
    compared_year = copied_dict.pop(min(sorted_years.keys()))

    # number_of_comparisons = len(list(copied_dict.keys()))
    # print("Number of comparisons: ", number_of_comparisons)

    qdf = comparative_year['questions_df']
    tdf = comparative_year['ETLd_df']
    file = comparative_year['file']
    comparative_smoothed_df = comparative_year['smoothed_df']

    # qdf = compared_year['questions_df']
    c_tdf = compared_year['ETLd_df']
    # file = compared_year['file']
    compared_smoothed_df = compared_year['smoothed_df']

    # Get Board Averages
    # board_question_averages = dict()
    # for section in comparative_smoothed_df['section'].unique():
    #     for number in comparative_smoothed_df[comparative_smoothed_df['section']==section]['number'].unique():
    #         try:
    #             avgs = []
    #             avg = comparative_smoothed_df[(comparative_smoothed_df['section']==section) & (comparative_smoothed_df['number']==number)]['response'].mean()
    #             avgs.append(avg)
    #
    #             avgs.append(compared_smoothed_df[(compared_smoothed_df['section']==section) & (compared_smoothed_df['number']==number)]['response'].mean())
    #             avgs = ["{0:.2f}".format(Decimal('{}'.format(x)).quantize(Decimal('1e-2'), ROUND_HALF_UP)) for x in avgs]
    #             board_question_averages[(section, number)] = avgs
    #         except:
    #             board_question_averages[(section, number)] = None


    data_dict = OrderedDict()
    rankFlag = False
    if anonymize ==True:
        director_encoder = dict()
        director_count = 1
    for section in qdf['section'].unique():
        if set(qdf[qdf['section']==section]['type'].tolist()) <= set(['skills_matrix', '', 'nan']):
            continue

        data_dict[section] = OrderedDict()
        for number in qdf[qdf['section']==section]['number'].unique():
            data_dict[section][number] = OrderedDict()
            try:
                avgs = []
                question_averages = comparative_smoothed_df[(comparative_smoothed_df['section']==section) & (comparative_smoothed_df['number']==number)]['response'].mean()
                question_averages = "{0:.2f}".format(Decimal('{}'.format(question_averages)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                avgs.append(question_averages)
                avgs.append(compared_smoothed_df[(compared_smoothed_df['section']==section) & (compared_smoothed_df['number']==number)]['response'].mean())
                avgs = ["{0:.2f}".format(Decimal('{}'.format(x)).quantize(Decimal('1e-2'), ROUND_HALF_UP)) for x in avgs]
            except:
                question_averages = None

            data_dict[section][number]['question_averages'] = avgs
            data_dict[section][number]['type'] = qdf[(qdf['section']==section)&(qdf['number']==number)]['type'].unique().tolist()[0]
            data_dict[section][number]['question'] = qdf[(qdf['section']==section)&(qdf['number']==number)]['question'].unique().tolist()[0]
            # data_dict[section][number]['board_average']
            # Iterate through raw questions because sub_section is undefined when
            # improper syntax is used
            data_dict[section][number]['raw_questions'] = OrderedDict()
            for raw_q in qdf[(qdf['section']==section)&(qdf['number']==number)]['question_raw']:
                data_dict[section][number]['raw_questions'][raw_q] = {}
                sub_q = qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['sub_section'].item()

                if anonymize:
                    if sub_q not in ['', '!__!']:
                        if director_encoder.get(sub_q) is None:
                            director_encoder[sub_q] = 'Director {}'.format(director_count)
                            data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = 'Director {}'.format(director_count)
                            director_count += 1
                        else:
                            data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = director_encoder.get(sub_q)
                    else:
                        data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = sub_q
                else:
                    data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = sub_q

                if qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['type'].item() in ['matrix', 'likert']:
                    answers = tdf[raw_q].replace({np.nan: None})
                    answers.fillna('N/A', inplace=True)
                    answers = answers.tolist()
                    answers = [int(x) if x != 'N/A' else x for x in answers]
                    # print(raw_q)
                    # print(answers)
                    if no_self_ratings_bool:
                        answers.remove('N/A')
                    if randomize:
                        shuffle(answers)
                        data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers
                    else:
                        data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers
                    # raw_averages [str(Decimal(np.mean(tdf[raw_q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP))]
                    data_dict[section][number]['raw_questions'][raw_q]['averages'] = [str(Decimal(np.mean(tdf[raw_q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP))]
                    # print(data_dict[section][number]['raw_questions'][raw_q]['averages'])
                    try:
                        data_dict[section][number]['raw_questions'][raw_q]['averages'].append(str(Decimal(np.mean(compared_year['ETLd_df'][raw_q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP)))
                    except:
                        data_dict[section][number]['raw_questions'][raw_q]['averages'].append('N/A')
                    # for year in
                    # print('\n\n', section, number)
                    # print(board_question_averages.get((section, number)))
                    # data_dict[section][number]['raw_questions'][raw_q]['board_averages'] = board_question_averages.get((section, number))
                elif qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['type'].item() in ['long_form', 'optional_long_form']:
                    try:
                        if math.isnan(sub_q):
                            data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = 'None'
                    except:
                        pass

                    answers = tdf[raw_q].dropna().tolist()
                    answers = [a for a in answers if a not in ['nan', '#NAME?', None, 'None', 'No Comment', 'no comment', 'none', np.nan]]

                    if (len(answers) == 0) and (data_dict[section][number]['type'] == 'optional_long_form'):
                        del data_dict[section][number]
                        continue

                    if randomize:
                        shuffle(answers)
                    else:
                        pass

                    data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers

    # if not anonymize:
    #     count = 0
    #     for s in data_dict:
    #         print('\n\nSection Name:  ', s)
    #         for number in data_dict[s]:
    #             print('Number:  ', number)
    #             try:
    #                 print('Question Average:  ', data_dict[s][number]['question_average'])
    #             except:
    #                 pass
    #             print('Type:  ', data_dict[s][number]['type'])
    #             print('Question: ', data_dict[s][number]['question'])
    #             for raw in data_dict[s][number]['raw_questions']:
    #                 print()
    #                 print('Raw:  ', raw)
    #                 print('Sub:  ', data_dict[s][number]['raw_questions'][raw]['sub_section'])
    #                 print('Answers:  ', data_dict[s][number]['raw_questions'][raw]['answers'])
    #                 try:
    #                     print('Average:  ', data_dict[s][number]['raw_questions'][raw]['averages'])
    #                     # print('Board Average:  ', data_dict[s][number]['raw_questions'][raw]['board_average'])
    #                 except:
    #                     pass
    #                 print('\n')
    #             count+=1
    #             # if count == 4:
    #             #     break
    #     # print(data_dict[list(data_dict.keys())[0]]['1.']['Comments/Suggestions'])

    # count = 0
    # for section in data_dict:
    #     print('\n\nSection Name:  ', section)
    #     for number in data_dict[section]:
    #         print('Number:  ', number)
    #         try:
    #             print('Question Average:  ', data_dict[section][number]['question_averages'])
    #         except:
    #             pass
    #         print('Type:  ', data_dict[section][number]['type'])
    #         print('Question:  ', data_dict[section][number]['question'])
    #         # print('Answers:  ', data_dict[section][number]['answers'])
    #         for raw_q in data_dict[section][number]['raw_questions']:
    #             print('Raw Q:  ', raw_q)
    #             print('Sub:  ', data_dict[section][number]['raw_questions'][raw_q]['sub_section'])
    #             print('Answers:  ', data_dict[section][number]['raw_questions'][raw_q]['answers'])
    #             # try:
    #             if data_dict[section][number]['type'] in ['matrix', 'likert']:
    #                 print('Averages:  ', data_dict[section][number]['raw_questions'][raw_q]['averages'])
    #                 # print('Board Averages:  ', data_dict[section][number]['raw_questions'][raw_q]['board_averages'])
    #             # except:
    #             #     pass
    #         print('\n')
    #         # count+=1
    #         # if count == 4:
    #         #     break



    # return data_dict, number_of_comparisons
    return data_dict

def create_comparative_peer_individuals_jinja_dict(id, director, sorted_years, smoothed_df, file, report_type, no_self_ratings_bool, anonymize, randomize):
    copied_dict = sorted_years.copy()
    comparative_year = copied_dict.pop(max(copied_dict.keys()), None)
    compared_year = copied_dict.pop(min(sorted_years.keys()))

    # number_of_comparisons = len(list(copied_dict.keys()))

    qdf = comparative_year['questions_df']
    tdf = comparative_year['ETLd_df']
    file = comparative_year['file']
    comparative_smoothed_df = comparative_year['smoothed_df']
    compared_smoothed_df = compared_year['smoothed_df']

    # qdf = compared_year['questions_df']
    c_tdf = compared_year['ETLd_df']
    # file = compared_year['file']
    compared_smoothed_df = compared_year['smoothed_df']

    # Get Board Averages
    board_question_averages = dict()
    for section in comparative_smoothed_df['section'].unique():
        for number in comparative_smoothed_df[comparative_smoothed_df['section']==section]['number'].unique():
            try:
                avg = comparative_smoothed_df[(comparative_smoothed_df['section']==section) & (comparative_smoothed_df['number']==number)]['response'].mean()
                board_question_averages[(section, number)] = "{0:.2f}".format(Decimal('{}'.format(avg)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                board_question_averages[(section, number)] = 'N/A'


    data_dict = dict()
    # for director in smoothed_df['sub_section'].unique():
    #     # print('\n\n', director)
    #     row_id = qdf[~(qdf['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (qdf['sub_section']==director)]['UniqueIdentifier'].unique()[0]
    #     # print(row_id)
    if director not in ['', '!__!']:
        data_dict[director] = OrderedDict()
        for section in comparative_smoothed_df[comparative_smoothed_df['sub_section']==director]['section'].unique():
            data_dict[director][section] = OrderedDict()
            for number in comparative_smoothed_df[(comparative_smoothed_df['section']==section)&(comparative_smoothed_df['sub_section']==director)]['number'].unique():
                data_dict[director][section][number] = dict()
                # comparative_smoothed_df.to_excel('comparative_smoothed_df.xlsx')
                # print('no_self_ratings_bool: ', no_self_ratings_bool)
                if not no_self_ratings_bool:
                    try:
                        new_self_rating = comparative_smoothed_df[(comparative_smoothed_df['section']==section)&(comparative_smoothed_df['sub_section']==director)&(comparative_smoothed_df['number']==number)&(comparative_smoothed_df['respondent_id']==id)]['response'].item()
                    except:
                        new_self_rating = 'N/A'
                    try:
                        old_self_rating = compared_smoothed_df[(compared_smoothed_df['section']==section)&(compared_smoothed_df['sub_section']==director)&(comparative_smoothed_df['number']==number)&(compared_smoothed_df['respondent_id']==id)]['response'].item()
                    except:
                        old_self_rating = 'N/A'

                    if pd.isna(new_self_rating):
                        new_self_rating = 'N/A'
                    if pd.isna(old_self_rating):
                        old_self_rating = 'N/A'

                    data_dict[director][section][number]['new_self_rating'] = new_self_rating
                    data_dict[director][section][number]['old_self_rating'] = old_self_rating

                    data_dict[director][section][number]['answers'] = [new_self_rating]
                else:
                    data_dict[director][section][number]['answers'] = []

                # print(data_dict[director][section][number]['answers'])
                # print("data_dict[director][section][number]['answers']: ", data_dict[director][section][number]['answers'])

                # try:
                #     board_average = comparative_smoothed_df[(comparative_smoothed_df['section']==section) & (comparative_smoothed_df['number']==number)]['response'].mean()
                #     board_average = "{0:.2f}".format(Decimal('{}'.format(board_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                # except:
                #     board_average = 'NaN'

                try:
                    respondent_average = comparative_smoothed_df[(comparative_smoothed_df['section']==section) & (comparative_smoothed_df['number']==number) & (comparative_smoothed_df['sub_section']==director)]['response'].mean()
                    respondent_average = "{0:.2f}".format(Decimal('{}'.format(respondent_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                except:
                    question_average = None

                # data_dict[director][section][number]['board_average'] = board_average
                data_dict[director][section][number]['respondent_average'] = respondent_average
                data_dict[director][section][number]['type'] = comparative_smoothed_df[(comparative_smoothed_df['section']==section)&(comparative_smoothed_df['number']==number)]['type'].unique().tolist()[0]
                data_dict[director][section][number]['question'] = comparative_smoothed_df[(comparative_smoothed_df['section']==section)&(comparative_smoothed_df['number']==number)]['question'].unique().tolist()[0]

                if data_dict[director][section][number]['type'] in ['matrix', 'likert']:
                    # calculate average
                    answers = comparative_smoothed_df[(comparative_smoothed_df['section']==section)&(comparative_smoothed_df['number']==number)&(comparative_smoothed_df['sub_section']==director)][['respondent_id', 'response']]
                    # print('\nanswers:', answers)
                    old_answers = compared_smoothed_df[(compared_smoothed_df['section']==section)&(compared_smoothed_df['number']==number)&(compared_smoothed_df['sub_section']==director)][['respondent_id', 'response']]

                    answers.set_index('respondent_id', inplace=True)
                    # print(answers)
                    avg = np.mean(answers['response'].dropna())
                    old_avg = np.mean(old_answers['response'].dropna())

                    answers = answers.replace({np.nan: None})
                    answers.fillna('N/A', inplace=True)
                    answers.drop(index=id, inplace=True)
                    # print(answers)
                    answers = answers['response'].tolist()
                    answers = [int(x) if x != 'N/A' else x for x in answers]
                    if randomize:
                        shuffle(answers)
                    # print(answers)
                    data_dict[director][section][number]['answers'].extend(answers)

                    data_dict[director][section][number]['average'] = str(Decimal(avg).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                    data_dict[director][section][number]['old_average'] = str(Decimal(old_avg).quantize(Decimal('1e-2'), ROUND_HALF_UP))

                    # try:
                    #     board_average = comparative_smoothed_df[(comparative_smoothed_df['section']==section) & (comparative_smoothed_df['number']==number)]['response'].mean()
                    #     board_average = "{0:.2f}".format(Decimal('{}'.format(board_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                    # except:
                    #     board_average = 'NaN'
                    data_dict[director][section][number]['board_average'] = board_question_averages[(section, number)]

                elif data_dict[director][section][number]['type']in ['long_form', 'optional_long_form']:
                    data_dict[director][section][number]['answers'] = []
                    answers = comparative_smoothed_df[(comparative_smoothed_df['section']==section)&(smoothed_df['number']==number)&(smoothed_df['sub_section']==director)]['response'].tolist()
                    answers = [a for a in answers if a not in ['nan', '#NAME?', None, 'None', 'No Comment', 'no comment', 'none', np.nan]]

                    if randomize:
                        shuffle(answers)
                    # print(section, director, number, id)
                    try:
                        if comparative_smoothed_df[(comparative_smoothed_df['section']==section)&(comparative_smoothed_df['sub_section']==director)&(comparative_smoothed_df['number']==number)&(comparative_smoothed_df['respondent_id']==id)]['response'].item() not in ['nan', '#NAME?', None, 'None', 'No Comment', 'no comment', 'none', np.nan]:
                            data_dict[director][section][number]['answers'].extend(answers)
                        else:
                            data_dict[director][section][number]['answers'] = answers
                    except:
                        data_dict[director][section][number]['answers'] = answers
    else:
        pass

    # for director, o_dict in data_dict.items():
    #     print('director: ', director)
    #     # print(o_dict)
    #     for s in o_dict.keys():
    #         print('\n\nSection Name:  ', s)
    #         for number in data_dict[director][s]:
    #             print('Number:  ', number)
    #             try:
    #                 print('Question Average:  ', data_dict[director][s][number]['question_average'])
    #             except:
    #                 pass
    #             try:
    #                 print('New Self-Rating: ', data_dict[director][s][number]['new_self_rating'])
    #                 print('Old Self-Rating: ', data_dict[director][s][number]['old_self_rating'])
    #             except:
    #                 pass
    #
    #             print('Type:  ', data_dict[director][s][number]['type'])
    #             print('Question: ', data_dict[director][s][number]['question'])
    #             print('Answers:  ', data_dict[director][s][number]['answers'])
    #             try:
    #                 print('Average:  ', data_dict[director][s][number]['average'])
    #                 print('Board Average:  ', data_dict[director][s][number]['board_average'])
    #             except:
    #                 pass
    #             print('\n')

    return data_dict

def ask_use_questionsdf_in_directory():
    check = str(input("Would you like to use the questions_df file in the project directory? (y/n): ")).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_use_questionsdf_in_directory()
    except Exception as error:
        print("Please enter a valid input")
        print(error)
        return ask_use_questionsdf_in_directory()

def join_questions_df_and_raw_data_into_dict(qdf, tdf, file, report_type, composite_join, randomize, anonymize):

    ############################################################################
    # ETL
    df = pd.DataFrame(columns=['client_name', 'report_type', 'section', 'number', 'question_raw', 'question', 'sub_section', 'type', 'average', 'response', 'respondent_id', 'question_mean'])
    try:
        client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file).group(1)
    except:
        client_name = None

    if anonymize ==True:
        director_encoder = dict()
        director_count = 1
    # qdf.to_excel('qdf.xlsx')
    # tdf.to_excel('tdf.xlsx')
    for s in qdf['section'].unique():
        for q in qdf[qdf['section']==s]['question_raw']:
            for i in tdf[q].index:
                if anonymize == False:
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section_number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_number'].item(), 'sub_section_value': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_value'].item(),
                            'sub_section': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item(), 'response': tdf.loc[i, q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}
                else:
                    sub_q = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item()
                    if sub_q not in ['', '!__!']:
                        if director_encoder.get(sub_q) is None:
                            director_encoder[sub_q] = 'Director {}'.format(director_count)
                            sub_q = 'Director {}'.format(director_count)
                            director_count += 1
                        else:
                            sub_q = director_encoder.get(sub_q)
                    else:
                        pass
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section_number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_number'].item(), 'sub_section_value': director_encoder.get(sub_q),
                            'sub_section': sub_q, 'response': tdf.loc[i, q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}

                df = df.append(row, ignore_index=True)

    question_means = []
    for section in df['section'].unique():
        for number in df[(df['section']==section)]['number'].unique():
            try:
                mean_by_question = df[(df['section']==section) & (df['number']==number)]['response'].mean()
                mean_by_question = "{0:.2f}%".format(Decimal('{}'.format(mean_by_question)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                mean_by_question = None
            mean_column_addition = [mean_by_question]*len(df[(df['section']==section) & (df['number']==number)]['response'])
            question_means.extend(mean_column_addition)

    df['question_mean'] = question_means
    # df.to_excel('SMOOTHED_{}.xlsx'.format(client_name))
    ############################################################################

    # Get Board Averages
    board_question_averages = dict()
    for section in df['section'].unique():
        for number in df[df['section']==section]['number'].unique():
            try:
                avg = df[(df['section']==section) & (df['number']==number)]['response'].mean()
                board_question_averages[(section, number)] = "{0:.2f}".format(Decimal('{}'.format(avg)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                board_question_averages[(section, number)] = None


    if composite_join:
        data_dict = OrderedDict()
        rankFlag = False
        if anonymize ==True:
            director_encoder = dict()
            director_count = 1
        for section in qdf['section'].unique():
            data_dict[section] = OrderedDict()
            for number in qdf[qdf['section']==section]['number'].unique():
                data_dict[section][number] = OrderedDict()
                try:
                    question_average = df[(df['section']==section) & (df['number']==number)]['response'].mean()
                    question_average = "{0:.2f}".format(Decimal('{}'.format(question_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                except:
                    question_average = None
                data_dict[section][number]['question_average'] = question_average
                data_dict[section][number]['type'] = qdf[(qdf['section']==section)&(qdf['number']==number)]['type'].unique().tolist()[0]
                data_dict[section][number]['question'] = qdf[(qdf['section']==section)&(qdf['number']==number)]['question'].unique().tolist()[0]
                # data_dict[section][number]['board_average']
                # Iterate through raw questions because sub_section is undefined when
                # improper syntax is used
                data_dict[section][number]['raw_questions'] = OrderedDict()
                for raw_q in qdf[(qdf['section']==section)&(qdf['number']==number)]['question_raw']:
                    data_dict[section][number]['raw_questions'][raw_q] = {}
                    sub_q = qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['sub_section'].item()

                    if anonymize:
                        if sub_q not in ['', '!__!']:
                            if director_encoder.get(sub_q) is None:
                                director_encoder[sub_q] = 'Director {}'.format(director_count)
                                data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = 'Director {}'.format(director_count)
                                director_count += 1
                            else:
                                data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = director_encoder.get(sub_q)
                        else:
                            data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = sub_q
                    else:
                        data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = sub_q

                    if qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['type'].item() in ['matrix', 'likert']:
                        answers = tdf[raw_q].replace({np.nan: None})
                        answers.fillna('N/A', inplace=True)
                        answers = answers.tolist()
                        answers = [int(x) if x != 'N/A' else x for x in answers]
                        if randomize:
                            shuffle(answers)
                            data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers
                        else:
                            data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers
                        data_dict[section][number]['raw_questions'][raw_q]['average'] = str(Decimal(np.mean(tdf[raw_q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP))


                        data_dict[section][number]['raw_questions'][raw_q]['board_average'] = board_question_averages.get((section, number))
                    elif qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['type'].item() in ['long_form']:
                        answers = tdf[raw_q].dropna().tolist()
                        answers = [a for a in answers if a not in ['nan', '#NAME?']]
                        if randomize:
                            shuffle(answers)
                        else:
                            pass
                        data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers
    else:
        data_dict = dict()
        for director in df['sub_section'].unique():
            # print('\n\n', director)
            row_id = qdf[~(qdf['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (qdf['sub_section']==director)]['UniqueIdentifier'].unique()[0]
            # print(row_id)
            if director not in ['', '!__!']:
                data_dict[director] = OrderedDict()
                for section in df[df['sub_section']==director]['section'].unique():
                    data_dict[director][section] = OrderedDict()
                    for number in df[(df['section']==section)&(df['sub_section']==director)]['number'].unique():
                        data_dict[director][section][number] = dict()
                        self_rating = df[(df['section']==section)&(df['sub_section']==director)&(df['number']==number)&(df['respondent_id']==row_id)]['response'].item()
                        data_dict[director][section][number]['self_rating'] = self_rating
                        data_dict[director][section][number]['answers'] = [self_rating]
                        try:
                            board_average = df[(df['section']==section) & (df['number']==number)]['response'].mean()
                            board_average = "{0:.2f}".format(Decimal('{}'.format(board_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                        except:
                            board_average = None
                        try:
                            respondent_average = df[(df['section']==section) & (df['number']==number) & (df['sub_section']==director)]['response'].mean()
                            respondent_average = "{0:.2f}".format(Decimal('{}'.format(respondent_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                        except:
                            question_average = None
                        data_dict[director][section][number]['board_average'] = board_average
                        data_dict[director][section][number]['respondent_average'] = respondent_average
                        data_dict[director][section][number]['type'] = df[(df['section']==section)&(df['number']==number)]['type'].unique().tolist()[0]
                        data_dict[director][section][number]['question'] = df[(df['section']==section)&(df['number']==number)]['question'].unique().tolist()[0]

                        if data_dict[director][section][number]['type'] in ['matrix', 'likert']:
                            # calculate average
                            answers = df[(df['section']==section)&(df['number']==number)&(df['sub_section']==director)][['respondent_id', 'response']]
                            # print(row_id)
                            answers.set_index('respondent_id', inplace=True)
                            # print(answers)
                            avg = np.mean(answers['response'].dropna())
                            answers = answers.replace({np.nan: None})
                            answers.fillna('N/A', inplace=True)
                            answers.drop(index=row_id, inplace=True)
                            # print(answers)
                            answers = answers['response'].tolist()
                            answers = [int(x) if x != 'N/A' else x for x in answers]
                            if randomize:
                                shuffle(answers)

                            data_dict[director][section][number]['answers'].extend(answers)
                            # print(data_dict[director][section][number]['answers'])
                            data_dict[director][section][number]['average'] = str(Decimal(avg).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                            data_dict[director][section][number]['board_average'] = board_question_averages.get((section, number))
                        elif data_dict[director][section][number]['type']in ['long_form']:
                            answers = df[(df['section']==section)&(df['number']==number)&(df['sub_section']==director)]['response'].tolist()
                            answers = [a for a in answers if a not in ['nan', '#NAME?']]
                            if randomize:
                                shuffle(answers)
                            data_dict[director][section][number]['answers'].extend(answers)
            else:
                pass

    # for director in list(data_dict.keys()):
    #     print(director)
    #     for section in data_dict[director]:
    #         print(section)
    #         if section == list(data_dict[director].keys())[0]:
    #           print('here', section)
    #         else:
    #           print('nope', section)
    #     break
    # print(data_dict[list(data_dict.keys())[0]])



    ## Director Decoder
    # director_encoder = dict()
    # director_count = 1
    # for section in qdf['section'].unique():
    #     for number in qdf[qdf['section']==section]['number'].unique():
    #         for raw_q in qdf[(qdf['section']==section)&(qdf['number']==number)]['question_raw']:
    #             print(section, number, raw_q)
    #             sub_q = qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['sub_section'].item()
    #             if sub_q not in ['', '!__!']:
    #                 if director_encoder.get(sub_q) is None:
    #                     director_encoder[sub_q] = 'Director {}'.format(director_count)
    #                     director_count += 1
    # print(director_encoder)


        # for s in data_dict:
    #     print('\n\n', s)
    #     for number in data_dict[s]:
    #         try:
    #             print(data_dict[s][number]['question_average'])
    #         except:
    #             pass
    #         print(data_dict[s][number]['type'])
    #         print(data_dict[s][number]['question'])
    #         for raw in data_dict[s][number]['raw_questions']:
    #             print(raw)
    #             print(data_dict[s][number]['raw_questions'][raw]['sub_section'])
    #             print(data_dict[s][number]['raw_questions'][raw]['answers'])

    return data_dict, df

def join_qdf_and_rawdata_into_smoothed_df(qdf, tdf, file, report_type, anonymize):

    ############################################################################
    # ETL
    df = pd.DataFrame(columns=['client_name', 'report_type', 'section', 'number', 'question_raw', 'question', 'sub_section', 'type', 'average', 'response', 'respondent_id', 'question_mean'])
    try:
        client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file).group(1)
    except:
        client_name = None

    if anonymize ==True:
        director_encoder = dict()
        director_count = 1
    # qdf.to_excel('qdf.xlsx')
    # tdf.to_excel('tdf.xlsx')
    for s in qdf['section'].unique():
        for q in qdf[qdf['section']==s]['question_raw']:
            for i in tdf[q].index:
                # if (tdf.loc[i,q]==np.nan or (tdf.loc[i,q]=='nan')):
                #     print('here!', i, q)
                #     response = None
                # else:
                #     response = tdf.loc[i,q]
                if anonymize == False:
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section_number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_number'].item(), 'sub_section_value': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_value'].item(),
                            'sub_section': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item(), 'response': tdf.loc[i,q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}
                else:
                    sub_q = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item()
                    if sub_q not in ['', '!__!']:
                        if director_encoder.get(sub_q) is None:
                            director_encoder[sub_q] = 'Director {}'.format(director_count)
                            sub_q = 'Director {}'.format(director_count)
                            director_count += 1
                        else:
                            sub_q = director_encoder.get(sub_q)
                    else:
                        pass
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section_number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_number'].item(), 'sub_section_value': director_encoder.get(sub_q),
                            'sub_section': sub_q, 'response': tdf.loc[i,q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}

                df = df.append(row, ignore_index=True)

    question_means = []
    for section in df['section'].unique():
        for number in df[(df['section']==section)]['number'].unique():
            try:
                mean_by_question = df[(df['section']==section) & (df['number']==number)]['response'].mean()
                mean_by_question = "{0:.2f}%".format(Decimal('{}'.format(mean_by_question)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                mean_by_question = None
            mean_column_addition = [mean_by_question]*len(df[(df['section']==section) & (df['number']==number)]['response'])
            question_means.extend(mean_column_addition)

    df['question_mean'] = question_means
    df.to_excel('benchmark_data\\SMOOTHED_{}.xlsx'.format(client_name))
    return df

if __name__ == "__main__":
    start_time = time.time()
    print(sys.version)
    print('\n------------------------------------------\n')

    cwd = os.getcwd()

    extension = 'csv'
    files = glob.glob("*.{}".format(extension))

    years = {}
    for f in files:
        year = f[-8:-4]
        years[int(year)] = {'file': f}

    sorted_years = OrderedDict(sorted(years.items(), reverse=True))
    # print('sorted_years: ', sorted_years)

    current_year = max(sorted_years.keys())
    current_year_file = sorted_years[current_year].get('file')

    ###################################################################

    for year in sorted_years.keys():
        sorted_years[year]['raw_data_dict'] = csv_opener(sorted_years.get(year).get('file'))

    for year in sorted_years.keys():
        sorted_years[year]['dict_of_dfs'] = peer_splitter(sorted_years.get(year).get('file'), sorted_years.get(year).get('raw_data_dict'))

    for year in sorted_years.keys():
        sorted_years[year]['ppl_df'] = ppl_parser(sorted_years.get(year).get('dict_of_dfs').get('meta'))

    for year in sorted_years.keys():
        # print('\n\nyear: ', year)
        for year2 in sorted_years.keys():
            # print('year2: ', year2)
            if set(sorted_years[year]['ppl_df'].index) != set(sorted_years[year2]['ppl_df'].index):
                warnings.warn('UniqueIdentifier columns do not match!')

    # for k, v in sorted_years.items():
    #     print('\nYear Found:: ', k)
    #     print('items in year:: ', v.keys())

    ###################################################################
    # ETL Process
    # The following block runs an ETL process to populate "sorted_years"

    for year in sorted_years.keys():
        for report_type in sorted_years.get(year).get('dict_of_dfs'):
            if report_type != 'meta':
                sorted_years[year]['not_app_or_obs'] = {}
                sorted_years[year]['ETLd_df'] = {}
                sorted_years[year]['data_dict'] = {}
                # sorted_years[year]['report_types'][report_type]['smoothed_df'] = {}
                # print(sorted_years)
                report_to_run = sorted_years[year]['dict_of_dfs'].get(report_type)

                questions_df, transformed_df, na_no_null_dict = peer_etl(report_to_run, sorted_years.get(year).get('ppl_df'), report_type, sorted_years.get(year).get('file'), True, year=year)

                # Check for no-self rating type peer reports
                ids_respondents = []
                respondent__self_rate_bool_list = []
                for respondent in questions_df[~(questions_df['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (~questions_df['sub_section'].isin(['', None, np.nan]))]['sub_section'].unique():
                    id = questions_df[~(questions_df['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (questions_df['sub_section']==respondent)]['UniqueIdentifier'].unique()[0]
                    # print('id: ', id)
                    # print('respondent: ', respondent)
                    ids_respondents.append((id, respondent))

                    raw_question_list = questions_df[(questions_df['sub_section']==respondent) & (questions_df['type'].isin(['likert', 'matrix']))]['question_raw']
                    self_ratings = transformed_df[raw_question_list].loc[id].unique().tolist()
                    # print('raw_question_list: ', raw_question_list)
                    # print('self_ratings: ', self_ratings)
                    if len(self_ratings) == 1:
                        # print('len(self_ratings) == 1: ', len(self_ratings) == 1)
                        import math
                        if math.isnan(self_ratings[0]):
                            # print('nan')
                            respondent__self_rate_bool_list.append((respondent, True))
                        else:
                            respondent__self_rate_bool_list.append((respondent, False))
                    else:
                        respondent__self_rate_bool_list.append((respondent, False))
                no_self_ratings_bool = all(item[-1] == True for item in respondent__self_rate_bool_list)
                # print('no_self_ratings_bool: ', no_self_ratings_bool)
                sorted_years[year]['no_self_ratings_bool'] = no_self_ratings_bool


                # response = ask_use_questionsdf_in_directory()
                response = True
                if response:
                    if os.path.isfile('{}_{}_questions_df.xlsx'.format(year, report_type)):
                        q_df = pd.read_excel('{}_{}_questions_df.xlsx'.format(year, report_type), index=False, converters={'number': str})
                        sorted_years[year]['questions_df'] = q_df
                        questions_df = q_df
                    else:
                        print('\n\nAborting... Question data frame file "questions_df.xlsx" not found in directory\n')
                        sys.exit()
                else:
                    sorted_years[year]['questions_df'] = questions_df

                sorted_years[year]['ETLd_df'] = transformed_df
                sorted_years[year]['na_no_null_dict'] = na_no_null_dict

                smoothed_df = join_qdf_and_rawdata_into_smoothed_df(questions_df, transformed_df, sorted_years.get(year).get('file'), report_type, False)
                sorted_years[year]['smoothed_df'] = smoothed_df


    # ###################################################################
    # Everything above has been commented out

    # import pickle
    #
    # with open('sorted_years.pickle', 'wb') as handle:
    #     pickle.dump(sorted_years, handle, protocol=pickle.HIGHEST_PROTOCOL)

    # with open('sorted_years.pickle', 'rb') as handle:
    #     sorted_years = pickle.load(handle)

    ####################################################################
    randomize = ask_randomize_report('Peer Comparative')

    jinja_composite_data_dict_randomized_named = create_comparative_peer_composite_jinja_dict(sorted_years, False, randomize, sorted_years[current_year]['no_self_ratings_bool'])
    sorted_years[current_year]['named'] = jinja_composite_data_dict_randomized_named

    jinja_composite_data_dict_randomized_anonymized = create_comparative_peer_composite_jinja_dict(sorted_years, True, randomize, sorted_years[current_year]['no_self_ratings_bool'])
    sorted_years[current_year]['anonymized'] = jinja_composite_data_dict_randomized_anonymized



    ####################################################################
    # Everything above has been commented out

    # import pickle

    # with open('sorted_years.pickle', 'wb') as handle:
    #     pickle.dump(sorted_years, handle, protocol=pickle.HIGHEST_PROTOCOL)
    #
    # with open('sorted_years.pickle', 'rb') as handle:
    #     sorted_years = pickle.load(handle)

    ####################################################################

    comparative_year_number = max(sorted_years.keys())
    compared_year_number = min(sorted_years.keys())
    # print(comparative_year_number)
    # print(sorted_years.get(comparative_year_number))

    comparative_year = sorted_years.get(comparative_year_number)
    # print()
    # for year in sorted_years.keys():
    #     for report_type in sorted_years.get(year).get('dict_of_dfs'):
    #         if report_type != 'meta':
                # print('Creating {} report for {}'.format(year, report_type))

    report_to_run = comparative_year.get('ETLd_df')

    questions_df = comparative_year.get('questions_df')
    if set(questions_df['type']).issubset(set(['skills_matrix', ''])):
        skills_response = True
    else:
        skills_response = False

    # ##########################################################################
    # COMPOSITE REPORTS

    PEER_COMPOSITE = peer_composite_report(comparative_year.get('file'), 'Peer', report_to_run, questions_df, sorted_years[comparative_year_number]['ppl_df'], sorted_years[comparative_year_number]['na_no_null_dict'], skills_response, randomize, no_self_ratings_bool=sorted_years[comparative_year_number]['no_self_ratings_bool'], year=comparative_year_number)

    member_variable_query = dict()
    member_variable_query['comparative_year_number'] = comparative_year_number
    member_variable_query['no_self_ratings_bool'] = sorted_years[comparative_year_number]['no_self_ratings_bool']

    try:
        # member_variable_query['skills_matrix_average'] = PEER_COMPOSITE.skills_matrix_average
        # member_variable_query['number_of_skills_matrix_questions'] = PEER_COMPOSITE.number_of_skills_matrix_questions
        member_variable_query['skills_matrix_df'] = PEER_COMPOSITE.skills_matrix_export
        member_variable_query['skills_matrix_df_width'] = np.arange(1, PEER_COMPOSITE.skills_matrix_export.shape[1] + 1, 1)
    except:
        pass


    member_variable_query['section_data'] = PEER_COMPOSITE.section_data
    member_variable_query['questions_df'] = PEER_COMPOSITE.questions_df

    if sorted_years[comparative_year_number]['no_self_ratings_bool']:
            member_variable_query['number_of_respondents'] = PEER_COMPOSITE.number_of_respondents - 1
    else:
        member_variable_query['number_of_respondents'] = PEER_COMPOSITE.number_of_respondents
    member_variable_query['section_averages'] = PEER_COMPOSITE.averages
    member_variable_query['five_point_scale'] = PEER_COMPOSITE.five_point_scale
    member_variable_query['eval_stats_list_of_dicts'] = PEER_COMPOSITE.eval_stats_list_of_dicts
    member_variable_query['eval_stats_list_of_dicts_last_item'] = tuple((list(PEER_COMPOSITE.eval_stats_list_of_dicts[-1].keys())[0], PEER_COMPOSITE.eval_stats_list_of_dicts[-1].get(list(PEER_COMPOSITE.eval_stats_list_of_dicts[-1].keys())[0])))
    member_variable_query['section_avg_list'] = PEER_COMPOSITE.section_avg_list
    member_variable_query['section_avg_list_last_item'] = tuple((list(PEER_COMPOSITE.section_avg_list[-1].keys())[0], PEER_COMPOSITE.section_avg_list[-1].get(list(PEER_COMPOSITE.section_avg_list[-1].keys())[0])))

    member_variable_query['jinja_hi'] = PEER_COMPOSITE.jinja_hi
    member_variable_query['jinja_lo'] = PEER_COMPOSITE.jinja_lo
    member_variable_query['number_of_comparisons'] = 1

    member_variable_query['eSigma_value'] = PEER_COMPOSITE.composite_eSigma

    member_variable_query['comparative_hm_section_length_counts'] = PEER_COMPOSITE.hm_section_length_counts
    member_variable_query['comparative_hm_numbers_borders'] = PEER_COMPOSITE.hm_numbers_borders
    member_variable_query['comparative_heat_map_averages'] = PEER_COMPOSITE.heat_map_averages
    member_variable_query['comparative_hm_rows_directions'] = PEER_COMPOSITE.hm_rows_directions
    member_variable_query['comparative_percentage_size'] = PEER_COMPOSITE.percentage_size
    member_variable_query['comparative_section_number_merge_ranges'] = PEER_COMPOSITE.section_number_merge_ranges
    member_variable_query['comparative_section_number_averages'] = PEER_COMPOSITE.section_number_averages

    member_variable_query['header_font_size'] = 11
    if PEER_COMPOSITE.percentage_size<13 and PEER_COMPOSITE.percentage_size>10:
        member_variable_query['header_font_size'] = 10
    elif PEER_COMPOSITE.percentage_size and PEER_COMPOSITE.percentage_size<=10:
        member_variable_query['header_font_size'] = 9

    member_variable_query['number_font_size'] = 10
    if PEER_COMPOSITE.percentage_size<13 and PEER_COMPOSITE.percentage_size>10:
        member_variable_query['number_font_size'] = 8
    elif PEER_COMPOSITE.percentage_size<=10:
        member_variable_query['number_font_size'] = 7

    hm_cell_height = 10
    if PEER_COMPOSITE.number_of_respondents < 16 and PEER_COMPOSITE.number_of_respondents >= 10:
        hm_cell_height = 8
    elif PEER_COMPOSITE.number_of_respondents < 20 and PEER_COMPOSITE.number_of_respondents >= 16:
        hm_cell_height = 6
    elif PEER_COMPOSITE.number_of_respondents >= 20:
        hm_cell_height = 5
    member_variable_query['hm_cell_height'] = hm_cell_height

    if PEER_COMPOSITE.percentage_size < 2:
        member_variable_query['comparative_extra_large_hm'] = True
    else:
        member_variable_query['comparative_extra_large_hm'] = False
    ############################################################################
    compared_year_report_to_run = sorted_years[compared_year_number]['ETLd_df']
    compared_year_questions_df = sorted_years[compared_year_number]['questions_df']

    member_variable_query['compared_year_number'] = compared_year_number
    b = peer_composite_report(sorted_years.get(compared_year_number).get('file'), 'Peer', compared_year_report_to_run, compared_year_questions_df, sorted_years.get(compared_year_number).get('ppl_df'), sorted_years[compared_year_number].get('na_no_null_dict'), skills_response, randomize, no_self_ratings_bool=sorted_years[compared_year_number]['no_self_ratings_bool'], year=compared_year_number)
    try:
        # member_variable_query['b_skills_matrix_average'] = b.skills_matrix_average
        # member_variable_query['b_number_of_skills_matrix_questions'] = b.number_of_skills_matrix_questions
        member_variable_query['b_skills_matrix_df'] = b.skills_matrix_export
        member_variable_query['also_b_has_a_skills_matrix'] = True
        member_variable_query['b_skills_matrix_df_width'] = np.arange(1, b.skills_matrix_export.shape[1] + 1, 1)
    except:
        pass
    member_variable_query['compared_hm_section_length_counts'] = b.hm_section_length_counts
    member_variable_query['compared_hm_numbers_borders'] = b.hm_numbers_borders
    member_variable_query['compared_heat_map_averages'] = b.heat_map_averages
    member_variable_query['compared_hm_rows_directions'] = b.hm_rows_directions
    member_variable_query['compared_percentage_size'] = b.percentage_size

    if b.percentage_size < 2:
        member_variable_query['compared_extra_large_hm'] = True
    else:
        member_variable_query['compared_extra_large_hm'] = False

    member_variable_query['compared_section_number_merge_ranges'] = b.section_number_merge_ranges
    member_variable_query['compared_section_number_averages'] = b.section_number_averages
    member_variable_query['b_esigma'] = b.composite_eSigma

    formattedText = 'The average rating for this section is {} ({} - {}). The average rating for each question in the section ranged from {} to {}. Individual ratings ranged from {} to {}.'
    equal_formattedText = 'The average rating for each question and this section is {} ({} - {}). Individual ratings ranged from {} to {}.'
    compared_section_data = dict()

    # print(PEER_COMPOSITE.averages)
    # print(PEER_COMPOSITE.section_data)
    for section in PEER_COMPOSITE.section_data:
        if section in list(PEER_COMPOSITE.averages.keys()):
            if PEER_COMPOSITE.questionAveragesBySectionMinsAndMaxes.get(section).get('max') == PEER_COMPOSITE.questionAveragesBySectionMinsAndMaxes.get(section).get('min') and (PEER_COMPOSITE.questionAveragesBySectionMinsAndMaxes.get(section).get('max') == PEER_COMPOSITE.averages.get(section)):
                compared_section_data[section] = equal_formattedText.format(PEER_COMPOSITE.averages.get(section), b.averagtes.get(section), compared_year_number, sectionMinsAndMaxes.get(section).get('min'), sectionMinsAndMaxes.get(section).get('max'))
            else:
                compared_section_data[section] = formattedText.format(PEER_COMPOSITE.averages.get(section), b.averages.get(section), compared_year_number,
                    PEER_COMPOSITE.questionAveragesBySectionMinsAndMaxes.get(section).get('min'),
                    PEER_COMPOSITE.questionAveragesBySectionMinsAndMaxes.get(section).get('max'),
                    PEER_COMPOSITE.sectionMinsAndMaxes.get(section).get('min'),
                    PEER_COMPOSITE.sectionMinsAndMaxes.get(section).get('max'))
        else:
            compared_section_data[section] = 'The open-ended questions provide directors with an opportunity to openly and anonymously comment on their colleagues’ competency and overall performance.'

    member_variable_query['compared_section_data'] = compared_section_data

    # print(member_variable_query['compared_section_data'])

    ############################################################################
    # na_present = False
    compared_year_na_present = False
    comparative_year_na_present = False

    eval_stats_list_of_dicts = PEER_COMPOSITE.eval_stats_list_of_dicts
    for category in eval_stats_list_of_dicts:
        category_key = list(category.keys())[0]
        if category_key in ['Percent of ratings of N/A', 'Number of ratings of N/A']:
            # na_present = True
            comparative_year_na_present = True

    eval_stats_list_of_dicts = b.eval_stats_list_of_dicts
    for category in eval_stats_list_of_dicts:
        category_key = list(category.keys())[0]
        if category_key in ['Percent of ratings of N/A', 'Number of ratings of N/A']:
            # na_present = True
            compared_year_na_present = True

    # print('na_present: ', na_present)
    # print('comparative_year_na_present: ', comparative_year_na_present)
    # print('compared_year_na_present: ', compared_year_na_present)

    if b.na_in_heat_map or PEER_COMPOSITE.na_in_heat_map:
        member_variable_query['na_in_heat_map'] = True
    else:
        member_variable_query['na_in_heat_map'] = False


    ############################################################################
    eval_stats_list_of_dicts = PEER_COMPOSITE.eval_stats_list_of_dicts
    new_eval_list_of_dicts = list()
    add = False
    for category in eval_stats_list_of_dicts:
        category_key = list(category.keys())[0]
        if category_key in ['Question analysis by Board member response:', 'Group response analysis:']:
            new_eval_list_of_dicts.append({category_key: ['']*2})
            continue
        score = category.get(list(category.keys())[0], '')
        list_of_scores = list()
        list_of_scores.append(score)
        score_list = b.eval_stats_list_of_dicts
        for i in score_list:
            for k, v in i.items():
                if k == category_key:
                    list_of_scores.append(i.get(category_key, ''))
                else:
                    pass

        # If there are n/a in the current year, but not last year's
        if (category_key in ['Percent of ratings of N/A', 'Number of ratings of N/A']) and (len(list_of_scores)==1):
            if category_key == 'Percent of ratings of N/A':
                list_of_scores.append('0%')
            elif category_key == 'Number of ratings of N/A':
                list_of_scores.append('0')

        if new_eval_list_of_dicts:
            # If there are n/a in last year but not the current year

            # Doesnt add the the last item properly
            # print(compared_year_na_present,comparative_year_na_present,list(new_eval_list_of_dicts[-1].keys())[0])
            if compared_year_na_present and not comparative_year_na_present and (category_key in ['Number of ratings of 3 or below', 'Number of ratings of 2 or below']):
                for i in score_list:
                    for k, v in i.items():
                        if k == 'Number of ratings of N/A':
                            add1_list_of_scores = ['0', i.get(k)]
                            add1_category_key = 'Number of ratings of N/A'
                            add = True
            if compared_year_na_present and not comparative_year_na_present and (category_key in ['Percent of ratings of 2 or below', 'Percent of ratings of 3 or below']):
                for i in score_list:
                    for k, v in i.items():
                        if k == 'Percent of ratings of N/A':
                            add1_list_of_scores = ['0%', i.get(k)]
                            add1_category_key = 'Percent of ratings of N/A'
                            add = True

        if add:
            new_eval_list_of_dicts.append({category_key: list_of_scores})
            new_eval_list_of_dicts.append({add1_category_key: add1_list_of_scores})
            add = False
        else:
            new_eval_list_of_dicts.append({category_key: list_of_scores})

    member_variable_query['eval_stats_list_of_dicts'] = new_eval_list_of_dicts
    ############################################################################



    # member_variable_query['section_avg_list_last_item'] = tuple((list(BEA.section_avg_list[-1].keys())[0], BEA.section_avg_list[-1].get(list(BEA.section_avg_list[-1].keys())[0])))
    # section_avg_list_last_item = tuple((list(BEA.section_avg_list[-1].keys())[0], BEA.section_avg_list[-1].get(list(BEA.section_avg_list[-1].keys())[0])))
    # print('here: ', section_avg_list_last_item)

    section_avg_list = PEER_COMPOSITE.section_avg_list
    new_section_avg_list = list()
    for section in section_avg_list:
        section_key = list(section.keys())[0]
        section_avg = section.get(list(section.keys())[0], '')
        # new_section_avg_list.append({section_key: []})
        list_of_section_averages = list()
        list_of_section_averages.append(section_avg)
        for item in [b]:
            avg_list = item.section_avg_list
            for i in avg_list:
                for k, v in i.items():
                    if k == section_key:
                        # if i == avg_list[-1]:
                        #     section_avg_list_last_item[1].append(i.get(section_key, ''))
                        # else:
                        list_of_section_averages.append(i.get(section_key, ''))

                    else:
                        pass
        new_section_avg_list.append({section_key: list_of_section_averages})

    # print('new_section_avg_list: ', new_section_avg_list)
    for section in new_section_avg_list:
        if len(section.get(list(section.keys())[0], '')) == 1:
            section.get(list(section.keys())[0], '').append('')

    # print('new_section_avg_list: ', new_section_avg_list)
    # print('section_avg_list_last_item: ', section_avg_list_last_item)
    member_variable_query['section_avg_list'] = new_section_avg_list

    member_variable_query['jinja_for_loop_dict'] = sorted_years[current_year]['named']
    comparative_peer_composite_templater(comparative_year.get('file'), member_variable_query, 'Peer', report_to_run, questions_df, sorted_years[comparative_year_number]['ppl_df'], randomize = randomize, anonymized=False)

    member_variable_query['jinja_for_loop_dict'] = sorted_years[current_year]['anonymized']

    member_variable_query['comparative_hm_rows_directions'] = PEER_COMPOSITE.anon_hm_rows_directions
    member_variable_query['comparative_percentage_size'] = PEER_COMPOSITE.anon_percentage_size

    member_variable_query['compared_hm_rows_directions'] = b.anon_hm_rows_directions
    member_variable_query['compared_percentage_size'] = b.anon_percentage_size

    comparative_peer_composite_templater(comparative_year.get('file'), member_variable_query, 'Peer', report_to_run, questions_df, sorted_years[comparative_year_number]['ppl_df'], randomize = randomize, anonymized=True)
    ###########################################################################

    print('Creating Individual Peer Reports for {}\n\n'.format(current_year_file))

    question_column_width = set_question_column_width('Peer', 'Individuals')
    heat_map_section_bool = ask_create_section('Peer Individuals', 'Heat Map')
    peer_self_comparison_bool = ask_create_section('Peer Individuals', 'Peer and Self-evaluation Comparison Graph')
    hilo_section_bool = ask_create_section('Peer Individuals', 'Hi-Lo Section')
    peer_self_report_bool = ask_create_section('Peer Individuals', 'Director Peer and Self-Evaluation Report')
    statistical_analysis_bool = ask_create_section('Peer Individuals', 'Statistical Analysis')

    # question_column_width = 35
    # heat_map_section_bool = True
    # peer_self_comparison_bool = True
    # hilo_section_bool = True
    # peer_self_report_bool = True
    # statistical_analysis_bool = True

    # One question to determine whether or not the report is randomized is asked above.
    # randomize = False

    member_variable_query = dict()

    member_variable_query['comparative_year_number'] = comparative_year_number
    member_variable_query['compared_year_number'] = compared_year_number


    PEER_INDIVIDUALS = peer_individual_reports(comparative_year.get('file'), 'Peer', report_to_run, questions_df, sorted_years[comparative_year_number]['ppl_df'], sorted_years[comparative_year_number]['na_no_null_dict'], randomize = randomize, no_self_ratings_bool=sorted_years[comparative_year_number]['no_self_ratings_bool'], year=comparative_year_number)

    compared_year_report_to_run = sorted_years[compared_year_number]['ETLd_df']
    compared_year_questions_df = sorted_years[compared_year_number]['questions_df']

    member_variable_query['compared_year_number'] = compared_year_number
    b = peer_individual_reports(sorted_years.get(compared_year_number).get('file'), 'Peer', compared_year_report_to_run, compared_year_questions_df, sorted_years.get(compared_year_number).get('ppl_df'), sorted_years[compared_year_number].get('na_no_null_dict'), randomize = randomize, no_self_ratings_bool=sorted_years[compared_year_number]['no_self_ratings_bool'], year=compared_year_number)

    smoothed_df = join_qdf_and_rawdata_into_smoothed_df(questions_df, report_to_run, comparative_year.get('file'), 'Peer', False)
    for id in sorted_years[comparative_year_number]['ppl_df'].index:
        print(id)
        if id in sorted_years[compared_year_number]['ppl_df'].index:
            director_in_last_year = True
            print('director found in last year input file.')
        else:
            director_in_last_year = False
            print('director not found in last year input file!')

        member_variable_query['director_in_last_year'] = director_in_last_year

        director = PEER_INDIVIDUALS.questions_df[~(PEER_INDIVIDUALS.questions_df['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (PEER_INDIVIDUALS.questions_df['UniqueIdentifier']==id)]['sub_section'].unique()[0]
        # print('test: ', PEER_INDIVIDUALS.questions_df[~(PEER_INDIVIDUALS.questions_df['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (PEER_INDIVIDUALS.questions_df['UniqueIdentifier']==id)]['sub_section'].unique())
        # print('director: ', director)
        # Decprecated?
        # data_dict, smoothed_df = join_questions_df_and_raw_data_into_dict(questions_df, report_to_run, comparative_year.get('file'), 'Peer', composite_join=False, randomize=True, anonymize = False)

        jinja_for_loop_dict = create_comparative_peer_individuals_jinja_dict(id, director, sorted_years, smoothed_df, comparative_year.get('file'), 'Peer', no_self_ratings_bool, False, randomize)

        member_variable_query['initials'] = PEER_INDIVIDUALS.initials.get(director)

        if b.na_in_heat_maps.get(director, False) or PEER_INDIVIDUALS.na_in_heat_maps.get(director, False):
            member_variable_query['na_in_heat_map'] = True
        else:
            member_variable_query['na_in_heat_map'] = False

        member_variable_query['section_data'] = PEER_INDIVIDUALS.section_data[director]

        member_variable_query['questions_df'] = PEER_INDIVIDUALS.questions_df
        member_variable_query['jinja_for_loop_dict'] = jinja_for_loop_dict.get(director)

        member_variable_query['question_column_width'] = question_column_width
        member_variable_query['heat_map_section_bool'] = heat_map_section_bool
        member_variable_query['peer_self_comparison_bool'] = peer_self_comparison_bool
        member_variable_query['hilo_section_bool'] = hilo_section_bool
        member_variable_query['peer_self_report_bool'] = peer_self_report_bool
        member_variable_query['statistical_analysis_bool'] = statistical_analysis_bool

        # if sorted_years[comparative_year_number]['no_self_ratings_bool']:
        #     member_variable_query['number_of_respondents'] = PEER_INDIVIDUALS.number_of_respondents - 1
        # else:
        #     member_variable_query['number_of_respondents'] = PEER_INDIVIDUALS.number_of_respondents

        member_variable_query['number_of_respondents'] = PEER_INDIVIDUALS.number_of_respondents

        member_variable_query['section_averages'] = PEER_INDIVIDUALS.averages_data[director]
        member_variable_query['five_point_scale'] = PEER_INDIVIDUALS.five_point_scale
        member_variable_query['eval_stats_list_of_dicts'] = PEER_INDIVIDUALS.agg_section_data[director]
        member_variable_query['eval_stats_list_of_dicts_last_item'] = tuple((list(PEER_INDIVIDUALS.agg_section_data[director][-1].keys())[0], PEER_INDIVIDUALS.agg_section_data[director][-1].get(list(PEER_INDIVIDUALS.agg_section_data[director][-1].keys())[0])))
        member_variable_query['section_avg_list'] = PEER_INDIVIDUALS.section_avg_list[director]
        member_variable_query['section_avg_list_last_item'] = tuple((list(PEER_INDIVIDUALS.section_avg_list[director][-1].keys())[0], PEER_INDIVIDUALS.section_avg_list[director][-1].get(list(PEER_INDIVIDUALS.section_avg_list[director][-1].keys())[0])))
        member_variable_query['jinja_hi'] = PEER_INDIVIDUALS.hilo_data[director]['jinja_hi']
        member_variable_query['jinja_lo'] = PEER_INDIVIDUALS.hilo_data[director]['jinja_lo']
        member_variable_query['number_of_comparisons'] = 1

        ############################################################################
        member_variable_query['comparative_hm_section_length_counts'] = PEER_INDIVIDUALS.heat_map_data[director].get('hm_section_length_counts')
        member_variable_query['comparative_hm_numbers_borders'] = PEER_INDIVIDUALS.heat_map_data[director].get('hm_numbers_borders')
        member_variable_query['comparative_heat_map_averages'] = PEER_INDIVIDUALS.heat_map_data[director].get('heat_map_averages')
        member_variable_query['comparative_hm_rows_directions'] = PEER_INDIVIDUALS.heat_map_data[director].get('hm_rows_directions')
        member_variable_query['comparative_percentage_size'] = PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')
        member_variable_query['hm_cell_height'] = PEER_INDIVIDUALS.hm_cell_height

        if director_in_last_year:
            try:
                member_variable_query['compared_hm_section_length_counts'] = b.heat_map_data[director].get('hm_section_length_counts')
                member_variable_query['compared_hm_numbers_borders'] = b.heat_map_data[director].get('hm_numbers_borders')
                member_variable_query['compared_heat_map_averages'] = b.heat_map_data[director].get('heat_map_averages')
                member_variable_query['compared_hm_rows_directions'] = b.heat_map_data[director].get('hm_rows_directions')
                member_variable_query['compared_percentage_size'] = b.heat_map_data[director].get('percentage_size')
                # member_variable_query['director_in_last_year_hm'] = True
            except:
                pass
                # member_variable_query['director_in_last_year_hm'] = False

        if director_in_last_year:
            try:
                member_variable_query['last_year_skills_matrix_df'] = b.skills_matrix_df[director]
                member_variable_query['last_year_skills_matrix_info'] = b.skills_matrix_info[director]
            except:
                pass

        member_variable_query['header_font_size'] = 11
        if PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')<13 and PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')>10:
            member_variable_query['header_font_size'] = 10
        elif PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')<=10:
            member_variable_query['header_font_size'] = 9

        member_variable_query['number_font_size'] = 10
        if PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')<13 and PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')>10:
            member_variable_query['number_font_size'] = 8
        elif PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')<=10:
            member_variable_query['number_font_size'] = 7

        ############################################################################
        member_variable_query['eSigma_value'] = PEER_INDIVIDUALS.esigmas[director]
        if director_in_last_year:
            member_variable_query['b_esigma'] = b.esigmas.get(director, 'N/A')
        else:
            member_variable_query['b_esigma'] = ''

        ############################################################################

        member_variable_query['no_self_ratings_bool'] = sorted_years[comparative_year_number]['no_self_ratings_bool']


        ############################################################################
        eval_stats_list_of_dicts = PEER_INDIVIDUALS.agg_section_data[director]
        new_eval_list_of_dicts = []
        add = False
        if director_in_last_year:
            # print(score_list)
            # print(comparative_year_na_present, compared_year_na_present)
            for category in eval_stats_list_of_dicts:
                category_key = list(category.keys())[0]
                # print(category)
                if category_key in ['Question analysis by Board member response:', 'Group response analysis:']:
                    new_eval_list_of_dicts.append({category_key: ['']*2})
                    continue
                score = category.get(list(category.keys())[0], '')
                list_of_scores = list()
                list_of_scores.append(score)
                for item in [b]:
                    # UPDATED
                    score_list = item.agg_section_data.get(director, '')
                    for i in score_list:
                        for k, v in i.items():
                            if k == category_key:
                                list_of_scores.append(i.get(category_key, ''))
                            else:
                                pass

                # If there are n/a in the current year, but not last year's
                if (category_key in ['Percent of ratings of N/A', 'Number of ratings of N/A']) and (len(list_of_scores)==1):
                    if category_key == 'Percent of ratings of N/A':
                        list_of_scores.append('0%')
                    elif category_key == 'Number of ratings of N/A':
                        list_of_scores.append('0')

                if new_eval_list_of_dicts:
                    # If there are n/a in last year but not the current year
                    if compared_year_na_present and not comparative_year_na_present and (category_key in ['Number of ratings of 3 or below', 'Number of ratings of 2 or below']):
                        for i in score_list:
                            for k, v in i.items():
                                if k == 'Number of ratings of N/A':
                                    add1_list_of_scores = ['0', i.get(k)]
                                    add1_category_key = 'Number of ratings of N/A'
                                    add = True

                    elif compared_year_na_present and not comparative_year_na_present and (category_key in ['Percent of ratings of 2 or below', 'Percent of ratings of 3 or below']):
                        for i in score_list:
                            for k, v in i.items():
                                if k == 'Percent of ratings of N/A':
                                    add1_list_of_scores = ['0%', i.get(k)]
                                    add1_category_key = 'Percent of ratings of N/A'
                                    add = True

                if add:
                    new_eval_list_of_dicts.append({category_key: list_of_scores})
                    new_eval_list_of_dicts.append({add1_category_key: add1_list_of_scores})
                    add = False
                else:
                    new_eval_list_of_dicts.append({category_key: list_of_scores})
        else:
            for category in eval_stats_list_of_dicts:
                category_key = list(category.keys())[0]
                if category_key in ['Question analysis by Board member response:', 'Group response analysis:']:
                    new_eval_list_of_dicts.append({category_key: ['']*2})
                    continue
                score = category.get(list(category.keys())[0], '')
                list_of_scores = list()
                list_of_scores.append(score)
                list_of_scores.append('')

                new_eval_list_of_dicts.append({category_key: list_of_scores})



        member_variable_query['eval_stats_list_of_dicts'] = new_eval_list_of_dicts
        ############################################################################



        # member_variable_query['section_avg_list_last_item'] = tuple((list(BEA.section_avg_list[-1].keys())[0], BEA.section_avg_list[-1].get(list(BEA.section_avg_list[-1].keys())[0])))
        # section_avg_list_last_item = tuple((list(BEA.section_avg_list[-1].keys())[0], BEA.section_avg_list[-1].get(list(BEA.section_avg_list[-1].keys())[0])))
        # print('here: ', section_avg_list_last_item)

        section_avg_list = PEER_INDIVIDUALS.section_avg_list
        new_section_avg_list = list()

        if director_in_last_year:
            for section in section_avg_list[director]:
                section_key = list(section.keys())[0]
                section_avg = section.get(list(section.keys())[0], '')
                # new_section_avg_list.append({section_key: []})
                list_of_section_averages = list()
                list_of_section_averages.append(section_avg)
                for item in [b]:
                    avg_list = item.section_avg_list.get(director, '')
                    for i in avg_list:
                        for k, v in i.items():
                            if k == section_key:
                                # if i == avg_list[-1]:
                                #     section_avg_list_last_item[1].append(i.get(section_key, ''))
                                # else:
                                list_of_section_averages.append(i.get(section_key, ''))

                            else:
                                pass
                new_section_avg_list.append({section_key: list_of_section_averages})
        else:
            for section in section_avg_list[director]:
                section_key = list(section.keys())[0]
                section_avg = section.get(list(section.keys())[0], '')
                # new_section_avg_list.append({section_key: []})
                list_of_section_averages = list()
                list_of_section_averages.append(section_avg)
                list_of_section_averages.append('')
                new_section_avg_list.append({section_key: list_of_section_averages})

        # print(new_section_avg_list)
        # section_avg_list_last_item = tuple((list(BEA.section_avg_list[-1].keys())[0], [a for a in new_section_avg_list[-1].get(list(new_section_avg_list[-1].keys())[0])]))


        # print('new_section_avg_list: ', new_section_avg_list)
        # print('section_avg_list_last_item: ', section_avg_list_last_item)
        member_variable_query['section_avg_list'] = new_section_avg_list
        member_variable_query['eval_stats_list_of_dicts'] = new_eval_list_of_dicts

        try:
            member_variable_query['this_year_skills_matrix_df'] = PEER_INDIVIDUALS.skills_matrix_df[director]
            member_variable_query['this_year_skills_matrix_info'] = PEER_INDIVIDUALS.skills_matrix_info[director]
            member_variable_query['self_skills_present'] = True
        except:
            member_variable_query['self_skills_present'] = False

        comparative_peer_individuals_templater(comparative_year.get('file'), member_variable_query, 'Peer Individuals', report_to_run, questions_df, sorted_years[comparative_year_number]['ppl_df'], director_name=director, randomize = randomize)



    #####################################
    print('\n\n--- Run Time ---')
    print('--- %s seconds ---' % (time.time() - start_time))
