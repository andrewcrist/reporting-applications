from flask import Flask
import os
app = Flask(__name__)

if app.config['ENV'] == 'production':
    app.config.from_object("config.ProductionConfig")
elif app.config['ENV'] == 'testing':
    app.config.from_object("config.TestingConfig")
else:
    app.config.from_object("config.DevelopmentConfig")

# app.config['CSV_UPLOADS'] = os.path.join(app.root_path, 'static\\client\\csv')
# app.config['EXCEL_OUTPUT'] = os.path.join(app.root_path, 'static\\client\\excel')
# app.config['ZIP_OUTPUT'] = os.path.join(app.root_path, 'static\\client\\ZIPS')

app.config['CSV_UPLOADS'] = os.path.join(app.root_path, 'static/client/csv')
app.config['EXCEL_OUTPUT'] = os.path.join(app.root_path, 'static/client/excel')
app.config['ZIP_OUTPUT'] = os.path.join(app.root_path, 'static/client/ZIPS')

from app import views
from app import error_handlers

