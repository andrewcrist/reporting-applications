from app import app
import os

from flask import render_template, request

@app.errorhandler(404)
def not_found(e):
    print(e)
    return render_template("public/404.html")

@app.errorhandler(500)
def server_error(e):
    app.logger.error(f"Server error: {e}, route: {request.url}")
    for file in os.listdir(app.config["EXCEL_OUTPUT"]):
        if file.endswith(".xlsx"):
            os.remove(os.path.join(app.config["EXCEL_OUTPUT"], file))
    for file in os.listdir(app.config["CSV_UPLOADS"]):
        if file.endswith(".csv"):
            os.remove(os.path.join(app.config["CSV_UPLOADS"], file))
    # Insert error emailer

    return render_template("public/500.html")

@app.errorhandler(403)
def forbidden(e):
    app.logger.error(f": {e}, route: {request.url}")

    return render_template("public/403.html")    