from app import app
from flask import render_template, request, redirect, jsonify, make_response, send_from_directory, abort, session, url_for, flash
from datetime import datetime
import os
from werkzeug.utils import secure_filename
import secrets
import re
import zipfile
from os.path import basename

from .ppl_parser import ppl_parser
from.csv_opener import csv_opener

from .fsr_etl import fsr_etl
from .fsr_report import fsr_report

# @app.route("/")
# def index():
#     # print(f"\nFlask Env is set to: {app.config['ENV']}")
#     return redirect(url_for('fsr_uploader'))

@app.route("/delete", methods=['POST'])
def delete_cache():
    for file in os.listdir(app.config["ZIP_OUTPUT"]):
        if file.endswith(".zip"):
            os.remove(os.path.join(app.config["ZIP_OUTPUT"], file))
    for file in os.listdir(app.config["EXCEL_OUTPUT"]):
        if file.endswith(".xlsx"):
            os.remove(os.path.join(app.config["EXCEL_OUTPUT"], file))
    for file in os.listdir(app.config["CSV_UPLOADS"]):
        if file.endswith(".csv"):
            os.remove(os.path.join(app.config["CSV_UPLOADS"], file))              

    return redirect(url_for('index'))

@app.route("/downloadfile/<filename>", methods = ['GET'])
def download_excel_output(filename):
    return send_from_directory(app.config["EXCEL_OUTPUT"], filename=filename, as_attachment=True, attachment_filename='')

@app.route("/downloadzip/<filename>", methods = ['GET'])
def download_zip_output(filename):
    return send_from_directory(app.config["ZIP_OUTPUT"], filename=filename, as_attachment=True, attachment_filename='')

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'modified' not in request.files or 'plain' not in request.files:
            return redirect(request.url)
        modified = request.files['modified']
        plain = request.files['plain']
        create_section_headers = request.form['create_section_headers']
        create_modified_instance = request.form['create_modified_instance']
        # print('section headers', section_headers)
        # print('modified instance', modified_instance)

        # if user does not select file, browser also
        # submit a empty part without filename
        if modified.filename == '' or plain.filename == '':
            return redirect(request.url)
        else:
            modified_filename = secure_filename(modified.filename)
            plain_filename = secure_filename(plain.filename)

            modified.save(os.path.join(app.config['CSV_UPLOADS'], modified_filename))
            plain.save(os.path.join(app.config['CSV_UPLOADS'], plain_filename))

            fsr = run_fsr(plain_filename, modified_filename, create_section_headers, create_modified_instance)
            fsr_output_filename = fsr.output_filename

            for file in os.listdir(app.config["CSV_UPLOADS"]):
                if file.endswith(".csv"):
                    os.remove(os.path.join(app.config['CSV_UPLOADS'], file))

            with zipfile.ZipFile(os.path.join(app.config['ZIP_OUTPUT'], '{}.zip'.format(fsr_output_filename)),'w') as zip:
                for report in fsr.output_file_names:
                    zip.write(os.path.join(app.config['EXCEL_OUTPUT'], report), basename(report))

            delete_excel_output()
            return redirect('/downloadzip/'+ '{}.zip'.format(fsr_output_filename))

    return render_template('public/fsr.html')

def run_fsr(plain_filename, modified_filename, create_section_headers, create_modified_instance):
    plain_path = os.path.join(app.config['CSV_UPLOADS'], plain_filename)
    modified_path = os.path.join(app.config['CSV_UPLOADS'], modified_filename)

    pdf_raw = csv_opener(file = plain_path)
    mdf_raw = csv_opener(file = modified_path)

    mdf, mquestions_df, mdict_of_dfs, symbol  = fsr_etl(mdf_raw)
    pdf, pquestions_df, pdict_of_dfs, symbol = fsr_etl(pdf_raw)

    ppl_df = ppl_parser(pdf_raw)
    print_headers = create_section_headers

    fsr = fsr_report(pdf, mdf, ppl_df, mquestions_df, pdict_of_dfs, mdict_of_dfs, print_headers, app, modified_filename, create_modified_instance)

    return fsr

def delete_excel_output():
    for file in os.listdir(app.config["EXCEL_OUTPUT"]):
        if file.endswith(".xlsx"):
            os.remove(os.path.join(app.config["EXCEL_OUTPUT"], file))
