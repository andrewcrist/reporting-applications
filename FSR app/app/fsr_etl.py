import pandas as pd
import numpy as np
import os
import glob
import re
from random import shuffle
from collections import Counter
import warnings
import xlsxwriter
from functools import reduce
from statistics import mean
import math
import sys
import itertools
import difflib
from pprint import pprint
import logging
import time
logging.basicConfig(level=os.environ.get("WARNING", "INFO"))

pd.options.mode.chained_assignment = None
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
np.seterr(all='raise')


def fsr_etl(dictionary):
    raw = pd.DataFrame.from_dict(dictionary)
    print('\nRaw Shape: ', raw.shape)


    if  'UniqueIdentifier' in raw.columns:
        raw.set_index('UniqueIdentifier', inplace=True)
    columns = raw.columns

    # Identify where the data begins and ends.
    # Identify what kind of survey style is in use
    re_alpha_first_col = re.compile('^(?!.*\(Continued\))[A-Za-z\s\t]*(Part)?[\t\s]*(A)[\s\t]?[\.:‐‑‒–—―]')
    alpha_firstCols = list(filter(re_alpha_first_col.match, columns))
    re_roman_first_col = re.compile('^(?!.*\(Continued\))[A-Za-z\s\t]*(Part)?[\t\s]*(I)[\s\t]?[\.:‐‑‒–—―]')
    roman_firstCols = list(filter(re_roman_first_col.match, columns))
    re_arabic_first_col = re.compile('^(?!.*\(Continued\))[A-Za-z\s\t]*(Part)?[\t\s]*(1)[\s\t]?[\.:‐‑‒–—―]')
    arabic_firstCols = list(filter(re_arabic_first_col.match, columns))


    # Some reports have data that proceed the first instance of section symbology
    # Example: GENERAL INFORMATION
    
    re_multipleWords_first_col = re.compile('\w+[\s\t]+\w+')
    multipleWords = list(filter(re_multipleWords_first_col.match, columns))
    re_one_word_ends_with_colon = re.compile('\w+:')
    oneWordColon = list(filter(re_one_word_ends_with_colon.match, columns))
    # Sometimes more than one style will be found
    first_col_list = []
    symbols = []
    if alpha_firstCols:
        alpha_firstCol = alpha_firstCols[0]
        alpha_symbol = re_alpha_first_col.match(alpha_firstCol).group(2)
        first_col_list.append(alpha_firstCol)
        symbols.append(alpha_symbol)
    if roman_firstCols:
        roman_firstCol = roman_firstCols[0]
        roman_symbol = re_roman_first_col.match(roman_firstCol).group(2)
        first_col_list.append(roman_firstCol)
        symbols.append(roman_symbol)
    if arabic_firstCols:
        arabic_firstCol = arabic_firstCols[0]
        arabic_symbol = re_arabic_first_col.match(arabic_firstCol).group(2)
        first_col_list.append(arabic_firstCol)
        symbols.append(arabic_symbol)
    if multipleWords:
        multipleWords_first_col = multipleWords[0]
        first_col_list.append(multipleWords_first_col)
        symbols.append('')
    if oneWordColon:
        oneWordFirstColumn = oneWordColon[0]
        first_col_list.append(oneWordFirstColumn)
        symbols.append('w')

    if len(symbols)==0:
        logging.warning('No symbology found!')


    # It's important to note here that, above, multipleWords is tested for last.
    # Because multipleWords is appended to first_col_list last, the first item in
    # first_col_list shouldn't be ''.

    # If there are reports using no symbology style, then the loop below will
    # need to be changed, and the last condition below resulting in:
    # logging.warning('\nNO SYMBOL!\n')
    # will need to be swapped out for handling of word column symobology
    for fc, sym in zip(first_col_list, symbols):
        if fc == first_col_list[0]:
            first_col_number = raw.columns.get_loc(fc)
            first_col_name = fc
            if sym != '' and sym != 'w':
                symbol = sym
        else:
            if raw.columns.get_loc(fc) < first_col_number:
                first_col_number = raw.columns.get_loc(fc)
                first_col_name = fc
                if sym != '' and sym != 'w':
                    symbol = sym
            else:
                continue

    # Trim down to the raw data
    data = raw.iloc[:, first_col_number:]
    questions_df = pd.DataFrame({'question_raw': data.columns})
    header_symbol_list = []
    questions = []
    header_list = []
    re_scrubbed_continued = re.compile('.*\(continued\)[\t\s]*(.*)', re.IGNORECASE)
    # re_headers only works in conjunction with each version of re_sections
    re_headers = re.compile('(.*?)\s\s')
    re_no_space_header_for_arabic = re.compile('(.*?\d+[\.:]?)')
    # re_test = re.compile('(.*?\d+[\s\t]?[\.:—–][\s\t]?.*?)(\s\s|(?<!:))')

    # Do the work of identifying
    for i, value in zip(questions_df.index, questions_df['question_raw']):
        if symbol == 'A':
            # section_name_with_number_and_content_between = re.compile('(?=(^|^[\t\s]*|^Part[\s\t]+)(^[A-Z]\.\s+[/A-Z0-9\(\)"\'\s\t\.-]+)[\t\s]*(_<)?[A-Z]?[a-z]+.*1\.\s+.*')
            re_sections = re.compile('^(?!.*\(Continued\))[\t\s]*(Part[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([A-Z]{1,2})[\s\t]?[\.:—–][\s\t]?(.*)')
            section_groups = re_sections.match(value)
            # print('\nvalue:', value)
            scrubbed_continued = re_scrubbed_continued.match(value)
            if section_groups is not None:
                # print('\nvalue:', value)
                # print(section_groups.groups())
                if section_groups.group(3) not in set(header_symbol_list):
                    header_symbol_list.append(section_groups.group(3))
                    header = re_headers.match(value)
                    if header is not None:
                        header_list.append(header.group(1))
                    else:
                        logging.warning('No formatted header found')
                        header_list.append(None)
                else:
                    header_symbol_list.append(None)
                    header_list.append(None)
                # print('\nvalue:', value)
                # print(section_groups.groups())
                questions.append(section_groups.group(4))
            elif scrubbed_continued is not None:
                # print('\n')
                # print(scrubbed_continued.group(0))
                # print(scrubbed_continued.group(1))
                questions.append(scrubbed_continued.group(1))
                header_symbol_list.append(None)
                header_list.append(None)
            else:
                header_symbol_list.append(None)
                questions.append(value)
                header_list.append(None)

        elif symbol == 'I':
            # re_sections = re.compile('^(?!.*\((?i:continued)\))[\t\s]*((?i:part)[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([IVX]+)[\s\t]?[\.:—-](.*)')
            re_sections = re.compile('^(?!.*\((?i:continued)\))[\t\s]*((?i:part)[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([IVX]+)[\s\t]?[\.:—–][\s\t]?(.*)')
            section_groups = re_sections.match(value)
            scrubbed_continued = re_scrubbed_continued.match(value)
            if section_groups is not None:
                if section_groups.group(3) not in set(header_symbol_list):
                    header_symbol_list.append(section_groups.group(3))
                    header = re_headers.match(value)
                    if header is not None:
                        header_list.append(header.group(1))
                    else:
                        logging.warning('No formatted header found')
                        header_list.append(None)
                else:
                    header_symbol_list.append(None)
                    header_list.append(None)
                questions.append(section_groups.group(4))
            elif scrubbed_continued is not None:
                questions.append(scrubbed_continued.group(1))
                header_symbol_list.append(None)
                header_list.append(None)
            else:
                header_symbol_list.append(None)
                questions.append(value)
                header_list.append(None)
        elif symbol == '1':
            re_sections = re.compile('^(?!.*\(Continued\))[\t\s]*(Part[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([\d]+)[\s\t]?[\.:—–][\s\t]?(?!\d)(.*)')
            section_groups = re_sections.match(value)
            scrubbed_continued = re_scrubbed_continued.match(value)
            if section_groups is not None:

                if section_groups.group(3) not in set(header_symbol_list):
                    header_symbol_list.append(section_groups.group(3))
                    header = re_headers.match(value)
                    # header = re_test.match(value)
                    no_space_header = re_no_space_header_for_arabic.match(value)
                    if header is not None:
                        header_list.append(header.group(1))
                    elif no_space_header is not None:
                        header_list.append(no_space_header.group(1))
                    else:

                        header_list.append(None)
                else:
                    header_symbol_list.append(None)
                    header_list.append(None)
                # print('\nvalue:', value)
                # print(section_groups.groups())
                questions.append(section_groups.group(4))
            elif scrubbed_continued is not None:

                questions.append(scrubbed_continued.group(1))
                header_symbol_list.append(None)
                header_list.append(None)
            else:
                header_symbol_list.append(None)
                questions.append(value)
                header_list.append(None)


        else:
            logging.warning('\nNO SYMBOL STYLE FOUND!\n')

    questions_df['header_symbol'] = header_symbol_list
    questions_df['question'] = questions
    questions_df['header'] = header_list


    cert_col = re.compile('(?!.*blank_column_)(?=.*(CERTIFICATION|ACKNOWLEDGEMENT|ACKNOWLEDGE))(?!.*__\d+$)(.*?)_(.*)_([\w]+)?', re.IGNORECASE)
    cert_section_list = []
    for i, raw_question in zip(questions_df.index, questions_df['question_raw']):
        cert_match = cert_col.match(raw_question)
        if cert_match is not None and cert_match.group(2) not in cert_section_list:
            # print('\ngroup2',cert_match.group(2))
            cert_section_list.append(cert_match.group(2))

            questions_df.at[i, 'header_symbol'] = 'cert'
            questions_df.at[i, 'header'] = cert_match.group(2)
            questions_df.at[i, 'question'] = cert_match.group(3)

            # print('\nstart header')
            # print(cert_match.groups())
        elif cert_match is not None:
            questions_df.at[i, 'question'] = cert_match.group(3)
            # print('\nnot beginning header')
            # print(cert_match.groups())
        else:
            pass

    dict_of_dfs = {}

    # print('SHAPE', data.shape, data.columns)

    # Check that the last instance of row/column data is retrieved
    ##########################
    # matrix comp re
    # re_row_multiValued = re.compile('(.*?)/RowHeader(\d+):+(.*?);(?=[^;]*?(/RowHeader)\d+|$)')
    ##########################
    re_detectsEverythingExceptForResponseCasewsemiw = re.compile('(.*?[^\s\t]/[^\s\t].*?:):*(.*?);(?=([^\s\t]|$))')
    re_row_multiValued = re.compile('(.*?)(/(RowHeader)\d+:+)(.*?);(?=[^;]*?(/RowHeader)\d+|$)')
    re_col_multiValued = re.compile('ColumnHeader\d+/(.*?:):?(?=([^:]*?)(;ColumnHeader\d+/|;$))')
    # re_unlabeled_multiValued_original = re.compile('(.*?/.*?):+(.*?);')
    # re_unlabeled_multiValued_usefulInDetectingListsOfRelativeInformation = re.compile('([^\s]*?/[^\s]*?):+(.*?);(?=[^\s;]*?/[^\s]*?:+|$)')
    type_flag = ''
    for q, i in zip(questions_df['question_raw'], questions_df.index):
        if (data[q].apply(lambda x: bool(re_row_multiValued.match(x))).any()) | (data[q].apply(lambda x: bool(re_col_multiValued.match(x))).any()) | (data[q].apply(lambda x: bool(re_detectsEverythingExceptForResponseCasewsemiw.match(x))).any()):
            # print(q, i)
            question_dict = {}
            # Iterate through the identified column's rows
            for ind, row in zip(data.index, data[q]):
                # print(ind, row=='')
                if re_row_multiValued.match(row) is not None:
                    multi = re_row_multiValued.findall(row)
                    type_flag = 'row'
                elif re_col_multiValued.match(row) is not None:
                    multi = re_col_multiValued.findall(row)
                    type_flag = 'col'
                elif re_detectsEverythingExceptForResponseCasewsemiw.match(row) is not None:
                    multi = re_detectsEverythingExceptForResponseCasewsemiw.findall(row)
                    type_flag = 'boundMatrix'
                elif row=='':
                    continue
                else:
                    print(row)
                    logging.warning('\n\n--------\nERROR\nCould not distinguish between col and row multiheaders')

                # For each respondent that has matrix responses, populate a dataframe
                # that contains questions, responses, and 'group' values.
                # A group value refers to a grouping of questions. Groups of questions
                # often repeat. We will want to examine these groupings, so we'll label
                # them here.
                ###########################
                # Matrix comp df
                # individual_df = pd.DataFrame(columns=['question', 'value', 'group'])
                ###########################
                individual_df = pd.DataFrame(columns=['question', 'value'])
                for ii, m in enumerate(multi):
                    if type_flag == 'row':
                        individual_df.loc[ii] = [m[0], m[3]]
                    elif type_flag == 'col':
                        individual_df.loc[ii] = [m[0], m[1]]
                    elif type_flag == 'boundMatrix':
                        individual_df.loc[ii] = [m[0], m[1]]
                    else:
                        print('ERROR')
                nbr_of_unique_questions = individual_df['question'].nunique()
                if type_flag == 'row':
                    if (individual_df['question'].nunique() != len(individual_df['question'])) and (set(individual_df.iloc[-nbr_of_unique_questions:]['value'].tolist()).issubset([None, ''])):
                        while (set(individual_df.iloc[-individual_df['question'].nunique():]['value'].tolist()).issubset(['', None])) and (not individual_df.empty):
                            individual_df.drop(individual_df.tail(individual_df['question'].nunique()).index,inplace=True)
                elif type_flag == 'col':
                    pass
                elif type_flag == 'boundMatrix':
                    pass
                else:
                    print('ERROR')

                question_dict[ind] = individual_df
            if type_flag == 'row':
                question_dict['meta_data'] = ['rowTypeData']
            elif type_flag == 'col':
                question_dict['meta_data'] = ['columnTypeData']
            elif type_flag == 'boundMatrix':
                question_dict['meta_data'] = ['boundMatrix']
            else:
                logging.warning('type_flag error')
            dict_of_dfs[i] = question_dict

    return data, questions_df, dict_of_dfs, symbol
