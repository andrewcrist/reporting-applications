from jinja2 import Template, Environment, PackageLoader, select_autoescape, FileSystemLoader
import datetime
import win32com.client as win32
from PIL import ImageGrab
import os
import excel2img
import time
import sys
import glob
import pandas as pd
from weasyprint import HTML, CSS
from decimal import Decimal, ROUND_HALF_UP
import pandas as pd
import numpy as np
from collections import Counter
import re

sys.path.insert(1, r'C:\Users\Drew\Nasdaq_Classes_and_Fns')
from ppl_parser import ppl_parser
from csv_opener import csv_opener

from bea_etl import bea_splitter, bea_etl
from bea_report import bea_report


def BEA_templater(file_name, member_variable_query, report_type, df, questions_df, ppl_df, not_applicable_count, not_observed_count, swot=True, highlights_actionpoints=True, randomize = True):
    now = datetime.datetime.now()
    formatted_date = str(now.strftime("%B")) + ' ' + str(now.day) + ', ' + str(now.year)

    excel2img.export_img("XYZ_BEA.xlsx", r'images\heat_map.png', "Heat Map", "A1:AL13")
    excel2img.export_img("XYZ_BEA.xlsx", r'images\aggregated_section_analysis.png', "Section Analysis", "A1:B37")
    # excel2img.export_img("XYZ_BEA.xlsx", r'images\aggregated_section_analysis.png', "Section Analysis", "A1:B37")

    file_loader =FileSystemLoader('templates')
    env = Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)
    template = env.get_template('bea_report_template.html')

    skills_matrix_present = 'skills_matrix' in questions_df['type'].unique()

    # print(skills_matrix_present)
    print('\n\nswot: ', swot, '\nhighlights: ', highlights_actionpoints)
# 'client_logo': r'images/client_logo.png'
    jinja_input_dict = {'title': 'test', 'nasdaq_logo': r'images/nasdaq_logo.png',
         'board_name': 'XYZ', 'year': str(now.year), 'formatted_date': formatted_date, 'skills_matrix_present': skills_matrix_present,
         'heat_map_image': r'images/heat_map.png', 'questions_df': questions_df, 'swot': swot, 'highlights_actionpoints': highlights_actionpoints,
         'swot_s': r'images/swot_s.png', 'swot_w': r'images/swot_w.png', 'swot_o': r'images/swot_o.png', 'swot_t': r'images/swot_t.png',
         'scale_image': r'images/scale.png', 'green_flag': r'images/green_flag.png', 'yellow_flag': r'images/yellow_flag.png', 'red_flag': r'images/red_flag.png'}

    # Get Assessment sections labels and section names
    section_names_and_labels = {}
    label_re = re.compile(r'^[\s\t]*([IVX]+\.)[\s\t]*(.*)')
    for section in questions_df['section'].unique().tolist():
        label = label_re.match(section)
        section_names_and_labels[label.group(1)] = label.group(2)
    jinja_input_dict['section_names_and_labels'] = section_names_and_labels

    # Control which sections are in the table of contents
    jinja_input_dict['table_of_contents_titles'] = ['#heat_map_title', '#hilo_title', '#CGPG_method_title', '#CG_flag_title', '#statistical_analysis_title']

    optional_sections = []
    if member_variable_query.get('skills_matrix_average') is not None:
        optional_sections.append('#skills_matrix_title')
    if swot:
        optional_sections.append('#swot_title')
    if highlights_actionpoints:
        optional_sections.append('#highlights_actionpts_title')

    if optional_sections:
        index = 2
        for optional_section in optional_sections:
            print(optional_section)
            jinja_input_dict['table_of_contents_titles'].insert(index, optional_section)
            index+=1

    client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file_name)
    jinja_input_dict['client_name'] = client_name.group(1)

    jinja_input_dict['number_of_sections'] = len(questions_df['section'].unique().tolist())
    jinja_input_dict['number_of_long_form_questions'] = len(questions_df[questions_df['type']=='long_form'])
    jinja_input_dict['number_of_rated_questions'] = len(questions_df['question_raw']) - len(questions_df[questions_df['type']=='long_form']) - len(questions_df[questions_df['type']=='skill_matrix']) - len(questions_df[questions_df['type']=='attribute_matrix'])

    jinja_input_dict['agg_sectional_analsysis_image'] = r'images\aggregated_section_analysis.png'

    jinja_input_dict['report_type'] = report_type

    for item in member_variable_query.keys():
        jinja_input_dict['{}'.format(item)] = member_variable_query[item]

    output = template.render(jinja_input_dict)
    with open("test.html", "w") as fh:
        fh.write(output)

    # print(member_variable_query['data_dict'])
    HTML(string=output, base_url='.').write_pdf('{}_{}.pdf'.format(client_name.group(1), report_type), stylesheets=[CSS(r'static\bea_template_style.css')])

def join_questions_df_and_raw_data_into_dict(qdf, tdf):
    data_dict = dict()
    rankFlag = False
    for s in qdf['section'].unique():
        data_dict[s] = {}
        for q in qdf[qdf['section']==s]['question_raw']:
            data_dict[s][q] = {}
            data_dict[s][q]['type'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item()
            data_dict[s][q]['number'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item()
            data_dict[s][q]['question'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item()
            data_dict[s][q]['subsection'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['subsection'].item()
            if qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['matrix', 'likert']:
                rankFlag = False
                answers = tdf[q].replace({np.nan: None})
                answers.fillna('N/A', inplace=True)
                answers = answers.tolist()
                answers = [int(x) if x != 'N/A' else x for x in answers]
                data_dict[s][q]['answers'] = answers
                data_dict[s][q]['average'] = str(Decimal(np.mean(tdf[q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            elif qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['long_form']:
                rankFlag = False
                answers = tdf[q].dropna().tolist()
                answers = [a for a in answers if a not in ['nan', '#NAME?']]
                data_dict[s][q]['answers'] = answers
            elif qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['rank']:
                if rankFlag == False:
                    rankFlag = True
                    count = 1
                    data_dict[s][q]['count'] = count
                    lengthOfRankedChoiceSection = len(qdf[(qdf['section']==s) & (qdf['number']==data_dict[s][q]['number'])])
                    data_dict[s][q]['lengthOfRankedChoiceSection'] = lengthOfRankedChoiceSection
                    data_dict[s][q]['ranks'] = list(np.arange(1, lengthOfRankedChoiceSection+1, 1))
                    data_dict[s][q]['first'] = True

                    dictonaryOfCounts = dict(Counter(tdf[q].tolist()))
                    listOfOrderedCounts = list()
                    for value in np.arange(1, lengthOfRankedChoiceSection+1, 1):
                        listOfOrderedCounts.append(dictonaryOfCounts.get(value, 0))
                    data_dict[s][q]['answers'] = listOfOrderedCounts
                else:
                    rankFlag = True
                    count += 1
                    data_dict[s][q]['count'] = count
                    data_dict[s][q]['first'] = False

                    dictonaryOfCounts = dict(Counter(tdf[q].tolist()))
                    listOfOrderedCounts = list()
                    for value in np.arange(1, lengthOfRankedChoiceSection+1, 1):
                        listOfOrderedCounts.append(dictonaryOfCounts.get(value, 0))
                    data_dict[s][q]['answers'] = listOfOrderedCounts

    return data_dict

if __name__ == "__main__":
    start_time = time.time()
    print(sys.version)
    print('\n------------------------------------------\n')

    cwd = os.getcwd()

    extension = 'csv'
    file = glob.glob("*.{}".format(extension))[0]

    print('\n-----------------\nProccessing BEA File...')
    print('File Name: ', file, '\n\n')

    raw_data_dict = csv_opener(file)
    dict_of_dfs = bea_splitter(file, raw_data_dict)
    try:
        meta_section = dict_of_dfs.get('meta')
    except:
        pass

    ppl_df = ppl_parser(meta_section, sort=False)

    not_app_or_obs = {}
    ETLd_question_dfs = {}
    ETLd_dfs = {}
    data_dicts = {}

    for report_type in dict_of_dfs:
        if report_type != 'meta':
            report_to_run = dict_of_dfs.get(report_type)
            questions_df, transformed_df, not_applicable_count, not_observed_count = bea_etl(report_to_run, ppl_df, report_type, file)
            ETLd_question_dfs[report_type] = questions_df
            ETLd_dfs[report_type] = transformed_df
            not_app_or_obs[report_type] = {}
            not_app_or_obs[report_type]['not_observed_count'] = not_observed_count
            not_app_or_obs[report_type]['not_applicable_count'] = not_applicable_count
            data_dicts[report_type] = join_questions_df_and_raw_data_into_dict(questions_df, transformed_df)


    randomize_bea_bool = False

    for report_type in dict_of_dfs:
        if report_type != 'meta':
            print('Creating {} report for {}'.format(report_type, file))
            report_to_run = ETLd_dfs.get(report_type)
            questions_df = ETLd_question_dfs.get(report_type)
            BEA = bea_report(file, report_type, report_to_run, questions_df, ppl_df, not_app_or_obs[report_type]['not_applicable_count'], not_app_or_obs[report_type]['not_observed_count'], randomize = randomize_bea_bool)
            member_variable_query = dict()
            try:
                member_variable_query['skills_matrix_average'] = BEA.skills_matrix_average
                member_variable_query['number_of_skills_matrix_questions'] = BEA.number_of_skills_matrix_questions
            except:
                pass
            member_variable_query['eSigma_value'] = BEA.eSigma_value
            member_variable_query['section_data'] = BEA.section_data
            member_variable_query['questions_df'] = BEA.questions_df
            member_variable_query['data_dict'] = data_dicts[report_type]
            member_variable_query['number_of_respondents'] = BEA.number_of_respondents
            member_variable_query['section_averages'] = BEA.averages
            member_variable_query['five_point_scale'] = BEA.five_point_scale
            member_variable_query['eval_stats_list_of_dicts'] = BEA.eval_stats_list_of_dicts
            member_variable_query['eval_stats_list_of_dicts_last_item'] = tuple((list(BEA.eval_stats_list_of_dicts[-1].keys())[0], BEA.eval_stats_list_of_dicts[-1].get(list(BEA.eval_stats_list_of_dicts[-1].keys())[0])))
            member_variable_query['section_avg_list'] = BEA.section_avg_list
            member_variable_query['section_avg_list_last_item'] = tuple((list(BEA.section_avg_list[-1].keys())[0], BEA.section_avg_list[-1].get(list(BEA.section_avg_list[-1].keys())[0])))
            member_variable_query['jinja_hi'] = BEA.jinja_hi
            member_variable_query['jinja_lo'] = BEA.jinja_lo
            BEA_templater(file, member_variable_query, report_type, report_to_run, questions_df, ppl_df, not_app_or_obs[report_type]['not_applicable_count'], not_app_or_obs[report_type]['not_observed_count'], randomize = randomize_bea_bool)


    #####################################
    print('\n\n--- Run Time ---')
    print('--- %s seconds ---' % (time.time() - start_time))
