from jinja2 import Template, Environment, PackageLoader, select_autoescape, FileSystemLoader
import datetime
import win32com.client as win32
from PIL import ImageGrab
import os
import excel2img
import time
import sys
import glob
import pandas as pd
from weasyprint import HTML, CSS
from decimal import Decimal, ROUND_HALF_UP
import pandas as pd
import numpy as np
from collections import Counter
import re
from collections import OrderedDict
from random import shuffle

sys.path.insert(1, r'C:\Users\Drew\Nasdaq_Classes_and_Fns')
from ppl_parser import ppl_parser
from csv_opener import csv_opener

from peer_etl import peer_splitter, peer_etl
from peer_reports import peer_composite_report, peer_individual_reports


def peer_composite_templater(file_name, member_variable_query, report_type, df, questions_df, ppl_df, not_applicable_count, not_observed_count, randomize = True, anonymized=False):
    now = datetime.datetime.now()
    formatted_date = str(now.strftime("%B")) + ' ' + str(now.day) + ', ' + str(now.year)

    file_loader =FileSystemLoader('templates')
    env = Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)
    template = env.get_template('test_peer_composite_report_template.html')

    skills_matrix_present = 'skills_matrix' in questions_df['type'].unique()


    jinja_input_dict = {'title': 'test', 'nasdaq_logo': r'images/nasdaq_logo.png', 'client_logo': r'images/client_logo.png',
         'board_name': 'XYZ', 'year': str(now.year), 'formatted_date': formatted_date, 'skills_matrix_present': skills_matrix_present,
         'heat_map_image': r'images/heat_map.png', 'questions_df': questions_df, 'scale_image': r'images/scale.png', 'green_flag': r'images/green_flag.png', 'yellow_flag': r'images/yellow_flag.png', 'red_flag': r'images/red_flag.png'}

    # Get Assessment sections labels and section names
    section_names_and_labels = {}
    label_re = re.compile(r'^[\s\t]*([IVX]+\.)[\s\t]*(.*)')
    for section in questions_df['section'].unique().tolist():
        label = label_re.match(section)
        section_names_and_labels[label.group(1)] = label.group(2)
    jinja_input_dict['section_names_and_labels'] = section_names_and_labels

    # Control which sections are in the table of contents
    jinja_input_dict['table_of_contents_titles'] = ['#heat_map_title', '#section_analysis_title', '#eSigma_title_bar_chart', '#hilo_title', '#peer_and_self_report_title', '#statistical_analysis_title']

    optional_sections = []
    if member_variable_query.get('skills_matrix_average') is not None:
        optional_sections.append('#skills_matrix_title')
    # if not swot:
    #     optional_sections.append('#swot_title')
    # if highlights_actionpoints:
    #     optional_sections.append('#highlights_actionpts_title')

    if optional_sections:
        index = 2
        for optional_section in optional_sections:
            print(optional_section)
            jinja_input_dict['table_of_contents_titles'].insert(index, optional_section)
            index+=1

    client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file_name)
    jinja_input_dict['client_name'] = client_name.group(1)

    jinja_input_dict['number_of_sections'] = len(questions_df['section'].unique().tolist())
    jinja_input_dict['number_of_long_form_questions'] = len(questions_df[questions_df['type']=='long_form'])
    jinja_input_dict['number_of_rated_questions'] = len(questions_df['question_raw']) - len(questions_df[questions_df['type']=='long_form']) - len(questions_df[questions_df['type']=='skill_matrix']) - len(questions_df[questions_df['type']=='attribute_matrix'])

    jinja_input_dict['report_type'] = report_type

    for item in member_variable_query.keys():
        jinja_input_dict['{}'.format(item)] = member_variable_query[item]

    # for section in member_variable_query['jinja_for_loop_dict']:
    #     print('\n\n', section)
    #     for number in member_variable_query['jinja_for_loop_dict'][section]:
    #         print(number)
    #         print(member_variable_query['jinja_for_loop_dict'][section][number])



    output = template.render(jinja_input_dict)
    # with open("test.html", "w") as fh:
    #     fh.write(output)

    # print(member_variable_query['data_dict'])
    if anonymized == False:
        HTML(string=output, base_url='.').write_pdf('{}_{}.pdf'.format(client_name.group(1), report_type), stylesheets=[CSS(r'static\peer_composite_template_style.css')])
    else:
        HTML(string=output, base_url='.').write_pdf('{}_{}_Anonymized.pdf'.format(client_name.group(1), report_type), stylesheets=[CSS(r'static\peer_composite_template_style.css')])

def peer_individuals_templater(file_name, member_variable_query, report_type, df, questions_df, ppl_df, not_applicable_count, not_observed_count, director_name, randomize = True):
    now = datetime.datetime.now()
    formatted_date = str(now.strftime("%B")) + ' ' + str(now.day) + ', ' + str(now.year)


    file_loader =FileSystemLoader('templates')
    env = Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)
    template = env.get_template('peer_individuals_report_template.html')

    skills_matrix_present = 'skills_matrix' in questions_df['type'].unique()

    jinja_input_dict = {'director_name': director_name, 'title': 'test', 'nasdaq_logo': r'images/nasdaq_logo.png', 'client_logo': r'images/client_logo.png',
         'board_name': 'XYZ', 'year': str(now.year), 'formatted_date': formatted_date, 'skills_matrix_present': skills_matrix_present,
         'heat_map_image': r'images/heat_map.png', 'questions_df': questions_df, 'scale_image': r'images/scale.png'}

    # Get Assessment sections labels and section names
    section_names_and_labels = {}
    label_re = re.compile(r'^[\s\t]*([IVX]+\.)[\s\t]*(.*)')
    for section in questions_df['section'].unique().tolist():
        label = label_re.match(section)
        section_names_and_labels[label.group(1)] = label.group(2)
    jinja_input_dict['section_names_and_labels'] = section_names_and_labels

    # Control which sections are in the table of contents
    jinja_input_dict['table_of_contents_titles'] = ['#heat_map_title', '#peer_self_comparison_graph_title', '#hilo_title', '#peer_and_self_report_title', '#statistical_analysis_title']

    optional_sections = []
    if member_variable_query.get('skills_matrix_average') is not None:
        optional_sections.append('#skills_matrix_title')
    # if not swot:
    #     optional_sections.append('#swot_title')
    # if highlights_actionpoints:
    #     optional_sections.append('#highlights_actionpts_title')

    if optional_sections:
        index = 2
        for optional_section in optional_sections:
            print(optional_section)
            jinja_input_dict['table_of_contents_titles'].insert(index, optional_section)
            index+=1

    client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file_name)
    jinja_input_dict['client_name'] = client_name.group(1)

    jinja_input_dict['number_of_sections'] = len(questions_df['section'].unique().tolist())
    jinja_input_dict['number_of_long_form_questions'] = len(questions_df[questions_df['type']=='long_form'])
    jinja_input_dict['number_of_rated_questions'] = len(questions_df['question_raw']) - len(questions_df[questions_df['type']=='long_form']) - len(questions_df[questions_df['type']=='skill_matrix']) - len(questions_df[questions_df['type']=='attribute_matrix'])

    # jinja_input_dict['agg_sectional_analsysis_image'] = r'images\aggregated_section_analysis.png'

    jinja_input_dict['report_type'] = report_type

    for item in member_variable_query.keys():
        jinja_input_dict['{}'.format(item)] = member_variable_query[item]

    output = template.render(jinja_input_dict)
    with open("test.html", "w") as fh:
        fh.write(output)

    # print(member_variable_query['data_dict'])
    HTML(string=output, base_url='.').write_pdf('{}_{}_{}.pdf'.format(director_name, client_name.group(1), report_type), stylesheets=[CSS(r'static\peer_individuals_template_style.css')])

def join_qdf_and_rawdata_into_smoothed_df(qdf, tdf, file, report_type, anonymize):

    ############################################################################
    # ETL
    df = pd.DataFrame(columns=['client_name', 'report_type', 'section', 'number', 'question_raw', 'question', 'sub_section', 'type', 'average', 'response', 'respondent_id', 'question_mean'])
    try:
        client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file).group(1)
    except:
        client_name = None

    if anonymize ==True:
        director_encoder = dict()
        director_count = 1
    # qdf.to_excel('qdf.xlsx')
    # tdf.to_excel('tdf.xlsx')
    for s in qdf['section'].unique():
        for q in qdf[qdf['section']==s]['question_raw']:
            for i in tdf[q].index:
                if anonymize == False:
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section_number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_number'].item(), 'sub_section_value': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_value'].item(),
                            'sub_section': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item(), 'response': tdf.loc[i, q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}
                else:
                    sub_q = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item()
                    if sub_q not in ['', '!__!']:
                        if director_encoder.get(sub_q) is None:
                            director_encoder[sub_q] = 'Director {}'.format(director_count)
                            sub_q = 'Director {}'.format(director_count)
                            director_count += 1
                        else:
                            sub_q = director_encoder.get(sub_q)
                    else:
                        pass
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section_number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_number'].item(), 'sub_section_value': director_encoder.get(sub_q),
                            'sub_section': sub_q, 'response': tdf.loc[i, q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}

                df = df.append(row, ignore_index=True)

    question_means = []
    for section in df['section'].unique():
        for number in df[(df['section']==section)]['number'].unique():
            try:
                mean_by_question = df[(df['section']==section) & (df['number']==number)]['response'].mean()
                mean_by_question = "{0:.2f}%".format(Decimal('{}'.format(mean_by_question)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                mean_by_question = None
            mean_column_addition = [mean_by_question]*len(df[(df['section']==section) & (df['number']==number)]['response'])
            question_means.extend(mean_column_addition)

    df['question_mean'] = question_means
    df.to_excel('SMOOTHED_{}.xlsx'.format(client_name))
    return df

def create_peer_composite_jinja_dict(qdf, tdf, file, report_type, anonymize, randomize):
    # Get Board Averages
    board_question_averages = dict()
    for section in smoothed_df['section'].unique():
        for number in smoothed_df[smoothed_df['section']==section]['number'].unique():
            try:
                avg = smoothed_df[(smoothed_df['section']==section) & (smoothed_df['number']==number)]['response'].mean()
                board_question_averages[(section, number)] = "{0:.2f}".format(Decimal('{}'.format(avg)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                board_question_averages[(section, number)] = None

    data_dict = OrderedDict()
    rankFlag = False
    if anonymize ==True:
        director_encoder = dict()
        director_count = 1
    for section in qdf['section'].unique():
        data_dict[section] = OrderedDict()
        for number in qdf[qdf['section']==section]['number'].unique():
            data_dict[section][number] = OrderedDict()
            try:
                question_average = smoothed_df[(smoothed_df['section']==section) & (smoothed_df['number']==number)]['response'].mean()
                question_average = "{0:.2f}".format(Decimal('{}'.format(question_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                question_average = None
            data_dict[section][number]['question_average'] = question_average
            data_dict[section][number]['type'] = qdf[(qdf['section']==section)&(qdf['number']==number)]['type'].unique().tolist()[0]
            data_dict[section][number]['question'] = qdf[(qdf['section']==section)&(qdf['number']==number)]['question'].unique().tolist()[0]
            # data_dict[section][number]['board_average']
            # Iterate through raw questions because sub_section is undefined when
            # improper syntax is used
            data_dict[section][number]['raw_questions'] = OrderedDict()
            for raw_q in qdf[(qdf['section']==section)&(qdf['number']==number)]['question_raw']:
                data_dict[section][number]['raw_questions'][raw_q] = {}
                sub_q = qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['sub_section'].item()

                if anonymize:
                    if sub_q not in ['', '!__!']:
                        if director_encoder.get(sub_q) is None:
                            director_encoder[sub_q] = 'Director {}'.format(director_count)
                            data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = 'Director {}'.format(director_count)
                            director_count += 1
                        else:
                            data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = director_encoder.get(sub_q)
                    else:
                        data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = sub_q
                else:
                    data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = sub_q

                if qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['type'].item() in ['matrix', 'likert']:
                    answers = tdf[raw_q].replace({np.nan: None})
                    answers.fillna('N/A', inplace=True)
                    answers = answers.tolist()
                    answers = [int(x) if x != 'N/A' else x for x in answers]
                    if randomize:
                        shuffle(answers)
                        data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers
                    else:
                        data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers
                    data_dict[section][number]['raw_questions'][raw_q]['average'] = str(Decimal(np.mean(tdf[raw_q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                    data_dict[section][number]['raw_questions'][raw_q]['board_average'] = board_question_averages.get((section, number))
                elif qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['type'].item() in ['long_form']:
                    answers = tdf[raw_q].dropna().tolist()
                    answers = [a for a in answers if a not in ['nan', '#NAME?']]
                    if randomize:
                        shuffle(answers)
                    else:
                        pass
                    data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers


    # for s in data_dict:
    #     print('\n\n', s)
    #     for number in data_dict[s]:
    #         try:
    #             print(data_dict[s][number]['question_average'])
    #         except:
    #             pass
    #         print(data_dict[s][number]['type'])
    #         print(data_dict[s][number]['question'])
    #         for raw in data_dict[s][number]['raw_questions']:
    #             print(raw)
    #             print(data_dict[s][number]['raw_questions'][raw]['sub_section'])
    #             print(data_dict[s][number]['raw_questions'][raw]['answers'])
    #             try:
    #                 print(data_dict[s][number]['raw_questions'][raw]['average'])
    #                 print(data_dict[s][number]['raw_questions'][raw]['board_average'])
    #             except:
    #                 pass
    return data_dict

def create_peer_individuals_jinja_dict(qdf, tdf, smoothed_df, file, report_type, anonymize, randomize):
    # Get Board Averages
    board_question_averages = dict()
    for section in smoothed_df['section'].unique():
        for number in smoothed_df[smoothed_df['section']==section]['number'].unique():
            try:
                avg = smoothed_df[(smoothed_df['section']==section) & (smoothed_df['number']==number)]['response'].mean()
                board_question_averages[(section, number)] = "{0:.2f}".format(Decimal('{}'.format(avg)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                board_question_averages[(section, number)] = None

    data_dict = dict()
    for director in smoothed_df['sub_section'].unique():
        # print('\n\n', director)
        row_id = qdf[~(qdf['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (qdf['sub_section']==director)]['UniqueIdentifier'].unique()[0]
        # print(row_id)
        if director not in ['', '!__!']:
            data_dict[director] = OrderedDict()
            for section in smoothed_df[smoothed_df['sub_section']==director]['section'].unique():
                data_dict[director][section] = OrderedDict()
                for number in smoothed_df[(smoothed_df['section']==section)&(smoothed_df['sub_section']==director)]['number'].unique():
                    data_dict[director][section][number] = dict()
                    self_rating = smoothed_df[(smoothed_df['section']==section)&(smoothed_df['sub_section']==director)&(smoothed_df['number']==number)&(smoothed_df['respondent_id']==row_id)]['response'].item()
                    data_dict[director][section][number]['self_rating'] = self_rating
                    data_dict[director][section][number]['answers'] = [self_rating]
                    try:
                        board_average = smoothed_df[(smoothed_df['section']==section) & (smoothed_df['number']==number)]['response'].mean()
                        board_average = "{0:.2f}".format(Decimal('{}'.format(board_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                    except:
                        board_average = None
                    try:
                        respondent_average = smoothed_df[(smoothed_df['section']==section) & (smoothed_df['number']==number) & (smoothed_df['sub_section']==director)]['response'].mean()
                        respondent_average = "{0:.2f}".format(Decimal('{}'.format(respondent_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                    except:
                        question_average = None
                    data_dict[director][section][number]['board_average'] = board_average
                    data_dict[director][section][number]['respondent_average'] = respondent_average
                    data_dict[director][section][number]['type'] = smoothed_df[(smoothed_df['section']==section)&(smoothed_df['number']==number)]['type'].unique().tolist()[0]
                    data_dict[director][section][number]['question'] = smoothed_df[(smoothed_df['section']==section)&(smoothed_df['number']==number)]['question'].unique().tolist()[0]

                    if data_dict[director][section][number]['type'] in ['matrix', 'likert']:
                        # calculate average
                        answers = smoothed_df[(smoothed_df['section']==section)&(smoothed_df['number']==number)&(smoothed_df['sub_section']==director)][['respondent_id', 'response']]
                        # print(row_id)
                        answers.set_index('respondent_id', inplace=True)
                        # print(answers)
                        avg = np.mean(answers['response'].dropna())
                        answers = answers.replace({np.nan: None})
                        answers.fillna('N/A', inplace=True)
                        answers.drop(index=row_id, inplace=True)
                        # print(answers)
                        answers = answers['response'].tolist()
                        answers = [int(x) if x != 'N/A' else x for x in answers]
                        if randomize:
                            shuffle(answers)

                        data_dict[director][section][number]['answers'].extend(answers)
                        # print(data_dict[director][section][number]['answers'])
                        data_dict[director][section][number]['average'] = str(Decimal(avg).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                        data_dict[director][section][number]['board_average'] = board_question_averages.get((section, number))
                    elif data_dict[director][section][number]['type']in ['long_form']:
                        answers = smoothed_df[(smoothed_df['section']==section)&(smoothed_df['number']==number)&(smoothed_df['sub_section']==director)]['response'].tolist()
                        answers = [a for a in answers if a not in ['nan', '#NAME?']]
                        if randomize:
                            shuffle(answers)
                        data_dict[director][section][number]['answers'].extend(answers)
        else:
            pass
    return data_dict

def join_questions_df_and_raw_data_into_dict(qdf, tdf, file, report_type, composite_join, randomize, anonymize):

    ############################################################################
    # ETL
    df = pd.DataFrame(columns=['client_name', 'report_type', 'section', 'number', 'question_raw', 'question', 'sub_section', 'type', 'average', 'response', 'respondent_id', 'question_mean'])
    try:
        client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file).group(1)
    except:
        client_name = None

    if anonymize ==True:
        director_encoder = dict()
        director_count = 1
    # qdf.to_excel('qdf.xlsx')
    # tdf.to_excel('tdf.xlsx')
    for s in qdf['section'].unique():
        for q in qdf[qdf['section']==s]['question_raw']:
            for i in tdf[q].index:
                if anonymize == False:
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section_number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_number'].item(), 'sub_section_value': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_value'].item(),
                            'sub_section': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item(), 'response': tdf.loc[i, q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}
                else:
                    sub_q = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item()
                    if sub_q not in ['', '!__!']:
                        if director_encoder.get(sub_q) is None:
                            director_encoder[sub_q] = 'Director {}'.format(director_count)
                            sub_q = 'Director {}'.format(director_count)
                            director_count += 1
                        else:
                            sub_q = director_encoder.get(sub_q)
                    else:
                        pass
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section_number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_number'].item(), 'sub_section_value': director_encoder.get(sub_q),
                            'sub_section': sub_q, 'response': tdf.loc[i, q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}

                df = df.append(row, ignore_index=True)

    question_means = []
    for section in df['section'].unique():
        for number in df[(df['section']==section)]['number'].unique():
            try:
                mean_by_question = df[(df['section']==section) & (df['number']==number)]['response'].mean()
                mean_by_question = "{0:.2f}%".format(Decimal('{}'.format(mean_by_question)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                mean_by_question = None
            mean_column_addition = [mean_by_question]*len(df[(df['section']==section) & (df['number']==number)]['response'])
            question_means.extend(mean_column_addition)

    df['question_mean'] = question_means
    # df.to_excel('SMOOTHED_{}.xlsx'.format(client_name))
    ############################################################################

    # Get Board Averages
    board_question_averages = dict()
    for section in df['section'].unique():
        for number in df[df['section']==section]['number'].unique():
            try:
                avg = df[(df['section']==section) & (df['number']==number)]['response'].mean()
                board_question_averages[(section, number)] = "{0:.2f}".format(Decimal('{}'.format(avg)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                board_question_averages[(section, number)] = None


    if composite_join:
        data_dict = OrderedDict()
        rankFlag = False
        if anonymize ==True:
            director_encoder = dict()
            director_count = 1
        for section in qdf['section'].unique():
            data_dict[section] = OrderedDict()
            for number in qdf[qdf['section']==section]['number'].unique():
                data_dict[section][number] = OrderedDict()
                try:
                    question_average = df[(df['section']==section) & (df['number']==number)]['response'].mean()
                    question_average = "{0:.2f}".format(Decimal('{}'.format(question_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                except:
                    question_average = None
                data_dict[section][number]['question_average'] = question_average
                data_dict[section][number]['type'] = qdf[(qdf['section']==section)&(qdf['number']==number)]['type'].unique().tolist()[0]
                data_dict[section][number]['question'] = qdf[(qdf['section']==section)&(qdf['number']==number)]['question'].unique().tolist()[0]
                # data_dict[section][number]['board_average']
                # Iterate through raw questions because sub_section is undefined when
                # improper syntax is used
                data_dict[section][number]['raw_questions'] = OrderedDict()
                for raw_q in qdf[(qdf['section']==section)&(qdf['number']==number)]['question_raw']:
                    data_dict[section][number]['raw_questions'][raw_q] = {}
                    sub_q = qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['sub_section'].item()

                    if anonymize:
                        if sub_q not in ['', '!__!']:
                            if director_encoder.get(sub_q) is None:
                                director_encoder[sub_q] = 'Director {}'.format(director_count)
                                data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = 'Director {}'.format(director_count)
                                director_count += 1
                            else:
                                data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = director_encoder.get(sub_q)
                        else:
                            data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = sub_q
                    else:
                        data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = sub_q

                    if qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['type'].item() in ['matrix', 'likert']:
                        answers = tdf[raw_q].replace({np.nan: None})
                        answers.fillna('N/A', inplace=True)
                        answers = answers.tolist()
                        answers = [int(x) if x != 'N/A' else x for x in answers]
                        if randomize:
                            shuffle(answers)
                            data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers
                        else:
                            data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers
                        data_dict[section][number]['raw_questions'][raw_q]['average'] = str(Decimal(np.mean(tdf[raw_q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP))


                        data_dict[section][number]['raw_questions'][raw_q]['board_average'] = board_question_averages.get((section, number))
                    elif qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['type'].item() in ['long_form']:
                        answers = tdf[raw_q].dropna().tolist()
                        answers = [a for a in answers if a not in ['nan', '#NAME?']]
                        if randomize:
                            shuffle(answers)
                        else:
                            pass
                        data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers
    else:
        data_dict = dict()
        for director in df['sub_section'].unique():
            # print('\n\n', director)
            row_id = qdf[~(qdf['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (qdf['sub_section']==director)]['UniqueIdentifier'].unique()[0]
            # print(row_id)
            if director not in ['', '!__!']:
                data_dict[director] = OrderedDict()
                for section in df[df['sub_section']==director]['section'].unique():
                    data_dict[director][section] = OrderedDict()
                    for number in df[(df['section']==section)&(df['sub_section']==director)]['number'].unique():
                        data_dict[director][section][number] = dict()
                        self_rating = df[(df['section']==section)&(df['sub_section']==director)&(df['number']==number)&(df['respondent_id']==row_id)]['response'].item()
                        data_dict[director][section][number]['self_rating'] = self_rating
                        data_dict[director][section][number]['answers'] = [self_rating]
                        try:
                            board_average = df[(df['section']==section) & (df['number']==number)]['response'].mean()
                            board_average = "{0:.2f}".format(Decimal('{}'.format(board_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                        except:
                            board_average = None
                        try:
                            respondent_average = df[(df['section']==section) & (df['number']==number) & (df['sub_section']==director)]['response'].mean()
                            respondent_average = "{0:.2f}".format(Decimal('{}'.format(respondent_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                        except:
                            question_average = None
                        data_dict[director][section][number]['board_average'] = board_average
                        data_dict[director][section][number]['respondent_average'] = respondent_average
                        data_dict[director][section][number]['type'] = df[(df['section']==section)&(df['number']==number)]['type'].unique().tolist()[0]
                        data_dict[director][section][number]['question'] = df[(df['section']==section)&(df['number']==number)]['question'].unique().tolist()[0]

                        if data_dict[director][section][number]['type'] in ['matrix', 'likert']:
                            # calculate average
                            answers = df[(df['section']==section)&(df['number']==number)&(df['sub_section']==director)][['respondent_id', 'response']]
                            # print(row_id)
                            answers.set_index('respondent_id', inplace=True)
                            # print(answers)
                            avg = np.mean(answers['response'].dropna())
                            answers = answers.replace({np.nan: None})
                            answers.fillna('N/A', inplace=True)
                            answers.drop(index=row_id, inplace=True)
                            # print(answers)
                            answers = answers['response'].tolist()
                            answers = [int(x) if x != 'N/A' else x for x in answers]
                            if randomize:
                                shuffle(answers)

                            data_dict[director][section][number]['answers'].extend(answers)
                            # print(data_dict[director][section][number]['answers'])
                            data_dict[director][section][number]['average'] = str(Decimal(avg).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                            data_dict[director][section][number]['board_average'] = board_question_averages.get((section, number))
                        elif data_dict[director][section][number]['type']in ['long_form']:
                            answers = df[(df['section']==section)&(df['number']==number)&(df['sub_section']==director)]['response'].tolist()
                            answers = [a for a in answers if a not in ['nan', '#NAME?']]
                            if randomize:
                                shuffle(answers)
                            data_dict[director][section][number]['answers'].extend(answers)
            else:
                pass

    # for director in list(data_dict.keys()):
    #     print(director)
    #     for section in data_dict[director]:
    #         print(section)
    #         if section == list(data_dict[director].keys())[0]:
    #           print('here', section)
    #         else:
    #           print('nope', section)
    #     break
    # print(data_dict[list(data_dict.keys())[0]])



    ## Director Decoder
    # director_encoder = dict()
    # director_count = 1
    # for section in qdf['section'].unique():
    #     for number in qdf[qdf['section']==section]['number'].unique():
    #         for raw_q in qdf[(qdf['section']==section)&(qdf['number']==number)]['question_raw']:
    #             print(section, number, raw_q)
    #             sub_q = qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['sub_section'].item()
    #             if sub_q not in ['', '!__!']:
    #                 if director_encoder.get(sub_q) is None:
    #                     director_encoder[sub_q] = 'Director {}'.format(director_count)
    #                     director_count += 1
    # print(director_encoder)


        # for s in data_dict:
    #     print('\n\n', s)
    #     for number in data_dict[s]:
    #         try:
    #             print(data_dict[s][number]['question_average'])
    #         except:
    #             pass
    #         print(data_dict[s][number]['type'])
    #         print(data_dict[s][number]['question'])
    #         for raw in data_dict[s][number]['raw_questions']:
    #             print(raw)
    #             print(data_dict[s][number]['raw_questions'][raw]['sub_section'])
    #             print(data_dict[s][number]['raw_questions'][raw]['answers'])

    return data_dict, df

if __name__ == "__main__":
    start_time = time.time()
    print(sys.version)
    print('\n------------------------------------------\n')

    cwd = os.getcwd()

    extension = 'csv'
    file = glob.glob("*.{}".format(extension))[0]

    print('\n-----------------\nProccessing Peer File...')
    print('File Name: ', file, '\n\n')

    raw_data_dict = csv_opener(file)
    dict_of_dfs = peer_splitter(file, raw_data_dict)
    try:
        meta_section = dict_of_dfs.get('meta')
    except:
        pass

    ppl_df = ppl_parser(meta_section, sort=False)
    # ppl_df.to_excel('ppl_df.xlsx')

    randomize = False

    not_app_or_obs = {}
    ETLd_question_dfs = {}
    ETLd_dfs = {}
    data_dicts = {}
    smoothed_dfs = {}

    for report_type in dict_of_dfs:
        if report_type != 'meta':
            data_dicts[report_type] = dict()
            report_to_run = dict_of_dfs.get(report_type)
            questions_df, transformed_df, not_applicable_count, not_observed_count = peer_etl(report_to_run, ppl_df, report_type, file)
            ETLd_question_dfs[report_type] = questions_df
            ETLd_dfs[report_type] = transformed_df
            not_app_or_obs[report_type] = {}
            not_app_or_obs[report_type]['not_observed_count'] = not_observed_count
            not_app_or_obs[report_type]['not_applicable_count'] = not_applicable_count

            smoothed_df = join_qdf_and_rawdata_into_smoothed_df(questions_df, transformed_df, file, report_type, False)
            jinja_individuals_data_dict = create_peer_individuals_jinja_dict(questions_df, transformed_df, smoothed_df, file, report_type, False, True)

            jinja_composite_data_dict_randomized_named = create_peer_composite_jinja_dict(questions_df, transformed_df, file, report_type, False, True)
            jinja_composite_data_dict_randomized_anonymized = create_peer_composite_jinja_dict(questions_df, transformed_df, file, report_type, True, True)
            data_dicts[report_type]['anonymized'] = jinja_composite_data_dict_randomized_anonymized
            data_dicts[report_type]['named'] = jinja_composite_data_dict_randomized_named

            # data_dict, smoothed_df = join_questions_df_and_raw_data_into_dict(questions_df, transformed_df, file, report_type, composite_join=True, randomize=True, anonymize = True)
            # data_dicts[report_type]['anonymized'] = data_dict
            # data_dict, smoothed_df = join_questions_df_and_raw_data_into_dict(questions_df, transformed_df, file, report_type, composite_join=True, randomize=True, anonymize = False)
            # data_dicts[report_type]['named'] = data_dict

            data_dicts[report_type]['individuals'] = jinja_individuals_data_dict
            smoothed_dfs[report_type] = smoothed_df



    if set(questions_df['type']).issubset(set(['skills_matrix', ''])):
        skills_response = True
    else:
        skills_response = False

    for report_type in dict_of_dfs:
        if report_type != 'meta':
            print('Creating {} report for {}'.format(report_type, file))
            report_to_run = ETLd_dfs.get(report_type)
            questions_df = ETLd_question_dfs.get(report_type)
            PEER_COMPOSITE = peer_composite_report(file, report_type, report_to_run, questions_df, ppl_df, not_app_or_obs[report_type]['not_applicable_count'], not_app_or_obs[report_type]['not_observed_count'], skills_response)
            member_variable_query = dict()

            try:
                member_variable_query['skills_matrix_average'] = PEER_COMPOSITE.skills_matrix_average
                member_variable_query['number_of_skills_matrix_questions'] = PEER_COMPOSITE.number_of_skills_matrix_questions
            except:
                pass
            member_variable_query['eSigma_value'] = PEER_COMPOSITE.composite_eSigma
            member_variable_query['section_data'] = PEER_COMPOSITE.section_data
            member_variable_query['questions_df'] = PEER_COMPOSITE.questions_df
            member_variable_query['number_of_respondents'] = PEER_COMPOSITE.number_of_respondents
            member_variable_query['section_averages'] = PEER_COMPOSITE.averages
            member_variable_query['five_point_scale'] = PEER_COMPOSITE.five_point_scale
            member_variable_query['eval_stats_list_of_dicts'] = PEER_COMPOSITE.eval_stats_list_of_dicts
            member_variable_query['eval_stats_list_of_dicts_last_item'] = tuple((list(PEER_COMPOSITE.eval_stats_list_of_dicts[-1].keys())[0], PEER_COMPOSITE.eval_stats_list_of_dicts[-1].get(list(PEER_COMPOSITE.eval_stats_list_of_dicts[-1].keys())[0])))
            member_variable_query['section_avg_list'] = PEER_COMPOSITE.section_avg_list
            member_variable_query['section_avg_list_last_item'] = tuple((list(PEER_COMPOSITE.section_avg_list[-1].keys())[0], PEER_COMPOSITE.section_avg_list[-1].get(list(PEER_COMPOSITE.section_avg_list[-1].keys())[0])))
            member_variable_query['jinja_hi'] = PEER_COMPOSITE.jinja_hi
            member_variable_query['jinja_lo'] = PEER_COMPOSITE.jinja_lo
            member_variable_query['smoothed_df'] = smoothed_dfs[report_type]
            member_variable_query['jinja_for_loop_dict'] = data_dicts[report_type]['named']
            peer_composite_templater(file, member_variable_query, report_type, report_to_run, questions_df, ppl_df, not_app_or_obs[report_type]['not_applicable_count'], not_app_or_obs[report_type]['not_observed_count'], randomize = randomize, anonymized=False)
            member_variable_query['jinja_for_loop_dict'] = data_dicts[report_type]['anonymized']
            peer_composite_templater(file, member_variable_query, report_type, report_to_run, questions_df, ppl_df, not_app_or_obs[report_type]['not_applicable_count'], not_app_or_obs[report_type]['not_observed_count'], randomize = randomize, anonymized=True)

    for report_type in dict_of_dfs:
        if report_type != 'meta':
            print('Creating {} report for {}'.format(report_type, file))
            # data_dict, smoothed_df = join_questions_df_and_raw_data_into_dict(questions_df, transformed_df, file, report_type, composite_join=False, randomize=True, anonymize = False)
            # smoothed_df = join_qdf_and_rawdata_into_smoothed_df(questions_df, transformed_df, file, report_type, False)
            # jinja_data_dict = create_peer_individuals_jinja_dict(questions_df, transformed_df, smoothed_df, file, report_type, False, True)
            report_to_run = ETLd_dfs.get(report_type)
            questions_df = ETLd_question_dfs.get(report_type)
            jinja_data_dict = data_dicts[report_type]['individuals']

            PEER_INDIVIDUALS = peer_individual_reports(file, report_type, report_to_run, questions_df, ppl_df, not_app_or_obs[report_type]['not_applicable_count'], not_app_or_obs[report_type]['not_observed_count'], randomize = True)
            for director in jinja_data_dict.keys():
                jinja_for_loop_dict = jinja_data_dict[director]
                member_variable_query = dict()
                try:
                    member_variable_query['skills_matrix_average'] = PEER_INDIVIDUALS.skills_matrix_average
                    member_variable_query['number_of_skills_matrix_questions'] = PEER_INDIVIDUALS.number_of_skills_matrix_questions
                except:
                    pass
                member_variable_query['section_data'] = PEER_INDIVIDUALS.section_data[director]
                member_variable_query['questions_df'] = PEER_INDIVIDUALS.questions_df
                member_variable_query['jinja_for_loop_dict'] = jinja_for_loop_dict
                member_variable_query['number_of_respondents'] = PEER_INDIVIDUALS.number_of_respondents
                member_variable_query['section_averages'] = PEER_INDIVIDUALS.averages_data[director]
                member_variable_query['five_point_scale'] = PEER_INDIVIDUALS.five_point_scale
                member_variable_query['eval_stats_list_of_dicts'] = PEER_INDIVIDUALS.agg_section_data[director]
                member_variable_query['eval_stats_list_of_dicts_last_item'] = tuple((list(PEER_INDIVIDUALS.agg_section_data[director][-1].keys())[0], PEER_INDIVIDUALS.agg_section_data[director][-1].get(list(PEER_INDIVIDUALS.agg_section_data[director][-1].keys())[0])))
                member_variable_query['section_avg_list'] = PEER_INDIVIDUALS.section_avg_list[director]
                member_variable_query['section_avg_list_last_item'] = tuple((list(PEER_INDIVIDUALS.section_avg_list[director][-1].keys())[0], PEER_INDIVIDUALS.section_avg_list[director][-1].get(list(PEER_INDIVIDUALS.section_avg_list[director][-1].keys())[0])))
                member_variable_query['jinja_hi'] = PEER_INDIVIDUALS.hilo_data[director]['jinja_hi']
                member_variable_query['jinja_lo'] = PEER_INDIVIDUALS.hilo_data[director]['jinja_lo']

                peer_individuals_templater(file, member_variable_query, report_type, report_to_run, questions_df, ppl_df, not_app_or_obs[report_type]['not_applicable_count'], not_app_or_obs[report_type]['not_observed_count'], director_name=director, randomize = randomize)


    #####################################
    print('\n\n--- Run Time ---')
    print('--- %s seconds ---' % (time.time() - start_time))
