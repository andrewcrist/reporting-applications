from decimal import Decimal, ROUND_HALF_UP
import pandas as pd
import numpy as np
import re
import warnings
warnings.filterwarnings("ignore", 'This pattern has match groups')

def bea_etl(df, ppl_df, report_type, file_name, write_questions_df, year=None):
    questions_df = pd.DataFrame({'question_raw': df.columns})
    survey = pd.DataFrame(columns = ['type', 'respondent_id', 'question_id', 'response', 'section', 'sub_section', 'number'])

    questions = []
    header_list = []

    search = []
    for values in questions_df['question_raw']:
        new_question = re.sub(r'__\d+$', '', values)
        search.append(new_question)
    questions_df['question'] = search

    re_continued_section = re.compile('(?!.*[\s\t]+1\.).*\(Continued\).*?[\s\t]+(\d+\.)[\s\t]+(.*)')
    re_continued_assessment = re.compile('(?!.*_[^a]\.)(?=.*[\s\t]+1\.).*\(Continued\).*[\s\t]+([IVX]+\..*)[\s\t]+(1\.)[\s\t]+(.*)')
    re_continued = re.compile('.*\(Continued\)')

    re_section_match = re.compile(r'(?!.*_[^a]\.)^.*?[\s\t]*([IVX]+\..*?)[\s\t]+(\d+\.)?[\s\t]+(.*)')
    re_question_number = re.compile('.*?(\d+\.).+')

    re_skills_matrix = re.compile('.*skills\s+matrix.*', re.IGNORECASE)
    re_attributes_matrix = re.compile('.*attributes\s+matrix.*', re.IGNORECASE)
    re_alpha_subsection = re.compile(':\s*_[a-z]\.')
    # re_rank_subsection = re.compile('[\d]\..*_[A-Z].*_$')
    re_rank_subsection = re.compile('.*_[A-Z].*_.*')
    committee_critique = re.compile(r'^_.*committee.*_$', re.IGNORECASE)

    sections = []
    numbers = []
    questions = []
    types = []
    sub_sections = []

    empty_cell_count = 0

    not_observed_dict = dict()
    not_applicable_dict = dict()
    empty_cell_dict = dict()

    unnumbered_question_indexer = 1

    for question, header in zip(questions_df['question'], df.columns):
        # print(file_name, report_type, df[header])
        if df[header].dtype == 'object':
            if df[header].str.contains('^([1-9]|10)$').any():
                # print(df[header])
                d = df[header].value_counts()
                try:
                    not_applicable_dict[question]=d['n/a']
                except:
                    pass
                try:
                    not_observed_dict[question]= d['n/o']
                except:
                    pass
                try:
                    empty_cell_count += df[header].isna().sum()
                    if df[header].isna().sum():
                        empty_cell_dict[question]= df[header].isna().sum()
                except:
                    pass

                df[header].replace({'n/a': np.nan}, inplace=True)
                df[header].replace({'n/o': np.nan}, inplace=True)

        if df[header].isnull().all():
             data = df[header].astype(str)
             df[header] = data
        elif df[header].str.isdigit().all():
            data = pd.to_numeric(df[header])
            df[header] = data
        else:
            data = df[header].astype(str)
            df[header] = data

        continued_match = re_continued.match(question)
        section_match = re_section_match.match(question)
        if continued_match is not None:
            if re_continued_section.match(question) is not None:
                section = sections[-1]
                try:
                    number = re_question_number.match(question).group(1)
                except:
                    number = ''
                try:
                    formatted_question = re_continued_section.match(question).group(2)
                except:
                    formatted_question = '!__!'

            elif re_continued_assessment.match(question) is not None:
                try:
                    section = re_continued_assessment.match(question).group(1)
                except:
                    section = '!__!'
                try:
                    number = re_question_number.match(question).group(1)
                except:
                    number = '!__!'
                try:
                    formatted_question = re_continued_assessment.match(question).group(3)
                except:
                    formatted_question = '!__!'

            else:
                section = '!__!'
                number = '!__!'
                formatted_question = '!__!'

            if re_skills_matrix.match(question) is not None:
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'skills_matrix'

            elif re_attributes_matrix.match(question) is not None:
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'attributes_matrix'

            elif (any(isinstance(answer, str) for answer in data)) | (all(data.isna())):
                sub_section = ''
                type = 'long_form'

            elif all(str(item).lower() in ['yes', 'no'] for item in data.tolist()):
                sub_section = ''
                type = 'yes/no'

            elif re.search(':_[\s\t]*[a-z]\.', question) is not None:
                try:
                    sub_section = re.search(':.*_[\s\t]*([a-z]\..*)_', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'matrix'

            elif re_rank_subsection.match(question) is not None:
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'rank'

            elif committee_critique.match(question):
                # question = re.search('_(.*)_', question).group(1)
                type = 'committee_critique'
                sub_section = ''

            else:
                type = 'likert'
                sub_section = ''

        elif section_match is not None:
            try:
                section = section_match.group(1)
            except:
                section = '!__!'
            try:
                number = re_question_number.match(question).group(1)
            except:
                number = '!__!'
            try:
                if re.search('.*_[A-Z].*_.*', question) is None and re.search(':.*_[a-z]\.', question) is None:
                    formatted_question = section_match.group(3)
                elif re_skills_matrix.match(question) is not None:
                    try:
                        formatted_question = re.search('(.*)_.*_$', header).group(1)
                    except:
                        formatted_question = '!__!'
                else:
                    # print(question)
                    formatted_question = re.search('^.*[\s\t]+(\d+\.)?[\s\t]+(.*:).*_', question).group(2)
                    # print(formatted_question)
            except:
                formatted_question = '!__!'

            if re_skills_matrix.match(question) is not None:
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'skills_matrix'

            elif re_attributes_matrix.match(question) is not None:
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'attributes_matrix'

            elif (any(isinstance(answer, str) for answer in data)) | (all(data.isna())):
                sub_section = ''
                type = 'long_form'

            elif all(str(item).lower() in ['yes', 'no'] for item in data.tolist()):
                sub_section = ''
                type = 'yes/no'

            elif re.search(':.*_[\s\t]*[a-z]\.', question) is not None:
                try:
                    sub_section = re.search(':.*_[\s\t]*([a-z]\..*)_', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'matrix'

            elif re_rank_subsection.match(question) is not None:
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section
                type = 'rank'

            elif committee_critique.match(question):
                try:
                    question = re.search('_(.*)_', question).group(1)
                except:
                    question = '!__!'
                type = 'committee_critique'
                sub_section = ''

            else:
                type = 'likert'
                sub_section = ''
        else:
            try:
                section = sections[-1]
            except:
                section = '!__!'

            ########
            # Added because there exist likert questions after skills_matrices
            if re.search('_(.*)_$', question) is not None:
                if re_skills_matrix.match(question) is not None or (types[-1] == 'skills_matrix' and '_a.' not in re.search('_(.*)_$', question).group(1)):
                    try:
                        # No greedy search needed. The line below finds the first instance of \d
                        number = re_question_number.match(question).group(1)
                    except:
                        number = '!__!'
                    try:
                        formatted_question = re.search('(.*)_.*_$', header).group(1)
                    except:
                        formatted_question = '!__!'
                    try:
                        skills_match = re.search('_(.*)_$', header).group(1)
                    except:
                        skills_match = '!__!'
                    type = 'skills_matrix'
                    sub_section = skills_match

                    sections.append(section)
                    numbers.append(number)
                    questions.append(formatted_question)
                    types.append(type)
                    sub_sections.append(sub_section)
                    continue

                elif re_attributes_matrix.match(question) is not None or (types[-1] == 'attributes_matrix' and '_a.' not in re.search('_(.*)_$', question).group(1)):
                    try:
                        # No greedy search needed. The line below finds the first instance of \d
                        number = re_question_number.match(question).group(1)
                    except:
                        number = '!__!'
                    try:
                        formatted_question = re.search('(.*)_.*_$', header).group(1)
                    except:
                        formatted_question = '!__!'
                    try:
                        skills_match = re.search('_(.*)_$', header).group(1)
                    except:
                        skills_match = '!__!'
                    type = 'attributes_matrix'
                    sub_section = skills_match

                    sections.append(section)
                    numbers.append(number)
                    questions.append(formatted_question)
                    types.append(type)
                    sub_sections.append(sub_section)
                    continue

            # Yes/No Questions
            if all(str(item).lower() in ['yes', 'no'] for item in data.tolist()):
                try:
                    # No greedy search needed. The line below finds the first instance of \d
                    number = re_question_number.match(question).group(1)
                except:
                    number = '!__!'
                try:
                    formatted_question = re.search('^.*?\d+\.[\s\t]*(.*)$', question).group(1)
                except:
                    formatted_question = '!__!'
                type = 'yes/no'
                sub_section = ''

            # Long form questions
            elif (any(isinstance(answer, str) for answer in data)) | (all(data.isna())):
                # If unnnumbered, just grab the whole string as the question value
                try:
                    if (re.search('^(?!.*\d\.[^$])(.*)$', question) is not None):
                        formatted_question = re.search('(.*)', question).group(0)
                        type = 'optional_long_form'

                        # We want to group unnumbered, optional questions that refer to
                        # numbered questions using a nomenclature that is different from
                        # those numbered questions. That way, the jinja_for_loop_dict
                        # can make a distinction between the two and format appropriately.

                        if '_unnumbered' in numbers[-1]:
                            number = re.sub('_\d+$', '_{}'.format(unnumbered_question_indexer), numbers[-1])
                            unnumbered_question_indexer += 1
                        elif number != '!__!':
                            number = 'question_{}_unnumbered'.format(number)
                        else:
                            number = 'question_empty_numbers_list_unnumbered{}'.format(unnumbered_question_indexer)
                            unnumbered_question_indexer += 1

                    else:
                        try:
                            # No greedy search needed. The line below finds the first instance of \d
                            number = re_question_number.match(question).group(1)
                        except:
                            number = '!__!'
                        # No greedy search necessary
                        formatted_question = re.search('\d+\.[\s\t]*(.*)$', question).group(1)
                        type = 'long_form'
                except:
                    formatted_question = '!__!'

                sub_section = ''

            # Matrix questions
            # elif re_alpha_subsection.match(question) is not None or re.search(':_\s*[a-z]\.', question) is not None:
            elif re.search(':[\s\t]*_[\s\t]*[a-z]\.', question) is not None:
                try:
                    # No greedy search needed. The line below finds the first instance of \d
                    number = re_question_number.match(question).group(1)
                except:
                    number = '!__!'
                try:
                    formatted_question = re.search('\d+\.\s+(.*:)[\s\t]*_[\s\t]*[a-z]\.[\s\t]*(.*)_', question).group(1)
                    if formatted_question is None:
                        formatted_question = re.search('\d+\.\s+(.*:).*_[\s\t]*[a-z]\.[\s\t]*(.*)_', question).group(1)
                except:
                    formatted_question = '!__!'
                try:
                    sub_section = re.search(':[\s\t]*_[\s\t]*([a-z]\..*)_', question).group(1)
                    if sub_section is None:
                        sub_section = re.search(':.*_[\s\t]*([a-z]\..*)_', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'matrix'

            # Check if it's a ranked choice question
            # Ranked choice questions must contain a substring with a capital letter that follows an underscore and the entire string must end in an underscore
            elif re_rank_subsection.match(question) is not None:
                try:
                    # No greedy search needed. The line below finds the first instance of \d
                    number = re_question_number.match(question).group(1)
                except:
                    number = '!__!'
                try:
                    formatted_question = re.search('\d+\.[\s\t]*(.*:)', question).group(1)
                except:
                    formatted_question = '!__!'
                try:
                    sub_section = re.search('_(.*)_$', question).group(1)
                except:
                    sub_section = '!__!'
                type = 'rank'

            elif committee_critique.match(question):
                try:
                    # No greedy search needed. The line below finds the first instance of \d
                    number = re_question_number.match(question).group(1)
                except:
                    number = '!__!'
                try:
                    formatted_question = re.search('_(.*)_', question).group(1)
                except:
                    formatted_question = '!__!'
                type = 'committee_critique'
                sub_section = ''
            else:
                try:
                    # No greedy search needed. The line below finds the first instance of \d
                    number = re_question_number.match(question).group(1)
                except:
                    number = '!__!'
                try:
                    formatted_question = re.search('\d+\.[\s\t]*(.*)', question).group(1)
                except:
                    formatted_question = '!__!'
                type = 'likert'
                sub_section = ''

        sections.append(section)
        numbers.append(number)
        questions.append(formatted_question)
        types.append(type)
        sub_sections.append(sub_section)

    questions_df['section'] = sections
    questions_df['number'] = numbers
    questions_df['question'] = questions
    questions_df['type'] = types
    questions_df['sub_section'] = sub_sections

    # if write_questions_df:
    #     if not year:
    #         try:
    #             questions_df.to_excel('{}_questions_df.xlsx'.format(report_type), index=False)
    #         except:
    #             pass
    #     else:
    #         try:
    #             questions_df.to_excel('{}_{}_questions_df.xlsx'.format(year, report_type), index=False)
    #         except:
    #             pass

    na_no_null_dict = {'na': not_applicable_dict, 'no': not_observed_dict, 'null': empty_cell_dict}

    return questions_df, df, na_no_null_dict

def bea_splitter(file_name, raw_data_dict):
    df = pd.DataFrame.from_dict(raw_data_dict)
    df.set_index(df['UniqueIdentifier'], inplace=True)
    columns = list(df.columns)
    columns = [re.sub(r'\xa0', ' ', c) for c in columns]
    df.columns = columns

    workbook_args = []

    # Skill matrices
    skills_matrix = re.compile('.*skills\s+matrix.*', re.IGNORECASE)
    skills_matrices = list(filter(skills_matrix.match, columns))

    # Attribute matrices
    attribute_matrix = re.compile('.*attributes\s+matrix.*', re.IGNORECASE)
    attribute_matrices = list(filter(attribute_matrix.match, columns))

    # Start columns for BEA and committee sections
    re_first_section = re.compile('(?!.*\([Cc]ontinued\))(?!.*[Cc]ommittee)(?=^I\.|.*[\s\t]+I\.)')
    re_committee_sections = re.compile('(?!.*\([Cc]ontinued\))(?!.*:_[b-z]\.)(?=.*[Cc]ommittee)(?=^I\.|.*[\s\t]+I\.)')

    first_section = list(filter(re_first_section.match, columns))
    committee_sections = list(filter(re_committee_sections.match, columns))
    # Often, section headers will duplicate the name of the section across
    # multiple questions. We want to iterate backward and remove column numbers
    # from first_section and committee_sections where this duplication occurs
    # This way, there is only one value for the start column for BEA and committee reports
    if first_section:
        if len(first_section) > 1:
            column_numbers = [df.columns.get_loc(x) for x in first_section]
            first_section = [df.columns[min(column_numbers)]]

    # Iterate backwards. If the column count is one higher than the column count
    # of the value that precedes it, delete the current column from the list of
    # potential start columns.
    if committee_sections:
        for index, value in list(reversed(list(enumerate(committee_sections))))[:-1]:
            if df.columns.get_loc(committee_sections[index-1]) == df.columns.get_loc(value)-1:
                del committee_sections[index]

    # Create a dictionary with committee sections as keys and a list of booleans
    # as values indicating whether a row is null or contains Yes/yes
    committeeDictWithBoolValueKeys = {}
    if first_section and len(first_section) == 1:
        for col in df.columns.tolist()[:df.columns.get_loc(first_section[0])]:
            boolist = df[col].astype(str).str.contains(r'yes', flags=re.IGNORECASE, regex=True)
            if any(boolist):
                committeeDictWithBoolValueKeys[col] = boolist.tolist()
    elif committee_sections:
        for col in df.columns.tolist()[:df.columns.get_loc(committee_sections[0])]:
            boolist = df[col].astype(str).str.contains(r'yes', flags=re.IGNORECASE, regex=True)
            if any(boolist):
                committeeDictWithBoolValueKeys[col] = boolist.tolist()
    else:
        logging.warning('\nIt is not clear what column begins the first section of the survey.\nThe first column of the Board Excellence Assessment must contain "I."\nCommittee Sections Must also contain "I." and contain the word "Committee\nRevise the input file to try again."')
        return 1

    # Note, the first answer in a committee section cannot be "n/a"

    if committee_sections:
        committee_names_and_start_col = []
        for committee_start_column in committee_sections:
            # Here, we match committee indicator columns to the first column of a committee
            # section. The idea being, if a committee indicator column (e.g. "Audit") contains
            # values "Yes" or "yes", then the positions of those yes values will match non null
            # positions in the first column of a committee section.
            # Also, by deleting keys as they are discovered, we allow the possibility that
            # committees contain the same members, and as long as the committee survey data
            # appears in the input file in the same order that the committee indicator columns
            # do, then the committee reports will be labeled correctly

            if set(df.iloc[:,df.columns.get_loc(committee_start_column)].tolist()).issubset(set(['yes', 'no', '', 'Yes', 'No'])):
                beginColsBoolList = [bool(x) for x in df.iloc[:,df.columns.get_loc(committee_start_column)].tolist()]
                for k in list(committeeDictWithBoolValueKeys.keys()):
                    if committeeDictWithBoolValueKeys[k] == beginColsBoolList:
                        committee_names_and_start_col.append((k, df.columns.get_loc(committee_start_column)))
                        del committeeDictWithBoolValueKeys[k]
                        break
            else:
                beginColsBoolList = pd.to_numeric(df.iloc[:,df.columns.get_loc(committee_start_column)], errors='coerce').notnull().tolist()
                for k in list(committeeDictWithBoolValueKeys.keys()):
                    if committeeDictWithBoolValueKeys[k] == beginColsBoolList:
                        committee_names_and_start_col.append((k, df.columns.get_loc(committee_start_column)))
                        del committeeDictWithBoolValueKeys[k]
                        break

        if len(committee_sections) > 1:
            for index, values in enumerate(committee_names_and_start_col[:-1]):
                committee_name, commitee_start_col_number = values
                workbook_args.append((committee_name, commitee_start_col_number, committee_names_and_start_col[index+1][1]))
            workbook_args.append((committee_names_and_start_col[-1][0], committee_names_and_start_col[-1][1], len(df.columns)))
        else:
            for committee_name, commitee_start_col_number in committee_names_and_start_col:
                workbook_args.append((committee_name, commitee_start_col_number, len(df.columns)))

    if first_section:
        if not committee_sections:
            workbook_args.append(('BEA', df.columns.get_loc(first_section[0]), len(df.columns)))
        else:
            start_cols = []
            for committee in committee_sections:
                start_cols.append(df.columns.get_loc(committee))
            workbook_args.append(('BEA', df.columns.get_loc(first_section[0]), min(start_cols)))


    # !!! NOTE !!!
    # There exists the possibility that if two committees contain the same members,
    # and the committee sections at the end of survey are in an order different from the
    # order that they appear in the meta data section, then the
    # committee report will be labeled incorrectly.

    # Additionally, if a member is present in a committee but does not take part in the
    # committee survey, that committee indicator column will not match the first column
    # of the committee section. Deletion of the yes row in the committee indicator column
    # will fix this.

    # Here, we split the dataframe into sections by column and add them to dict_of_raw_bea_and_committee_data
    # What trimmed_df.replace(r'^\s*$', np.nan, regex=True) allows us is removing any
    # amount of white space. Including the string: "''" from columns that should be int type

    dict_of_raw_bea_and_committee_data = {}
    for arg in workbook_args:
        trimmed_df = df.iloc[:,arg[1]:arg[2]]
        trimmed_df = trimmed_df.replace(r'^\s*$', np.nan, regex=True)
        trimmed_df = trimmed_df.dropna(how='all', axis=0)
        dict_of_raw_bea_and_committee_data[arg[0]] = trimmed_df

    first_section_col_num = min(workbook_args, key = lambda t: t[1])[1]
    dict_of_raw_bea_and_committee_data['meta'] = df.iloc[:,:first_section_col_num]


    return dict_of_raw_bea_and_committee_data
