from flask import Flask
import os
app = Flask(__name__, static_url_path='/static')

if app.config['ENV'] == 'production':
    app.config.from_object("config.ProductionConfig")
elif app.config['ENV'] == 'testing':
    app.config.from_object("config.TestingConfig")
else:
    app.config.from_object("config.DevelopmentConfig")

# app.config['CSV_UPLOADS'] = os.path.join(app.root_path, 'static\\client\\csv')
# app.config['EXCEL_OUTPUT'] = os.path.join(app.root_path, 'static\\client\\excel')
# app.config['ZIP_OUTPUT'] = os.path.join(app.root_path, 'static\\client\\ZIPS')
# app.config['IMAGES'] = os.path.join(app.root_path, 'static\\img')
# app.config['CSS'] = os.path.join(app.root_path, 'static\\css')
# app.config['REPORTS'] = os.path.join(app.root_path, 'static\\client\\reports')
# app.config['REPORT_TEMPLATES'] = os.path.join(app.root_path, 'templates\\report_templates')
# app.config['CHARTS'] = os.path.join(app.root_path, 'static\\charts')

app.config['CSV_UPLOADS'] = os.path.join(app.root_path, 'static/client/csv')
app.config['EXCEL_OUTPUT'] = os.path.join(app.root_path, 'static/client/excel')
app.config['ZIP_OUTPUT'] = os.path.join(app.root_path, 'static/client/ZIPS')
app.config['IMAGES'] = os.path.join(app.root_path, 'static/img')
app.config['CSS'] = os.path.join(app.root_path, 'static/css')
app.config['REPORTS'] = os.path.join(app.root_path, 'static/client/reports')
app.config['REPORT_TEMPLATES'] = os.path.join(app.root_path, 'templates/report_templates')
app.config['CHARTS'] = os.path.join(app.root_path, 'static/charts')


from app import views
from app import error_handlers

