import pandas as pd
import numpy as np
import os
import glob
import re
from random import shuffle
from collections import Counter
import warnings
import xlsxwriter
from functools import reduce
from statistics import mean
import math
import sys
import itertools
import difflib
import logging
import time
from pandas import ExcelWriter
from decimal import Decimal, ROUND_HALF_UP
from itertools import cycle
from collections import OrderedDict
import matplotlib.pyplot as plt
# plt.switch_backend('agg')
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import matplotlib.colors as mc
# import seaborn as sns
from matplotlib.ticker import *
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import matplotlib.transforms
import matplotlib.ticker as ticker
from nameparser import HumanName


pd.options.mode.chained_assignment = None
class peer_individual_reports(object):
    def __init__(self, file_name, report_type, df, questions_df, ppl_df, na_no_null_dict, app, randomize, no_self_ratings_bool, year=None):
        self.file_name = file_name
        self.report_type = report_type
        self.df = df
        self.questions_df = questions_df
        self.ppl_df = ppl_df
        self.na_no_null_dict = na_no_null_dict
        self.section_data = dict()
        self.section_avg_list = dict()
        self.randomize = randomize
        self.no_self_ratings_bool = no_self_ratings_bool
        self.year = year
        self.skills_matrix_df = dict()
        self.skills_matrix_info = dict()

        self.app = app
        self.output_file_names = []

        self.hilo_data = dict()
        self.agg_section_data = dict()
        self.averages_data = dict()

        self.hilo_data = dict()
        self.agg_section_data = dict()
        self.averages_data = dict()
        self.na_in_heat_maps = dict()

        self.rows_of_survey_object = []
        self.shuffled_rows_of_survey_object = []
        self.anonymize = True
        self.skill_matrix_dfs = {}
        self.five_point_scale = False
        self.eval_stats_list_of_dicts = []
        self.esigmas = dict()
        self.heat_map_data = dict()
        self.initials = dict()

        self.ind_handler()

    def ind_handler(self):
        self.report_name = re.search('^(.*).csv', self.file_name).group(1)
        self.number_of_respondents = len(self.df)

        # Get Board wide statistics
        self.create_board_averages()

        for respondent in self.questions_df[~(self.questions_df['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (~self.questions_df['sub_section'].isin(['', None, np.nan]))]['sub_section'].unique():
            if len(self.questions_df[~(self.questions_df['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (self.questions_df['sub_section']==respondent)]['UniqueIdentifier'].unique()) > 1:
                pass
                # print('\nError!\nThere is a mismatch between vertical and horizontal axes.\n Review questions df for respondent: {} to ensure the UniqueIdentifier column matches the sub_section column'.format(respondent))
            else:
                id = self.questions_df[~(self.questions_df['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (self.questions_df['sub_section']==respondent)]['UniqueIdentifier'].unique()[0]
                if id in ['', np.nan, None, 'nan']:
                    id = respondent

            self.respondent = respondent
            self.id = id
            self.initial = '{}.{}.'.format(self.ppl_df.loc[id]['FirstName'][0].upper(), self.ppl_df.loc[id]['LastName'][0].upper())
            self.initials[respondent] = self.initial

            self.section_data[respondent] = dict()
            self.jinja_hi = None
            self.jinja_lo = None
            self.hilo_data[respondent] = dict()

            self.eval_stats_list_of_dicts = []

            self.section_avg_list[respondent] = []
            raw_q_list = self.questions_df[(self.questions_df['sub_section']==respondent) | (self.questions_df['UniqueIdentifier']==id)]['question_raw']
            raw_q_list_likert_matrix = self.questions_df[( (self.questions_df['sub_section']==respondent) | (self.questions_df['UniqueIdentifier']==id) ) & (self.questions_df['type'].isin(['likert', 'matrix']))]['question_raw']
            self.raw_q_list = raw_q_list
            self.ind_df = self.df.copy()
            self.ind_df = self.ind_df[raw_q_list]

            self.numeric_ind_df = self.df.copy()
            self.numeric_ind_df = self.numeric_ind_df[raw_q_list_likert_matrix]

            self.ind_q = self.questions_df[(self.questions_df['sub_section']==respondent) | (self.questions_df['UniqueIdentifier']==id)]
            if self.year:
                report_name = '{}_{}_Individual Peer Report.xlsx'.format(self.year, respondent, self.report_name)
            else:
                report_name = '{}_Individual Peer Report.xlsx'.format(respondent, self.report_name)

            self.output_file_names.append(report_name)
            self.workbook = xlsxwriter.Workbook(os.path.join(self.app.config['EXCEL_OUTPUT'], report_name), {'nan_inf_to_errors': True})

            self.rows_of_survey_object = []
            self.shuffled_rows_of_survey_object = []
            self.overview()
            self.statistics()

            self.hi_lo()
            self.heat_map()
            self.aggregated_section_analysis(respondent, id)
            self.peer_and_self_bar_chart(respondent, id)

            if self.skill_matrix_dfs:
                for tup in self.skill_matrix_dfs:
                    self.skills_matrix(tup)
                self.skills_matrix_dict_builder()

            self.workbook.close()


    def skills_matrix_dict_builder(self):
        temp_dict = dict()

        q_skills_df = self.questions_df[self.questions_df['type']=='skills_matrix']
        subsections = self.questions_df[self.questions_df['type']=='skills_matrix']['sub_section'].tolist()
        temp_dict['Skill Areas'] = subsections

        raw_q_skills_df = self.questions_df[self.questions_df['type']=='skills_matrix']['question_raw'].tolist()
        director_responses = self.df[raw_q_skills_df].loc[self.id]
        temp_dict['Self-Rating'] = director_responses

        board_averages = []
        for col in self.df[raw_q_skills_df]:
            board_averages.append(self.df[col].mean())

        temp_dict['Average Rating of All Directors'] = board_averages
        ind_skills_df = pd.DataFrame.from_dict(temp_dict)

        ind_skills_df.sort_values(by=['Self-Rating', 'Average Rating of All Directors'], inplace=True, ascending=False)

        board_average = str(Decimal('{}'.format(ind_skills_df['Average Rating of All Directors'].mean())).quantize(Decimal('1e-2'), ROUND_HALF_UP))
        ind_skills_df['Average Rating of All Directors'] = ind_skills_df['Average Rating of All Directors'].apply(lambda x: str(Decimal(x).quantize(Decimal('1e-2'), ROUND_HALF_UP)))
        ind_skills_df['Self-Rating'] = ind_skills_df['Self-Rating'].astype(int)

        # temp_dict = ind_skills_df.to_dict('list')
        temp_dict = dict()
        temp_dict['Self-Average'] = str(Decimal('{}'.format(ind_skills_df['Self-Rating'].mean())).quantize(Decimal('1e-2'), ROUND_HALF_UP))
        temp_dict['Board-Average'] = board_average

        self.skills_matrix_df[self.respondent] = ind_skills_df
        self.skills_matrix_info[self.respondent] = temp_dict

        self.write_df(ind_skills_df, 'Skills_Matrix')


    def create_board_averages(self):
        self.board_wide_averages = dict()
        for section in self.questions_df['section'].unique():
            if self.questions_df[self.questions_df['section']==section]['type'].unique()[0] not in ['nan', np.nan, None, 'skills_matrix', 'attribute_matrix']:
                likert_matrix_questions = self.questions_df[(self.questions_df['type'].isin(['likert', 'matrix'])) & (self.questions_df['section']==section)]['question_raw'].tolist()
                if likert_matrix_questions:
                    self.board_wide_averages[section] = str(Decimal('{}'.format(np.nanmean(self.df[likert_matrix_questions].stack()))).quantize(Decimal('1e-2'), ROUND_HALF_UP))

    def eSigma(self):
        es = pd.DataFrame(list(self.averages.items()), columns=['Section', 'Averages'])
        es['Section'] = es.apply(lambda row: row['Section'].replace(u'\xa0', u' '), axis=1)
        es['Averages'] = es['Averages'].astype(float)
        average_of_section_averages = es['Averages'].mean()
        qa = self.survey_object.copy()
        qa = qa[qa['question_type'].isin(['likert', 'matrix'])]
        qa = qa.loc[:,'1':].apply(pd.to_numeric)
        qa['average'] = qa.loc[:,'1':].apply(lambda row: row.mean(), axis=1)
        average_of_question_averages = qa['average'].mean()
        # This is a convient place to calculate part of the section analysis
        self.average_total_rating_given = qa['average'].sum()

        esigma = (average_of_question_averages + average_of_section_averages) / 2
        return esigma

    def individual_esigma(self, respondent):
        # print(respondent)
        q = self.questions_df.copy()

        # Different method to calculate average_of_section_averages by user
        data_dict = {}
        raw = q[(q['type'].isin(['likert', 'matrix']))&(q['sub_section']==respondent)]
        for section in raw['section'].unique():
            q_list = raw[raw['section']==section]['question_raw'].tolist()
            section_avg = self.df[q_list].stack().mean()
            data_dict[section] = section_avg
            # print(respondent, section, section_avg)
        es = pd.DataFrame(list(data_dict.items()), columns=['Section', 'Averages'])

        average_of_section_averages = es['Averages'].mean()
        # print(average_of_section_averages)

        # Calculate the average of all questions answered by respondent
        list_of_averages_by_question = []
        q_list = q[(q['type'].isin(['likert', 'matrix']))&(q['sub_section']==respondent)]['question_raw'].tolist()
        for q in q_list:
            question_avg = self.df[q].mean()
            list_of_averages_by_question.append(question_avg)

        average_of_question_averages = np.mean(list_of_averages_by_question)
        # print(average_of_question_averages)
        esigma = (average_of_question_averages + average_of_section_averages) / 2
        # return esigma
        return Decimal('{}'.format(esigma)).quantize(Decimal('1e-2'), ROUND_HALF_UP)

    def aggregated_section_analysis(self, respondent, id):
        worksheet = self.workbook.add_worksheet('Section Analysis')

        sec = self.survey_object.copy()

        header = self.workbook.add_format({"font_name": "Garamond",
                                         'bg_color': '#c4590f',
                                         'font_color': '#FFFFFF',
                                         'align': 'center'})
        header_left_align = self.workbook.add_format({"font_name": "Garamond",
                                         'bg_color': '#c4590f',
                                         'font_color': '#FFFFFF',
                                         'align': 'left',
                                         'bottom': 1,
                                         'left': 1,
                                         'top': 1,
                                         'border_color': '#c4590f'})
        cell_fmt1 = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#fbe4d5',
                                           'align': 'left',
                                           'bottom': 1,
                                           'left': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt2 = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#FFFFFF',
                                           'align': 'left',
                                           'bottom': 1,
                                           'left': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt1_centered = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#fbe4d5',
                                           'align': 'center',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt2_centered = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#FFFFFF',
                                           'align': 'center',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt1_right = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#fbe4d5',
                                           'align': 'right',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt2_right = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#FFFFFF',
                                           'align': 'right',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_bold = self.workbook.add_format({"font_name": "Garamond",
                                           'align': 'right',
                                           'bold': 1})
        cell_bold_left = self.workbook.add_format({"font_name": "Garamond",
                                           'align': 'left',
                                           'border': 1,
                                           'bold': 1,
                                           'border_color': '#f5b083'})
        orange_border_orange_bg = self.workbook.add_format({'border': 1,
                                                'border_color': '#f5b083',
                                                "font_name": "Garamond",
                                                'bg_color': '#fbe4d5',
                                                'align': 'center'
                                                })
        orange_border_white_bg = self.workbook.add_format({'border': 1,
                                                'border_color': '#f5b083',
                                                "font_name": "Garamond",
                                                'bg_color': '#FFFFFF',
                                                'align': 'center'
                                                })
        orange_border = self.workbook.add_format({'border': 1, 'border_color': '#f5b083'})

        worksheet.write('A1', 'Aggregated Section Analysis', header_left_align)
        worksheet.write('B1', 'Average', header)

        averages = pd.DataFrame(list(self.averages.items()), columns=['Section', 'Averages'])
        averages = averages[averages['Averages']!='nan']
        # averages['Section'] = averages.apply(lambda row: row['Section'].replace(u'\xa0', u' '), axis=1)
        currentRow = 2
        for index, row in averages.iterrows():
            if index % 2 == 0:
                worksheet.write(r'A{}'.format(index+2), row['Section'], cell_fmt1)
                worksheet.write(r'B{}'.format(index+2), str(row['Averages']), cell_fmt1_centered)
            else:
                worksheet.write(r'A{}'.format(index+2), row['Section'], cell_fmt2)
                worksheet.write(r'B{}'.format(index+2), str(row['Averages']), cell_fmt2_centered)
            currentRow += 1

        eSigma_value = self.eSigma()
        self.eSigma_value = eSigma_value

        currentRow += 2
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'Evaluation Statistics', header_left_align)
        currentRow += 1

        # Assessment Statistics
        #######
        worksheet.write('A{}'.format(currentRow), 'Number of rated questions', cell_fmt1)
        d = sec[sec['question_type'].isin(['likert', 'matrix'])]
        d = d.groupby(['section', 'number']).agg({'number': 'count'})

        worksheet.write('B{}'.format(currentRow), len(d['number']), cell_fmt1_right)
        self.eval_stats_list_of_dicts.append({'Number of rated questions': int(len(d['number']))})
        currentRow += 1
        #######

        worksheet.write('A{}'.format(currentRow), 'Number of open-ended questions', cell_fmt2)
        d = sec[sec['question_type'].isin(['long_form'])]
        d = d.groupby(['section', 'number']).agg({'number': 'count'})
        worksheet.write('B{}'.format(currentRow), len(d['number']), cell_fmt2_right)
        self.eval_stats_list_of_dicts.append({'Number of open-ended questions': int(len(d['number']))})
        currentRow += 1
        #######

        worksheet.write('A{}'.format(currentRow), 'Number of Board members surveyed', cell_fmt1)
        if self.no_self_ratings_bool:
            worksheet.write('B{}'.format(currentRow), self.number_of_respondents-1, cell_fmt1_right)
            self.eval_stats_list_of_dicts.append({'Number of Board members surveyed': self.number_of_respondents-1})
        else:
            worksheet.write('B{}'.format(currentRow), self.number_of_respondents, cell_fmt1_right)
            self.eval_stats_list_of_dicts.append({'Number of Board members surveyed': self.number_of_respondents})
        currentRow += 1
        #######

        worksheet.write('A{}'.format(currentRow), 'Percentage participation', cell_fmt2)
        # likert_matrix_questions = self.questions_df[self.questions_df['type'].isin(['likert', 'matrix'])]
        # print(self.df[likert_matrix_questions['question_raw'].tolist()].apply(pd.value_counts))

        raw_answers = sec[sec['question_type'].isin(['likert', 'matrix'])]
        raw_answers = raw_answers.loc[:,'1':]

        number_of_empty_strings = 0
        number_of_non_responses = 0
        for q in self.raw_q_list:
            number_of_non_responses += self.na_no_null_dict.get('na').get(q, 0)
            number_of_non_responses += self.na_no_null_dict.get('no').get(q, 0)
            number_of_empty_strings += self.na_no_null_dict.get('null').get(q, 0)

        # print('\n\n', self.respondent)

        # print(number_of_non_responses)
        len_index_times_len_columns = (len(raw_answers.index) * len(raw_answers.columns))
        # print(len_index_times_len_columns)
        number_of_eligible_responses = len_index_times_len_columns - number_of_empty_strings
        # print(number_of_eligible_responses)
        # number_of_questions_answered = number_of_eligible_responses - number_of_non_responses
        number_of_questions_answered = number_of_eligible_responses
        # print(number_of_questions_answered)
        percent_participation = ((number_of_questions_answered) / number_of_eligible_responses) * 100

        worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(percent_participation)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)

        percent_participation_formatted = "{0:.2f}%".format(Decimal('{}'.format(percent_participation)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
        if percent_participation_formatted == '100.00%':
            self.eval_stats_list_of_dicts.append({'Percentage participation': "100%"})
        elif percent_participation_formatted == '0.00%':
            self.eval_stats_list_of_dicts.append({'Percentage participation': "0%"})
        else:
            self.eval_stats_list_of_dicts.append({'Percentage participation': percent_participation_formatted})

        currentRow += 1
        #######

        q = self.questions_df.copy()
        likert_matrix_questions = q[(q['type'].isin(['likert', 'matrix']))]['question_raw'].tolist()

        worksheet.write('A{}'.format(currentRow), 'Total number of responses', cell_fmt1)
        total_number_of_responses = len_index_times_len_columns - number_of_empty_strings

        # try:
        #     d = self.df[likert_matrix_questions].loc[id]
        #     worksheet.write('B{}'.format(currentRow), len(d.dropna()), cell_fmt1_right)
        # except:
        #     worksheet.write('B{}'.format(currentRow), 0, cell_fmt1_right)

        worksheet.write('B{}'.format(currentRow), total_number_of_responses, cell_fmt1_right)

        self.eval_stats_list_of_dicts.append({'Total number of responses': int(total_number_of_responses)})
        currentRow += 1
        #######
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'Question analysis by Board member response:', cell_bold_left)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Question analysis by Board member response:': ''})
        currentRow += 1
        #######
        worksheet.write('A{}'.format(currentRow), 'Maximum possible total rating', cell_fmt1)
        if self.five_point_scale == False:
            worksheet.write('B{}'.format(currentRow), int(len(sec[sec['question_type'].isin(['likert', 'matrix'])])*10), cell_fmt1_right)
            max_rating = int(len(raw_answers)*10)
        else:
            worksheet.write('B{}'.format(currentRow), int(len(sec[sec['question_type'].isin(['likert', 'matrix'])])*5), cell_fmt1_right)
            max_rating = int(len(raw_answers)*5)
        self.eval_stats_list_of_dicts.append({'Maximum possible total rating': max_rating})
        currentRow += 1
        #######

        # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
        #     print(raw_answers)

        raw_answers = raw_answers.apply(pd.to_numeric)

        worksheet.write('A{}'.format(currentRow), 'Average total rating given', cell_fmt2)
        worksheet.write('B{}'.format(currentRow), str(Decimal('{}'.format((raw_answers.sum(axis=0).sum())/len(raw_answers.columns))).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
        self.eval_stats_list_of_dicts.append({'Average total rating given': str(Decimal('{}'.format((raw_answers.sum(axis=0).sum())/len(raw_answers.columns))).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
        currentRow += 1




        #######

        check = 0
        low_df = self.numeric_ind_df.copy()
        for k, v in low_df.iterrows():
            if (len(v.dropna()) > 0) and (k != self.id):
                if v.dropna().values.sum() > check:
                    check = v.dropna().values.sum()

        worksheet.write('A{}'.format(currentRow), 'Highest total rating given', cell_fmt1)
        # check = 0
        raw_answers = raw_answers.apply(pd.to_numeric)
        # for c in raw_answers.columns:
        #     if raw_answers[c].sum()>check:
        #         check = raw_answers[c].sum()
        worksheet.write('B{}'.format(currentRow), int(check), cell_fmt1_right)
        self.eval_stats_list_of_dicts.append({'Highest total rating given': int(check)})
        currentRow += 1
        #######
        # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
        #     print(self.ind_df.dropna())

        # Sum counts n/a as a 0
        check = max_rating
        low_df = self.numeric_ind_df.copy()
        for k, v in low_df.iterrows():
            if (len(v.dropna()) > 0) and (k != self.id):
                if v.dropna().values.sum() < check:
                    check = v.dropna().values.sum()

        worksheet.write('A{}'.format(currentRow), 'Lowest total rating given', cell_fmt2)
        # check = len(sec[sec['question_type'].isin(['likert', 'matrix'])])*10
        # for c in raw_answers.columns:
        #     if raw_answers[c].sum()<check:
        #         check = raw_answers[c].sum()
        worksheet.write('B{}'.format(currentRow), int(check), cell_fmt2_right)
        self.eval_stats_list_of_dicts.append({'Lowest total rating given': int(check)})
        currentRow += 1
        #######
        worksheet.write('A{}'.format(currentRow), 'Maximum possible average', cell_fmt1)
        if self.five_point_scale == False:
            worksheet.write('B{}'.format(currentRow), '10.00', cell_fmt1_right)
            scale_value = '10.00'
        else:
            worksheet.write('B{}'.format(currentRow), '5.00', cell_fmt1_right)
            scale_value = '5.00'
        self.eval_stats_list_of_dicts.append({'Maximum possible average': scale_value})
        currentRow += 1
        #######
        worksheet.write('A{}'.format(currentRow), 'Highest rating average given', cell_fmt2)
        check = 0
        for c in raw_answers.columns:
            if raw_answers[c].mean()>check:
                check = raw_answers[c].mean()
        worksheet.write('B{}'.format(currentRow), str(Decimal('{}'.format(check)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
        self.eval_stats_list_of_dicts.append({'Highest rating average given': str(Decimal('{}'.format(check)).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
        currentRow += 1
        #######
        worksheet.write('A{}'.format(currentRow), 'Lowest rating average given', cell_fmt1)
        check = 10
        for c in raw_answers.columns:
            if raw_answers[c].mean()<check:
                check = raw_answers[c].mean()
        worksheet.write('B{}'.format(currentRow), str(Decimal('{}'.format(check)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
        self.eval_stats_list_of_dicts.append({'Lowest rating average given': str(Decimal('{}'.format(check)).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
        currentRow += 1
        #######
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'Group response analysis:', cell_bold_left)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Group response analysis:': ''})
        # This section needs try, except clauses for the case that value counts returns a zero count for whole range of answers
        # Create one series from the raw answers to use value_counts function
        series_of_all_answers = pd.Series(dtype='float')
        for c in raw_answers.columns:
            series_of_all_answers = series_of_all_answers.append(raw_answers[c], ignore_index=True)
        counts = raw_answers.apply(pd.value_counts)
        counts.fillna(0, inplace=True)
        if self.five_point_scale == False:
            new_index = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        else:
            new_index = [1, 2, 3, 4, 5]
        counts = counts.reindex(new_index, fill_value=0)
        currentRow += 1
        if self.five_point_scale == False:
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 10', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format((counts.loc[10].sum()/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            percentage_of_ten_ratings_formatted = "{0:.2f}%".format(Decimal('{}'.format((counts.loc[10].sum()/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if percentage_of_ten_ratings_formatted == '100.00%':
                percentage_of_ten_ratings_formatted = '100%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 10': percentage_of_ten_ratings_formatted})
            elif percentage_of_ten_ratings_formatted == '0.00%':
                percentage_of_ten_ratings_formatted = '0%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 10': percentage_of_ten_ratings_formatted})
            else:
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 10': percentage_of_ten_ratings_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 8 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            eight_or_above_formatted = "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if eight_or_above_formatted == '100.00%':
                eight_or_above_formatted = '100%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 8 or above': eight_or_above_formatted})
            elif eight_or_above_formatted == '0.00%':
                eight_or_above_formatted = '0%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 8 or above': eight_or_above_formatted})
            else:
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 8 or above': eight_or_above_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 7 or below', cell_fmt1)
            seven_or_below = number_of_questions_answered - (counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format((seven_or_below/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            percentage_seven_below_formatted = "{0:.2f}%".format(Decimal('{}'.format((seven_or_below/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if percentage_seven_below_formatted == '100.00%':
                percentage_seven_below_formatted = '100%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 7 or below': percentage_seven_below_formatted})
            elif percentage_seven_below_formatted == '0.00%':
                percentage_seven_below_formatted = '0%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 7 or below': percentage_seven_below_formatted})
            else:
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 7 or below': percentage_seven_below_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 3 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            percentage_three_below_formatted = "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if percentage_three_below_formatted == '100.00%':
                percentage_three_below_formatted = '100%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted})
            elif percentage_three_below_formatted == '0.00%':
                percentage_three_below_formatted = '0%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted})
            else:
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted})
            currentRow += 1
            if number_of_non_responses:
                x = cell_fmt2
                cell_fmt2 = cell_fmt1
                cell_fmt1 = x
                x = cell_fmt2_right
                cell_fmt2_right = cell_fmt1_right
                cell_fmt1_right = x
                worksheet.write('A{}'.format(currentRow), 'Percent of ratings of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), str(Decimal('{}'.format(((number_of_non_responses)/(number_of_questions_answered)))).quantize(Decimal('1e-2'), ROUND_HALF_UP))+'%', cell_fmt2_right)
                self.eval_stats_list_of_dicts.append({'Percent of ratings of N/A': str(Decimal('{}'.format(((number_of_non_responses)/(number_of_questions_answered)))).quantize(Decimal('1e-2'), ROUND_HALF_UP))+'%'})
                currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 10', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), counts.loc[10].sum(), cell_fmt1_right)
            self.eval_stats_list_of_dicts.append({'Number of ratings of 10': int(counts.loc[10].sum())})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 8 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum()), cell_fmt2_right)
            self.eval_stats_list_of_dicts.append({'Number of ratings of 8 or above': int(counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 7 or below', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), seven_or_below, cell_fmt1_right)
            self.eval_stats_list_of_dicts.append({'Number of ratings of 7 or below': int(seven_or_below)})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 3 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum()), cell_fmt2_right)
            self.eval_stats_list_of_dicts.append({'Number of ratings of 3 or below': int(counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())})
            currentRow += 1
            if number_of_non_responses:
                worksheet.write('A{}'.format(currentRow), 'Number of ratings of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), str(int(number_of_non_responses)), cell_fmt2_right)
                self.eval_stats_list_of_dicts.append({'Number of ratings of N/A': str(int(number_of_non_responses))})
                currentRow += 1
        else:
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 5', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format((counts.loc[5].sum()/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            percentage_five_formatted = "{0:.2f}%".format(Decimal((counts.loc[5].sum()/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if percentage_five_formatted == '100.00%':
                percentage_five_formatted = '100%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 5': percentage_five_formatted})
            elif percentage_five_formatted == '0.00%':
                percentage_five_formatted = '0%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 5': percentage_five_formatted})
            else:
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 5': percentage_five_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 4 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[4].sum() + counts.loc[5].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            percentage_four_above_formatted = "{0:.2f}%".format(Decimal(((counts.loc[4].sum() + counts.loc[5].sum())/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if percentage_four_above_formatted == '100.00%':
                percentage_four_above_formatted = '100%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 4 or above': percentage_four_above_formatted})
            elif percentage_four_above_formatted == '0.00%':
                percentage_four_above_formatted = '0%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 4 or above': percentage_four_above_formatted})
            else:
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 4 or above': percentage_four_above_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 3 or below', cell_fmt1)
            three_or_below = (counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format((three_or_below/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            percentage_three_below_formatted_five_scale = "{0:.2f}%".format(Decimal((three_or_below/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if percentage_three_below_formatted_five_scale == '100.00%':
                percentage_three_below_formatted_five_scale = '100%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted_five_scale})
            elif percentage_three_below_formatted_five_scale == '0.00%':
                percentage_three_below_formatted_five_scale = '0%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted_five_scale})
            else:
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted_five_scale})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 2 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[1].sum() + counts.loc[2].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            percentage_two_below_formatted = "{0:.2f}%".format(Decimal(((counts.loc[1].sum() + counts.loc[2].sum())/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if percentage_two_below_formatted == '100.00%':
                percentage_two_below_formatted = '100%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 2 or below': percentage_two_below_formatted})
            elif percentage_two_below_formatted == '0.00%':
                percentage_two_below_formatted = '0%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 2 or below': percentage_two_below_formatted})
            else:
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 2 or below': percentage_two_below_formatted})
            currentRow += 1
            if number_of_non_responses:
                x = cell_fmt2
                cell_fmt2 = cell_fmt1
                cell_fmt1 = x
                x = cell_fmt2_right
                cell_fmt2_right = cell_fmt1_right
                cell_fmt1_right = x
                worksheet.write('A{}'.format(currentRow), 'Percent of ratings of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((number_of_non_responses)/(number_of_questions_answered))*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
                self.eval_stats_list_of_dicts.append({'Percent of ratings of N/A': "{0:.2f}%".format(Decimal('{}'.format(((number_of_non_responses)/(number_of_questions_answered))*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
                currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 5', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), counts.loc[5].sum(), cell_fmt1_right)
            self.eval_stats_list_of_dicts.append({'Number of ratings of 5': int(counts.loc[5].sum())})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 4 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[4].sum() + counts.loc[5].sum()), cell_fmt2_right)
            self.eval_stats_list_of_dicts.append({'Number of ratings of 4 or above': int(counts.loc[4].sum() + counts.loc[5].sum())})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 3 or below', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), three_or_below, cell_fmt1_right)
            self.eval_stats_list_of_dicts.append({'Number of ratings of 3 or below': int(three_or_below)})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 2 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[1].sum() + counts.loc[2].sum()), cell_fmt2_right)
            self.eval_stats_list_of_dicts.append({'Number of ratings of 2 or below': int(counts.loc[1].sum() + counts.loc[2].sum())})
            currentRow += 1
            if number_of_non_responses:
                worksheet.write('A{}'.format(currentRow), 'Number of ratings of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), str(int(number_of_non_responses)), cell_fmt2_right)
                currentRow += 1
                self.eval_stats_list_of_dicts.append({'Number of ratings of N/A': str(int(number_of_non_responses))})
        currentRow += 2
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'e∑igma = {0:.2f}'.format(Decimal('{}'.format(eSigma_value)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), orange_border_white_bg)
        self.esigmas[self.respondent]= str(Decimal('{}'.format(eSigma_value)).quantize(Decimal('1e-2'), ROUND_HALF_UP))

        worksheet.set_column(0,0 ,75)
        worksheet.set_column(1,1,10)
        self.agg_section_data[self.respondent] = self.eval_stats_list_of_dicts
        return

    def hi_lo(self):
        worksheet = self.workbook.add_worksheet('Hi-Lo')
        q = self.ind_q.copy()
        q = q[q['type'].isin(['likert', 'matrix'])]

        worksheet.set_column(1,1 ,6)
        worksheet.set_column(2,2 ,22)
        worksheet.set_column(3,3 ,4)
        worksheet.set_column(4,4 ,60)

        question_averages = []
        sections = []
        numbers = []
        questions = []
        for section in q['section'].unique():
            for number in q['number'].unique():
                try:
                    raw = q[(q['number']==number)&(q['section']==section)]['question_raw'].tolist()
                    question_averages.append(self.df[raw].stack().mean())
                    sections.append(section)
                    numbers.append(number)
                    questions.append(q[(q['number']==number)&(q['section']==section)]['question'].tolist()[0])
                except:
                    pass
        hilo = pd.DataFrame({'section': sections, 'number': numbers, 'question_average': question_averages, 'question': questions})

        if len(self.questions_df.drop_duplicates(['section','number']).index) < 10:
            number_for_hi_and_low = math.floor(len(self.questions_df.drop_duplicates(['section','number']).index)/2)
            hi = hilo.nlargest(number_for_hi_and_low, 'question_average', keep='all')
            lo = hilo.nsmallest(number_for_hi_and_low, 'question_average', keep='all')

            jinja_hi = hilo.nlargest(number_for_hi_and_low, 'question_average', keep='first')
            jinja_lo = hilo.nsmallest(number_for_hi_and_low, 'question_average', keep='first')
        else:
            hi = hilo.nlargest(5, 'question_average', keep='all')
            lo = hilo.nsmallest(5, 'question_average', keep='all')

            jinja_hi = hilo.nlargest(5, 'question_average', keep='first')
            jinja_lo = hilo.nsmallest(5, 'question_average', keep='first')


        hi['question_average'] = hi.apply(lambda row: str(Decimal('{}'.format(row['question_average'])).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)
        lo['question_average'] = lo.apply(lambda row: str(Decimal('{}'.format(row['question_average'])).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)
        hi['section'] = hi['section'].str.extract(r'\b([IVX]+\.)?')
        lo['section'] = lo['section'].str.extract(r'\b([IVX]+\.)?')


        jinja_hi['question_average'] = jinja_hi.apply(lambda row: str(Decimal(row['question_average']).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)
        jinja_lo['question_average'] = jinja_lo.apply(lambda row: str(Decimal(row['question_average']).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)

        hi_header = self.workbook.add_format({"font_name": "Garamond",
                                         'bold': 2,
                                         'bg_color': '#008000',
                                         'font_color': '#FFFFFF',
                                         'align': 'center',
                                         'border': 1,
                                         'border_color': '#FFFFFF',
                                         'font_size': 12})
        hi_header_left_align = self.workbook.add_format({"font_name": "Garamond",
                                         'bold': 1,
                                         'bg_color': '#008000',
                                         'font_color': '#FFFFFF',
                                         'align': 'left',
                                         'border': 1,
                                         'border_color': '#FFFFFF',
                                         'font_size': 12})
        cell_fmt = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#F3F3F3',
                                           'align': 'left',
                                           'border': 1,
                                           'border_color': '#FFFFFF',
                                           'valign': 'top',
                                           'text_wrap': True,
                                           'font_size': 12})
        cell_fmt_centered = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#F3F3F3',
                                           'align': 'center',
                                           'border': 1,
                                           'border_color': '#FFFFFF',
                                           'valign': 'top',
                                           'font_size': 12})

        worksheet.merge_range('B2:E2', 'Highest Rated Questions', hi_header)
        worksheet.write('B3', 'Rating', hi_header)
        worksheet.write('C3', 'Section', hi_header)
        worksheet.merge_range('D3:E3', 'Question', hi_header_left_align)

        ###############
        hi['formatted_number'] = hi['number']
        jinja_hi['formatted_number'] = jinja_hi['number']

        worksheet.write_column('B4', hi['question_average'], cell_fmt_centered)
        worksheet.write_column('C4', hi['section'], cell_fmt_centered)
        ###############

        # # hi['formatted_number'] = hi['formatted_number'].str.replace(r'\.', r'')
        # # hi['formatted_number'] += '.'
        # worksheet.write_column('D4', hi['number'], cell_fmt)
        # worksheet.write_column('E4', hi['question'], cell_fmt)

        ###############
        hi['formatted_number'] = hi['formatted_number'].str.replace(r'\.', r'')
        hi['formatted_number'] += '.'

        jinja_hi['formatted_number'] = jinja_hi['formatted_number'].str.replace(r'\.', r'')
        jinja_hi['formatted_number'] += '.'
        ###############

        worksheet.write_column('D4', hi['formatted_number'], cell_fmt_centered)
        worksheet.write_column('E4', hi['question'], cell_fmt)

        lo_header = self.workbook.add_format({"font_name": "Garamond",
                                         'bold': 1,
                                         'bg_color': '#FFFF00',
                                         'align': 'center',
                                         'border': 1,
                                         'border_color': '#FFFFFF',
                                         'font_size': 12})
        lo_header_left_align = self.workbook.add_format({"font_name": "Garamond",
                                         'bold': 1,
                                         'bg_color': '#FFFF00',
                                         'align': 'left',
                                         'border': 1,
                                         'border_color': '#FFFFFF',
                                     'font_size': 12})

        worksheet.merge_range('B{0}:E{0}'.format(int(4 + len(hi))), 'Lowest Rated Questions', lo_header)
        worksheet.write('B{}'.format(int(5 + len(hi))), 'Rating', lo_header)
        worksheet.write('C{}'.format(int(5 + len(hi))), 'Section', lo_header)
        worksheet.merge_range('D{0}:E{0}'.format(int(5 + len(hi))), 'Question', lo_header_left_align)

        # lo['formatted_number'] = lo['number']
        #
        # worksheet.write_column('B{}'.format(int(6 + len(hi))), lo['question_average'], cell_fmt)
        # worksheet.write_column('C{}'.format(int(6 + len(hi))), lo['section'], cell_fmt)
        # # lo['formatted_number'] = lo['formatted_number'].str.replace(r'\.', r'')
        # # lo['formatted_number'] += '.'
        # worksheet.write_column('D{}'.format(int(6 + len(hi))), lo['formatted_number'], cell_fmt)
        # worksheet.write_column('E{}'.format(int(6 + len(hi))), lo['question'], cell_fmt)

        ###############
        lo['formatted_number'] = lo['number']
        jinja_lo['formatted_number'] = jinja_lo['number']
        ###############

        worksheet.write_column('B{}'.format(int(6 + len(hi))), lo['question_average'], cell_fmt_centered)
        worksheet.write_column('C{}'.format(int(6 + len(hi))), lo['section'], cell_fmt_centered)

        ###############
        lo['formatted_number'] = lo['formatted_number'].str.replace(r'\.', r'')
        lo['formatted_number'] += '.'

        jinja_lo['formatted_number'] = jinja_lo['formatted_number'].str.replace(r'\.', r'')
        jinja_lo['formatted_number'] += '.'
        ###############

        worksheet.write_column('D{}'.format(int(6 + len(hi))), lo['formatted_number'], cell_fmt_centered)
        worksheet.write_column('E{}'.format(int(6 + len(hi))), lo['question'], cell_fmt)

        jinja_hi['section'] = jinja_hi['section'].str.extract(r'\b([IVX]+\.)?')
        jinja_lo['section'] = jinja_lo['section'].str.extract(r'\b([IVX]+\.)?')

        self.jinja_hi = jinja_hi
        self.jinja_lo = jinja_lo

        self.hilo_data[self.respondent]['jinja_hi'] = jinja_hi
        self.hilo_data[self.respondent]['jinja_lo'] = jinja_lo
        return

    def peer_and_self_bar_chart(self, respondent, id):
        respondent = re.sub('&#8217;', "", respondent)
        worksheet = self.workbook.add_worksheet('Peer and Self Bar Chart')

        headers = ['Section', 'Director Self-Rating', 'Peer Rating of {}'.format(respondent), 'Average Rating of All Directors']
        fmt = self.workbook.add_format({"font_name": "Garamond"})
        bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': 1, 'num_format': "0.00"})

        df = pd.DataFrame()

        # Get Director Self-Rating
        bar_dict = {}
        bar_dict['Director Self-Rating'] = []
        sections = []
        q = self.questions_df.copy()
        q = q[q['type'].isin(['likert', 'matrix'])]

        bar_dict['Director Self-Rating'] = []
        bar_dict['Peer Rating of {}'.format(respondent)] = []
        bar_dict['Average Rating of All Directors'] = []
        for section in q['section'].unique():
            likert_matrix_questions = q[(q['type'].isin(['likert', 'matrix'])) & (q['section']==section) & (q['UniqueIdentifier']==id)]['question_raw'].tolist()
            if not likert_matrix_questions:
                likert_matrix_questions = q[(q['type'].isin(['likert', 'matrix'])) & (q['section']==section) & (q['sub_section']==id)]['question_raw'].tolist()
            # Get Director Self-Rating
            try:
                d = self.df[likert_matrix_questions].loc[id]
            except:
                d = []
            # print(type(d))
            if len(d) > 0:
                avg = str(Decimal('{}'.format(d.mean())).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                bar_dict['Director Self-Rating'].append(avg)
                section_abr = re.search(r'\b([IVX]+\.)?', section)
                if section_abr:
                    section_label = section_abr.group(1)
                    sections.append(section_label)
                else:
                    sections.append(section)
            else:
                bar_dict['Director Self-Rating'].append(0)
                section_abr = re.search(r'\b([IVX]+\.)?', section)
                if section_abr:
                    section_label = section_abr.group(1)
                    sections.append(section_label)
                else:
                    sections.append(section)

            # Get Peer Rating of Director
            others_ids = self.ppl_df.index
            try:
                others_ids = others_ids.drop(id)
            except:
                pass
            d = self.df[likert_matrix_questions].loc[others_ids]
            # print('\n\n', self.id, self.respondent, d)
            # print('here', type(d))
            if len(d) > 0:
                bar_dict['Peer Rating of {}'.format(respondent)].append(d.stack().mean())

            # Get Average of all directors
            others_ratings = q[(q['type'].isin(['likert', 'matrix'])) & (q['section']==section)]['question_raw'].tolist()
            # print(others_ratings)
            d = self.df[others_ratings]
            if len(d) > 0:
                bar_dict['Average Rating of All Directors'].append(d.stack().mean())

        df = pd.DataFrame(bar_dict)
        # print(df)
        df['Peer Rating of {}'.format(respondent)] = df.apply(lambda row: str(Decimal('{}'.format(row['Peer Rating of {}'.format(respondent)])).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)
        df['Average Rating of All Directors'] = df.apply(lambda row: str(Decimal('{}'.format(row['Average Rating of All Directors'])).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)
        df['Peer Rating of {}'.format(respondent)] = pd.to_numeric(df['Peer Rating of {}'.format(respondent)])
        df['Average Rating of All Directors'] = pd.to_numeric(df['Average Rating of All Directors'])

        df['Director Self-Rating'] = df.apply(lambda row: str(Decimal('{}'.format(row['Director Self-Rating'])).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)
        try:
            df['Director Self-Rating'] = pd.to_numeric(df['Director Self-Rating'])
        except:
            df['Director Self-Rating'] = 0

        # print(df)
        worksheet.write_row(0, 0, headers, bold_fmt)
        worksheet.write_column(1, 0, sections, bold_fmt)
        col = 1

        for l in df:
            worksheet.write_column(1, col, df[l], bold_fmt)
            col+= 1

        chart1 = self.workbook.add_chart({'type': 'column', 'name_font': {'font_name': 'Garamond', 'bold': True}})

        col = 1
        for c in range(0,3):
            if col == 1:
                chart1.add_series({
                    'name':       ['Peer and Self Bar Chart', 0, col],
                    'categories': ['Peer and Self Bar Chart', 1, 0, len(sections), 0],
                    'values':     ['Peer and Self Bar Chart', 1, col, len(sections), col],
                    'fill': {'color': '#F6B524'},
                    'data_labels': {'value': True, 'position': 'inside_end', 'font': {'rotation': -90, 'name': 'Garamond', 'bold': True, 'size': 22,}},
                    'gap': 50,
                    'name_font': {'font_name': 'Garamond', 'bold': True}
                })
                col+=1
            elif col == 2:
                chart1.add_series({
                    'name':       ['Peer and Self Bar Chart', 0, col],
                    'categories': ['Peer and Self Bar Chart', 1, 0, len(sections), 0],
                    'values':     ['Peer and Self Bar Chart', 1, col, len(sections), col],
                    'fill': {'color': '#BD5E28'},
                    'data_labels': {'value': True, 'position': 'inside_end', 'font': {'rotation': -90, 'name': 'Garamond', 'bold': True, 'size': 22, 'color':'white'}},
                    'gap': 50,
                    'name_font': {'font_name': 'Garamond', 'bold': True}
                })
                col+=1
            else:
                chart1.add_series({
                    'name':       ['Peer and Self Bar Chart', 0, col],
                    'categories': ['Peer and Self Bar Chart', 1, 0, len(sections), 0],
                    'values':     ['Peer and Self Bar Chart', 1, col, len(sections), col],
                    'fill': {'color': '#A31E23'},
                    'data_labels': {'value': True, 'position': 'inside_end', 'font': {'rotation': -90, 'name': 'Garamond', 'bold': True, 'size':22, 'color':'white'}},
                    'gap': 50,
                    'name_font': {'font_name': 'Garamond', 'bold': True}
                })
                col+=1

        if self.five_point_scale == False:
            chart1.set_y_axis({'text_axis': True, 'major_gridlines': {'visible': True, 'line': {'width': 1}}, 'min': 1.00, 'max': 10.00, 'major_unit': 1, 'num_font':  {'name': 'Garamond'}})
        else:
            chart1.set_y_axis({'text_axis': True, 'major_gridlines': {'visible': True, 'line': {'width': 1}}, 'min': 1.00, 'max': 5.00, 'major_unit': 1, 'num_font':  {'name': 'Garamond'}})


        chart1.set_x_axis({'text_axis': True, 'major_gridlines': {'visible': True, 'line': {'width': 1}}, 'minor_gridlines': {'visible': True, 'line': {'width': 1}}, 'num_font':  {'name': 'Garamond', 'bold':True, 'size':16}})

        chart1.set_legend({'font':  {'name': 'Garamond'}})

        worksheet.insert_chart('D2', chart1, {'x_offset': 25, 'y_offset': 10, 'x_scale': 2, 'y_scale': 1.5})

        ########################
        df['sections'] = sections
        # df.to_pickle(r'C:\Users\Drew\Peer Reports\peer_self_bar.pkl')

        # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
        #     print(df)


        fig, ax = plt.subplots(figsize=(20,10))

        if not self.no_self_ratings_bool:
            # Setting the positions and width for the bars
            pos = list(range(len(df)))
            num_col = len(df.columns) - 1
            width = .6/ num_col
            bar_colors = ['#F6B524', '#BD5E28', '#A31E23']
            bar_labels = df.columns[:-1]

            for i, (colname, color, lbl) in enumerate(zip(df.columns[:-1], bar_colors, bar_labels)):
                # print(i, colname, color, lbl)
                delta_p = .3 + width*i
            #     print(delta_p)
                plt.bar([p + delta_p for p in pos],
                        df[colname], width, color=color, label=lbl)

            labels = list(df[df.columns[:-1]].unstack().values)
        else:
            # Setting the positions and width for the bars
            pos = list(range(len(df)))
            num_col = len(df.columns) - 2
            width = .8/ num_col
            bar_colors = ['#BD5E28', '#A31E23']
            bar_labels = df.columns[1:-1]

            for i, (colname, color, lbl) in enumerate(zip(df.columns[1:-1], bar_colors, bar_labels)):
                # print(i, colname, color, lbl)
                delta_p = .3 + width*i
            #     print(delta_p)
                plt.bar([p + delta_p for p in pos],
                        df[colname], width, color=color, label=lbl)

            labels = list(df[df.columns[1:-1]].unstack().values)

        ax.set_xticks(pos)

        def update_ticks(x, pos):
            return df['sections'][pos]

        ax.xaxis.set_major_formatter(ticker.NullFormatter())
        ax.xaxis.set_minor_formatter(ticker.FuncFormatter(update_ticks))
        ax.xaxis.set_minor_locator(ticker.FixedLocator([p+0.5 for p in pos]))

        for tick in ax.xaxis.get_minor_ticks():
            tick.tick1line.set_markersize(0)
            tick.tick2line.set_markersize(0)
            tick.label1.set_horizontalalignment('center')
        plt.xlim(min(pos), max(pos)+1)

        # plt.ylim([0, 10.0+max([max(df[colname]) for colname in df.columns[:-1]])])

        # if len(self.questions_df['section'].unique()) > 1:
        plt.legend()
        plt.legend(bbox_to_anchor=(1,  .5), prop={'size':28, 'family': 'Garamond'})

        plt.grid(which='major', axis='y', linestyle='solid')

        if len(df) > 1:
            ax.set_xlabel('Sections', weight='bold', size=30, fontname='Garamond')
        else:
            ax.set_xlabel('Section', weight='bold', size=30, fontname='Garamond')
        ax.xaxis.labelpad = 20

        plt.setp(ax.get_xminorticklabels(), weight='bold', fontsize=30, fontname='Garamond')

        ax.set_axisbelow(True)

        rects = ax.patches
        if not self.no_self_ratings_bool:
            count = 1
            for rect, label in zip(rects, labels):
                height = rect.get_height()
                if count <= len(rects)/3:
                    if label > 1.0:
                        if not self.five_point_scale:
                            ax.text(rect.get_x() + rect.get_width() / 2, height -1.5, str(Decimal('{}'.format(label)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), rotation=90,
                                ha='center', va='bottom', **{'fontname':'Garamond', 'fontsize': 38, 'weight':'bold'})
                        else:
                            ax.text(rect.get_x() + rect.get_width() / 2, height -.7, str(Decimal('{}'.format(label)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), rotation=90,
                                ha='center', va='bottom', **{'fontname':'Garamond', 'fontsize': 38, 'weight':'bold'})
                    else:
                        pass
                else:
                    if label > 1.0:
                        if not self.five_point_scale:
                            ax.text(rect.get_x() + rect.get_width() / 2, height -1.5, str(Decimal('{}'.format(label)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), rotation=90,
                                ha='center', va='bottom', **{'fontname':'Garamond', 'fontsize': 38, 'weight':'bold', 'color':'white'})
                        else:
                            ax.text(rect.get_x() + rect.get_width() / 2, height -.7, str(Decimal('{}'.format(label)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), rotation=90,
                                ha='center', va='bottom', **{'fontname':'Garamond', 'fontsize': 38, 'weight':'bold', 'color':'white'})
                    else:
                        pass

                count+=1

        else:
            for rect, label in zip(rects, labels):
                height = rect.get_height()
                if label > 1.0:
                    if not self.five_point_scale:
                        ax.text(rect.get_x() + rect.get_width() / 2, height -1.5, str(Decimal('{}'.format(label)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), rotation=90,
                            ha='center', va='bottom', **{'fontname':'Garamond', 'fontsize': 38, 'weight':'bold', 'color':'white'})
                    else:
                        ax.text(rect.get_x() + rect.get_width() / 2, height -.7, str(Decimal('{}'.format(label)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), rotation=90,
                            ha='center', va='bottom', **{'fontname':'Garamond', 'fontsize': 38, 'weight':'bold', 'color':'white'})
                else:
                    pass

        if self.five_point_scale == True:
            number1 = 5.00
            number2 = 6.00
        elif self.five_point_scale == False:
            number1 = 10.00
            number2 = 11.00

        ax.set_ylim([1, number1])
        ax.set_yticklabels(np.arange(1.00, number2, 1))

        ax.set_ylabel('Rating', weight='bold', size=24, fontname='Garamond')
        ax.set_yticklabels(ax.get_yticks(), weight='bold', size=16, fontname='Garamond')

        # print('\n\n')
        # print(self.respondent)
        # print('\n\n')
        print_safe_respondent = re.sub('&#8217;', "", self.respondent)
        # print('\n\nHERE: ', print_safe_respondent, '\n\n')
        if self.year:
            plt.savefig(os.path.join(self.app.config['CHARTS'], '{}_{}_peer_self.png'.format(self.year, print_safe_respondent)), bbox_inches='tight', dpi=300)
        else:
            plt.savefig(os.path.join(self.app.config['CHARTS'], '{}_peer_self.png'.format(print_safe_respondent)), bbox_inches='tight')

        plt.close(fig)

    def skills_matrix_row_builder(self, section, number, skillArea, firstName, lastName, answers):
        firstName = firstName.tolist()
        lastName = lastName.tolist()
        listOfFirstIndex = []
        for f, l in zip(firstName, lastName):
            joined = f + ' ' + l
            first = joined.split()
            letters = [word[0] for word in first]
            ind = ''.join(letters)
            listOfFirstIndex.append(ind)
        uniqueID = list(np.arange(1, len(listOfFirstIndex) + 1))
        arrays = [uniqueID, listOfFirstIndex]
        mi = pd.MultiIndex.from_arrays(arrays,names=['uniqueID', 'initials'])

        if (section, number) not in self.skill_matrix_dfs:
            self.skill_matrix_dfs[(section, number)] = []
            self.skill_matrix_dfs[(section, number)].append(pd.DataFrame(answers.tolist(), columns=[skillArea], index=mi))
        else:
            self.skill_matrix_dfs[(section, number)].append(pd.DataFrame(answers.tolist(), columns=[skillArea], index=mi))

    def skills_matrix(self, tup):
        skills_matrix = self.skill_matrix_dfs[tup][0].join(self.skill_matrix_dfs[tup][1:])

        skills_matrix.loc[(len(skills_matrix)+1, 'board'), :] = skills_matrix.apply(lambda row: float(str(Decimal('{}'.format(mean(row))).quantize(Decimal('1e-2'), ROUND_HALF_UP))), axis = 0)
        skills_matrix = skills_matrix.sort_values((len(skills_matrix), 'board'), axis = 1, ascending=False)

        # self.write_df(skills_matrix, 'Skills Matrix_{}_{}'.format(tup[0][:12], tup[1][:3]))

    def overview(self):
        # Identify the columns of data in an iterative manner.
        columns = list(self.ind_df.columns)
        # Format rows one at a time and append them to a list
        formatted_reportsheet_rows = []

        # Need the ability to set and reset values for ranked choice questions
        # the process is iterative, so it is necessary to count ahead as well as behind
        # rankFlag is True as long as the question is a ranked question.
        rankFlag = False
        lengthOfRankedChoiceSection = 0
        yAxisCount = 1
        sectionNames = []
        firstColumnFound = False
        continuedSectionFlag = False

        section_set = set()

        # Remove the respondent's index in order to filter below
        removed_id_index = self.ind_df.index.tolist()
        try:
            removed_id_index.remove(self.id)
        except:
            pass

        for index, row in self.ind_q.iterrows():
            # Get responses in order
            try:
                sorted_responses = [self.ind_df.at[self.id, row['question_raw']]]
            except:
                sorted_responses = ['N/A']
            if self.randomize:
                other_responses = self.ind_df.loc[removed_id_index][row['question_raw']].tolist()
                shuffle(other_responses)
                sorted_responses.extend(other_responses)
            else:
                sorted_responses.extend(self.ind_df.loc[removed_id_index][row['question_raw']].tolist())


            # Funnel skills_matrix questions out of the report sheet
            if row['type'] == 'skills_matrix':
                # print(self.file_name, self.report_type)
                self.skills_matrix_row_builder(row['section'], row['number'], row['sub_section'], self.ppl_df['FirstName'], self.ppl_df['LastName'], self.df[row['question_raw']])
                continue

            elif row['type'] == 'attribute_matrix':
                # print(self.file_name, self.report_type)
                self.skills_matrix_row_builder(row['section'], row['number'], row['sub_section'], self.ppl_df['FirstName'], self.ppl_df['LastName'], self.df[row['question_raw']])
                continue
            # Create an average row to appear above section header row if the
            # section does not just consist of long_form question types
            # print(row['section'], section_set, row['section'] in section_set)
            if row['section'] not in section_set:
                section_set.add(row['section'])
                new_section_flag = True

                if not set(self.ind_q[self.ind_q['section'] == row['section']]['type']).issubset(set(['long_form', 'yes/no'])):
                    # print(row['section'])
                    # print(set(self.questions_df[self.questions_df['section'] == row['section']]['type']))
                    formatted_reportsheet_rows.append((None, None, None, None, None) + tuple([None]*(len(self.df[row['question_raw']])-1)) + ('Average', 'Board Average', 'average_value'))
            else:
                new_section_flag = False

            # Long form questions
            if row['type'] == 'long_form':
                # If long_form question types are unnumbered
                if row['number'] == '' or row['number'] == np.nan:
                    if new_section_flag:
                        long_form_title_row = (row['section'], row['number'], row['question'], None, None)
                    else:
                        long_form_title_row = (None, None, row['question'], None, None)

                    sorted_responses = [str(answer) for answer in sorted_responses]

                    # If the question is unnumbered, only display it if it is answered
                    if len(set(sorted_responses)) == 1:
                        continue
                    else:
                        #
                        self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.ind_df[row['question_raw']].tolist())
                        #
                        formatted_reportsheet_rows.append(long_form_title_row)
                        optional_question_list = []
                        for optional in sorted_responses:
                            if optional != 'nan':
                                formatted_reportsheet_rows.append((None, None, optional))
                                optional_question_list.append(optional)

                        continue

                # If it's a numbered, optional question that needs displayed
                elif row['number'] != '' and row['number'] != np.nan:
                    if new_section_flag:
                        long_form_title_row = (row['section'], row['number'], row['question'], None, None)
                    else:
                        long_form_title_row = (None, row['number'], row['question'], None, None)
                    #
                    self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.ind_df[row['question_raw']].tolist())
                    #
                    formatted_reportsheet_rows.append(long_form_title_row)
                    if set(sorted_responses) == set(['nan']) | set(sorted_responses) == set([np.nan]):
                        formatted_reportsheet_rows.append((None, None, "no answers given"))
                        continue
                    else:
                        for optional in sorted_responses:
                            if optional != 'nan':
                                formatted_reportsheet_rows.append((None, None, optional))
                        continue
                else:
                    pass
                    # print('long_form error in: ', self.file_name, self.report_type)


            # Yes/No Questions
            elif row['type'] == 'yes/no':
                if new_section_flag:
                    yes_no_title_row = (row['section'], row['number'], row['question'], None, None)
                else:
                    yes_no_title_row = (None, row['number'], row['question'], None, None)

                yesNoCheck = self.ind_df[row['question_raw']].tolist()
                yesNoCheck = [answer.lower() for answer in yesNoCheck]
                #
                self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.ind_df[row['question_raw']].tolist())
                #
                yesNoCheck = dict(Counter(yesNoCheck))

                formatted_reportsheet_rows.append(yes_no_title_row)
                formatted_reportsheet_rows.append((None, None, 'Yes', yesNoCheck.get('yes', 0)))
                formatted_reportsheet_rows.append((None, None, 'No', yesNoCheck.get('no', 0)))
                continue
            # Determine if question has multiple parts
            elif row['type'] == 'matrix':
                # Reset Flags
                rankFlag = False
                yAxisCount = 1

                if np.nan in sorted_responses:
                    sorted_responses = ['N/A' if x == np.nan else x for x in sorted_responses]

                try:
                    sub_section_identifier = re.search('^([a-z]\.)', row['sub_section']).group(1)
                except:
                    sub_section_identifier = '!__!'
                try:
                    sub_section_value = re.search('^[a-z]\.[\s\t]*(.*)', row['sub_section']).group(1)
                except:
                    sub_section_value = '!__!'

                if re.search('(^[a-z]\.)', row['sub_section']).group(1) != 'a.':
                    self.create_multi_index_rows(row['section'], row['number'], sub_section_identifier, row['type'], row['question'] + ' ' + sub_section_value, self.ind_df[row['question_raw']].tolist())

                    formatted_reportsheet_row = (None, None, sub_section_identifier, sub_section_value) + tuple(sorted_responses) + (str(Decimal('{}'.format(np.mean(self.ind_df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    formatted_reportsheet_rows.append(formatted_reportsheet_row)
                    continue

                else:
                    if new_section_flag:
                        matrix_title_row = (row['section'], row['number'], row['question'], None, None)
                    else:
                        matrix_title_row = (None, row['number'], row['question'], None, None)
                    self.create_multi_index_rows(row['section'], row['number'], sub_section_identifier, row['type'], row['question'] + ' ' + sub_section_value, self.ind_df[row['question_raw']].tolist())
                    # self.create_multi_index_rows(sectionNames[-1], number, re.search(r':_([a-z]\.)', c).group(1), 'matrix', re.search('\d+\.\s(.*:)_[a-z]\.\s(.*)_', c).group(1) + ' ' + re.search('\d+\.\s(.*:)_[a-z]\.\s(.*)_', c).group(2), self.df[c].tolist())

                    # if new_section_flag:
                    #     formatted_reportsheet_row (row['section'], row['number'], row['question'], row['sub_section']) + tuple(answers) + ("{0:.2f}".format(round(np.mean(self.df[row['question_raw']].dropna()),2)),)
                    # else:
                    formatted_reportsheet_row = (None, None, sub_section_identifier, sub_section_value) + tuple(sorted_responses) + (str(Decimal('{}'.format(np.mean(self.ind_df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    # formatted_reportsheet_rows.append(sub_sectionHeader)
                    formatted_reportsheet_rows.append(matrix_title_row)
                    formatted_reportsheet_rows.append(formatted_reportsheet_row)
                    continue

            # Check if it's a ranked choice question
            # Ranked choice questions must contain a substring with a capital letter that follows an underscore and the entire string must end in an underscore
            elif row['type'] == 'rank':
                # Ranked questions have no sequential identification.
                # "Flip a switch" to indicate that the iterative process has encountered a ranked Choice question
                # We will switch the rankFlag value to False when the loop encounters a different type of question
                if rankFlag == False:
                    # We know that, if rankFlag is false, this is the first question.
                    # Create and format the rank choice section's titles
                    if new_section_flag:
                        rank_title_row = (row['section'], row['number'], row['question'], None, None)
                    else:
                        rank_title_row = (None, row['number'], row['question'], None, None)

                    xAxisTitle = (None, None, None, None, "Number of Responses")

                    # Iterate ahead in order to get length of the section
                    # beginningIndex = columns.index(column)
                    # endingIndex = beginningIndex
                    # numberCheck = number
                    # while numberCheck == number:
                    #     endingIndex += 1
                    #     try:
                    #         numberCheck = re.search('(\d+\.)', columns[endingIndex]).group(0)
                    #     except:
                    #         break
                    # lengthOfRankedChoiceSection = endingIndex - beginningIndex
                    lengthOfRankedChoiceSection = len(self.ind_q[(self.ind_q['section']==row['section']) & (self.ind_q['number'] == row['number'])])
                    xAxis = (None, None, 'Rank:', None) + tuple(np.arange(1, lengthOfRankedChoiceSection+1, 1))

                    formatted_reportsheet_rows.append(rank_title_row)
                    formatted_reportsheet_rows.append(xAxisTitle)
                    formatted_reportsheet_rows.append(xAxis)

                # Count up the respective rankings, add them to list, and insert 0s where necessary
                dictonaryOfCounts = dict(Counter(self.ind_df[row['question_raw']].tolist()))
                listOfOrderedCounts = list()
                for value in np.arange(1, lengthOfRankedChoiceSection+1, 1):
                    listOfOrderedCounts.append(dictonaryOfCounts.get(value, 0))

                formatted_reportsheet_row = (None, None, yAxisCount, row['sub_section']) + tuple(listOfOrderedCounts)
                formatted_reportsheet_rows.append(formatted_reportsheet_row)
                # Ranked questions have a number of responses based on the number of choices for the whole section

                self.create_multi_index_rows(row['section'], row['number'], yAxisCount, row['type'], row['question'] + ' ' + row['sub_section'], self.ind_df[row['question_raw']].tolist())
                #
                rankFlag = True
                yAxisCount+=1
                continue

            # The simple instance of a rating question
            else:
                if np.nan in sorted_responses:
                    sorted_responses = ['N/A' if x == np.nan else x for x in sorted_responses]

                # Get average of this question
                column_list = self.questions_df[(self.questions_df['section']==row['section']) & (self.questions_df['number']==row['number']) & (self.questions_df['type'].isin(['likert', 'matrix']))]['question_raw'].tolist()
                # print(row['section'], row['number'])
                # print(column_list)
                average_of_question = self.df[column_list].stack().dropna().mean()

                rankFlag = False
                yAxisCount = 1
                if new_section_flag:
                    formatted_reportsheet_row = (row['section'], row['number'], row['question'], None) + tuple(sorted_responses) + (str(Decimal('{}'.format(np.mean(self.ind_df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),str(Decimal('{}'.format(average_of_question)).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    formatted_reportsheet_rows.append(formatted_reportsheet_row)

                    #
                    self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.ind_df[row['question_raw']].tolist())
                    #
                    # self.peer_self_report_page[self.id]
                else:
                    formatted_reportsheet_row = (None, row['number'], row['question'], None) + tuple(sorted_responses) + (str(Decimal('{}'.format(np.mean(self.ind_df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),str(Decimal('{}'.format(average_of_question)).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    formatted_reportsheet_rows.append(formatted_reportsheet_row)
                    #
                    self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())
                    #
                    continue

        self.report_sheet = pd.DataFrame(formatted_reportsheet_rows)

    def statistics(self):
        df = pd.concat(self.rows_of_survey_object)
        df_shuffled = pd.concat(self.shuffled_rows_of_survey_object)

        df.reset_index(inplace=True)
        df_shuffled.reset_index(inplace=True)

        # Calculate Averages for each section
        df.columns = [str(i) for i in df.columns]
        df_shuffled.columns = [str(i) for i in df.columns]

        #
        df[df['question_type'].isin(['likert', 'matrix'])].loc[:, '1':] = df[df['question_type'].isin(['likert', 'matrix'])].loc[:, '1':].apply(pd.to_numeric)
        #

        numericAnswers = df[df['question_type'].isin(['likert', 'matrix'])]

        # Check if the report is based on a 5 point scale
        # if numericAnswers.loc[:,'1':].max(numeric_only=True) <= 5:
        five_point_test_df = numericAnswers.copy()
        five_point_test_df = five_point_test_df.fillna(0)
        if max(five_point_test_df.loc[:,'1':].max()) <=5:
            self.five_point_scale = True

        averages = {}
        for s in df['section'].unique():
            try:
                sub_frame = numericAnswers[numericAnswers['section']==s]
                totalNumberOfNullReponses = sub_frame.isna().sum().sum()
                sums = []
                # print(sub_frame)
                for index, row in sub_frame.loc[:, '1':].iterrows():
                    sums.append(row.sum())
                total = sum(sums)
                totalNumberOfResponses = len(sums)*len(sub_frame.loc[:, '1':].columns)
                if totalNumberOfResponses == 0:
                    continue
                average = total/(totalNumberOfResponses-totalNumberOfNullReponses)
                # Create Dictionary of Averages for each Section
                str(Decimal('{}'.format(average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                averages[s.replace(u'\xa0', u' ')] = str(Decimal('{}'.format(average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if self.anonymize:
                    self.section_avg_list[self.respondent].append({s: str(Decimal(average).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
            except:
                pass

        self.report_sheet.columns = [str(i) for i in self.report_sheet.columns]
        sectionIndices = self.report_sheet.index[self.report_sheet[self.report_sheet.columns[-1]] == 'average_value']
        sectionIndices = [i + 1 for i in sectionIndices]
        self.row_count_of_sections = sectionIndices
        for i in sectionIndices:
            section = self.report_sheet.iloc[i, 0]
            section = section.replace(u'\xa0', u' ')
            self.report_sheet.iloc[i-1, [-1]] = averages.get(section)
        df['sub_section'] = df['sub_section'].astype(str)
        # df['section'] = df['section'].str.replace(u'\xa0', u' ')
        df_shuffled['sub_section'] = df_shuffled['sub_section'].astype(str)
        # print(self.report_sheet)
        # Save members averages and survey_object
        self.averages = averages
        self.averages_data[self.respondent] = averages
        self.survey_object = df.copy()
        self.shuffled_survey_object = df_shuffled.copy()

        self.sectionMinsAndMaxes = {}
        for section in self.survey_object['section'].unique():
            sub_frame = self.survey_object[(self.survey_object['section']==section) & (self.survey_object['question_type'].isin(['likert', 'matrix']))]
            sub_frame.loc[:,'1':] = sub_frame.loc[:,'1':].apply(pd.to_numeric)
            if sub_frame.empty:
                continue
            try:
                self.sectionMinsAndMaxes[section.replace(u'\xa0', u' ')] = {'min': int(min(sub_frame.loc[:,'1':].min())), 'max': int(max(sub_frame.loc[:, '1':].max()))}
            except:
                # print('This error is being thrown most likely because a question has been assigned an incorrect question type.\nSee the dataframe below to debug:\n')
                # print(sub_frame)
                pass

        # write dataframe to existing Workbook
        self.write_df(self.report_sheet, 'Report')
        self.write_df(self.survey_object, 'Survey Object')

    def write_df(self, df, sheetName):
        worksheet = self.workbook.add_worksheet(sheetName)
        df.fillna('', inplace=True)
        colNum = 0

        if sheetName == 'Survey Object':
            worksheet.write_row(0, 0, df.columns)
            for col in df.columns:
                worksheet.write_column(1, colNum, df[col])
                colNum += 1

        elif 'Skills_Matrix' in sheetName:
            # print(self.respondent)
            # print(df)

            fmt = self.workbook.add_format({"font_name": "Garamond"})
            left_top_fmt = self.workbook.add_format({"font_name": "Garamond", 'left': 1, 'top': 1})
            top_bold_fmt_left_right = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1, 'left': 1, 'right': 1})
            top_bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1})
            top_right_bold_center = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1, 'right': 1})
            left_bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'left': 1, 'bottom': 1})
            right_bottom_bold_center_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'right': 1, 'bottom': 1})
            left_fmt = self.workbook.add_format({"font_name": "Garamond", 'left': 1})
            right_fmt = self.workbook.add_format({"font_name": "Garamond", 'right': 1})
            bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True})
            center_bold_fmt_left_right = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1, 'bottom': 1})
            center_bold_fmt_bottom = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'bottom': 1})
            center_bold = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1})
            center_bold_top = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1, 'top': 1})

            # worksheet.write(1, 1, '', left_top_fmt)
            # worksheet.write(1, 2, 'Board', top_bold_fmt_left_right)

            # number_of_skills_matrix_respondents = df.shape[0] - 1
            number_of_skills_matrix_questions = df.shape[1]
            # worksheet.merge_range(1, 3, 1, 2 + number_of_skills_matrix_respondents, 'Director Self-Ratings', top_right_bold_center)
            worksheet.write(2, 1, 'Skill Areas:', left_bold_fmt)
            worksheet.write(2, 2, 'Self-Rating', center_bold_fmt_left_right)
            worksheet.write(2, 3, 'Average Rating of All Directors', center_bold_fmt_left_right)

            # initials = list(df.index.get_level_values('initials'))
            # initials.remove('board')
            # worksheet.write_row(2, 3, initials[:-1], center_bold_fmt_bottom)
            # worksheet.write(2, 2+len(initials), initials[-1], right_bottom_bold_center_fmt)

            df.reset_index(inplace=True)

            colNum += 1
            start_row = 3
            end_row   = start_row +  df.shape[0]
            # print('\nEND ROW: ', end_row, df.shape[0])
            start_col = 2
            # end_col   = start_col + number_of_skills_matrix_respondents + 1
            end_col   = 4
            # print('\n COL: ', start_col, end_col)

            ten_format = self.workbook.add_format({'bg_color': '#63be7b'})
            nine_format = self.workbook.add_format({'bg_color': '#86c97e'})
            eight_format = self.workbook.add_format({'bg_color': '#a9d27f'})
            seven_format = self.workbook.add_format({'bg_color': '#ccdd82'})
            six_format = self.workbook.add_format({'bg_color': '#eee683'})
            five_format = self.workbook.add_format({'bg_color': '#fedd81'})
            four_format = self.workbook.add_format({'bg_color': '#fcbf7b'})
            three_format = self.workbook.add_format({'bg_color': '#fba276'})
            two_format = self.workbook.add_format({'bg_color': '#f98570'})
            one_format = self.workbook.add_format({'bg_color': '#f8696b'})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '9.01',
            'maximum': '10',
            'format': ten_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '8.01',
            'maximum': '9',
            'format': nine_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '7.01',
            'maximum': '8',
            'format': eight_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '6.01',
            'maximum': '7',
            'format': seven_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '5.01',
            'maximum': '6',
            'format': six_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '4.01',
            'maximum': '5',
            'format': five_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '3.01',
            'maximum': '4',
            'format': four_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '2.01',
            'maximum': '3',
            'format': three_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '1.01',
            'maximum': '2',
            'format': two_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '0.01',
            'maximum': '1',
            'format': one_format})

            # worksheet.write_column(3, 1, df.columns[2:], left_fmt)
            # worksheet.write_column(3, 2, df.iloc[number_of_skills_matrix_respondents, 2:], center_bold)
            # colNum += 2

            # for col in df.columns[1:]:
            #     worksheet.write_column(3, colNum, df[col])
            #     colNum+=1
            worksheet.write_column(3, 1, df['Skill Areas'], left_top_fmt)
            # worksheet.write_column(3, 2, df['Self-Rating'], left_top_fmt)
            worksheet.write_column(3, 2, df['Average Rating of All Directors'].astype(float))

            worksheet.write(end_row, 1, 'Average')
            worksheet.write(end_row, 2, self.skills_matrix_info[self.respondent]['Self-Average'])
            worksheet.write(end_row, 3, self.skills_matrix_info[self.respondent]['Board-Average'])

            # for i, u in zip(initials, df['uniqueID'].tolist()):
            #     value = df.loc[(df['uniqueID']==u) & (df['initials']==i)].values.flatten().tolist()
            #     value.remove(i)
            #     value.remove(u)
            #     if i == initials[-1]:
            #         worksheet.write_column(3, colNum, value, right_fmt)
            #     else:
            #         worksheet.write_column(3, colNum, value, fmt)
            #     colNum += 1

            # average_of_averages = df.loc[(df['initials']=='board')].values.flatten().tolist()
            # average_of_averages = average_of_averages[2:]
            # average_of_averages = np.mean(average_of_averages)

            # bold_align_right_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'right', 'left': 1, 'top': 1})
            # worksheet.write(number_of_skills_matrix_questions + 3, 1, 'Overall Average:', bold_align_right_fmt)
            # worksheet.write(number_of_skills_matrix_questions+3, 2, str(Decimal('{}'.format(average_of_averages)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), center_bold_top)
            #
            # top_bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1})
            # top_right_bold_center_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'right': 1, 'top': 1})
            #
            # del df['uniqueID']
            # del df['initials']
            # df.drop(df.tail(1).index,inplace=True)

            # colNum = 3
            #
            # df['respondentAverage'] = df.apply(lambda row: row.mean(), axis=1)
            # writer = pd.ExcelWriter('skills.xlsx')
            # df.to_excel(writer)
            # writer.save()
            # for i, r in df.iterrows():
            #     if i == len(df) - 1:
            #         worksheet.write(number_of_skills_matrix_questions+3, colNum, str(Decimal('{}'.format(r['respondentAverage'].mean())).quantize(Decimal('1e-2'), ROUND_HALF_UP)), top_right_bold_center_fmt)
            #     else:
            #         worksheet.write(number_of_skills_matrix_questions+3, colNum, str(Decimal('{}'.format(r['respondentAverage'].mean())).quantize(Decimal('1e-2'), ROUND_HALF_UP)), top_bold_fmt)
            #     colNum += 1

            # for i, r in df.iterrows():
            #     worksheet.write(number_of_skills_matrix_questions+3, colNum, str(Decimal('{}'.format(r[''].mean())).quantize(Decimal('1e-2'), ROUND_HALF_UP)), top_bold_fmt)
            #     colNum += 1

            blank_left_bottom = self.workbook.add_format({"font_name": "Garamond", 'left': 1, 'bottom': 1})
            blank_left_right_bottom = self.workbook.add_format({"font_name": "Garamond", 'left': 1, 'bottom': 1, 'right': 1})
            blank_bottom_right_bold = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'bottom': 1, 'right': 1})
            # worksheet.write(number_of_skills_matrix_questions + 4, 1, '', blank_left_bottom)
            # worksheet.write(number_of_skills_matrix_questions + 4, 2, '', blank_left_right_bottom)
            # worksheet.merge_range(number_of_skills_matrix_questions + 4, 3, number_of_skills_matrix_questions + 4, 2 + number_of_skills_matrix_respondents, 'Total Expertise Average by Directors', blank_bottom_right_bold)

        elif sheetName == 'Report':
            fmt = self.workbook.add_format({'font_name': 'Garamond',
                                                    'font_size': 12})
            fmt_center = self.workbook.add_format({'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True})
            right_border = self.workbook.add_format({'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True,
                                                    'right': 1})

            # Identify the columns we want to center_across, and where we want the
            # right border line to go
            str_columns = [str(x) for x in range(4, 5 + self.number_of_respondents)]
            # print(self.ind_q[self.ind_q['sub_section']==self.respondent])
            # print(self.ind_df)
            # print(self.ind_df.loc[self.id])
            for col in df.columns:
                # print(col)
                # print(df[col])
                # print(df.loc[self.id][col])
                # print(df)
                if col in [str(self.number_of_respondents + 3), '4', str(self.number_of_respondents + 4)]:
                    # print(df[col])
                    worksheet.write_column(0, colNum, df[col], right_border)
                    colNum += 1
                elif col in str_columns:
                    worksheet.write_column(0, colNum, df[col], fmt_center)
                    colNum += 1
                else:
                    worksheet.write_column(0, colNum, df[col], fmt)
                    colNum += 1

            # Get min and max average rating for each question in a section,
            # Get the lowest and highest absolute response in each section
            sectionMinsAndMaxes = {}
            questionAveragesBySectionMinsAndMaxes = {}
            for section in self.ind_q['section'].unique():
                questions_sub_frame = self.ind_q[(self.ind_q['section']==section) & (self.ind_q['type'].isin(['likert', 'matrix']))]
                raw_questions_list = questions_sub_frame['question_raw'].tolist()
                sub_frame = self.ind_df[raw_questions_list]
                if sub_frame.empty:
                    self.section_data[self.respondent][section] = 'The open-ended questions provide directors with an opportunity to openly and anonymously comment on their colleagues’ competency and overall performance.'
                    continue
                try:
                    sectionMinsAndMaxes[section] = {'min': int(np.nanmin(sub_frame.values)), 'max': int(np.nanmax(sub_frame.values))}
                except:
                    # print('This error is being thrown most likely because a question has been assigned an incorrect question type.\nSee the dataframe below to debug:\n')
                    # print(sub_frame)
                    pass
                means = []
                for col in sub_frame:
                    means.append(np.mean(sub_frame[col]))


                questionAveragesBySectionMinsAndMaxes[section.replace(u'\xa0', u' ')] = {'min': str(Decimal('{}'.format(min(means))).quantize(Decimal('1e-2'), ROUND_HALF_UP)), 'max': str(Decimal('{}'.format(max(means))).quantize(Decimal('1e-2'), ROUND_HALF_UP))}
            report_format = self.workbook.add_format({'bold': True,
                                                    'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True,
                                                    'bottom': 1,
                                                    'right': 1})
            underline = self.workbook.add_format({'bold': True,
                                                    'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True,
                                                    'bottom': 1})

            for rowIndex, section in zip(self.row_count_of_sections, self.survey_object['section'].unique()):
                sub_frame = self.survey_object[(self.survey_object['section']==section) & (self.survey_object['question_type'].isin(['likert', 'matrix']))]
                sub_frame.loc[:,'1':] = sub_frame.loc[:,'1':].apply(pd.to_numeric)
                if sub_frame.empty:
                    continue
                worksheet.write(rowIndex-1, 4, 'Self-Rating', report_format)
                worksheet.merge_range(rowIndex - 1, 5, rowIndex - 1, 3 + self.number_of_respondents, 'Individual Ratings', report_format)
                try:
                    worksheet.write(rowIndex - 1, 4 + self.number_of_respondents, "{} Average".format(self.ppl_df.loc[self.id]['FirstName'][0] + self.ppl_df.loc[self.id]['LastName'][0]), underline)
                except:
                    worksheet.write(rowIndex - 1, 4 + self.number_of_respondents, "{} Average".format(self.respondent), underline)
                worksheet.write(rowIndex - 1, 5 + self.number_of_respondents, 'Board Average', underline)

                formattedText = '{}&#8217;s average rating for this section is {}, and the Board&#8217;s average is {}. The average rating for each question in the section ranged from {} to {}. Individual ratings ranged from {} to {}.'
                equal_formattedText = '{}&#8217;s average rating for each question and this section is {}, and the Board&#8217;s average is {}. Individual ratings ranged from {} to {}.'
                if questionAveragesBySectionMinsAndMaxes.get(section).get('max') == questionAveragesBySectionMinsAndMaxes.get(section).get('min') and (questionAveragesBySectionMinsAndMaxes.get(section).get('max') == self.averages.get(section)):
                    self.section_data[self.respondent][section] = equal_formattedText.format(self.respondent, self.averages.get(section), self.board_wide_averages.get(section),
                    sectionMinsAndMaxes.get(section).get('min'),
                    sectionMinsAndMaxes.get(section).get('max'))

                    worksheet.write(rowIndex - 1, 5 + self.number_of_respondents, equal_formattedText.format(self.respondent, self.averages.get(section), self.board_wide_averages.get(section),
                    sectionMinsAndMaxes.get(section).get('min'),
                    sectionMinsAndMaxes.get(section).get('max')), fmt)
                else:
                    self.section_data[self.respondent][section] = formattedText.format(self.respondent, self.averages.get(section), self.board_wide_averages.get(section),
                        questionAveragesBySectionMinsAndMaxes.get(section).get('min'),
                        questionAveragesBySectionMinsAndMaxes.get(section).get('max'),
                        sectionMinsAndMaxes.get(section).get('min'),
                        sectionMinsAndMaxes.get(section).get('max'))

                    worksheet.write(rowIndex - 1, 5 + self.number_of_respondents, formattedText.format(self.respondent, self.averages.get(section), self.board_wide_averages.get(section),
                        questionAveragesBySectionMinsAndMaxes.get(section).get('min'),
                        questionAveragesBySectionMinsAndMaxes.get(section).get('max'),
                        sectionMinsAndMaxes.get(section).get('min'),
                        sectionMinsAndMaxes.get(section).get('max')), fmt)

    def create_multi_index_rows(self, section, number, sub_section, qtype, question, answers):
        index = pd.MultiIndex.from_arrays([[section], [number], [sub_section], [qtype], [question]], \
                                          names = ['section', 'number', 'sub_section', 'question_type', 'question'])
        formatted_reportsheet_row = pd.DataFrame([tuple(answers)], index = index, columns = np.arange(1, len(self.df)+1))

        self.rows_of_survey_object.append(formatted_reportsheet_row)
        shuffle(answers)
        shuffled_row= pd.DataFrame([tuple(answers)], index = index, columns = np.arange(1, len(self.df)+1))
        self.shuffled_rows_of_survey_object.append(shuffled_row)

    def ToArabic(self, roman):
        roman = self.check_valid(roman)
        keys = ['IV', 'IX', 'XL', 'XC', 'CD', 'CM', 'I', 'V', 'X', 'L', 'C', 'D', 'M']
        to_arabic = {'IV': '4', 'IX': '9', 'XL': '40', 'XC': '90', 'CD': '400', 'CM': '900',
                'I': '1', 'V': '5', 'X': '10', 'L': '50', 'C': '100', 'D': '500', 'M': '1000'}
        for key in keys:
            if key in roman:
                roman = roman.replace(key, ' {}'.format(to_arabic.get(key)))
        arabic = sum(int(num) for num in roman.split())
        return arabic

    def check_valid(self, roman):
        roman = roman.upper()
        invalid = ['IIII', 'VV', 'XXXX', 'LL', 'CCCC', 'DD', 'MMMM']
        if any(sub in roman for sub in invalid):
            raise ValueError('Numerus invalidus est: {}\n.The number is an invalid Roman Numeral'.format(roman))
        return roman

    def format_heat_map(self, row):
        # print(self.report_name, self.report_type, self.survey_object)
        # print(row)
        row['section'] = re.search(r'\b([IVX]+)\.?', row['section']).group(1)
        row['number'] += row['sub_section']
        # remove spaces from likert questions
        match_likert = re.search('^(\d+)\.$', row['number'])
        if match_likert is not None:
            row['number'] = match_likert.group(1)
        # match everything except 'a' so we can format correctly
        match_b_z = re.search('^\d+\.(?![a])([a-z])', row['number'])
        if match_b_z is not None:
            row['number'] = match_b_z.group(1)
        # pair 'a' with the corresponding question number without a period
        match_a = re.search('^(\d+)\.(a)\.', row['number'])
        if match_a is not None:
            row['number'] = match_a.group(1) + match_a.group(2)
        return row

    def heat_map(self):
        worksheet = self.workbook.add_worksheet('Heat Map')
        worksheet.hide_gridlines(2)
        # print('Randomize: ', self.randomize)
        if self.randomize:
            hm = self.shuffled_survey_object.copy()
        else:
            hm = self.survey_object.copy()

        hm = hm[hm['question_type'].isin(['likert', 'matrix'])]
        hm_copy = hm.copy()
        hm = hm.apply(lambda row: self.format_heat_map(row), axis = 1)
        del hm['sub_section']
        del hm['question_type']
        del hm['question']
        hm.set_index(['section', 'number'], inplace=True)
        # Get the size of each section in order to merge cells
        indexSize = hm.groupby(level=0).size()
        # hm = hm.sample(frac=1, axis=1)
        hm = hm.T

        # Add a header format.
        header_format1 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'bg_color': '#BE6E28', 'font_size': 20, 'center_across': True, 'border': 2})
        header_format2 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 14, 'center_across': True})
        # Add fill formats
        green_format = self.workbook.add_format({'bg_color': '#008000', 'font_color': '#008000'})
        yellow_format = self.workbook.add_format({'bg_color': '#FFFF00', 'font_color': '#FFFF00'})
        red_format = self.workbook.add_format({'bg_color': '#FF0000', 'font_color': '#FF0000'})
        white_format = self.workbook.add_format({'bg_color': '#FFFFFF', 'font_color': '#FFFFFF'})

        # Conditional formatting for colors
        start_row = 3
        if not self.no_self_ratings_bool:
            end_row   = start_row + hm.shape[0] - 1
        else:
            end_row   = start_row + hm.shape[0] - 2

        start_col = 1
        end_col   = start_col + hm.shape[1] - 1

        if self.five_point_scale == False:
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
                {'type': 'cell',
                 'criteria': 'between',
                 'minimum': '1',
                 'maximum': '3',
                 'format': red_format})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
                {'type': 'cell',
                 'criteria': 'between',
                 'minimum': '4',
                 'maximum': '7',
                 'format': yellow_format})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
                {'type': 'cell',
                 'criteria': '>',
                 'value': '7',
                 'format': green_format})
        else:
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
             'criteria': 'between',
             'minimum': '1',
             'maximum': '2',
             'format': red_format})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
             'criteria': '==',
             'value': '3',
             'format': yellow_format})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
             'criteria': '>',
             'value': '3',
             'format': green_format})

        worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
             'criteria': '==',
             'value': '0',
             'format': white_format})

        # Write the column headers with correct format.
        header_format1 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'bg_color': '#BE6E28', 'font_size': 28, 'center_across': True, 'border': 2})
        header_format2 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 20, 'center_across': True, 'right': 1})
        header_format2_leftBorder = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 20, 'center_across': True, 'left': 2})
        header_format2_rightBorder = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 20, 'center_across': True, 'right': 2})

        indexSize = pd.DataFrame({'section':indexSize.index, 'length':indexSize.values})
        # Ensure correct ordering
        indexSize['arabic'] = indexSize.apply(lambda x: self.ToArabic(x['section']), axis = 1)
        indexSize.sort_values('arabic', inplace=True)
        indexSize.reset_index(drop=True, inplace=True)
        indexSize.set_index('section', inplace=True)

        # Y Axis: Number of directors
        directors = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 18, 'center_across': True})
        if not self.no_self_ratings_bool:
            formatted_y_axis = ['Self-Rating']
            extension = ['Director {}'.format(x) for x in np.arange(2, self.number_of_respondents+1, 1)]
            formatted_y_axis.extend(extension)
        else:
            formatted_y_axis = []
            extension = ['Director {}'.format(x) for x in np.arange(1, self.number_of_respondents, 1)]
            formatted_y_axis.extend(extension)

        worksheet.write_column(3, 0, formatted_y_axis, directors)

        # Section Headers
        col = 1
        for ind in indexSize.index:
            if indexSize.loc[ind]['length'] > 1:
                worksheet.merge_range(1, col, 1, col+indexSize.loc[ind]['length'] - 1, ind, header_format1)
                col += indexSize.loc[ind]['length']
            elif indexSize.loc[ind]['length'] == 1:
                worksheet.write(1, col, ind, header_format1)
                col += indexSize.loc[ind]['length']

        # Create a dataframe that contains the ending column count for each section.
        indexSizeTotals = indexSize.copy()
        indexSizeTotals.reset_index(inplace=True)
        for i in range(1, len(indexSize)):
            indexSizeTotals.loc[i, 'length'] += indexSizeTotals.loc[i-1, 'length']

        worksheet.write(2, 1, hm.columns.values[0][1], header_format2_leftBorder)

        # Question Number Headers
        col = 2
        for value in hm.columns.values[1:]:
            if col == indexSizeTotals.loc[indexSizeTotals['section'] == value[0], 'length'].item():
                worksheet.write(2, col, value[1], header_format2_rightBorder)
            else:
                worksheet.write(2, col, value[1], header_format2)
            col += 1

        # Global font
        fmt = self.workbook.add_format({"font_name": "Garamond"})

        # Adjust row and column widths
        worksheet.set_column(1, 550, 3, fmt)
        worksheet.set_column(0, 0, 24, fmt)
        worksheet.set_default_row(20.2)
        worksheet.set_row(1, 33, fmt)
        worksheet.set_row(2, 24, fmt)
        worksheet.set_row(0, 3, fmt )
        # section average row height
        worksheet.set_row(self.number_of_respondents+5, 3, fmt)
        worksheet.set_column(int(end_col)+1, int(end_col)+1, 33, fmt)

        top_thick_border = self.workbook.add_format({'top': 2})
        bottom_thick_border = self.workbook.add_format({'bottom': 2})
        left_thick_border = self.workbook.add_format({'left': 2})
        right_thick_border = self.workbook.add_format({'right': 2})
        thick = self.workbook.add_format({'top':2, 'bottom': 2, 'left': 2, 'right': 2})

        cell = self.workbook.add_format({'border': 1, "font_name": "Garamond"})
        cell_leftBorder = self.workbook.add_format({'left': 2, 'bottom': 1, 'top': 1, 'right': 1})
        cell_rightBorder = self.workbook.add_format({'right': 2, 'top': 1, 'bottom': 1, 'left': 1})

        hm.fillna(0, inplace=True)

        # Sort responses
        removed_id_index = self.ind_df.index.tolist()
        try:
            removed_id_index.remove(self.id)
        except:
            pass

        sorted_columns = []
        hm_q = self.ind_q[self.ind_q['type'].isin(['likert', 'matrix'])]
        for index, row in hm_q.iterrows():
            try:
                if not self.no_self_ratings_bool:
                    sorted_responses = [self.ind_df.at[self.id, row['question_raw']]]
                else:
                    sorted_responses = []

            except:
                sorted_responses = ['N/A']
            if self.randomize:
                shuffled_responses = self.ind_df.loc[removed_id_index][row['question_raw']].tolist()
                shuffle(shuffled_responses)
                sorted_responses.extend(shuffled_responses)
            else:
                sorted_responses.extend(self.ind_df.loc[removed_id_index][row['question_raw']].tolist())
            sorted_columns.append(sorted_responses)

        # Write the colored response cells
        worksheet.write_column(3, 1, sorted_columns[0], cell_leftBorder)
        col = 2
        for colName in hm.columns[1:]:
            if col == indexSizeTotals.loc[indexSizeTotals['section'] == colName[0], 'length'].item():
                worksheet.write_column(3, col, sorted_columns[col - 1], cell_rightBorder)
            else:
                worksheet.write_column(3, col, sorted_columns[col - 1], cell)
            col += 1


        # Format the bottom row containing averages
        startColumnMerge = 1
        endColumnMerge = 0
        for ind, size, avg in zip(indexSize.index, indexSize['length'], self.averages.values()):
            if indexSize.loc[ind]['length'] > 1:
                endColumnMerge += size
                if not self.no_self_ratings_bool:
                    worksheet.merge_range(len(hm)+3, startColumnMerge, len(hm)+3, endColumnMerge, avg, header_format1)
                else:
                    worksheet.merge_range(len(hm)+2, startColumnMerge, len(hm)+3, endColumnMerge, avg, header_format1)
                startColumnMerge = endColumnMerge + 1
            elif indexSize.loc[ind]['length'] == 1:
                endColumnMerge += size
                if not self.no_self_ratings_bool:
                    worksheet.write(len(hm)+3, startColumnMerge, avg, header_format1)
                else:
                    worksheet.write(len(hm)+2, startColumnMerge, avg, header_format1)
                startColumnMerge = endColumnMerge + 1

        # bottom row height
        if not self.no_self_ratings_bool:
            worksheet.set_row(hm.shape[0] + 3, 33, fmt)
        else:
            worksheet.set_row(hm.shape[0] + 2, 33, fmt)

        ########################################################################

        # print(hm)
        # hm = hm.T
        # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
        #     print(hm)

        # hm = self.survey_object.copy()
        #
        # hm = hm[hm['question_type'].isin(['likert', 'matrix'])]
        # del hm['sub_section']
        # del hm['question_type']
        # del hm['question']
        # hm.set_index(['section', 'number'], inplace=True)
        # # Get the size of each section in order to merge cells
        # indexSize = hm.groupby(level=0).size()
        # hm = hm.T
        # hm.fillna(0, inplace=True)
        # hm = pd.concat([hm[:1], hm[1:].sample(frac=1)]).reset_index(drop=True)
        # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
        #     print(hm)

        # if self.randomize:
        #     hm = self.shuffled_survey_object.copy()
        # else:

        # print('\n\n', self.respondent)
        hm = self.survey_object.copy()

        hm = hm[hm['question_type'].isin(['likert', 'matrix'])]
        # hm_copy = hm.copy()
        hm = hm.apply(lambda row: self.format_heat_map(row), axis = 1)
        del hm['sub_section']
        del hm['question_type']
        del hm['question']
        hm.set_index(['section', 'number'], inplace=True)
        # Get the size of each section in order to merge cells
        # indexSize = hm.groupby(level=0).size()

        hm = hm.T
        hm.fillna(0, inplace=True)
        hm = hm.replace('', 0)
        # print(hm)
        # Sort responses
        removed_id_index = self.ind_df.index.tolist()
        try:
            removed_id_index.remove(self.id)
        except:
            pass

        # print('removed_id_index: ', removed_id_index)
        sorted_columns = []
        hm_q = self.ind_q[self.ind_q['type'].isin(['likert', 'matrix'])]
        for index, row in hm_q.iterrows():
            # print(index, row)
            try:
                sorted_responses = [self.ind_df.at[self.id, row['question_raw']]]
            except:
                sorted_responses = ['N/A']

            if self.randomize == False:
                sorted_responses.extend(self.ind_df.loc[removed_id_index][row['question_raw']].tolist())
            else:
                shuffled_hm_col = self.ind_df.loc[removed_id_index][row['question_raw']].tolist()
                # print('old: ', shuffled_hm_col)
                shuffle(shuffled_hm_col)
                # santas_naughty_list = ['', np.nan, None, 'nan']
                # shuffled_hm_col = [0 if x in santas_naughty_list else x for x in shuffled_hm_col]
                # print('new: ', shuffled_hm_col)
                sorted_responses.extend(shuffled_hm_col)

            sorted_columns.append(sorted_responses)

        # print(sorted_columns)

        for col, new_col in zip(hm.columns, sorted_columns):
            # print(hm[col])
            # print(col, new_col)
            hm[col] = new_col

        hm.fillna(0, inplace=True)
        hm = hm.replace('', 0)
        hm = hm.apply(pd.to_numeric, errors='coerce',  downcast='integer')

        # print(hm)

        hm_sections = hm.columns.get_level_values(0)
        hm_section_length_counts = dict()
        heat_map_averages = dict()
        for i in hm_sections:
            hm_section_length_counts[i+'.'] = list(hm_sections).count(i)

        for (k, v), (section, avg) in zip(hm_section_length_counts.items(), self.averages.items()):
            heat_map_averages[k] = (v, avg)

        hm_numbers_borders = dict()
        # for i in range(0, len(hm_numbers)):
        #     hm_numbers_borders[i] = ()

        # print(hm.columns)
        duplicate_count = 0
        section_set = set()
        hm_values_borders = []

        new_col_list = []
        count=0
        for col in hm.columns:
            i = list(col)
            i.append(count)
            new_col_list.append(tuple(i))
            count+=1
        last_item = list(list(hm.columns)[-1])
        last_item.append(len(hm.columns)-1)
        last_item = tuple(last_item)
        # print(last_item)

        for sec_num in new_col_list:
            # print(sec_num)
            if sec_num[0] not in section_set:
                section_set.add(sec_num[0])
                hm_numbers_borders[sec_num] = (sec_num[1], 'left')
                hm_values_borders.append('left')
            elif sec_num == last_item:
                # print('\nhere!')
                hm_numbers_borders[sec_num] = (sec_num[1], 'right')
                hm_values_borders.append('right')
            else:
                hm_numbers_borders[sec_num] = (sec_num[1], 'None')
                hm_values_borders.append('None')

        last_tup = hm_numbers_borders[list(hm_numbers_borders.keys())[-1]]
        last_tup = list(last_tup)
        last_tup[-1] = 'right'
        hm_numbers_borders[list(hm_numbers_borders.keys())[-1]] = tuple(last_tup)
        # print(hm_numbers_borders)

        # print(self.respondent)
        # print(hm)
        hm_rows_directions = []
        self.na_in_heat_maps[self.respondent] = False
        if not self.no_self_ratings_bool:
            for index, row in hm.iterrows():
                if 0 in row.tolist():
                    self.na_in_heat_maps[self.respondent] = True
                if index == '1':
                    hm_rows_directions.append({'Self-Rating': zip(list(row), hm_values_borders)})
                else:
                    hm_rows_directions.append({index: zip(list(row), hm_values_borders)})
        else:
            hm.drop('1', inplace=True)
            new_index = np.arange(1, len(hm)+1, 1)
            new_index = [str(x) for x in new_index]
            hm.index = new_index

            for index, row in hm.iterrows():
                if 0 in row.tolist():
                    self.na_in_heat_maps[self.respondent] = True
                hm_rows_directions.append({index: zip(list(row), hm_values_borders)})

        # Debugging when/how the peer_self chart gets bumped to the next page
        # count = 1
        # for index, row in hm.iterrows():
            # if count == 8:
            #     break
            # hm_rows_directions.append({index+'1': zip(list(row), hm_values_borders)})
            # count+=1

        self.hm_cell_height = 10
        if len(hm_rows_directions) < 16 and len(hm_rows_directions) >= 10:
            self.hm_cell_height = 8
        elif len(hm_rows_directions) < 20 and len(hm_rows_directions) >= 16:
            self.hm_cell_height = 6
        elif len(hm_rows_directions) >= 20:
            self.hm_cell_height = 5


        # print(hm_values_borders)
        # print(hm_rows_directions)
        # for row in hm_rows_directions:
        #     print(list(row.keys())[0])
        #     for index, tup in row.items():
        #         for num, direction in tup:
        #             print(num, direction)
        # print(hm_rows_directions)
        # print(type(hm_rows_directions[0]))

        self.heat_map_data[self.respondent] = {'hm_rows_directions': hm_rows_directions}
        # print(self.hm_rows_directions)
        # print('\n\nHERE\n\n')
        #
        # print(type(self.hm_rows_directions))
        # print(self.hm_rows_directions)
        # for k, row in self.hm_rows_directions.items():
        #     print(k, type(k), v ,type(v))

        self.heat_map_data[self.respondent]['hm_section_length_counts'] = hm_section_length_counts
        self.heat_map_data[self.respondent]['hm_numbers_borders'] = hm_numbers_borders
        self.heat_map_data[self.respondent]['heat_map_averages'] = heat_map_averages

        column_width = math.floor(560/(len(hm_numbers_borders)+1))
        if column_width > 24:
            column_width = 24

        self.heat_map_data[self.respondent]['percentage_size'] = column_width

    def NonLinCdict(self, steps, hexcol_array):
        cdict = {'red': (), 'green': (), 'blue': ()}
        for s, hexcol in zip(steps, hexcol_array):
            rgb =matplotlib.colors.hex2color(hexcol)
            cdict['red'] = cdict['red'] + ((s, rgb[0], rgb[0]),)
            cdict['green'] = cdict['green'] + ((s, rgb[1], rgb[1]),)
            cdict['blue'] = cdict['blue'] + ((s, rgb[2], rgb[2]),)
        return cdict

class peer_composite_report(object):
    def __init__(self, file_name, report_type, df, questions_df, ppl_df, na_no_null_dict, only_skills_matrices, randomize, app, no_self_ratings_bool=False, year=None):
        self.file_name = file_name
        self.report_type = report_type
        self.df = df
        self.questions_df = questions_df
        self.na_no_null_dict = na_no_null_dict
        self.ppl_df = ppl_df
        self.only_skills_matrices = only_skills_matrices
        self.section_data = dict()
        self.section_avg_list = []
        self.randomize = randomize
        self.no_self_ratings_bool = no_self_ratings_bool
        self.year = year

        self.app = app
        self.output_file_names = []

        self.rows_of_survey_object = []
        self.shuffled_rows_of_survey_object = []
        self.anonymize = True
        self.skill_matrix_dfs = {}
        self.five_point_scale = False
        self.eval_stats_list_of_dicts = []

        self.anonymized_labels = {}

        unique_subsections = [x for x in questions_df['sub_section'].unique() if x not in ['', np.nan, None]]
        number_of_subsections = len(unique_subsections)
        self.number_of_subsections = number_of_subsections
        # print('number_of_subsections: ', number_of_subsections)

        for i, v in zip(unique_subsections, range(1, number_of_subsections+1)):
            self.anonymized_labels[i] = v

        # for i, v in zip(ppl_df.index, range(1, len(ppl_df.index)+1)):
        #     self.anonymized_labels[i] = v
        # print('anonymized_labels', self.anonymized_labels)

        self.peer_handler()

    def skills_matrix_row_builder(self, section, number, skillArea, firstName, lastName, answers):
        firstName = firstName.tolist()
        lastName = lastName.tolist()
        listOfFirstIndex = []
        for f, l in zip(firstName, lastName):
            joined = f + ' ' + l
            first = joined.split()
            letters = [word[0] for word in first]
            ind = ''.join(letters)
            listOfFirstIndex.append(ind)
        uniqueID = list(np.arange(1, len(listOfFirstIndex) + 1))
        arrays = [uniqueID, listOfFirstIndex]
        mi = pd.MultiIndex.from_arrays(arrays,names=['uniqueID', 'initials'])

        if (section, number) not in self.skill_matrix_dfs:
            self.skill_matrix_dfs[(section, number)] = []
            self.skill_matrix_dfs[(section, number)].append(pd.DataFrame(answers.tolist(), columns=[skillArea], index=mi))
        else:
            self.skill_matrix_dfs[(section, number)].append(pd.DataFrame(answers.tolist(), columns=[skillArea], index=mi))

    def peer_handler(self):
        self.report_name = re.search('^(.*).csv', self.file_name).group(1)

        self.output_filename_anonymized = 'Peer_Anonymized_{}_{}.xlsx'.format(self.report_name, self.report_type)
        self.workbook = xlsxwriter.Workbook(os.path.join(self.app.config['EXCEL_OUTPUT'], self.output_filename_anonymized), {'nan_inf_to_errors': True})
        self.output_file_names.append(self.output_filename_anonymized)
        # self.workbook = xlsxwriter.Workbook('output\\Peer_Anonymized_{}_{}.xlsx'.format(self.report_name, self.report_type), {'nan_inf_to_errors': True})

        self.number_of_respondents = len(self.df)

        # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
        #     print(self.questions_df)


        self.overview()
        if len(self.rows_of_survey_object) > 0:
            self.statistics()

            self.hi_lo()
            self.esigma_bar_chart()
            self.heat_map()
            self.aggregated_section_analysis()
            self.bar_chart()

        if self.skill_matrix_dfs:
            for tup in self.skill_matrix_dfs:
                self.skills_matrix(tup)


        self.workbook.close()

        ################
        if not self.only_skills_matrices:
            self.skill_matrix_dfs = {}
            self.anonymize = False

            self.report_name = re.search('^(.*).csv', self.file_name).group(1)
            self.output_filename = 'Peer_{}_{}.xlsx'.format(self.report_name, self.report_type)
            self.output_file_names.append(self.output_filename)
            self.workbook = xlsxwriter.Workbook(os.path.join(self.app.config['EXCEL_OUTPUT'], self.output_filename), {'nan_inf_to_errors': True})

            # self.workbook = xlsxwriter.Workbook('output\\Peer_{}_{}.xlsx'.format(self.report_name, self.report_type), {'nan_inf_to_errors': True})

            self.number_of_respondents = len(self.df)

            self.rows_of_survey_object = []
            self.shuffled_rows_of_survey_object = []
            self.overview()
            if len(self.rows_of_survey_object) > 0:
                self.statistics()

                self.hi_lo()
                self.esigma_bar_chart()
                self.heat_map()
                self.aggregated_section_analysis()
                self.bar_chart()

            if self.skill_matrix_dfs:
                for tup in self.skill_matrix_dfs:
                    self.skills_matrix(tup)


            self.workbook.close()

    def skills_matrix(self, tup):

        # print(self.skill_matrix_dfs)

        skills_matrix = self.skill_matrix_dfs[tup][0].join(self.skill_matrix_dfs[tup][1:])

        skills_matrix.loc[(len(skills_matrix)+1, 'board'), :] = skills_matrix.apply(lambda row: float(str(Decimal('{}'.format(np.nanmean(row))).quantize(Decimal('1e-2'), ROUND_HALF_UP))), axis = 0)

        skills_matrix = skills_matrix.sort_values((len(skills_matrix), 'board'), axis = 1, ascending=False)
        self.write_df(skills_matrix, 'Skills Matrix_{}_{}'.format(tup[0][:12], tup[1][:3]))

    def esigma_bar_chart(self):
        worksheet = self.workbook.add_worksheet('eSigma Bar Chart')
        fmt = self.workbook.add_format({"font_name": "Garamond"})
        bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': 1, 'num_format': "0.00"})

        q = self.questions_df.copy()

        individuals = []
        individuals.append('Board')

        q = q[(q['type'].isin(['likert', 'matrix']))]
        count = 1
        for respondent in q['sub_section'].unique():
            if self.anonymize == True:
                individuals.append('Director {}'.format(count))
                count += 1
            else:
                individuals.append(respondent)

        esigmas = []
        report_esigma = self.eSigma()
        report_esigma = Decimal('{}'.format(report_esigma)).quantize(Decimal('1e-2'), ROUND_HALF_UP)
        esigmas.append(report_esigma)
        for respondent in q['sub_section'].unique():
            esigmas.append(self.individual_esigma(respondent))

        headers = ['respondents', 'Assessment Average by Director']
        worksheet.write_row(0, 0, headers, bold_fmt)
        worksheet.write_column(1, 0, individuals[::-1], bold_fmt)
        worksheet.write_column(1, 1, esigmas[::-1], bold_fmt)

        chart1 = self.workbook.add_chart({'type': 'bar', 'name_font': {'font_name': 'Garamond', 'bold': True}})

        if self.five_point_scale == False:
            chart1.set_x_axis({'min': 1.00, 'max': 10.00, 'major_unit': 1, 'num_font':  {'name': 'Garamond'}})
        else:
            chart1.set_x_axis({'min': 1.00, 'max': 5.00, 'major_unit': 1, 'num_font':  {'name': 'Garamond'}})

        chart1.set_y_axis({'major_gridlines': {'visible': True, 'line': {'width': 1}},
            'num_font': {'name': 'Garamond', 'bold': True}
        })

        fill_colors = [{'fill': {'color': '#BE6E28'}}] * (len(individuals)-1)
        fill_colors.extend([{'fill': {'color': '#F6B524'}}])

        chart1.add_series({
            'name': ['eSigma Bar Chart', 0, 1],
            'categories': ['eSigma Bar Chart', 1, 0, len(individuals), 0],
            'values': ['eSigma Bar Chart', 1, 1, len(esigmas), 1],
            'data_labels': {'value': True, 'position': 'inside_end', 'font': {'name': 'Garamond', 'bold': True, 'size':14}},
            'gap': 50,
            'name_font': {'font_name': 'Garamond', 'bold': True},
            'points': fill_colors
        })

        chart1.set_legend({'none': True})
        chart1.set_title({'name': 'Assessment Average by Director', 'name_font': {'name': 'Garamond', 'size': 16, 'bold': True}})
        worksheet.insert_chart('G2', chart1, {'x_offset': 25, 'y_offset': 10, 'x_scale': 2, 'y_scale': 1.5})

        df = pd.DataFrame(list(zip(individuals, esigmas)), columns=['respondent', 'Assessment Average by Director'])

        ########################################

        # if self.anonymize == False:
        #     df.to_pickle(r'C:\Users\Drew\Peer Reports\esigma_bar_chart.pkl')

        fig = plt.figure(figsize=(16,10))
        ax = fig.add_subplot(1,1,1)

        ax.xaxis.grid(color='gray', linestyle='solid')
        ax.set_axisbelow(True)

        if self.five_point_scale:
            ax.set_xlim([1.00,5.00])
        else:
            ax.set_xlim([1.00,10.00])

        # plt.gca().invert_yaxis()
        # df = df.iloc[::-1]
        df.reset_index(inplace=True)
        del df['index']

        plt.xticks(fontname='Garamond')
        plt.yticks(fontname='Garamond', weight='bold', size=18)

        if self.five_point_scale:
            ax.set_xlim([1.0,5.0])
            ax.set_xticklabels(np.arange(1.00, 6.00, 1))
        else:
            ax.set_xlim([1.0,10.0])
            ax.set_xticklabels(np.arange(1.00, 11.00, 1))
        ax.set_xticklabels(ax.get_xticks(), weight='bold', size=16, fontname='Garamond')

        reorder_indices = [df.index.tolist()[0]]
        reorder_indices.extend(df.index.tolist()[::-1][:-1])
        df = df.reindex(reorder_indices)
        df.reset_index(inplace=True, drop=True)

        bar_chart = ax.barh(df['respondent'], df['Assessment Average by Director'], color='#BE6E28')
        bar_chart[0].set_color('#F6B524')

        rects = ax.patches
        for rect, label in zip(rects, df['Assessment Average by Director'].tolist()):
            # print(rect, type(rect))
            # print(label)
            # width = rect.get_width()
            # print('height: ', width)
            # ax.text(rect.get_width()-.45, rect.get_y()-.1, label,
            #         ha='center', va='bottom')
            width = rect.get_width()
            plt.text(.95*float(rect.get_width()), float(rect.get_y())+0.5*float(rect.get_height()),
                     str(label),
                     ha='center', va='center', **{'fontname':'Garamond', 'fontsize': 24, 'weight':'bold'})

        # for a, b in enumerate(df['Assessment Average by Director']):
        #     print(a,b)
            # plt.text(float(b)-.45, a-.1, b, **{'fontname':'Garamond', 'fontsize': 24, 'weight':'bold'})

        if self.anonymize==False:
            # plt.savefig('charts\\esigma_bar_chart.png', bbox_inches='tight')
            if self.year:
                plt.savefig(os.path.join(self.app.config['CHARTS'], '{}_esigma_bar_chart.png'.format(self.year)), bbox_inches='tight', dpi=300)
            else:
                plt.savefig(os.path.join(self.app.config['CHARTS'], 'esigma_bar_chart.png'), bbox_inches='tight', dpi=300)
        else:
            # plt.savefig('charts\\anonymized_esigma_bar_chart.png', bbox_inches='tight')
            if self.year:
                plt.savefig(os.path.join(self.app.config['CHARTS'], '{}_anonymized_esigma_bar_chart.png'.format(self.year)), bbox_inches='tight', dpi=300)
            else:
                plt.savefig(os.path.join(self.app.config['CHARTS'], 'anonymized_esigma_bar_chart.png'), bbox_inches='tight', dpi=300)

        plt.close(fig)

    def bar_chart(self):
        worksheet = self.workbook.add_worksheet('Bar Chart')
        fmt = self.workbook.add_format({"font_name": "Garamond"})
        bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': 1, 'num_format': "0.00"})

        q = self.questions_df.copy()

        individuals = []
        individuals.append('Board')

        q = q[q['type'].isin(['likert', 'matrix'])]
        count = 1
        for respondent in q['sub_section'].unique():
            if self.anonymize == True:
                individuals.append('Director {}'.format(count))
                count += 1
            else:
                individuals.append(HumanName(respondent).last)

        headers = ['respondent']
        values = []
        for section in q['section'].unique():
            l = []
            raw = q[(q['section']==section) & (q['type'].isin(['likert', 'matrix']))]['question_raw'].tolist()
            section_avg = self.df[raw].stack().mean()
            l.append(Decimal('{}'.format(section_avg)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            headers.append(section)
            for respondent in q['sub_section'].unique():
                # print('\n', respondent)
                raw = q[(q['sub_section']==respondent)&(q['section']==section)]['question_raw'].tolist()
                try:
                    value = self.df[raw].stack().mean()
                    # print('value: ', value)
                except:
                    # print('error raw\n\n', raw)
                    # print('error data\n\n', self.df[raw])
                    pass

                l.extend([Decimal('{}'.format(value)).quantize(Decimal('1e-2'), ROUND_HALF_UP)])
            values.append(l)

        worksheet.write_row(0, 0, headers, bold_fmt)
        worksheet.write_column(1, 0, individuals, bold_fmt)
        col = 1
        for l in values:
            worksheet.write_column(1, col, l, bold_fmt)
            col+=1

        chart1 = self.workbook.add_chart({'type': 'column', 'name_font': {'font_name': 'Garamond', 'bold': True}})
        col = 1
        for l in values:
            if l == values[0]:
                chart1.add_series({
                    'name':       ['Bar Chart', 0, col],
                    'categories': ['Bar Chart', 1, 0, len(individuals), 0],
                    'values':     ['Bar Chart', 1, col, len(individuals), col],
                    'fill': {'color': '#A31E23'},
                    'data_labels': {'value': True, 'position': 'inside_end', 'font': {'rotation': -90, 'name': 'Garamond', 'bold': True, 'size': 22, 'color':'white'}},
                    'gap': 50,
                    'name_font': {'font_name': 'Garamond', 'bold': True}
                })
                col+=1
            else:
                chart1.add_series({
                    'name':       ['Bar Chart', 0, col],
                    'categories': ['Bar Chart', 1, 0, len(individuals), 0],
                    'values':     ['Bar Chart', 1, col, len(individuals), col],
                    'fill': {'color': '#F6B524'},
                    'data_labels': {'value': True, 'position': 'inside_end', 'font': {'rotation': -90, 'name': 'Garamond', 'bold': True, 'size':22}},
                    'gap': 50,
                    'name_font': {'font_name': 'Garamond', 'bold': True}
                })
                col+=1

        if self.five_point_scale == False:
            chart1.set_y_axis({'min': 1.00, 'max': 10.00, 'major_unit': 1, 'num_font':  {'name': 'Garamond'}})
        else:
            chart1.set_y_axis({'min': 1.00, 'max': 5.00, 'major_unit': 1, 'num_font':  {'name': 'Garamond'}})
        chart1.set_x_axis({'min': 1.00, 'max': 5.00, 'major_unit': 1, 'num_font':  {'name': 'Garamond'}})

        chart1.set_legend({'position': 'bottom', 'font': {'name': "Garamond", 'size': 10, 'bold': False}})
        worksheet.insert_chart('D2', chart1, {'x_offset': 25, 'y_offset': 10, 'x_scale': 2.5, 'y_scale': 1.5})

        # print('\n\n------------')
        # print(individuals)
        # print(values)

        ###########################

        lists = []
        lists.append(individuals)
        for l in values:
            lists.append(l)
        df = pd.DataFrame(lists, columns=['respondent'].extend(headers))
        df = df.T
        # print(['respondent'].extend(list(q['section'].unique())))
        df.columns = headers
        # df.to_pickle(r'C:\Users\Drew\Peer Reports\section_avg_bar.pkl')

        # print(df)
        # Setting the positions and width for the bars
        pos = list(range(len(df)))
        # pos = [x-.5 for x in pos]
        # print('pos: ', pos)
        num_col = len(df.columns) - 1
        # print(num_col)

        width = .8/ num_col

        fig, ax = plt.subplots(figsize=(22,10))
        bar_colors = ['#A31E23', '#F6B524']

        while len(bar_colors) < (len(df.columns) -1):
            if len(bar_colors)%2 == 0:
                bar_colors.append('#A31E23')
            else:
                bar_colors.append('#F6B524')


        bar_labels = df.columns[1:]
        # print('bar_labels: ', bar_labels)

        # print(df)
        # print(len(df.columns[1:]))
        for i, (colname, color, lbl) in enumerate(zip(df.columns[1:], bar_colors, bar_labels)):
            # print(i, colname, color, lbl)
            if len(df.columns[1:])== 1:
                delta_p = ((1/len(df.columns[1:]))-.5) + width*i
            elif len(df.columns[1:])== 2:
                delta_p = ((1/len(df.columns[1:]))-.2) + width*i
            elif len(df.columns[1:])== 3:
                delta_p = ((1/len(df.columns[1:]))-.1) + width*i
            elif len(df.columns[1:])== 4:
                delta_p = ((1/len(df.columns[1:]))-.05) + width*i
            else:
                delta_p = ((1/len(df.columns[1:]))-.025) + width*i

            plt.bar([p + delta_p for p in pos],
                    df[colname], width, color=color, label=lbl)

        ax.set_xticks(pos)

        def update_ticks(x, pos):
            return df['respondent'][pos]

        ax.xaxis.set_major_formatter(ticker.NullFormatter())
        ax.xaxis.set_minor_formatter(ticker.FuncFormatter(update_ticks))
        ax.xaxis.set_minor_locator(ticker.FixedLocator([p+0.5 for p in pos]))

        for tick in ax.xaxis.get_minor_ticks():
            tick.tick1line.set_markersize(0)
            tick.tick2line.set_markersize(0)
            tick.label1.set_horizontalalignment('center')
        plt.xlim(min(pos), max(pos)+1)
        # plt.ylim([0, 10.0+max([max(df[colname]) for colname in df.columns[:-1]])])

        if self.five_point_scale:
            plt.ylim([0, 5])
        else:
            plt.ylim([0, 10])

        ax.grid(axis='y')
        if self.five_point_scale:
            ax.set_ylim([1.0,5.0])
        else:
            ax.set_ylim([1.0,10.0])

        if self.five_point_scale:
            ax.set_yticklabels(np.arange(1.00, 6.00, 1))
        else:
            ax.set_yticklabels(np.arange(1.00, 11.00, 1))

        ax.set_ylabel('Rating', weight='bold', size=24, fontname='Garamond')

        if len(df) > 1:
            ax.set_xlabel('Sections', weight='bold', size=24, fontname='Garamond')
        else:
            ax.set_xlabel('Section', weight='bold', size=24, fontname='Garamond')
        ax.xaxis.labelpad = 20
        # for text in ax.get_xminorticklabels():
        #     text.set_fontweight='bold'

        plt.setp(ax.get_xminorticklabels(), weight='bold', fontsize=16, fontname='Garamond')

        ax.set_yticklabels(ax.get_yticks(), weight='bold', size=16, fontname='Garamond')

        if len(self.questions_df['section'].unique()) > 1:
            plt.legend(bbox_to_anchor=(1,  .5), prop={'size':20, 'family': 'Garamond'})

        ax.set_axisbelow(True)

        rects = ax.patches

        # Make some labels.
        labels = list(df[df.columns[1:]].unstack().values)

        # Annotate bars
        count = 1
        switch = 0
        for rect, label in zip(rects, labels):
            # print(rect, label)
            # print(count)
            height = rect.get_height()
            if count%2==1:
                # print('white')
                if label != 1.0:
                    ax.text((rect.get_x() + rect.get_width() / 2)+.02, float(height)*.87, label, rotation=90,
                        ha='center', va='bottom', **{'fontname':'Garamond', 'fontsize': 30, 'weight':'bold', 'color':'white'})
                else:
                    pass
            else:
                # print('black')
                if label != 1.0:
                    ax.text((rect.get_x() + rect.get_width() / 2)+.02, float(height)*.87, label, rotation=90,
                        ha='center', va='bottom', **{'fontname':'Garamond', 'fontsize': 30, 'weight':'bold'})
                else:
                    pass

            switch += 1
            if switch == len(df):
                count+=1
                switch = 0

        if self.anonymize==False:
            if self.year:
                plt.savefig(os.path.join(self.app.config['CHARTS'], '{}_section_avg_bar_chart.png'.format(self.year)), bbox_inches='tight', dpi=300)
            else:
                plt.savefig(os.path.join(self.app.config['CHARTS'], 'section_avg_bar_chart.png'), bbox_inches='tight', dpi=300)
        else:
            if self.year:
                plt.savefig(os.path.join(self.app.config['CHARTS'], '{}_anonymized_section_avg_bar_chart.png'.format(self.year)), bbox_inches='tight', dpi=300)
            else:
                plt.savefig(os.path.join(self.app.config['CHARTS'], 'anonymized_section_avg_bar_chart.png'), bbox_inches='tight', dpi=300)


        return

    def hi_lo(self):
        worksheet = self.workbook.add_worksheet('Hi-Lo')
        q = self.questions_df.copy()
        q = q[q['type'].isin(['likert', 'matrix'])]

        worksheet.set_column(1,1 ,6)
        worksheet.set_column(2,2 ,22)
        worksheet.set_column(3,3 ,4)
        worksheet.set_column(4,4 ,60)

        question_averages = []
        sections = []
        numbers = []
        questions = []
        sub_sections = []
        for section in q['section'].unique():
            for number in q['number'].unique():
                try:
                    raw = q[(q['number']==number)&(q['section']==section)]['question_raw'].tolist()
                    # print(raw)
                    question_averages.append(self.df[raw].stack().mean())
                    sections.append(section)
                    numbers.append(number)
                    # print('\n\n', raw['sub_section'])
                    # sub_sections.append(raw['sub_section'])
                    questions.append(q[(q['number']==number)&(q['section']==section)]['question'].tolist()[0])
                except:
                    pass
        hilo = pd.DataFrame({'section': sections, 'number': numbers, 'question_average': question_averages, 'question': questions})

        if len(self.questions_df.drop_duplicates(['section','number']).index) < 10:
            number_for_hi_and_low = math.floor(len(self.questions_df.drop_duplicates(['section','number']).index)/2)
            hi = hilo.nlargest(number_for_hi_and_low, 'question_average', keep='all')
            lo = hilo.nsmallest(number_for_hi_and_low, 'question_average', keep='all')

            jinja_hi = hilo.nlargest(number_for_hi_and_low, 'question_average', keep='first')
            jinja_lo = hilo.nsmallest(number_for_hi_and_low, 'question_average', keep='first')
        else:
            hi = hilo.nlargest(5, 'question_average', keep='all')
            lo = hilo.nsmallest(5, 'question_average', keep='all')

            jinja_hi = hilo.nlargest(5, 'question_average', keep='first')
            jinja_lo = hilo.nsmallest(5, 'question_average', keep='first')

        hi['question_average'] = hi.apply(lambda row: str(Decimal('{}'.format(row['question_average'])).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)
        lo['question_average'] = lo.apply(lambda row: str(Decimal('{}'.format(row['question_average'])).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)
        hi['section'] = hi['section'].str.extract(r'\b([IVX]+\.)?')
        lo['section'] = lo['section'].str.extract(r'\b([IVX]+\.)?')

        jinja_hi['question_average'] = jinja_hi.apply(lambda row: str(Decimal(row['question_average']).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)
        jinja_lo['question_average'] = jinja_lo.apply(lambda row: str(Decimal(row['question_average']).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)

        hi_header = self.workbook.add_format({"font_name": "Garamond",
                                         'bold': 2,
                                         'bg_color': '#008000',
                                         'font_color': '#FFFFFF',
                                         'align': 'center',
                                         'border': 1,
                                         'border_color': '#FFFFFF',
                                         'font_size': 12})
        hi_header_left_align = self.workbook.add_format({"font_name": "Garamond",
                                         'bold': 1,
                                         'bg_color': '#008000',
                                         'font_color': '#FFFFFF',
                                         'align': 'left',
                                         'border': 1,
                                         'border_color': '#FFFFFF',
                                         'font_size': 12})
        cell_fmt = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#F3F3F3',
                                           'align': 'left',
                                           'border': 1,
                                           'border_color': '#FFFFFF',
                                           'valign': 'top',
                                           'text_wrap': True,
                                           'font_size': 12})
        cell_fmt_centered = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#F3F3F3',
                                           'align': 'center',
                                           'border': 1,
                                           'border_color': '#FFFFFF',
                                           'valign': 'top',
                                           'font_size': 12})

        worksheet.merge_range('B2:E2', 'Highest Rated Questions', hi_header)
        worksheet.write('B3', 'Rating', hi_header)
        worksheet.write('C3', 'Section', hi_header)
        worksheet.merge_range('D3:E3', 'Question', hi_header_left_align)

        ###############
        hi['formatted_number'] = hi['number']
        jinja_hi['formatted_number'] = jinja_hi['number']
        ###############

        worksheet.write_column('B4', hi['question_average'], cell_fmt_centered)
        worksheet.write_column('C4', hi['section'], cell_fmt_centered)

        ###############
        try:
            hi['formatted_number'] = hi['formatted_number'].str.replace(r'\.', r'')
            hi['formatted_number'] += '.'

            jinja_hi['formatted_number'] = jinja_hi['formatted_number'].str.replace(r'\.', r'')
            jinja_hi['formatted_number'] += '.'
        except:
            pass
        ###############

        worksheet.write_column('D4', hi['formatted_number'], cell_fmt_centered)
        worksheet.write_column('E4', hi['question'], cell_fmt)

        lo_header = self.workbook.add_format({"font_name": "Garamond",
                                         'bold': 1,
                                         'bg_color': '#FFFF00',
                                         'align': 'center',
                                         'border': 1,
                                         'border_color': '#FFFFFF',
                                         'font_size': 12})
        lo_header_left_align = self.workbook.add_format({"font_name": "Garamond",
                                         'bold': 1,
                                         'bg_color': '#FFFF00',
                                         'align': 'left',
                                         'border': 1,
                                         'border_color': '#FFFFFF',
                                         'font_size': 12})

        worksheet.merge_range('B{0}:E{0}'.format(int(4 + len(hi))), 'Lowest Rated Questions', lo_header)
        worksheet.write('B{}'.format(int(5 + len(hi))), 'Rating', lo_header)
        worksheet.write('C{}'.format(int(5 + len(hi))), 'Section', lo_header)
        worksheet.merge_range('D{0}:E{0}'.format(int(5 + len(hi))), 'Question', lo_header_left_align)

        ###############
        lo['formatted_number'] = lo['number']
        jinja_lo['formatted_number'] = jinja_lo['number']
        ###############

        worksheet.write_column('B{}'.format(int(6 + len(hi))), lo['question_average'], cell_fmt_centered)
        worksheet.write_column('C{}'.format(int(6 + len(hi))), lo['section'], cell_fmt_centered)
        # lo['formatted_number'] = lo['formatted_number'].str.replace(r'\.', r'')
        # lo['formatted_number'] += '.'

        ###############
        try:
            lo['formatted_number'] = lo['formatted_number'].str.replace(r'\.', r'')
            lo['formatted_number'] += '.'

            jinja_lo['formatted_number'] = jinja_lo['formatted_number'].str.replace(r'\.', r'')
            jinja_lo['formatted_number'] += '.'
        except:
            pass
        ###############

        worksheet.write_column('D{}'.format(int(6 + len(hi))), lo['formatted_number'], cell_fmt_centered)
        worksheet.write_column('E{}'.format(int(6 + len(hi))), lo['question'], cell_fmt)

        jinja_hi['section'] = jinja_hi['section'].str.extract(r'\b([IVX]+\.)?')
        jinja_lo['section'] = jinja_lo['section'].str.extract(r'\b([IVX]+\.)?')

        self.jinja_hi = jinja_hi
        self.jinja_lo = jinja_lo
        return

    def eSigma(self):
        es = pd.DataFrame(list(self.averages.items()), columns=['Section', 'Averages'])
        es['Section'] = es.apply(lambda row: row['Section'].replace(u'\xa0', u' '), axis=1)
        es['Averages'] = es['Averages'].astype(float)
        average_of_section_averages = es['Averages'].mean()
        qa = self.survey_object.copy()
        qa = qa[qa['question_type'].isin(['likert', 'matrix'])]
        qa = qa.loc[:,'1':].apply(pd.to_numeric)
        qa['average'] = qa.loc[:,'1':].apply(lambda row: row.mean(), axis=1)
        average_of_question_averages = qa['average'].mean()
        # This is a convient place to calculate part of the section analysis
        self.average_total_rating_given = qa['average'].sum()

        # print('\nHERE:')
        # print(es)
        # print(qa)

        esigma = (average_of_question_averages + average_of_section_averages) / 2
        return esigma

    def individual_esigma(self, respondent):
        # print(respondent)
        q = self.questions_df.copy()

        # Different method to calculate average_of_section_averages by user
        data_dict = {}
        raw = q[(q['type'].isin(['likert', 'matrix']))&(q['sub_section']==respondent)]
        for section in raw['section'].unique():
            q_list = raw[raw['section']==section]['question_raw'].tolist()
            section_avg = self.df[q_list].stack().mean()
            data_dict[section] = section_avg
            # print(respondent, section, section_avg)
        es = pd.DataFrame(list(data_dict.items()), columns=['Section', 'Averages'])

        average_of_section_averages = es['Averages'].mean()
        # print(average_of_section_averages)

        # Calculate the average of all questions answered by respondent
        list_of_averages_by_question = []
        q_list = q[(q['type'].isin(['likert', 'matrix']))&(q['sub_section']==respondent)]['question_raw'].tolist()
        for q in q_list:
            question_avg = self.df[q].mean()
            list_of_averages_by_question.append(question_avg)

        average_of_question_averages = np.mean(list_of_averages_by_question)
        # print('average_of_question_averages: ', average_of_question_averages)
        esigma = (average_of_question_averages + average_of_section_averages) / 2
        # print('esigma: ', esigma)
        # return esigma
        return Decimal('{}'.format(esigma)).quantize(Decimal('1e-2'), ROUND_HALF_UP)

    def create_multi_index_rows(self, section, number, sub_section, qtype, question, answers):
        # question = re.sub(r'\.\.\d+$', r'.', question)
        index = pd.MultiIndex.from_arrays([[section], [number], [sub_section], [qtype], [question]], \
                                          names = ['section', 'number', 'sub_section', 'question_type', 'question'])
        formatted_reportsheet_row = pd.DataFrame([tuple(answers)], index = index, columns = np.arange(1, len(self.df)+1))

        self.rows_of_survey_object.append(formatted_reportsheet_row)
        shuffle(answers)
        shuffled_row= pd.DataFrame([tuple(answers)], index = index, columns = np.arange(1, len(self.df)+1))
        self.shuffled_rows_of_survey_object.append(shuffled_row)

    def statistics(self):
        df = pd.concat(self.rows_of_survey_object)
        df_shuffled = pd.concat(self.shuffled_rows_of_survey_object)

        df.reset_index(inplace=True)
        df_shuffled.reset_index(inplace=True)

        # Calculate Averages for each section
        df.columns = [str(i) for i in df.columns]
        df_shuffled.columns = [str(i) for i in df.columns]

        #
        df[df['question_type'].isin(['likert', 'matrix'])].loc[:, '1':] = df[df['question_type'].isin(['likert', 'matrix'])].loc[:, '1':].apply(pd.to_numeric)
        #

        numericAnswers = df[df['question_type'].isin(['likert', 'matrix'])]
        # Check if the report is based on a 5 point scale
        # if numericAnswers.loc[:,'1':].max(numeric_only=True) <= 5:
        if max(numericAnswers.loc[:,'1':].max()) <=5:
            self.five_point_scale = True

        averages = {}
        for s in df['section'].unique():
            try:
                sub_frame = numericAnswers[numericAnswers['section']==s]
                totalNumberOfNullReponses = sub_frame.isna().sum().sum()
                sums = []
                # print(sub_frame)
                for index, row in sub_frame.loc[:, '1':].iterrows():
                    sums.append(row.sum())
                total = sum(sums)
                totalNumberOfResponses = len(sums)*len(sub_frame.loc[:, '1':].columns)
                if totalNumberOfResponses == 0:
                    continue
                average = total/(totalNumberOfResponses-totalNumberOfNullReponses)
                # Create Dictionary of Averages for each Section
                str(Decimal('{}'.format(average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                averages[s.replace(u'\xa0', u' ')] = str(Decimal('{}'.format(average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if self.anonymize:
                    self.section_avg_list.append({s: str(Decimal(average).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
            except:
                pass

        self.report_sheet.columns = [str(i) for i in self.report_sheet.columns]
        # print(self.report_sheet)
        sectionIndices = self.report_sheet.index[self.report_sheet[self.report_sheet.columns[-1]] == 'average_value']
        sectionIndices = [i + 1 for i in sectionIndices]
        self.row_count_of_sections = sectionIndices
        for i in sectionIndices:
            section = self.report_sheet.iloc[i, 0]
            section = section.replace(u'\xa0', u' ')
            self.report_sheet.iloc[i-1, [-1]] = averages.get(section)

        df['sub_section'] = df['sub_section'].astype(str)
        df_shuffled['sub_section'] = df_shuffled['sub_section'].astype(str)

        # Save members averages and survey_object
        self.averages = averages

        self.survey_object = df.copy()
        self.shuffled_survey_object = df_shuffled.copy()

        self.sectionMinsAndMaxes = {}
        for section in self.survey_object['section'].unique():
            sub_frame = self.survey_object[(self.survey_object['section']==section) & (self.survey_object['question_type'].isin(['likert', 'matrix']))]
            sub_frame.loc[:,'1':] = sub_frame.loc[:,'1':].apply(pd.to_numeric)
            if sub_frame.empty:
                continue
            try:
                self.sectionMinsAndMaxes[section.replace(u'\xa0', u' ')] = {'min': int(min(sub_frame.loc[:,'1':].min())), 'max': int(max(sub_frame.loc[:, '1':].max()))}
            except:
                # print('This error is being thrown most likely because a question has been assigned an incorrect question type.\nSee the dataframe below to debug:\n')
                with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
                    pass
                    # print(sub_frame)
                pass

        # write dataframe to existing Workbook
        self.write_df(self.report_sheet, 'Report')
        self.write_df(self.survey_object, 'Survey Object')

    def write_df(self, df1, sheetName):
        df = df1.copy()
        worksheet = self.workbook.add_worksheet(sheetName)
        df.fillna('', inplace=True)
        colNum = 0

        if sheetName == 'Survey Object':
            worksheet.write_row(0, 0, df.columns)
            for col in df.columns:
                worksheet.write_column(1, colNum, df[col])
                colNum += 1

        elif 'Skills Matrix' in sheetName:
            fmt = self.workbook.add_format({"font_name": "Garamond"})
            left_top_fmt = self.workbook.add_format({"font_name": "Garamond", 'left': 1, 'top': 1})
            top_bold_fmt_left_right = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1, 'left': 1, 'right': 1})
            top_bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1})
            top_right_bold_center = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1, 'right': 1})
            left_bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'left': 1, 'bottom': 1})
            right_bottom_bold_center_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'right': 1, 'bottom': 1})
            left_fmt = self.workbook.add_format({"font_name": "Garamond", 'left': 1})
            right_fmt = self.workbook.add_format({"font_name": "Garamond", 'right': 1})
            bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True})
            center_bold_fmt_left_right = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1, 'bottom': 1})
            center_bold_fmt_bottom = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'bottom': 1})
            center_bold = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1})
            center_bold_top = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1, 'top': 1})

            worksheet.write(1, 1, '', left_top_fmt)
            worksheet.write(1, 2, 'Board', top_bold_fmt_left_right)

            number_of_skills_matrix_respondents = df.shape[0] - 1
            number_of_skills_matrix_questions = df.shape[1]
            worksheet.merge_range(1, 3, 1, 2 + number_of_skills_matrix_respondents, 'Director Self-Ratings', top_right_bold_center)
            worksheet.write(2, 1, 'Skill Areas:', left_bold_fmt)
            worksheet.write(2, 2, 'Average', center_bold_fmt_left_right)
            initials = list(df.index.get_level_values('initials'))
            initials.remove('board')
            worksheet.write_row(2, 3, initials[:-1], center_bold_fmt_bottom)
            worksheet.write(2, 2+len(initials), initials[-1], right_bottom_bold_center_fmt)

            df.reset_index(inplace=True)

            colNum += 1
            start_row = 3
            end_row   = start_row + number_of_skills_matrix_questions
            start_col = 1
            end_col   = start_col + number_of_skills_matrix_respondents + 1

            ten_format = self.workbook.add_format({'bg_color': '#63be7b'})
            nine_format = self.workbook.add_format({'bg_color': '#86c97e'})
            eight_format = self.workbook.add_format({'bg_color': '#a9d27f'})
            seven_format = self.workbook.add_format({'bg_color': '#ccdd82'})
            six_format = self.workbook.add_format({'bg_color': '#eee683'})
            five_format = self.workbook.add_format({'bg_color': '#fedd81'})
            four_format = self.workbook.add_format({'bg_color': '#fcbf7b'})
            three_format = self.workbook.add_format({'bg_color': '#fba276'})
            two_format = self.workbook.add_format({'bg_color': '#f98570'})
            one_format = self.workbook.add_format({'bg_color': '#f8696b'})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '9.01',
            'maximum': '10',
            'format': ten_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '8.01',
            'maximum': '9',
            'format': nine_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '7.01',
            'maximum': '8',
            'format': eight_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '6.01',
            'maximum': '7',
            'format': seven_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '5.01',
            'maximum': '6',
            'format': six_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '4.01',
            'maximum': '5',
            'format': five_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '3.01',
            'maximum': '4',
            'format': four_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '2.01',
            'maximum': '3',
            'format': three_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '1.01',
            'maximum': '2',
            'format': two_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '0.01',
            'maximum': '1',
            'format': one_format})

            worksheet.write_column(3, 1, df.columns[2:], left_fmt)
            worksheet.write_column(3, 2, df.iloc[number_of_skills_matrix_respondents, 2:], center_bold)
            colNum += 2
            for i, u in zip(initials, df['uniqueID'].tolist()):
                value = df.loc[(df['uniqueID']==u) & (df['initials']==i)].values.flatten().tolist()
                value.remove(i)
                value.remove(u)
                if i == initials[-1]:
                    worksheet.write_column(3, colNum, value, right_fmt)
                else:
                    worksheet.write_column(3, colNum, value, fmt)
                colNum += 1

            average_of_averages = df.loc[(df['initials']=='board')].values.flatten().tolist()
            average_of_averages = average_of_averages[2:]
            # print(average_of_averages)
            average_of_averages = np.mean(average_of_averages)

            bold_align_right_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'right', 'left': 1, 'top': 1})
            worksheet.write(number_of_skills_matrix_questions + 3, 1, 'Overall Average:', bold_align_right_fmt)
            worksheet.write(number_of_skills_matrix_questions+3, 2, str(Decimal('{}'.format(average_of_averages)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), center_bold_top)

            self.skills_matrix_average = str(Decimal('{}'.format(average_of_averages)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            self.number_of_skills_matrix_questions = number_of_skills_matrix_questions

            top_bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1})
            top_right_bold_center_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'right': 1, 'top': 1})

            del df['uniqueID']
            with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
                df2 = df.T
                cols = list(df2.columns)
                cols = [cols[-1]] + cols[:-1]
                df2 = df2[cols]
                df2.rename(columns={x:y for x,y in zip(df2.columns,range(0,len(df2.columns)))}, inplace=True)
                # print(df2)
                self.skills_matrix_export = df2
                df2.loc['Overall Average:'] = df2.iloc[1:,0:].mean()
                # print(df2)
                # print(type(df2.loc['Overall Average:', 4]))
                df2.loc['Overall Average:'] = df2.loc['Overall Average:'].apply(lambda x: str(Decimal(x).quantize(Decimal('1e-2'), ROUND_HALF_UP)))
                df2.iloc[1:,0] = df2.iloc[1:, 0].apply(lambda x: str(Decimal(x).quantize(Decimal('1e-2'), ROUND_HALF_UP)))
                # print(df2)

            # for _, row in df2.iloc[1:-1,:].iterrows():
            #     print('index: ', _, '\nrow:', row)
            #     print(row[0])


            del df['initials']
            df.drop(df.tail(1).index,inplace=True)

            colNum = 3

            df['respondentAverage'] = df.apply(lambda row: row.mean(), axis=1)
            # print(df)
            # writer = pd.ExcelWriter('skills.xlsx')
            # df.to_excel(writer)
            # writer.save()
            for i, r in df.iterrows():
                if i == len(df) - 1:
                    worksheet.write(number_of_skills_matrix_questions+3, colNum, str(Decimal('{}'.format(r['respondentAverage'].mean())).quantize(Decimal('1e-2'), ROUND_HALF_UP)), top_right_bold_center_fmt)
                else:
                    worksheet.write(number_of_skills_matrix_questions+3, colNum, str(Decimal('{}'.format(r['respondentAverage'].mean())).quantize(Decimal('1e-2'), ROUND_HALF_UP)), top_bold_fmt)
                colNum += 1

            blank_left_bottom = self.workbook.add_format({"font_name": "Garamond", 'left': 1, 'bottom': 1})
            blank_left_right_bottom = self.workbook.add_format({"font_name": "Garamond", 'left': 1, 'bottom': 1, 'right': 1})
            blank_bottom_right_bold = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'bottom': 1, 'right': 1})
            worksheet.write(number_of_skills_matrix_questions + 4, 1, '', blank_left_bottom)
            worksheet.write(number_of_skills_matrix_questions + 4, 2, '', blank_left_right_bottom)
            worksheet.merge_range(number_of_skills_matrix_questions + 4, 3, number_of_skills_matrix_questions + 4, 2 + number_of_skills_matrix_respondents, 'Total Expertise Average by Directors', blank_bottom_right_bold)

        elif sheetName == 'Report':
            fmt = self.workbook.add_format({'font_name': 'Garamond',
                                                    'font_size': 12})
            fmt_center = self.workbook.add_format({'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True})
            right_border = self.workbook.add_format({'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True,
                                                    'right': 1})
            question_row_fmt = self.workbook.add_format({'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True,
                                                    'bold': True})
            question_avg_row_fmt = self.workbook.add_format({'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True,
                                                    'align': 'right',
                                                    'bold': True})
            # Identify the columns we want to center_across, and where we want the
            # right border line to go
            str_columns = [str(x) for x in range(4, 5 + self.number_of_respondents)]

            for col in df.columns:
                if col == str(self.number_of_respondents + 3):
                    worksheet.write_column(0, colNum, df[col], right_border)
                    colNum += 1
                elif col in str_columns:
                    worksheet.write_column(0, colNum, df[col], fmt_center)
                    colNum += 1
                else:
                    worksheet.write_column(0, colNum, df[col], fmt)
                    colNum += 1

            # Get min and max average rating for each question in a section,
            # Get the lowest and highest absolute response in each section
            sectionMinsAndMaxes = {}
            questionAveragesBySectionMinsAndMaxes = {}
            for section in self.questions_df['section'].unique():
                questions_sub_frame = self.questions_df[(self.questions_df['section']==section) & (self.questions_df['type'].isin(['likert', 'matrix']))]
                raw_questions_list = questions_sub_frame['question_raw'].tolist()
                sub_frame = self.df[raw_questions_list]
                # sub_frame.loc[:,'1':] = sub_frame.loc[:,'1':].apply(pd.to_numeric)
                if sub_frame.empty:
                    self.section_data[section] = 'The open-ended questions provide directors with an opportunity to openly and anonymously comment on their colleagues’ competency and overall performance.'
                    continue
                try:
                    sectionMinsAndMaxes[section] = {'min': int(np.nanmin(sub_frame.values)), 'max': int(np.nanmax(sub_frame.values))}
                except:
                    # print('This error is being thrown most likely because a question has been assigned an incorrect question type.\nSee the dataframe below to debug:\n')
                    with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
                        pass
                        # print(sub_frame)
                    pass

                means = []
                for number in questions_sub_frame['number'].unique():
                    q_list = questions_sub_frame[questions_sub_frame['number']==number]['question_raw']
                    means.append(self.df[q_list].stack().mean())


                questionAveragesBySectionMinsAndMaxes[section.replace(u'\xa0', u' ')] = {'min': str(Decimal('{}'.format(min(means))).quantize(Decimal('1e-2'), ROUND_HALF_UP)), 'max': str(Decimal('{}'.format(max(means))).quantize(Decimal('1e-2'), ROUND_HALF_UP))}

            report_format = self.workbook.add_format({'bold': True,
                                                    'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True,
                                                    'bottom': 1,
                                                    'right': 1})
            underline = self.workbook.add_format({'bold': True,
                                                    'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True,
                                                    'bottom': 1})

            for rowIndex, section in zip(self.row_count_of_sections, self.survey_object['section'].unique()):
                sub_frame = self.survey_object[(self.survey_object['section']==section) & (self.survey_object['question_type'].isin(['likert', 'matrix']))]
                sub_frame.loc[:,'1':] = sub_frame.loc[:,'1':].apply(pd.to_numeric)
                if sub_frame.empty:
                    continue
                worksheet.merge_range(rowIndex - 1, 4, rowIndex - 1, 3 + self.number_of_respondents, 'Individual Ratings', report_format)
                worksheet.write(rowIndex - 1, 4 + self.number_of_respondents, 'Average', underline)
                formattedText = r'The average rating for this section is {}. The average rating for each question in the section ranged from {} to {}. Individual ratings ranged from {} to {}.'
                equal_formattedText = 'The average rating for each question and this section is {}. Individual ratings ranged from {} to {}.'
                if questionAveragesBySectionMinsAndMaxes.get(section).get('max') == questionAveragesBySectionMinsAndMaxes.get(section).get('min') and (questionAveragesBySectionMinsAndMaxes.get(section).get('max') == self.averages.get(section)):
                    self.section_data[section] = equal_formattedText.format(self.averages.get(section),
                    sectionMinsAndMaxes.get(section).get('min'),
                    sectionMinsAndMaxes.get(section).get('max'))

                    worksheet.write(rowIndex - 1, 5 + self.number_of_respondents, equal_formattedText.format(self.averages.get(section),
                    sectionMinsAndMaxes.get(section).get('min'),
                    sectionMinsAndMaxes.get(section).get('max')), fmt)
                else:
                    self.section_data[section] = formattedText.format(self.averages.get(section),
                        questionAveragesBySectionMinsAndMaxes.get(section).get('min'),
                        questionAveragesBySectionMinsAndMaxes.get(section).get('max'),
                        sectionMinsAndMaxes.get(section).get('min'),
                        sectionMinsAndMaxes.get(section).get('max'))

                    worksheet.write(rowIndex - 1, 5 + self.number_of_respondents, formattedText.format(self.averages.get(section),
                        questionAveragesBySectionMinsAndMaxes.get(section).get('min'),
                        questionAveragesBySectionMinsAndMaxes.get(section).get('max'),
                        sectionMinsAndMaxes.get(section).get('min'),
                        sectionMinsAndMaxes.get(section).get('max')), fmt)

            self.questionAveragesBySectionMinsAndMaxes = questionAveragesBySectionMinsAndMaxes
            self.sectionMinsAndMaxes = sectionMinsAndMaxes

    def aggregated_section_analysis(self):
        worksheet = self.workbook.add_worksheet('Section Analysis')

        sec = self.survey_object.copy()

        header = self.workbook.add_format({"font_name": "Garamond",
                                         'bg_color': '#c4590f',
                                         'font_color': '#FFFFFF',
                                         'align': 'center'})
        header_left_align = self.workbook.add_format({"font_name": "Garamond",
                                         'bg_color': '#c4590f',
                                         'font_color': '#FFFFFF',
                                         'align': 'left',
                                         'bottom': 1,
                                         'left': 1,
                                         'top': 1,
                                         'border_color': '#c4590f'})
        cell_fmt1 = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#fbe4d5',
                                           'align': 'left',
                                           'bottom': 1,
                                           'left': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt2 = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#FFFFFF',
                                           'align': 'left',
                                           'bottom': 1,
                                           'left': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt1_centered = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#fbe4d5',
                                           'align': 'center',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt2_centered = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#FFFFFF',
                                           'align': 'center',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt1_right = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#fbe4d5',
                                           'align': 'right',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt2_right = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#FFFFFF',
                                           'align': 'right',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_bold = self.workbook.add_format({"font_name": "Garamond",
                                           'align': 'right',
                                           'bold': 1})
        cell_bold_left = self.workbook.add_format({"font_name": "Garamond",
                                           'align': 'left',
                                           'border': 1,
                                           'bold': 1,
                                           'border_color': '#f5b083'})
        orange_border_orange_bg = self.workbook.add_format({'border': 1,
                                                'border_color': '#f5b083',
                                                "font_name": "Garamond",
                                                'bg_color': '#fbe4d5',
                                                'align': 'center'
                                                })
        orange_border_white_bg = self.workbook.add_format({'border': 1,
                                                'border_color': '#f5b083',
                                                "font_name": "Garamond",
                                                'bg_color': '#FFFFFF',
                                                'align': 'center'
                                                })
        orange_border = self.workbook.add_format({'border': 1, 'border_color': '#f5b083'})

        worksheet.write('A1', 'Aggregated Section Analysis', header_left_align)
        worksheet.write('B1', 'Average', header)

        averages = pd.DataFrame(list(self.averages.items()), columns=['Section', 'Averages'])
        averages = averages[averages['Averages']!='nan']
        # averages['Section'] = averages.apply(lambda row: row['Section'].replace(u'\xa0', u' '), axis=1)
        currentRow = 2
        for index, row in averages.iterrows():
            if index % 2 == 0:
                worksheet.write(r'A{}'.format(index+2), row['Section'], cell_fmt1)
                worksheet.write(r'B{}'.format(index+2), str(row['Averages']), cell_fmt1_centered)
            else:
                worksheet.write(r'A{}'.format(index+2), row['Section'], cell_fmt2)
                worksheet.write(r'B{}'.format(index+2), str(row['Averages']), cell_fmt2_centered)
            currentRow += 1

        eSigma_value = self.eSigma()
        self.eSigma_value = eSigma_value
        self.composite_eSigma = str(Decimal('{}'.format(eSigma_value)).quantize(Decimal('1e-2'), ROUND_HALF_UP))

        currentRow += 2
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'Evaluation Statistics', header_left_align)
        currentRow += 1


        # Assessment Statistics
        #######




        worksheet.write('A{}'.format(currentRow), 'Number of rated questions', cell_fmt1)
        d = sec[sec['question_type'].isin(['likert', 'matrix'])]
        d = d.groupby(['section', 'number']).agg({'number': 'count'})
        worksheet.write('B{}'.format(currentRow), len(d['number']), cell_fmt1_right)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Number of rated questions': int(len(d['number']))})
        currentRow += 1
        #######

        worksheet.write('A{}'.format(currentRow), 'Number of open-ended questions', cell_fmt2)
        d = sec[sec['question_type'].isin(['long_form'])]
        d = d.groupby(['section', 'number']).agg({'number': 'count'})
        worksheet.write('B{}'.format(currentRow), len(d['number']), cell_fmt2_right)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Number of open-ended questions': len(d['number'])})
        currentRow += 1
        #######

        worksheet.write('A{}'.format(currentRow), 'Number of Board members surveyed', cell_fmt1)
        worksheet.write('B{}'.format(currentRow), self.number_of_respondents, cell_fmt1_right)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Number of Board members surveyed': self.number_of_respondents})
        currentRow += 1
        #######

        worksheet.write('A{}'.format(currentRow), 'Percentage participation', cell_fmt2)
        # likert_matrix_questions = self.questions_df[self.questions_df['type'].isin(['likert', 'matrix'])]
        # print(self.df[likert_matrix_questions['question_raw'].tolist()].apply(pd.value_counts))
        raw_answers = sec[sec['question_type'].isin(['likert', 'matrix'])]
        raw_answers = raw_answers.loc[:,'1':]

        number_of_empty_strings = 0
        number_of_non_responses = 0
        for key, value in self.na_no_null_dict.items():
            for question, count in self.na_no_null_dict.get(key).items():
                if key == 'na':
                    number_of_non_responses += self.na_no_null_dict.get(key).get(question, 0)
                elif key == 'no':
                    number_of_non_responses += self.na_no_null_dict.get(key).get(question, 0)
                else:
                    number_of_empty_strings += self.na_no_null_dict.get(key).get(question, 0)

        len_index_times_len_columns = (len(raw_answers.index) * len(raw_answers.columns))
        # print(len_index_times_len_columns)
        number_of_eligible_responses = len_index_times_len_columns - number_of_empty_strings
        # print(number_of_eligible_responses)
        # number_of_questions_answered = number_of_eligible_responses - number_of_non_responses
        number_of_questions_answered = number_of_eligible_responses
        # print(number_of_questions_answered)
        percent_participation = ((number_of_questions_answered) / number_of_eligible_responses) * 100
        # percent_participation = ((number_of_eligible_responses) / len_index_times_len_columns) * 100
        # print(percent_participation)

        worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(percent_participation)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
        if self.anonymize:
            percent_participation_formatted = "{0:.2f}%".format(Decimal('{}'.format(percent_participation)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if percent_participation_formatted == '100.00%':
                self.eval_stats_list_of_dicts.append({'Percentage participation': "100%"})
            elif percent_participation_formatted == '0.00%':
                self.eval_stats_list_of_dicts.append({'Percentage participation': "0%"})
            else:
                self.eval_stats_list_of_dicts.append({'Percentage participation': percent_participation_formatted})

        currentRow += 1
        #######
        worksheet.write('A{}'.format(currentRow), 'Total number of responses', cell_fmt1)
        # worksheet.write('B{}'.format(currentRow), len_index_times_len_columns - number_of_non_responses, cell_fmt1_right)
        worksheet.write('B{}'.format(currentRow), len_index_times_len_columns - number_of_empty_strings, cell_fmt1_right)
        if self.anonymize:
            # self.eval_stats_list_of_dicts.append({'Total number of responses': len_index_times_len_columns - number_of_non_responses})
            # According to CCRN feedback, replace number_of_non_responses with number_of_empty_strings
            self.eval_stats_list_of_dicts.append({'Total number of responses': len_index_times_len_columns - number_of_empty_strings})
        currentRow += 1
        #######
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'Question analysis by Board member response:', cell_bold_left)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Question analysis by Board member response:': ''})
        currentRow += 1
        #######
        worksheet.write('A{}'.format(currentRow), 'Maximum possible total rating', cell_fmt1)
        if self.five_point_scale == False:

            total_number_of_applicable_questions = len(raw_answers)/self.number_of_respondents

            if self.no_self_ratings_bool:
                max_rating = total_number_of_applicable_questions * (self.number_of_respondents-1) * 10
            else:
                max_rating = total_number_of_applicable_questions * (self.number_of_respondents) * 10

            worksheet.write('B{}'.format(currentRow), int(max_rating), cell_fmt1_right)
        else:
            # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
            #     print(raw_answers)
            # print('len_index_times_len_columns: ', len_index_times_len_columns)
            # print('number_of_empty_strings: ', number_of_empty_strings)
            # print('self.number_of_respondents: ', self.number_of_respondents)
            # print('len(raw_answers): ', len(raw_answers))

            total_number_of_applicable_questions = len(raw_answers)/self.number_of_respondents
            if self.no_self_ratings_bool:
                max_rating = total_number_of_applicable_questions * (self.number_of_respondents-1) * 5
            else:
                max_rating = total_number_of_applicable_questions * (self.number_of_respondents) * 5
            # print('\nmax_rating: ', max_rating)
            worksheet.write('B{}'.format(currentRow), int(max_rating), cell_fmt1_right)

        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Maximum possible total rating': int(max_rating)})
        currentRow += 1
        #######
        worksheet.write('A{}'.format(currentRow), 'Average total rating given', cell_fmt2)
        worksheet.write('B{}'.format(currentRow), str(Decimal('{}'.format((raw_answers.sum(axis=0).sum())/len(raw_answers.columns))).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Average total rating given': str(Decimal('{}'.format((raw_answers.sum(axis=0).sum())/len(raw_answers.columns))).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
        currentRow += 1


        #######
        worksheet.write('A{}'.format(currentRow), 'Highest total rating given', cell_fmt1)
        check = 0
        raw_answers = raw_answers.apply(pd.to_numeric)
        for c in raw_answers.columns:
            if raw_answers[c].sum()>check:
                check = raw_answers[c].sum()
        worksheet.write('B{}'.format(currentRow), int(check), cell_fmt1_right)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Highest total rating given': int(check)})
        currentRow += 1
        #######
        worksheet.write('A{}'.format(currentRow), 'Lowest total rating given', cell_fmt2)
        check = len(sec[sec['question_type'].isin(['likert', 'matrix'])])*10
        for c in raw_answers.columns:
            if raw_answers[c].sum()<check:
                check = raw_answers[c].sum()
        worksheet.write('B{}'.format(currentRow), int(check), cell_fmt2_right)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Lowest total rating given': int(check)})
        currentRow += 1
        #######
        worksheet.write('A{}'.format(currentRow), 'Maximum possible average', cell_fmt1)
        if self.five_point_scale == False:
            worksheet.write('B{}'.format(currentRow), '10.00', cell_fmt1_right)
            scale_value = '10.00'
        else:
            worksheet.write('B{}'.format(currentRow), '5.00', cell_fmt1_right)
            scale_value = '5.00'
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Maximum possible average': scale_value})
        currentRow += 1



        #######
        worksheet.write('A{}'.format(currentRow), 'Highest rating average given', cell_fmt2)
        check = 0
        for c in raw_answers.columns:
            if raw_answers[c].mean()>check:
                check = raw_answers[c].mean()

        worksheet.write('B{}'.format(currentRow), str(Decimal('{}'.format(check)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
        currentRow += 1
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Highest rating average given': str(Decimal('{}'.format(check)).quantize(Decimal('1e-2'), ROUND_HALF_UP))})


        #######
        worksheet.write('A{}'.format(currentRow), 'Lowest rating average given', cell_fmt1)

        if self.five_point_scale:
            check = 5
        else:
            check = 10

        for c in raw_answers.columns:
            if raw_answers[c].mean()<check:
                check = raw_answers[c].mean()
        worksheet.write('B{}'.format(currentRow), str(Decimal('{}'.format(check)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Lowest rating average given': str(Decimal('{}'.format(check)).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
        currentRow += 1

        #######
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'Group response analysis:', cell_bold_left)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Group response analysis:': ''})

        # This section needs try, except clauses for the case that value counts returns a zero count for whole range of answers
        # Create one series from the raw answers to use value_counts function
        series_of_all_answers = pd.Series(dtype='float')
        for c in raw_answers.columns:
            series_of_all_answers = series_of_all_answers.append(raw_answers[c], ignore_index=True)
        counts = raw_answers.apply(pd.value_counts)
        counts.fillna(0, inplace=True)
        if self.five_point_scale == False:
            new_index = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        else:
            new_index = [1, 2, 3, 4, 5]
        counts = counts.reindex(new_index, fill_value=0)
        currentRow += 1

        if self.five_point_scale == False:
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 10', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format((counts.loc[10].sum()/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            if self.anonymize:
                percentage_of_ten_ratings_formatted = "{0:.2f}%".format(Decimal('{}'.format((counts.loc[10].sum()/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if percentage_of_ten_ratings_formatted == '100.00%':
                    percentage_of_ten_ratings_formatted = '100%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 10': percentage_of_ten_ratings_formatted})
                elif percentage_of_ten_ratings_formatted == '0.00%':
                    percentage_of_ten_ratings_formatted = '0%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 10': percentage_of_ten_ratings_formatted})
                else:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 10': percentage_of_ten_ratings_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 8 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            if self.anonymize:
                eight_or_above_formatted = "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if eight_or_above_formatted == '100.00%':
                    eight_or_above_formatted = '100%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 8 or above': eight_or_above_formatted})
                elif eight_or_above_formatted == '0.00%':
                    eight_or_above_formatted = '0%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 8 or above': eight_or_above_formatted})
                else:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 8 or above': eight_or_above_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 7 or below', cell_fmt1)
            seven_or_below = number_of_questions_answered - (counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format((seven_or_below/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            if self.anonymize:
                percentage_seven_below_formatted = "{0:.2f}%".format(Decimal('{}'.format((seven_or_below/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if percentage_seven_below_formatted == '100.00%':
                    percentage_seven_below_formatted = '100%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 7 or below': percentage_seven_below_formatted})
                elif percentage_seven_below_formatted == '0.00%':
                    percentage_seven_below_formatted = '0%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 7 or below': percentage_seven_below_formatted})
                else:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 7 or below': percentage_seven_below_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 3 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            if self.anonymize:
                percentage_three_below_formatted = "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if percentage_three_below_formatted == '100.00%':
                    percentage_three_below_formatted = '100%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted})
                elif percentage_three_below_formatted == '0.00%':
                    percentage_three_below_formatted = '0%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted})
                else:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted})


            currentRow += 1
            if number_of_non_responses:
                x = cell_fmt2
                cell_fmt2 = cell_fmt1
                cell_fmt1 = x
                x = cell_fmt2_right
                cell_fmt2_right = cell_fmt1_right
                cell_fmt1_right = x
                worksheet.write('A{}'.format(currentRow), 'Percent of ratings of N/A', cell_fmt2)
                # print('number_of_non_responses: ', number_of_non_responses)
                # print('number_of_questions_answered: ', number_of_questions_answered)
                percent_na = int(number_of_non_responses)/int(number_of_questions_answered)

                worksheet.write('B{}'.format(currentRow), '{0:.2f}%'.format(Decimal('{}'.format(percent_na)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
                if self.anonymize:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of N/A': '{0:.2f}%'.format(Decimal('{}'.format(percent_na)).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
                currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 10', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), counts.loc[10].sum(), cell_fmt1_right)
            if self.anonymize:
                self.eval_stats_list_of_dicts.append({'Number of ratings of 10': int(counts.loc[10].sum())})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 8 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum()), cell_fmt2_right)
            if self.anonymize:
                self.eval_stats_list_of_dicts.append({'Number of ratings of 8 or above': int(counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 7 or below', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), seven_or_below, cell_fmt1_right)
            if self.anonymize:
                self.eval_stats_list_of_dicts.append({'Number of ratings of 7 or below': int(seven_or_below)})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 3 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum()), cell_fmt2_right)
            if self.anonymize:
                self.eval_stats_list_of_dicts.append({'Number of ratings of 3 or below': int(counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())})
            currentRow += 1
            if number_of_non_responses:
                worksheet.write('A{}'.format(currentRow), 'Number of ratings of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), str(int(number_of_non_responses)), cell_fmt2_right)
                if self.anonymize:
                    self.eval_stats_list_of_dicts.append({'Number of ratings of N/A': str(int(number_of_non_responses))})
                currentRow += 1
        else:
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 5', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format((counts.loc[5].sum()/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            if self.anonymize:
                percentage_five_formatted = "{0:.2f}%".format(Decimal((counts.loc[5].sum()/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if percentage_five_formatted == '100.00%':
                    percentage_five_formatted = '100%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 5': percentage_five_formatted})
                elif percentage_five_formatted == '0.00%':
                    percentage_five_formatted = '0%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 5': percentage_five_formatted})
                else:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 5': percentage_five_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 4 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[4].sum() + counts.loc[5].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            if self.anonymize:
                percentage_four_above_formatted = "{0:.2f}%".format(Decimal(((counts.loc[4].sum() + counts.loc[5].sum())/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if percentage_four_above_formatted == '100.00%':
                    percentage_four_above_formatted = '100%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 4 or above': percentage_four_above_formatted})
                elif percentage_four_above_formatted == '0.00%':
                    percentage_four_above_formatted = '0%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 4 or above': percentage_four_above_formatted})
                else:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 4 or above': percentage_four_above_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 3 or below', cell_fmt1)
            three_or_below = (counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format((three_or_below/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            if self.anonymize:
                percentage_three_below_formatted_five_scale = "{0:.2f}%".format(Decimal((three_or_below/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if percentage_three_below_formatted_five_scale == '100.00%':
                    percentage_three_below_formatted_five_scale = '100%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted_five_scale})
                elif percentage_three_below_formatted_five_scale == '0.00%':
                    percentage_three_below_formatted_five_scale = '0%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted_five_scale})
                else:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted_five_scale})

            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 2 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[1].sum() + counts.loc[2].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            if self.anonymize:
                percentage_two_below_formatted = "{0:.2f}%".format(Decimal(((counts.loc[1].sum() + counts.loc[2].sum())/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if percentage_two_below_formatted == '100.00%':
                    percentage_two_below_formatted = '100%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 2 or below': percentage_two_below_formatted})
                elif percentage_two_below_formatted == '0.00%':
                    percentage_two_below_formatted = '0%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 2 or below': percentage_two_below_formatted})
                else:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 2 or below': percentage_two_below_formatted})

            currentRow += 1
            if number_of_non_responses:
                x = cell_fmt2
                cell_fmt2 = cell_fmt1
                cell_fmt1 = x
                x = cell_fmt2_right
                cell_fmt2_right = cell_fmt1_right
                cell_fmt1_right = x
                worksheet.write('A{}'.format(currentRow), 'Percent of ratings of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((number_of_non_responses)/(number_of_questions_answered))*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
                if self.anonymize:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of N/A': "{0:.2f}%".format(Decimal('{}'.format(((number_of_non_responses)/(number_of_questions_answered))*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
                currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 5', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), counts.loc[5].sum(), cell_fmt1_right)
            if self.anonymize:
                self.eval_stats_list_of_dicts.append({'Number of ratings of 5': int(counts.loc[5].sum())})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 4 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[4].sum() + counts.loc[5].sum()), cell_fmt2_right)
            if self.anonymize:
                self.eval_stats_list_of_dicts.append({'Number of ratings of 4 or above': int(counts.loc[4].sum() + counts.loc[5].sum())})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 3 or below', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), three_or_below, cell_fmt1_right)
            if self.anonymize:
                self.eval_stats_list_of_dicts.append({'Number of ratings of 3 or below': int(three_or_below)})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 2 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[1].sum() + counts.loc[2].sum()), cell_fmt2_right)
            if self.anonymize:
                self.eval_stats_list_of_dicts.append({'Number of ratings of 2 or below': int(counts.loc[1].sum() + counts.loc[2].sum())})
            currentRow += 1
            if number_of_non_responses:
                worksheet.write('A{}'.format(currentRow), 'Number of ratings of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), str(int(number_of_non_responses)), cell_fmt2_right)
                currentRow += 1
                if self.anonymize:
                    self.eval_stats_list_of_dicts.append({'Number of ratings of N/A': str(int(number_of_non_responses))})
        currentRow += 2
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'e∑igma = {0:.2f}'.format(Decimal('{}'.format(eSigma_value)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), orange_border_white_bg)
        worksheet.set_column(0,0 ,75)
        worksheet.set_column(1,1,10)

    def aggregated_section_analysis(self):
        worksheet = self.workbook.add_worksheet('Section Analysis')

        sec = self.survey_object.copy()

        header = self.workbook.add_format({"font_name": "Garamond",
                                         'bg_color': '#c4590f',
                                         'font_color': '#FFFFFF',
                                         'align': 'center'})
        header_left_align = self.workbook.add_format({"font_name": "Garamond",
                                         'bg_color': '#c4590f',
                                         'font_color': '#FFFFFF',
                                         'align': 'left',
                                         'bottom': 1,
                                         'left': 1,
                                         'top': 1,
                                         'border_color': '#c4590f'})
        cell_fmt1 = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#fbe4d5',
                                           'align': 'left',
                                           'bottom': 1,
                                           'left': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt2 = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#FFFFFF',
                                           'align': 'left',
                                           'bottom': 1,
                                           'left': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt1_centered = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#fbe4d5',
                                           'align': 'center',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt2_centered = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#FFFFFF',
                                           'align': 'center',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt1_right = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#fbe4d5',
                                           'align': 'right',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_fmt2_right = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#FFFFFF',
                                           'align': 'right',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083'})
        cell_bold = self.workbook.add_format({"font_name": "Garamond",
                                           'align': 'right',
                                           'bold': 1})
        cell_bold_left = self.workbook.add_format({"font_name": "Garamond",
                                           'align': 'left',
                                           'border': 1,
                                           'bold': 1,
                                           'border_color': '#f5b083'})
        orange_border_orange_bg = self.workbook.add_format({'border': 1,
                                                'border_color': '#f5b083',
                                                "font_name": "Garamond",
                                                'bg_color': '#fbe4d5',
                                                'align': 'center'
                                                })
        orange_border_white_bg = self.workbook.add_format({'border': 1,
                                                'border_color': '#f5b083',
                                                "font_name": "Garamond",
                                                'bg_color': '#FFFFFF',
                                                'align': 'center'
                                                })
        orange_border = self.workbook.add_format({'border': 1, 'border_color': '#f5b083'})

        worksheet.write('A1', 'Aggregated Section Analysis', header_left_align)
        worksheet.write('B1', 'Average', header)

        averages = pd.DataFrame(list(self.averages.items()), columns=['Section', 'Averages'])
        averages = averages[averages['Averages']!='nan']
        # averages['Section'] = averages.apply(lambda row: row['Section'].replace(u'\xa0', u' '), axis=1)
        currentRow = 2
        for index, row in averages.iterrows():
            if index % 2 == 0:
                worksheet.write(r'A{}'.format(index+2), row['Section'], cell_fmt1)
                worksheet.write(r'B{}'.format(index+2), str(row['Averages']), cell_fmt1_centered)
            else:
                worksheet.write(r'A{}'.format(index+2), row['Section'], cell_fmt2)
                worksheet.write(r'B{}'.format(index+2), str(row['Averages']), cell_fmt2_centered)
            currentRow += 1

        eSigma_value = self.eSigma()
        self.eSigma_value = eSigma_value
        self.composite_eSigma = str(Decimal('{}'.format(eSigma_value)).quantize(Decimal('1e-2'), ROUND_HALF_UP))

        currentRow += 2
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'Evaluation Statistics', header_left_align)
        currentRow += 1


        # Assessment Statistics
        #######




        worksheet.write('A{}'.format(currentRow), 'Number of rated questions', cell_fmt1)
        d = sec[sec['question_type'].isin(['likert', 'matrix'])]
        d = d.groupby(['section', 'number']).agg({'number': 'count'})
        worksheet.write('B{}'.format(currentRow), len(d['number']), cell_fmt1_right)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Number of rated questions': int(len(d['number']))})
        currentRow += 1
        #######

        worksheet.write('A{}'.format(currentRow), 'Number of open-ended questions', cell_fmt2)
        d = sec[sec['question_type'].isin(['long_form'])]
        d = d.groupby(['section', 'number']).agg({'number': 'count'})
        worksheet.write('B{}'.format(currentRow), len(d['number']), cell_fmt2_right)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Number of open-ended questions': len(d['number'])})
        currentRow += 1
        #######

        worksheet.write('A{}'.format(currentRow), 'Number of Board members surveyed', cell_fmt1)
        worksheet.write('B{}'.format(currentRow), self.number_of_respondents, cell_fmt1_right)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Number of Board members surveyed': self.number_of_respondents})
        currentRow += 1
        #######

        worksheet.write('A{}'.format(currentRow), 'Percentage participation', cell_fmt2)
        # likert_matrix_questions = self.questions_df[self.questions_df['type'].isin(['likert', 'matrix'])]
        # print(self.df[likert_matrix_questions['question_raw'].tolist()].apply(pd.value_counts))
        raw_answers = sec[sec['question_type'].isin(['likert', 'matrix'])]
        raw_answers = raw_answers.loc[:,'1':]

        number_of_empty_strings = 0
        number_of_non_responses = 0
        for key, value in self.na_no_null_dict.items():
            for question, count in self.na_no_null_dict.get(key).items():
                if key == 'na':
                    number_of_non_responses += self.na_no_null_dict.get(key).get(question, 0)
                elif key == 'no':
                    number_of_non_responses += self.na_no_null_dict.get(key).get(question, 0)
                else:
                    number_of_empty_strings += self.na_no_null_dict.get(key).get(question, 0)

        len_index_times_len_columns = (len(raw_answers.index) * len(raw_answers.columns))
        # print(len_index_times_len_columns)
        number_of_eligible_responses = len_index_times_len_columns - number_of_empty_strings
        # print(number_of_eligible_responses)
        # number_of_questions_answered = number_of_eligible_responses - number_of_non_responses
        number_of_questions_answered = number_of_eligible_responses
        # print(number_of_questions_answered)
        percent_participation = ((number_of_questions_answered) / number_of_eligible_responses) * 100
        # percent_participation = ((number_of_eligible_responses) / len_index_times_len_columns) * 100
        # print(percent_participation)

        worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(percent_participation)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
        if self.anonymize:
            percent_participation_formatted = "{0:.2f}%".format(Decimal('{}'.format(percent_participation)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if percent_participation_formatted == '100.00%':
                self.eval_stats_list_of_dicts.append({'Percentage participation': "100%"})
            elif percent_participation_formatted == '0.00%':
                self.eval_stats_list_of_dicts.append({'Percentage participation': "0%"})
            else:
                self.eval_stats_list_of_dicts.append({'Percentage participation': percent_participation_formatted})

        currentRow += 1
        #######
        worksheet.write('A{}'.format(currentRow), 'Total number of responses', cell_fmt1)
        # worksheet.write('B{}'.format(currentRow), len_index_times_len_columns - number_of_non_responses, cell_fmt1_right)
        worksheet.write('B{}'.format(currentRow), len_index_times_len_columns - number_of_empty_strings, cell_fmt1_right)
        if self.anonymize:
            # self.eval_stats_list_of_dicts.append({'Total number of responses': len_index_times_len_columns - number_of_non_responses})
            # According to CCRN feedback, replace number_of_non_responses with number_of_empty_strings
            self.eval_stats_list_of_dicts.append({'Total number of responses': len_index_times_len_columns - number_of_empty_strings})
        currentRow += 1
        #######
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'Question analysis by Board member response:', cell_bold_left)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Question analysis by Board member response:': ''})
        currentRow += 1
        #######
        worksheet.write('A{}'.format(currentRow), 'Maximum possible total rating', cell_fmt1)
        if self.five_point_scale == False:

            total_number_of_applicable_questions = len(raw_answers)/self.number_of_respondents

            if self.no_self_ratings_bool:
                max_rating = total_number_of_applicable_questions * (self.number_of_respondents-1) * 10
            else:
                max_rating = total_number_of_applicable_questions * (self.number_of_respondents) * 10

            worksheet.write('B{}'.format(currentRow), int(max_rating), cell_fmt1_right)
        else:
            # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
            #     print(raw_answers)
            # print('len_index_times_len_columns: ', len_index_times_len_columns)
            # print('number_of_empty_strings: ', number_of_empty_strings)
            # print('self.number_of_respondents: ', self.number_of_respondents)
            # print('len(raw_answers): ', len(raw_answers))

            total_number_of_applicable_questions = len(raw_answers)/self.number_of_respondents
            if self.no_self_ratings_bool:
                max_rating = total_number_of_applicable_questions * (self.number_of_respondents-1) * 5
            else:
                max_rating = total_number_of_applicable_questions * (self.number_of_respondents) * 5
            # print('\nmax_rating: ', max_rating)
            worksheet.write('B{}'.format(currentRow), int(max_rating), cell_fmt1_right)

        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Maximum possible total rating': int(max_rating)})
        currentRow += 1
        #######
        worksheet.write('A{}'.format(currentRow), 'Average total rating given', cell_fmt2)
        worksheet.write('B{}'.format(currentRow), str(Decimal('{}'.format((raw_answers.sum(axis=0).sum())/len(raw_answers.columns))).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Average total rating given': str(Decimal('{}'.format((raw_answers.sum(axis=0).sum())/len(raw_answers.columns))).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
        currentRow += 1


        #######
        worksheet.write('A{}'.format(currentRow), 'Highest total rating given', cell_fmt1)
        check = 0
        raw_answers = raw_answers.apply(pd.to_numeric)
        for c in raw_answers.columns:
            if raw_answers[c].sum()>check:
                check = raw_answers[c].sum()
        worksheet.write('B{}'.format(currentRow), int(check), cell_fmt1_right)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Highest total rating given': int(check)})
        currentRow += 1
        #######
        worksheet.write('A{}'.format(currentRow), 'Lowest total rating given', cell_fmt2)
        check = len(sec[sec['question_type'].isin(['likert', 'matrix'])])*10
        for c in raw_answers.columns:
            if raw_answers[c].sum()<check:
                check = raw_answers[c].sum()
        worksheet.write('B{}'.format(currentRow), int(check), cell_fmt2_right)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Lowest total rating given': int(check)})
        currentRow += 1
        #######
        worksheet.write('A{}'.format(currentRow), 'Maximum possible average', cell_fmt1)
        if self.five_point_scale == False:
            worksheet.write('B{}'.format(currentRow), '10.00', cell_fmt1_right)
            scale_value = '10.00'
        else:
            worksheet.write('B{}'.format(currentRow), '5.00', cell_fmt1_right)
            scale_value = '5.00'
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Maximum possible average': scale_value})
        currentRow += 1



        #######
        worksheet.write('A{}'.format(currentRow), 'Highest rating average given', cell_fmt2)
        check = 0
        for c in raw_answers.columns:
            if raw_answers[c].mean()>check:
                check = raw_answers[c].mean()

        worksheet.write('B{}'.format(currentRow), str(Decimal('{}'.format(check)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
        currentRow += 1
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Highest rating average given': str(Decimal('{}'.format(check)).quantize(Decimal('1e-2'), ROUND_HALF_UP))})


        #######
        worksheet.write('A{}'.format(currentRow), 'Lowest rating average given', cell_fmt1)

        if self.five_point_scale:
            check = 5
        else:
            check = 10

        for c in raw_answers.columns:
            if raw_answers[c].mean()<check:
                check = raw_answers[c].mean()
        worksheet.write('B{}'.format(currentRow), str(Decimal('{}'.format(check)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Lowest rating average given': str(Decimal('{}'.format(check)).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
        currentRow += 1

        #######
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'Group response analysis:', cell_bold_left)
        if self.anonymize:
            self.eval_stats_list_of_dicts.append({'Group response analysis:': ''})

        # This section needs try, except clauses for the case that value counts returns a zero count for whole range of answers
        # Create one series from the raw answers to use value_counts function
        series_of_all_answers = pd.Series(dtype='float')
        for c in raw_answers.columns:
            series_of_all_answers = series_of_all_answers.append(raw_answers[c], ignore_index=True)
        counts = raw_answers.apply(pd.value_counts)
        counts.fillna(0, inplace=True)
        if self.five_point_scale == False:
            new_index = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        else:
            new_index = [1, 2, 3, 4, 5]
        counts = counts.reindex(new_index, fill_value=0)
        currentRow += 1

        if self.five_point_scale == False:
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 10', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format((counts.loc[10].sum()/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            if self.anonymize:
                percentage_of_ten_ratings_formatted = "{0:.2f}%".format(Decimal('{}'.format((counts.loc[10].sum()/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if percentage_of_ten_ratings_formatted == '100.00%':
                    percentage_of_ten_ratings_formatted = '100%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 10': percentage_of_ten_ratings_formatted})
                elif percentage_of_ten_ratings_formatted == '0.00%':
                    percentage_of_ten_ratings_formatted = '0%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 10': percentage_of_ten_ratings_formatted})
                else:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 10': percentage_of_ten_ratings_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 8 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            if self.anonymize:
                eight_or_above_formatted = "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if eight_or_above_formatted == '100.00%':
                    eight_or_above_formatted = '100%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 8 or above': eight_or_above_formatted})
                elif eight_or_above_formatted == '0.00%':
                    eight_or_above_formatted = '0%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 8 or above': eight_or_above_formatted})
                else:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 8 or above': eight_or_above_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 7 or below', cell_fmt1)
            seven_or_below = number_of_questions_answered - (counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format((seven_or_below/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            if self.anonymize:
                percentage_seven_below_formatted = "{0:.2f}%".format(Decimal('{}'.format((seven_or_below/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if percentage_seven_below_formatted == '100.00%':
                    percentage_seven_below_formatted = '100%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 7 or below': percentage_seven_below_formatted})
                elif percentage_seven_below_formatted == '0.00%':
                    percentage_seven_below_formatted = '0%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 7 or below': percentage_seven_below_formatted})
                else:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 7 or below': percentage_seven_below_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 3 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            if self.anonymize:
                percentage_three_below_formatted = "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if percentage_three_below_formatted == '100.00%':
                    percentage_three_below_formatted = '100%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted})
                elif percentage_three_below_formatted == '0.00%':
                    percentage_three_below_formatted = '0%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted})
                else:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted})


            currentRow += 1
            if number_of_non_responses:
                x = cell_fmt2
                cell_fmt2 = cell_fmt1
                cell_fmt1 = x
                x = cell_fmt2_right
                cell_fmt2_right = cell_fmt1_right
                cell_fmt1_right = x
                worksheet.write('A{}'.format(currentRow), 'Percent of ratings of N/A', cell_fmt2)
                # print('number_of_non_responses: ', number_of_non_responses)
                # print('number_of_questions_answered: ', number_of_questions_answered)
                percent_na = int(number_of_non_responses)/int(number_of_questions_answered)

                worksheet.write('B{}'.format(currentRow), '{0:.2f}%'.format(Decimal('{}'.format(percent_na)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
                if self.anonymize:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of N/A': '{0:.2f}%'.format(Decimal('{}'.format(percent_na)).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
                currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 10', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), counts.loc[10].sum(), cell_fmt1_right)
            if self.anonymize:
                self.eval_stats_list_of_dicts.append({'Number of ratings of 10': int(counts.loc[10].sum())})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 8 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum()), cell_fmt2_right)
            if self.anonymize:
                self.eval_stats_list_of_dicts.append({'Number of ratings of 8 or above': int(counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 7 or below', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), seven_or_below, cell_fmt1_right)
            if self.anonymize:
                self.eval_stats_list_of_dicts.append({'Number of ratings of 7 or below': int(seven_or_below)})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 3 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum()), cell_fmt2_right)
            if self.anonymize:
                self.eval_stats_list_of_dicts.append({'Number of ratings of 3 or below': int(counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())})
            currentRow += 1
            if number_of_non_responses:
                worksheet.write('A{}'.format(currentRow), 'Number of ratings of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), str(int(number_of_non_responses)), cell_fmt2_right)
                if self.anonymize:
                    self.eval_stats_list_of_dicts.append({'Number of ratings of N/A': str(int(number_of_non_responses))})
                currentRow += 1
        else:
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 5', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format((counts.loc[5].sum()/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            if self.anonymize:
                percentage_five_formatted = "{0:.2f}%".format(Decimal((counts.loc[5].sum()/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if percentage_five_formatted == '100.00%':
                    percentage_five_formatted = '100%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 5': percentage_five_formatted})
                elif percentage_five_formatted == '0.00%':
                    percentage_five_formatted = '0%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 5': percentage_five_formatted})
                else:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 5': percentage_five_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 4 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[4].sum() + counts.loc[5].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            if self.anonymize:
                percentage_four_above_formatted = "{0:.2f}%".format(Decimal(((counts.loc[4].sum() + counts.loc[5].sum())/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if percentage_four_above_formatted == '100.00%':
                    percentage_four_above_formatted = '100%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 4 or above': percentage_four_above_formatted})
                elif percentage_four_above_formatted == '0.00%':
                    percentage_four_above_formatted = '0%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 4 or above': percentage_four_above_formatted})
                else:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 4 or above': percentage_four_above_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 3 or below', cell_fmt1)
            three_or_below = (counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format((three_or_below/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            if self.anonymize:
                percentage_three_below_formatted_five_scale = "{0:.2f}%".format(Decimal((three_or_below/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if percentage_three_below_formatted_five_scale == '100.00%':
                    percentage_three_below_formatted_five_scale = '100%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted_five_scale})
                elif percentage_three_below_formatted_five_scale == '0.00%':
                    percentage_three_below_formatted_five_scale = '0%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted_five_scale})
                else:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted_five_scale})

            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 2 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[1].sum() + counts.loc[2].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            if self.anonymize:
                percentage_two_below_formatted = "{0:.2f}%".format(Decimal(((counts.loc[1].sum() + counts.loc[2].sum())/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                if percentage_two_below_formatted == '100.00%':
                    percentage_two_below_formatted = '100%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 2 or below': percentage_two_below_formatted})
                elif percentage_two_below_formatted == '0.00%':
                    percentage_two_below_formatted = '0%'
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 2 or below': percentage_two_below_formatted})
                else:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of 2 or below': percentage_two_below_formatted})

            currentRow += 1
            if number_of_non_responses:
                x = cell_fmt2
                cell_fmt2 = cell_fmt1
                cell_fmt1 = x
                x = cell_fmt2_right
                cell_fmt2_right = cell_fmt1_right
                cell_fmt1_right = x
                worksheet.write('A{}'.format(currentRow), 'Percent of ratings of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((number_of_non_responses)/(number_of_questions_answered))*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
                if self.anonymize:
                    self.eval_stats_list_of_dicts.append({'Percent of ratings of N/A': "{0:.2f}%".format(Decimal('{}'.format(((number_of_non_responses)/(number_of_questions_answered))*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
                currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 5', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), counts.loc[5].sum(), cell_fmt1_right)
            if self.anonymize:
                self.eval_stats_list_of_dicts.append({'Number of ratings of 5': int(counts.loc[5].sum())})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 4 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[4].sum() + counts.loc[5].sum()), cell_fmt2_right)
            if self.anonymize:
                self.eval_stats_list_of_dicts.append({'Number of ratings of 4 or above': int(counts.loc[4].sum() + counts.loc[5].sum())})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 3 or below', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), three_or_below, cell_fmt1_right)
            if self.anonymize:
                self.eval_stats_list_of_dicts.append({'Number of ratings of 3 or below': int(three_or_below)})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 2 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[1].sum() + counts.loc[2].sum()), cell_fmt2_right)
            if self.anonymize:
                self.eval_stats_list_of_dicts.append({'Number of ratings of 2 or below': int(counts.loc[1].sum() + counts.loc[2].sum())})
            currentRow += 1
            if number_of_non_responses:
                worksheet.write('A{}'.format(currentRow), 'Number of ratings of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), str(int(number_of_non_responses)), cell_fmt2_right)
                currentRow += 1
                if self.anonymize:
                    self.eval_stats_list_of_dicts.append({'Number of ratings of N/A': str(int(number_of_non_responses))})
        currentRow += 2
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'e∑igma = {0:.2f}'.format(Decimal('{}'.format(eSigma_value)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), orange_border_white_bg)
        worksheet.set_column(0,0 ,75)
        worksheet.set_column(1,1,10)

    def overview(self):
        # Identify the columns of data in an iterative manner.
        columns = list(self.df.columns)
        # Format rows one at a time and append them to a list
        formatted_reportsheet_rows = []

        # Need the ability to set and reset values for ranked choice questions
        # the process is iterative, so it is necessary to count ahead as well as behind
        # rankFlag is True as long as the question is a ranked question.
        rankFlag = False
        lengthOfRankedChoiceSection = 0
        yAxisCount = 1
        sectionNames = []
        firstColumnFound = False
        continuedSectionFlag = False

        section_set = set()
        section_number_set = set()

        for index, row in self.questions_df.iterrows():
            # Funnel skills_matrix questions out of the report sheet
            if row['type'] == 'skills_matrix':
                self.skills_matrix_row_builder(row['section'], row['number'], row['sub_section'], self.ppl_df['FirstName'], self.ppl_df['LastName'], self.df[row['question_raw']])
                continue

            elif row['type'] == 'attribute_matrix':
                # print(self.file_name, self.report_type)
                self.skills_matrix_row_builder(row['section'], row['number'], row['sub_section'], self.ppl_df['FirstName'], self.ppl_df['LastName'], self.df[row['question_raw']])
                continue
            # Create an average row to appear above section header row if the
            # section does not just consist of long_form question types
            # print(row['section'], section_set, row['section'] in section_set)

            if row['section'] not in section_set:
                section_set.add(row['section'])
                new_section_flag = True

                if not set(self.questions_df[self.questions_df['section'] == row['section']]['type']).issubset(set(['long_form', 'yes/no'])):
                    # print(row['section'])
                    # print(set(self.questions_df[self.questions_df['section'] == row['section']]['type']))
                    formatted_reportsheet_rows.append((None, None, None, None, None) + tuple([None]*(len(self.df[row['question_raw']])-1)) + ('Average', 'average_value'))
            else:
                new_section_flag = False

            # if (row['section'], row['number']) not in section_number_set:
            #     section_number_set.add((row['section'], row['number'])
                # print((row['section'], row['number']))

            # if (row['section'], row['number']) not in section_number:
            #     section_number.add((row['section'], row['number']))
            #     formatted_reportsheet_rows.append((None, row['number'], row['question'], None, None) + tuple([None]*(len(self.df[row['question_raw']])-1)))

            # Long form questions
            if row['type'] in ['long_form', 'optional_long_form']:
                # If long_form question types are unnumbered
                if row['number'] == '' or row['number'] == np.nan:
                    if new_section_flag:
                        long_form_title_row = (row['section'], row['number'], row['question'], None, None)
                    else:
                        long_form_title_row = (None, None, row['question'], None, None)
                    director_title_row = (None, None, row['sub_section'], None, None)

                    answers = self.df[row['question_raw']].tolist()
                    answers = [str(answer) for answer in answers]

                    # If the question is unnumbered, only display it if it is answered
                    if len(set(answers)) == 1:
                        continue
                    else:
                        #
                        self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())
                        #
                        formatted_reportsheet_rows.append(long_form_title_row)
                        formatted_reportsheet_rows.append(director_title_row)
                        if self.randomize == True:
                            shuffle(answers)
                        for optional in answers:
                            if optional != 'nan':
                                formatted_reportsheet_rows.append((None, None, optional))
                        continue

                # If it's a numbered, optional question that needs displayed
                elif row['number'] != '' and row['number'] != np.nan:
                    if new_section_flag:
                        long_form_title_row = (row['section'], row['number'], row['question'], None, None)
                    else:
                        long_form_title_row = (None, row['number'], row['question'], None, None)
                    director_title_row = (None, None, row['sub_section'], None, None)

                    answers = self.df[row['question_raw']].tolist()
                    #
                    self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())
                    #
                    formatted_reportsheet_rows.append(long_form_title_row)
                    # formatted_reportsheet_rows.append(director_title_row)
                    if self.randomize == True:
                        shuffle(answers)

                    if set(answers) == set(['nan']) | set(answers) == set([np.nan]):
                        formatted_reportsheet_rows.append((None, None, "no answers given"))
                        continue
                    else:
                        for optional in answers:
                            if optional != 'nan':
                                formatted_reportsheet_rows.append((None, None, optional))
                        continue
                else:
                    pass
                    # print('long_form error in: ', self.file_name, self.report_type)


            # Yes/No Questions
            elif row['type'] == 'yes/no':
                if new_section_flag:
                    yes_no_title_row = (row['section'], row['number'], row['question'], None, None)
                else:
                    yes_no_title_row = (None, row['number'], row['question'], None, None)

                yesNoCheck = self.df[row['question_raw']].tolist()
                yesNoCheck = [answer.lower() for answer in yesNoCheck]
                #
                self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())
                #
                yesNoCheck = dict(Counter(yesNoCheck))

                formatted_reportsheet_rows.append(yes_no_title_row)
                formatted_reportsheet_rows.append((None, None, 'Yes', yesNoCheck.get('yes', 0)))
                formatted_reportsheet_rows.append((None, None, 'No', yesNoCheck.get('no', 0)))
                continue
            # Determine if question has multiple parts
            elif row['type'] == 'matrix':
                # Reset Flags
                rankFlag = False
                yAxisCount = 1

                if any(self.df[row['question_raw']].isna()):
                    answers = self.df[row['question_raw']].replace({np.nan: None})
                    answers.fillna('N/A', inplace=True)
                    answers = answers.tolist()
                else:
                    answers = self.df[row['question_raw']].tolist()

                if self.randomize ==True:
                    shuffle(answers)

                try:
                    sub_section_identifier = re.search('^([a-z]\.)', row['sub_section']).group(1)
                except:
                    sub_section_identifier = '!__!'
                try:
                    sub_section_value = re.search('^[a-z]\.[\s\t]*(.*)', row['sub_section']).group(1)
                except:
                    sub_section_value = '!__!'

                if re.search('(^[a-z]\.)', row['sub_section']).group(1) != 'a.':
                    self.create_multi_index_rows(row['section'], row['number'], sub_section_identifier, row['type'], row['question'] + ' ' + sub_section_value, self.df[row['question_raw']].tolist())

                    formatted_reportsheet_row = (None, None, sub_section_identifier, sub_section_value) + tuple(answers) + (str(Decimal('{}'.format(np.mean(self.df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    formatted_reportsheet_rows.append(formatted_reportsheet_row)
                    continue

                else:
                    if new_section_flag:
                        matrix_title_row = (row['section'], row['number'], row['question'], None, None)
                    else:
                        matrix_title_row = (None, row['number'], row['question'], None, None)
                    self.create_multi_index_rows(row['section'], row['number'], sub_section_identifier, row['type'], row['question'] + ' ' + sub_section_value, self.df[row['question_raw']].tolist())
                    # self.create_multi_index_rows(sectionNames[-1], number, re.search(r':_([a-z]\.)', c).group(1), 'matrix', re.search('\d+\.\s(.*:)_[a-z]\.\s(.*)_', c).group(1) + ' ' + re.search('\d+\.\s(.*:)_[a-z]\.\s(.*)_', c).group(2), self.df[c].tolist())

                    # if new_section_flag:
                    #     formatted_reportsheet_row (row['section'], row['number'], row['question'], row['sub_section']) + tuple(answers) + ("{0:.2f}".format(round(np.mean(self.df[row['question_raw']].dropna()),2)),)
                    # else:
                    formatted_reportsheet_row = (None, None, sub_section_identifier, sub_section_value) + tuple(answers) + (str(Decimal('{}'.format(np.mean(self.df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    # formatted_reportsheet_rows.append(sub_sectionHeader)
                    formatted_reportsheet_rows.append(matrix_title_row)
                    formatted_reportsheet_rows.append(formatted_reportsheet_row)
                    continue

            # Check if it's a ranked choice question
            # Ranked choice questions must contain a substring with a capital letter that follows an underscore and the entire string must end in an underscore
            elif row['type'] == 'rank':
                # Ranked questions have no sequential identification.
                # "Flip a switch" to indicate that the iterative process has encountered a ranked Choice question
                # We will switch the rankFlag value to False when the loop encounters a different type of question
                if rankFlag == False:
                    # We know that, if rankFlag is false, this is the first question.
                    # Create and format the rank choice section's titles
                    if new_section_flag:
                        rank_title_row = (row['section'], row['number'], row['question'], None, None)
                    else:
                        rank_title_row = (None, row['number'], row['question'], None, None)

                    xAxisTitle = (None, None, None, None, "Number of Responses")

                    # Iterate ahead in order to get length of the section
                    # beginningIndex = columns.index(column)
                    # endingIndex = beginningIndex
                    # numberCheck = number
                    # while numberCheck == number:
                    #     endingIndex += 1
                    #     try:
                    #         numberCheck = re.search('(\d+\.)', columns[endingIndex]).group(0)
                    #     except:
                    #         break
                    # lengthOfRankedChoiceSection = endingIndex - beginningIndex
                    lengthOfRankedChoiceSection = len(self.questions_df[(self.questions_df['section']==row['section']) & (self.questions_df['number'] == row['number'])])
                    xAxis = (None, None, 'Rank:', None) + tuple(np.arange(1, lengthOfRankedChoiceSection+1, 1))

                    formatted_reportsheet_rows.append(rank_title_row)
                    formatted_reportsheet_rows.append(xAxisTitle)
                    formatted_reportsheet_rows.append(xAxis)

                # Count up the respective rankings, add them to list, and insert 0s where necessary
                dictonaryOfCounts = dict(Counter(self.df[row['question_raw']].tolist()))
                listOfOrderedCounts = list()
                for value in np.arange(1, lengthOfRankedChoiceSection+1, 1):
                    listOfOrderedCounts.append(dictonaryOfCounts.get(value, 0))

                formatted_reportsheet_row = (None, None, yAxisCount, row['sub_section']) + tuple(listOfOrderedCounts)
                formatted_reportsheet_rows.append(formatted_reportsheet_row)
                # Ranked questions have a number of responses based on the number of choices for the whole section

                self.create_multi_index_rows(row['section'], row['number'], yAxisCount, row['type'], row['question'] + ' ' + row['sub_section'], self.df[row['question_raw']].tolist())
                #
                rankFlag = True
                yAxisCount+=1
                continue

            # The simple instance of a rating question
            else:
                # print(self.df[row['question_raw']])
                if any(self.df[row['question_raw']].isna()):
                    answers = self.df[row['question_raw']].replace({np.nan: None})
                    answers.fillna('N/A', inplace=True)
                    answers = answers.tolist()
                else:
                    answers = self.df[row['question_raw']].tolist()

                rankFlag = False
                yAxisCount = 1
                if new_section_flag:
                    if self.randomize:
                        shuffle(answers)

                    section_number_set.add((row['section'], row['number']))

                    raw_q_list = self.questions_df[(self.questions_df['type']=='likert') & (self.questions_df['section']==row['section'])&(self.questions_df['number']==row['number'])]['question_raw'].tolist()
                    question_avg = self.df[raw_q_list].stack().mean()
                    formatted_reportsheet_row = (row['section'], row['number'], row['question'], None) + tuple([None]*(len(self.df[row['question_raw']]))) + (str(Decimal('{}'.format(question_avg)).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    formatted_reportsheet_rows.append(formatted_reportsheet_row)
                    # print(row)
                    if self.anonymize:
                        formatted_reportsheet_row = (None, None, 'Director {}'.format(self.anonymized_labels[row['sub_section']]), None) + tuple(answers) + (str(Decimal('{}'.format(np.mean(self.df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    else:
                        formatted_reportsheet_row = (None, None, row['sub_section'], None) + tuple(answers) + (str(Decimal('{}'.format(np.mean(self.df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    formatted_reportsheet_rows.append(formatted_reportsheet_row)

                    self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())

                else:
                    if self.randomize:
                        shuffle(answers)

                    if (row['section'], row['number']) not in section_number_set:
                        section_number_set.add((row['section'], row['number']))

                        raw_q_list = self.questions_df[(self.questions_df['type']=='likert') & (self.questions_df['section']==row['section'])&(self.questions_df['number']==row['number'])]['question_raw'].tolist()
                        question_avg = self.df[raw_q_list].stack().dropna().mean()

                        question_title = (None, row['number'], row['question'], None) + tuple([None]*(len(self.df[row['question_raw']]))) + (str(Decimal('{}'.format(question_avg)).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                        formatted_reportsheet_rows.append(question_title)
                        if self.anonymize:
                            formatted_reportsheet_row = (None, None, 'Director {}'.format(self.anonymized_labels[row['sub_section']]), None) + tuple(answers) + (str(Decimal('{}'.format(np.mean(self.df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                        else:
                            formatted_reportsheet_row = (None, None, row['sub_section'], None) + tuple(answers) + (str(Decimal('{}'.format(np.mean(self.df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)

                        formatted_reportsheet_rows.append(formatted_reportsheet_row)

                        self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())
                    else:
                        if self.anonymize:
                            # if not self.anonymized_labels.get(row['UniqueIdentifier']):
                            # print('keyerror: ', row['UniqueIdentifier'])
                            # print(self.anonymized_labels)
                            # print(row)
                            formatted_reportsheet_row = (None, None, 'Director {}'.format(self.anonymized_labels[row['sub_section']]), None) + tuple(answers) + (str(Decimal('{}'.format(np.mean(self.df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                            # else:
                                # formatted_reportsheet_row = (None, None, None), None) + tuple(answers) + (str(Decimal('{}'.format(np.mean(self.df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                        else:
                            formatted_reportsheet_row = (None, None, row['sub_section'], None) + tuple(answers) + (str(Decimal('{}'.format(np.mean(self.df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)

                        formatted_reportsheet_rows.append(formatted_reportsheet_row)

                        self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())

        self.report_sheet = pd.DataFrame(formatted_reportsheet_rows)

    def to_arabic(self, roman):
        roman = self.check_valid(roman)
        keys = ['IV', 'IX', 'XL', 'XC', 'CD', 'CM', 'I', 'V', 'X', 'L', 'C', 'D', 'M']
        to_arabic = {'IV': '4', 'IX': '9', 'XL': '40', 'XC': '90', 'CD': '400', 'CM': '900',
                'I': '1', 'V': '5', 'X': '10', 'L': '50', 'C': '100', 'D': '500', 'M': '1000'}
        for key in keys:
            if key in roman:
                roman = roman.replace(key, ' {}'.format(to_arabic.get(key)))
        arabic = sum(int(num) for num in roman.split())
        return arabic

    def check_valid(self, roman):
        roman = roman.upper()
        invalid = ['IIII', 'VV', 'XXXX', 'LL', 'CCCC', 'DD', 'MMMM']
        if any(sub in roman for sub in invalid):
            raise ValueError('Numerus invalidus est: {}\n.The number is an invalid Roman Numeral'.format(roman))
        return roman

    def heat_map(self):
        worksheet = self.workbook.add_worksheet('Heat Map')
        worksheet.hide_gridlines(2)

        # if self.randomize:
            # hm = self.shuffled_survey_object.copy()
        # else:
        hm = self.survey_object.copy()

        hm = hm[hm['question_type'].isin(['likert', 'matrix'])]
        formatted_question_numbers_list = []
        int_question_numbers_list = []
        question_averages = {}

        # Get averages of each question indexed by tuple of section name and question number
        averages = {}
        for q in hm['section'].unique():
            q2 = re.search('[\s\t]*([IVX]+)\.', q).group(1)
            qs = hm[hm['section']==q]['number'].unique().tolist()
            qs = [str(x) for x in qs]
            tuples = [(q2, x) for x in qs]
            formatted_question_numbers_list.extend(tuples)
            l = [re.sub(r'\.$', '', x) for x in qs]
            tuples = [(q2, int(x)) for x in l]
            int_question_numbers_list.extend(tuples)

            # Get averages of each question indexed by tuple of section name and question number
            # Including the average calculation in this loop allows sequential indexing identical
            # to the formatted list of tuples above
            for n in hm[hm['section']== q]['number'].unique():
                sub_frame = hm[(hm['section']==q) & (hm['number']== n)]
                totalNumberOfNullReponses = sub_frame.isna().sum().sum()
                sums = []
                for index, row in sub_frame.loc[:, '1':].iterrows():
                    sums.append(row.sum())
                total = sum(sums)
                totalNumberOfResponses = len(sums)*len(sub_frame.loc[:, '1':].columns)
                if totalNumberOfResponses == 0:
                    break
                average = total/(totalNumberOfResponses-totalNumberOfNullReponses)
                # Create Dictionary of Averages for each Section
                averages[q2, n] = str(Decimal('{}'.format(average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))

        hm = hm.apply(lambda row: self.format_heat_map(row), axis = 1)
        del hm['sub_section']
        del hm['question_type']
        del hm['question']

        unique_section_number_list = list(hm.groupby(['section', 'number']).groups)
        # print('unique_section_number_list: ', unique_section_number_list)

        # Get the size of each section in order to merge cells
        hm.set_index(['section', 'number'], inplace=True)
        if self.no_self_ratings_bool:
            ordered_values = []
            for i in unique_section_number_list:
                subframe = hm.loc[i]
                # print(subframe)
                listed_subframe = subframe.values.tolist()
                deleated_diagonal_subframe = [[el for idxx, el in enumerate(ele) if idxx != idx] for idx, ele in enumerate(listed_subframe)]
                # print(deleated_diagonal_subframe)
                if self.randomize:
                    for r in deleated_diagonal_subframe:
                        shuffle(r)
                # print(subframe)
                # print(deleated_diagonal_subframe)
                # print(subframe.index)
                # print(subframe.columns[:-1])
                subframe = pd.DataFrame(deleated_diagonal_subframe, columns=subframe.columns[:-1], index=subframe.index)
                ordered_values.append(subframe)
            # print(ordered_values)
            # sorted_values_df = pd.DataFrame(np.vstack(ordered_values))
            sorted_values_df = pd.concat(ordered_values)
            # print(sorted_values_df)
            # sorted_values_df.to_excel('sorted_values_df.xlsx')
            hm = sorted_values_df
        else:
            ordered_values = []
            for i in unique_section_number_list:
                subframe = hm.loc[i]
                listed_subframe = subframe.values.tolist()
                if self.randomize:
                    for r in listed_subframe:
                        shuffle(r)
                subframe = pd.DataFrame(listed_subframe, columns=subframe.columns, index=subframe.index)
                ordered_values.append(subframe)
            sorted_values_df = pd.concat(ordered_values)
            hm = sorted_values_df


        ###################################
        hm2 = hm.T
        # Transpose sections individually and stack horrizontally
        ordered_values = []
        col_set = set()
        for col in hm2.columns:
            if col not in col_set:
                col_set.add(col)
                subframe = hm2[col].T

                if self.no_self_ratings_bool:
                    # print(subframe)
                    listed_subframe = subframe.values.tolist()
                    deleated_diagonal_subframe = [[el for idxx, el in enumerate(ele) if idxx != idx] for idx, ele in enumerate(listed_subframe)]
                    # print(deleated_diagonal_subframe)
                    if self.randomize:
                        for r in deleated_diagonal_subframe:
                            shuffle(r)
                    midx = pd.MultiIndex.from_product([list(set(hm2[col].columns.get_level_values(0)))*(self.number_of_respondents-1), list(set(hm2[col].columns.get_level_values(1)))])
                    subframe = pd.DataFrame(deleated_diagonal_subframe, columns=midx, index=np.arange(1, len(subframe)+1, 1).tolist())
                    # print('self.no_self_ratings_bool: ', self.no_self_ratings_bool)
                else:
                    midx = pd.MultiIndex.from_product([list(set(hm2[col].columns.get_level_values(0)))*(self.number_of_respondents), list(set(hm2[col].columns.get_level_values(1)))])
                    if self.randomize:
                        listed_subframe = subframe.values.tolist()
                        for r in listed_subframe:
                            shuffle(r)
                        # midx = pd.MultiIndex.from_product([list(set(hm2[col].columns.get_level_values(0)))*(self.number_of_respondents), list(set(hm2[col].columns.get_level_values(1)))*(self.number_of_respondents)])
                        subframe = pd.DataFrame(listed_subframe, columns=midx, index=np.arange(1, len(subframe)+1, 1).tolist())
                    else:
                        # midx = pd.MultiIndex.from_product([list(set(hm2[col].columns.get_level_values(0)))*(self.number_of_respondents), list(set(hm2[col].columns.get_level_values(1)))*(self.number_of_respondents)])
                        subframe.columns = midx
                        subframe.index = np.arange(1, len(subframe)+1, 1).tolist()
                    # print(subframe)
                ordered_values.append(subframe)


        sorted_values_df = pd.DataFrame(np.hstack(ordered_values))
        sorted_values_df = pd.concat(ordered_values, axis=1)
        hm2 = sorted_values_df.copy()
        indexSize = hm2.groupby(level=0, axis=1).size()

        # Implemented the HTML method instead
        # indexSize = hm.groupby(level=0).size()


        if self.no_self_ratings_bool:
            hm_copied = hm.copy()
            hm_copied.reset_index(inplace=True)
            for s in hm_copied['section'].unique():
                # print(hm_copied[hm_copied['section']==s]['number'].unique())
                total = (len(hm_copied.columns)-2) * len(hm_copied[hm_copied['section']==s]['number'].unique())
                dif = indexSize.loc[s] - total
                indexSize.loc[s] -= dif
                # for n in hm_copied[hm_copied['section']==s]['number'].unique():
                #     print(hm_copied[(hm_copied['section']==s) & (hm_copied['number']==n)])

        # print('\nhere', indexSize, type(indexSize))
        ######################
        # print(hm.groupby('section').value_counts().reset_index(name='count'))
        # v_counts = hm['section'].value_counts()
        # v_counts = v_counts.to_frame().reset_index()
        # indexSize = v_counts.rename(columns={'index': 'section', 'section': 'length'})
        # indexSize['section'] = indexSize['section'].str.extract('[\s\t]*([IVX]+)\.')
        # # print(indexSize)
        # hm = hm.T
        # # print(hm)
        ########################

        # Add a header format.
        # header_format1 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'bg_color': '#BE6E28', 'font_size': 28, 'center_across': True, 'border': 2})
        # header_format2 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 20, 'center_across': True})
        # Add fill formats
        green_format = self.workbook.add_format({'bg_color': '#008000', 'font_color': '#008000'})
        yellow_format = self.workbook.add_format({'bg_color': '#FFFF00', 'font_color': '#FFFF00'})
        red_format = self.workbook.add_format({'bg_color': '#FF0000', 'font_color': '#FF0000'})
        white_format = self.workbook.add_format({'bg_color': '#FFFFFF', 'font_color': '#FFFFFF'})

        # Conditional formatting for colors
        start_row = 3
        end_row   = start_row + self.number_of_subsections - 1
        start_col = 1
        end_col   = start_col + (len(int_question_numbers_list)*self.number_of_respondents) - 1

        if self.five_point_scale == False:
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
                {'type': 'cell',
                 'criteria': 'between',
                 'minimum': '1',
                 'maximum': '3',
                 'format': red_format})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
                {'type': 'cell',
                 'criteria': 'between',
                 'minimum': '4',
                 'maximum': '7',
                 'format': yellow_format})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
                {'type': 'cell',
                 'criteria': '>',
                 'value': '7',
                 'format': green_format})
        else:
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
             'criteria': 'between',
             'minimum': '1',
             'maximum': '2',
             'format': red_format})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
             'criteria': '==',
             'value': '3',
             'format': yellow_format})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
             'criteria': '>',
             'value': '3',
             'format': green_format})

        worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
             'criteria': '==',
             'value': '0',
             'format': white_format})

        # Write the column headers with correct format.
        header_format1 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'bg_color': '#BE6E28', 'font_size': 28, 'center_across': True, 'border': 2})
        header_format2 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 20, 'center_across': True})
        header_format2_leftBorder = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 20, 'center_across': True, 'left': 2})
        header_format2_rightBorder = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 20, 'center_across': True, 'right': 2})
        header_format2_border = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 20, 'center_across': True, 'right': 2, 'left': 2})

        ###################
        # Ensure correct ordering
        # indexSize['arabic'] = indexSize.apply(lambda x: self.to_arabic(x['section']), axis = 1)
        # indexSize.sort_values('arabic', inplace=True)
        # indexSize.reset_index(drop=True, inplace=True)
        # indexSize.set_index('section', inplace=True)
        ###################

        indexSize = pd.DataFrame({'section':indexSize.index, 'length':indexSize.values})
        # Ensure correct ordering
        indexSize['arabic'] = indexSize.apply(lambda x: self.to_arabic(x['section']), axis = 1)
        indexSize.sort_values('arabic', inplace=True)
        indexSize.reset_index(drop=True, inplace=True)
        indexSize.set_index('section', inplace=True)

        # Y Axis: Number of directors
        directors = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 20, 'center_across': True})
        # worksheet.write_column(3, 0, np.arange(1, self.number_of_respondents+1, 1), directors)

        director_names = self.questions_df[~(self.questions_df['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (~self.questions_df['sub_section'].isin(['', None, np.nan, 'nan']))]['sub_section'].unique()
        worksheet.write_column(3, 0, director_names, directors)

        # Section Headers
        col = 1
        for ind in indexSize.index:
            if indexSize.loc[ind]['length'] > 1:
                worksheet.merge_range(1, col, 1, col+indexSize.loc[ind]['length'] - 1, ind, header_format1)
                col += indexSize.loc[ind]['length']
            elif indexSize.loc[ind]['length'] == 1:
                worksheet.write(1, col, ind, header_format1)
                col += indexSize.loc[ind]['length']

        # Create a dataframe that contains the ending column count for each section.
        indexSizeTotals = indexSize.copy()
        indexSizeTotals.reset_index(inplace=True)
        for i in range(1, len(indexSize)):
            indexSizeTotals.loc[i, 'length'] += indexSizeTotals.loc[i-1, 'length']


        if not self.no_self_ratings_bool:
            worksheet.merge_range(2, 1, 2, self.number_of_respondents, int_question_numbers_list[0][1], header_format2_border)
        else:
            worksheet.merge_range(2, 1, 2, self.number_of_respondents-1, int_question_numbers_list[0][1], header_format2_border)

        # Question Number Headers
        if not self.no_self_ratings_bool:
            col = 1 + self.number_of_respondents
        else:
            col = 1 + self.number_of_respondents-1
        for value in int_question_numbers_list[1:]:
            # print(value)
            # print(col)
            # print(indexSizeTotals.loc[indexSizeTotals['section'] == value[0], 'length'].item()-self.number_of_respondents+1)
            # print(col+self.number_of_subsections)

            # print(indexSizeTotals[indexSizeTotals['section']==value[0]])
            # print('value: ', value[1])
            # print('\n', indexSizeTotals.loc[indexSizeTotals['section'] == value[0], 'length'].item())
            # print(col)
            if not self.no_self_ratings_bool:
                if col == indexSizeTotals.loc[indexSizeTotals['section'] == value[0], 'length'].item()-self.number_of_respondents+1:
                    worksheet.merge_range(2, col, 2, col+self.number_of_respondents-1, value[1], header_format2_border)
                else:
                    worksheet.merge_range(2, col, 2, col+self.number_of_respondents-1, value[1], header_format2_border)
                col += self.number_of_respondents
            else:
                if col == indexSizeTotals.loc[indexSizeTotals['section'] == value[0], 'length'].item()-self.number_of_respondents+1:
                    worksheet.merge_range(2, col, 2, col+self.number_of_respondents-2, value[1], header_format2_border)
                else:
                    worksheet.merge_range(2, col, 2, col+self.number_of_respondents-2, value[1], header_format2_border)
                col += self.number_of_respondents-1

        # Global font
        fmt = self.workbook.add_format({"font_name": "Garamond"})

        # Adjust row and column widths
        worksheet.set_column(1, 550, 3, fmt)
        worksheet.set_column(0, 0, 30, directors)
        worksheet.set_default_row(20.2)
        worksheet.set_row(1, 33, fmt)
        worksheet.set_row(2, 24, fmt)
        worksheet.set_row(0, 3, fmt )
        # section average row height
        worksheet.set_row(hm.shape[0] + 4, 33, fmt)
        worksheet.set_row(self.number_of_respondents+5, 3, fmt)
        worksheet.set_column(int(end_col)+1, int(end_col)+1, 30, fmt)
        # worksheet.set_row(hm.shape[0] + 3, 33, fmt)

        # print('self.number_of_respondents: ', self.number_of_respondents)
        top_thick_border = self.workbook.add_format({'top': 2})
        bottom_thick_border = self.workbook.add_format({'bottom': 2})
        left_thick_border = self.workbook.add_format({'left': 2})
        right_thick_border = self.workbook.add_format({'right': 2})
        thick = self.workbook.add_format({'top':2, 'bottom': 2, 'left': 2, 'right': 2})

        cell = self.workbook.add_format({'border': 1, "font_name": "Garamond"})
        cell_leftBorder = self.workbook.add_format({'left': 2, 'bottom': 1, 'top': 1, 'right': 1})
        cell_rightBorder = self.workbook.add_format({'right': 2, 'top': 1, 'bottom': 1, 'left': 1})

        hm.fillna(0, inplace=True)

        # Write the colored response cells
        row = 3
        col = 1
        # hm.to_excel('test.xlsx')j
        count = 0
        for index, r in hm.iterrows():
            # print(index, r)
            # print(index, row, col, r)
            if not self.no_self_ratings_bool:
                self.write_partial_row(row, col, col+self.number_of_respondents, r.tolist(), worksheet, cell_leftBorder)
            else:
                self.write_partial_row(row, col, col+self.number_of_respondents-1, r.tolist(), worksheet, cell_leftBorder)
            row += 1
            # print('\n', row, col, self.number_of_respondents)
            # print('modulo', self.number_of_respondents % row)
            # print(index,r)
            if (row - 3) % self.number_of_subsections == 0:
                row = 3
                if not self.no_self_ratings_bool:
                    col += self.number_of_respondents
                else:
                    col += self.number_of_respondents-1


        # Format question averages
        # worksheet.merge_range(self.number_of_respondents+3, 1, self.number_of_respondents+3, self.number_of_respondents, int_question_numbers_list[0][1], header_format2_leftBorder)
        col = 1
        header_format3 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 28, 'center_across': True, 'border': 2})
        for value in formatted_question_numbers_list:
            # print(col)
            # print(indexSizeTotals.loc[indexSizeTotals['section'] == value[0], 'length'].item()-self.number_of_respondents+1)
            # print(col+self.number_of_respondents)
            #
            # print(indexSizeTotals[indexSizeTotals['section']==value[0]])
            # print('value: ', value[1])
            # print('\n', indexSizeTotals.loc[indexSizeTotals['section'] == value[0], 'length'].item())
            # print(col)
            if not self.no_self_ratings_bool:
                worksheet.merge_range(self.number_of_subsections+3, col, self.number_of_subsections+3, col+self.number_of_respondents-1, averages[value], header_format3)
                col += self.number_of_respondents
            else:
                worksheet.merge_range(self.number_of_subsections+3, col, self.number_of_subsections+3, col+self.number_of_respondents-2, averages[value], header_format3)
                col += self.number_of_respondents-1

        # Section Headers
        # print(indexSize)
        # print(self.averages)
        col = 1
        for ind,avg in zip(indexSize.index, self.averages.keys()):
            if indexSize.loc[ind]['length'] > 1:
                worksheet.merge_range(self.number_of_subsections+4, col, self.number_of_subsections+4, col+indexSize.loc[ind]['length'] - 1, self.averages[avg], header_format1)
                col += indexSize.loc[ind]['length']
            elif indexSize.loc[ind]['length'] == 1:
                worksheet.write(self.number_of_subsections+4, col, self.averages[avg], header_format1)
                col += indexSize.loc[ind]['length']

        ########################################################################
        # HTML Formatting

        # Anonymized and Named Peer Reports can reuse this block.
        # Named must be run first

        hm = self.survey_object.copy()
        hm = hm[hm['question_type'].isin(['likert', 'matrix'])]

        hm = hm.apply(lambda row: self.format_heat_map(row), axis = 1)
        del hm['sub_section']
        del hm['question_type']
        del hm['question']
        hm.set_index(['section', 'number'], inplace=True)
        hm.fillna(0, inplace=True)

        if not self.anonymize:
            # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
            #     print(hm)
            hm = hm.T
            # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
            # print(hm)
            # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
                # pd.options.display.width = 0
                # print(self.survey_object)

            # Transpose sections individually and stack horrizontally
            ordered_values = []
            col_set = set()
            for col in hm.columns:
                if col not in col_set:
                    col_set.add(col)
                    subframe = hm[col].T

                    if self.no_self_ratings_bool:
                        # print(subframe)
                        listed_subframe = subframe.values.tolist()
                        deleated_diagonal_subframe = [[el for idxx, el in enumerate(ele) if idxx != idx] for idx, ele in enumerate(listed_subframe)]
                        # print(deleated_diagonal_subframe)
                        if self.randomize:
                            for r in deleated_diagonal_subframe:
                                shuffle(r)
                        midx = pd.MultiIndex.from_product([list(set(hm[col].columns.get_level_values(0)))*(self.number_of_respondents-1), list(set(hm[col].columns.get_level_values(1)))])
                        subframe = pd.DataFrame(deleated_diagonal_subframe, columns=midx, index=np.arange(1, len(subframe)+1, 1).tolist())
                    else:
                        midx = pd.MultiIndex.from_product([list(set(hm[col].columns.get_level_values(0)))*(self.number_of_respondents), list(set(hm[col].columns.get_level_values(1)))])
                        if self.randomize:
                            listed_subframe = subframe.values.tolist()
                            for r in listed_subframe:
                                shuffle(r)
                            # midx = pd.MultiIndex.from_product([list(set(hm[col].columns.get_level_values(0)))*(self.number_of_respondents), list(set(hm[col].columns.get_level_values(1)))*(self.number_of_respondents)])
                            subframe = pd.DataFrame(listed_subframe, columns=midx, index=np.arange(1, len(subframe)+1, 1).tolist())
                        else:
                            # midx = pd.MultiIndex.from_product([list(set(hm[col].columns.get_level_values(0)))*(self.number_of_respondents), list(set(hm[col].columns.get_level_values(1)))*(self.number_of_respondents)])
                            subframe.columns = midx
                            subframe.index = np.arange(1, len(subframe)+1, 1).tolist()
                        # print(subframe)
                    ordered_values.append(subframe)


            sorted_values_df = pd.DataFrame(np.hstack(ordered_values))
            sorted_values_df = pd.concat(ordered_values, axis=1)
            hm = sorted_values_df.copy()
            # indexSize = hm.groupby(level=0, axis=1).size()
            # print(sorted_values_df)
            # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
            #     print(sorted_values_df)

            # If the number of respondents is different than the number of subsections,
            # regardless of the value of self.no_self_ratings_bool,
            # then the number of questions in a section should be calculated as in the block below
            ######
            # Old Method
            hm_sections = hm.columns.get_level_values(0)
            # print('hm_sections: ', list(hm_sections), len(hm_sections))
            ######
            # New Method:
            # hm_sections = []
            # for section in self.questions_df[self.questions_df['type'].isin(['likert', 'matrix'])]['section'].unique():
            #     formatted_section = re.search('[\s\t]*([IVX]+)\.', section).group(1)
            #     numbers = self.questions_df[(self.questions_df['type'].isin(['likert', 'matrix'])) & (self.questions_df['section']==section)]['number'].unique().tolist()
            #     if self.no_self_ratings_bool:
            #         number_in_section = len(numbers) * (self.number_of_respondents -1 )
            #     else:
            #         number_in_section = len(numbers) * self.number_of_respondents
            #     section_multiplied = [formatted_section]*number_in_section
            #
            #     hm_sections.extend(section_multiplied)
            # print('hm_sections: ', list(hm_sections), len(hm_sections))


            # Get the number of questions for each section. Counts determine merge ranges.
            hm_section_length_counts = dict()
            if not self.no_self_ratings_bool:
                for i in hm_sections:
                    hm_section_length_counts[i+'.'] = list(hm_sections).count(i)

            # Test this block
            else:
                for section in self.questions_df[self.questions_df['type'].isin(['likert', 'matrix'])]['section'].unique():
                    # print('section: ', section)
                    section_count = 0
                    formatted_section = re.search('[\s\t]*([IVX]+)\.', section).group(1)
                    for number in self.questions_df[(self.questions_df['type'].isin(['likert', 'matrix'])) & (self.questions_df['section']==section)]['number'].unique():
                        section_count+= (len(self.questions_df[(self.questions_df['type'].isin(['likert', 'matrix'])) & (self.questions_df['section']==section) & (self.questions_df['number']==number)]) - 1)
                        # print('section_count: ', section_count)
                    hm_section_length_counts[formatted_section+'.'] = section_count

            # print('\nhm_section_length_counts: ', hm_section_length_counts)

            # Get counts for unique section/number pairs. Counts determine
            # cell merge ranges for numbers in each section
            section_number_merge_ranges = dict()
            section_number_averages = dict()

            # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
            #     print(hm)

            # for section in self.questions_df[self.questions_df['type'].isin(['likert', 'matrix'])]['section'].unique():
            #     formatted_section = re.search('[\s\t]*([IVX]+)\.', section).group(1)
            #     for number in self.questions_df[(self.questions_df['type'].isin(['likert', 'matrix'])) & (self.questions_df['section']==section)]['number'].unique():
            #         raw_number = re.search('(\d+)', number)
            #         if raw_number:
            #             number = raw_number.group(1)
            #
            #         sec_num = (formatted_section, number)
            #         if self.no_self_ratings_bool:
            #             section_number_merge_ranges[sec_num] = (self.number_of_respondents -1 )
            #         else:
            #             section_number_merge_ranges[sec_num] = self.number_of_respondents
            #
            #         section_number_averages[sec_num] = str(Decimal('{}'.format(hm[sec_num].mean().mean())).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            #
            # print('\nsection_number_merge_ranges: ', section_number_merge_ranges)

            # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
            #     print(hm)
            number_count_set = set()
            for sec_num in hm.columns:
                if sec_num not in number_count_set:
                    number_count_set.add(sec_num)
                    # print('sec_num: ', sec_num)

                    # Already removed no_self_ratings_bool from hm, so the column count
                    # is correct in either case
                    section_number_merge_ranges[sec_num] = list(hm.columns).count(sec_num)
                    # if not self.no_self_ratings_bool:
                    #     section_number_merge_ranges[sec_num] = list(hm.columns).count(sec_num)
                    # else:
                    #     # print((list(hm.columns).count(sec_num) - 1))
                    #     section_number_merge_ranges[sec_num] = (list(hm.columns).count(sec_num) - 1)

                    section_number_averages[sec_num] = str(Decimal('{}'.format(hm[sec_num].mean().mean())).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                else:
                    pass


            # print('\nsection_number_merge_ranges: ', section_number_merge_ranges)


            # Create unique iterable keys with values containing:
            # (formatted number, border direction, merge range)
            # Not necessary for peer, because all numbers have a 2px border
            # The following block is for number row and average row
            sec_num_set = set()
            for sec_num, length in section_number_merge_ranges.items():
                # print(sec_num, length)
                if sec_num[0] not in sec_num_set:
                    sec_num_set.add(sec_num[0])
                    section_number_merge_ranges[sec_num] = (sec_num[1], 'left', length)
                    section_number_averages[sec_num] = (section_number_averages.get(sec_num), 'left', length)
                elif sec_num == list(section_number_merge_ranges.keys())[-1]:
                    section_number_merge_ranges[sec_num] = (sec_num[1], 'right', length)
                    section_number_averages[sec_num] = (section_number_averages.get(sec_num), 'right', length)
                else:
                    section_number_merge_ranges[sec_num] = (sec_num[1], 'None', length)
                    section_number_averages[sec_num] = (section_number_averages.get(sec_num), 'None', length)


            # print('\nsection_number_merge_ranges: ', section_number_merge_ranges)
            # print('\nsection_number_averages: ', section_number_averages)


            # Create dictionary with keys containing sections and values containg
            # (merge ranges, average value)
            heat_map_averages = dict()
            for (k, v), (section, avg) in zip(hm_section_length_counts.items(), self.averages.items()):
                heat_map_averages[k] = (v, avg)
            # print('\nheat_map_averages: ', heat_map_averages)


            hm_numbers_borders = dict()
            duplicate_count = 0
            hm_values_borders = []

            # (Section, Number) tuples are duplicated in peer files
            # Create unique iterables
            new_col_list = []
            count=0


            for section in self.questions_df[self.questions_df['type'].isin(['likert', 'matrix'])]['section'].unique():
                formatted_section = re.search('[\s\t]*([IVX]+)\.', section).group(1)
                for number in self.questions_df[(self.questions_df['type'].isin(['likert', 'matrix'])) & (self.questions_df['section']==section)]['number'].unique():
                    raw_number = re.search('(\d+)', number)
                    if raw_number:
                        number = raw_number.group(1)
                    col = (formatted_section, number)
                    if not self.no_self_ratings_bool:
                        for i in range(self.number_of_respondents):
                            numbered_col = list(col)
                            numbered_col.append(count)
                            new_col_list.append(tuple(numbered_col))
                            count+=1
                    else:
                        for i in range(self.number_of_respondents-1):
                            numbered_col = list(col)
                            numbered_col.append(count)
                            new_col_list.append(tuple(numbered_col))
                            count+=1

            # Old method--relied on numer of respondents equalling number of
            # subsections, or in the case of, no_self_ratings_bool, one less
            # see USB for an example of why this code block breaks
            # no_self_ratings_check = set()
            # for col in hm.columns:
            #     if not self.no_self_ratings_bool:
            #         i = list(col)
            #         i.append(count)
            #         new_col_list.append(tuple(i))
            #         count+=1
            #     else:
            #         # skip first instance every time
            #         if col not in no_self_ratings_check:
            #             no_self_ratings_check.add(col)
            #         else:
            #             i = list(col)
            #             i.append(count)
            #             new_col_list.append(tuple(i))
            #             count+=1

            # print('\nnew_col_list: ', new_col_list)

            # the two if/else blocks below could be combined using the syntax in the else block
            section_set = set()
            if not self.no_self_ratings_bool:
                count = 0
                for sec_num, raw_sec_num in zip(new_col_list, list(hm.columns)):
                    # print(sec_num, raw_sec_num)
                    # print(section_set)
                    if raw_sec_num not in section_set:
                        section_set.add(raw_sec_num)
                        hm_numbers_borders[sec_num] = (sec_num[1], 'left', section_number_merge_ranges.get(raw_sec_num))
                        try:
                            hm_values_borders[-1] = 'right'
                        except:
                            pass
                        hm_values_borders.append('left')
                    elif sec_num == list(hm.columns)[-1]:
                        hm_numbers_borders[sec_num] = (sec_num[1], 'right', section_number_merge_ranges.get(raw_sec_num))
                        hm_values_borders.append('right')
                    else:
                        hm_numbers_borders[sec_num] = (sec_num[1], 'None', section_number_merge_ranges.get(raw_sec_num))
                        hm_values_borders.append('None')
                    count+=1
                hm_values_borders[-1] = 'right'
            else:
                count = 0
                for sec_num in new_col_list:
                    if tuple([sec_num[0], sec_num[1]]) not in section_set:
                        section_set.add(tuple([sec_num[0], sec_num[1]]))
                        hm_numbers_borders[sec_num] = (sec_num[1], 'left', section_number_merge_ranges.get(tuple([sec_num[0], sec_num[1]])))
                        try:
                            hm_values_borders[-1] = 'right'
                        except:
                            pass
                        hm_values_borders.append('left')
                    # This elif block does nothing. Same above
                    elif sec_num == list(hm.columns)[-1]:
                        hm_numbers_borders[sec_num] = (sec_num[1], 'right', section_number_merge_ranges.get(tuple([sec_num[0], sec_num[1]])))
                        hm_values_borders.append('right')
                    else:
                        hm_numbers_borders[sec_num] = (sec_num[1], 'None', section_number_merge_ranges.get(tuple([sec_num[0], sec_num[1]])))
                        hm_values_borders.append('None')
                    count+=1
                hm_values_borders[-1] = 'right'

            # print('hm_numbers_borders: ', hm_numbers_borders)

            last_names = [HumanName(name).last for name in director_names]

            if len(set(last_names)) != len(last_names):
                new_names = list()
                for name in director_names:
                    new_names.append('{}. {}'.format(HumanName(name).first[0], HumanName(name).last))
                last_names = new_names

            hm_rows_directions = []
            self.na_in_heat_map = False
            for (index, row), name in zip(sorted_values_df.iterrows(), last_names):
                if 0 in row.tolist():
                    self.na_in_heat_map = True
                hm_rows_directions.append({name: zip(list(row), hm_values_borders)})

            anon_hm_rows_directions = []
            for (index, row), name in zip(sorted_values_df.iterrows(), range(1, len(director_names)+1, 1)):
                anon_hm_rows_directions.append({name: zip(list(row), hm_values_borders)})


            # print(hm_rows_directions)
            # for row in hm_rows_directions:
            #     print(list(row.keys())[0])
            #     for index, tup in row.items():
            #         for num, direction in tup:
            #             print(num, direction)
            # print(hm_rows_directions)
            # print(type(hm_rows_directions[0]))


            self.hm_section_length_counts = hm_section_length_counts
            self.section_number_merge_ranges = section_number_merge_ranges
            self.section_number_averages = section_number_averages
            self.hm_numbers_borders = hm_numbers_borders
            self.heat_map_averages = heat_map_averages


            total_pixels_of_two_px_wide_borders = (len(number_count_set)+1)*2
            total_pixels_of_one_px_wide_borders = (self.number_of_respondents-1)*len(number_count_set)
            # print('self.number_of_respondents: ',self.number_of_respondents)
            # print('len(number_count_set): ', len(number_count_set))
            #
            # print('total_pixels_of_two_px_wide_borders: ', total_pixels_of_two_px_wide_borders)
            # print('total_pixels_of_one_px_wide_borders: ', total_pixels_of_one_px_wide_borders)

            pixel_size = math.floor((540-total_pixels_of_one_px_wide_borders-total_pixels_of_two_px_wide_borders)/(len(hm_numbers_borders)))
            if pixel_size > 24:
                pixel_size = 24
            self.percentage_size = pixel_size
            self.hm_rows_directions = hm_rows_directions


            anon_pixel_size = math.floor(((560-total_pixels_of_one_px_wide_borders)-total_pixels_of_two_px_wide_borders)/(len(hm_numbers_borders)))
            if anon_pixel_size > 24:
                anon_pixel_size = 24

            self.anon_percentage_size = anon_pixel_size
            self.anon_hm_rows_directions = anon_hm_rows_directions

            # print('\n\nPercentage Size: ', pixel_size)
            # print('Anon Percentage Size: ', anon_pixel_size, '\n\n')
            return


    def NonLinCdict(self, steps, hexcol_array):
        import matplotlib.pyplot as plt
        plt.switch_backend('agg')
        cdict = {'red': (), 'green': (), 'blue': ()}
        for s, hexcol in zip(steps, hexcol_array):
            rgb =plt.colors.hex2color(hexcol)
            cdict['red'] = cdict['red'] + ((s, rgb[0], rgb[0]),)
            cdict['green'] = cdict['green'] + ((s, rgb[1], rgb[1]),)
            cdict['blue'] = cdict['blue'] + ((s, rgb[2], rgb[2]),)
        return cdict

    def write_partial_row(self, row, col_start, col_end, values, worksheet, cell_format):
        for c, v in zip(range(col_start, col_end), values):
            worksheet.write(row, c, v, cell_format)

    def format_heat_map(self, row):
        # print(self.report_name, self.report_type, self.survey_object)
        # print(row)
        row['section'] = re.search(r'\b([IVX]+)\.?', row['section']).group(1)
        row['number'] += row['sub_section']
        # remove spaces from likert questions
        match_likert = re.search('^(\d+)\.$', row['number'])
        if match_likert is not None:
            row['number'] = match_likert.group(1)
        # match everything except 'a' so we can format correctly
        match_b_z = re.search('^\d+\.(?![a])([a-z])', row['number'])
        if match_b_z is not None:
            row['number'] = match_b_z.group(1)
        # pair 'a' with the corresponding question number without a period
        match_a = re.search('^(\d+)\.(a)\.', row['number'])
        if match_a is not None:
            row['number'] = match_a.group(1) + match_a.group(2)
        return row
