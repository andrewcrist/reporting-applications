import pandas as pd
import numpy as np
import os
import glob
import re
from random import shuffle
from collections import Counter
import warnings
import xlsxwriter
from functools import reduce
from statistics import mean
import math
import sys
import itertools
import difflib
import logging
import time
logging.basicConfig(level=os.environ.get("WARNING", "INFO"))

pd.options.mode.chained_assignment = None
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
np.seterr(all='raise')

class fsr_report:
    def __init__(self, p_df, m_df, ppl_df, questions_df, plain_dict_of_dfs, mod_dict_of_dfs, print_headers, app, modified_filename, create_modified_instance):
        self.p_df = p_df
        self.m_df = m_df
        self.ppl_df = ppl_df
        self.questions_df = questions_df
        self.plain_dict_of_dfs = plain_dict_of_dfs
        self.mod_dict_of_dfs = mod_dict_of_dfs
        self.print_headers = print_headers
        self.write_titles = True
        self.ppl_ids_set = set()
        self.app = app
        self.produce_mod = bool(create_modified_instance)

        re_fileName = re.compile('(.*?)(_|plain|modified|final)+.*?\.csv', re.IGNORECASE)
        output_name = re_fileName.match(modified_filename).group(1)
        self.output_name = output_name
        self.output_file_names = ['{}_Final Summary Report.xlsx'.format(output_name)]

        self.workbook = xlsxwriter.Workbook(os.path.join(app.config['EXCEL_OUTPUT'], '{}_Final Summary Report.xlsx'.format(output_name)), {'nan_inf_to_errors': True})
        self.output_filename = '{}_Final Summary Report.xlsx'.format(output_name)
        if self.produce_mod:
            self.output_file_names.append('{}_Final Summary Report_MODIFIED.xlsx'.format(self.output_name))
            self.mod_workbook = xlsxwriter.Workbook(os.path.join(app.config['EXCEL_OUTPUT'], '{}_Final Summary Report_MODIFIED.xlsx'.format(self.output_name)), {'nan_inf_to_errors': True})
        self.fsr_handler()

    def write_df(self, df, sheetName):
        worksheet = self.workbook.add_worksheet(sheetName)
        df.fillna('', inplace=True)
        colNum = 1
        worksheet.write_row(0, 1, df.columns)
        worksheet.write_column(1, 0, df.index)

        for col in df.columns:
            worksheet.write_column(1, colNum, df[col])
            colNum += 1

    def write_dfs_in_dict(self, d1, d2, sheetName):
        worksheet = self.workbook.add_worksheet(sheetName)
        bold_fmt = self.workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'text_wrap': True})
        fmt = self.workbook.add_format({"font_name": "Times New Roman", 'text_wrap': True})

        # Ensure that pairwise keys in each dictionary exist. This loop ensures
        # that each dictionary is written in the same order, even when an item
        # does not occur in one or the other.
        for ppl_id in self.m_df.index:
            for q_id in self.questions_df.index:
                if (d1.get(q_id) is not None) or (d2.get(q_id) is not None):
                    plain_dict_item = d1.get(q_id)
                    plain_subframe= plain_dict_item.get(ppl_id)
                    mod_dict_item = d2.get(q_id)
                    mod_subframe = mod_dict_item.get(ppl_id)
                    if plain_subframe is None and mod_subframe is None:
                        continue
                    # Fill out the plain_subframe if it's None
                    elif plain_subframe is None and mod_subframe is not None:
                        plain_subframe = pd.DataFrame(index=mod_subframe.index, columns=mod_subframe.columns)
                        # plain_subframe.fillna('', inplace=True)
                        d1[q_id][ppl_id] = plain_subframe
                    # Fill out the mod_subframe if it is None
                    elif plain_subframe is not None and mod_subframe is None:
                        mod_subframe = pd.DataFrame(index=plain_subframe.index, columns=mod_subframe.columns)
                        # mod_subframe.fillna('', inplace=True)
                        d2[q_id][ppl_id] = mod_subframe

        row = 1
        count = 1
        worksheet.write(0, 0, 'PLAIN', bold_fmt)
        for q_id in self.questions_df.index:
            for person in self.m_df.index:
                if d1.get(q_id) is not None or d2.get(q_id) is not None:
                    if d1[q_id][person].empty:
                        row += len(d2[q_id][person])
                        continue
                    else:
                        worksheet.write(row, 0, q_id, bold_fmt)
                        worksheet.write(row, 1, self.questions_df.loc[q_id]['question_raw'], bold_fmt)
                        worksheet.write(row, 2, person, bold_fmt)
                        row+=1
                        worksheet.write(row, 1, 'question_id', bold_fmt)
                        worksheet.write(row, 2, 'question_value', bold_fmt)
                        worksheet.write(row, 3, 'answer_value', bold_fmt)
                        row+= 1

                        worksheet.write_column(row, 1, d1[q_id][person].index, fmt)
                        colNum = 2
                        for col in d1[q_id][person].columns:
                            worksheet.write_column(row, colNum, d1[q_id][person][col], fmt)
                            colNum += 1
                        row += len(d1[q_id][person]) + 1

                        if d2[q_id].get(person) is not None:
                            if len(d2[q_id][person]) > len(d1[q_id][person]):
                                
                                rowSkipCount = len(d2[q_id][person]) - len(d1[q_id][person])
                                
                                row += rowSkipCount
                                worksheet.write(row, 0, 'here', fmt)
                            count += 1
                        # break
                    # break

        row = 1
        count = 1
        worksheet.write(0, 4, 'MODIFIED', bold_fmt)
        for q_id in self.questions_df.index:
            for person in self.m_df.index:
                if d1.get(q_id) is not None or d2.get(q_id) is not None:
                    if d2[q_id][person].empty:
                        row += len(d1[q_id][person])
                        continue
                    else:
                        colNum = 6

                        worksheet.write(row, 4, q_id, bold_fmt)
                        worksheet.write(row, 5, self.questions_df.loc[q_id]['question_raw'], bold_fmt)
                        worksheet.write(row, 6, person, bold_fmt)
                        row+=1
                        worksheet.write(row, 5, 'question_id', bold_fmt)
                        worksheet.write(row, 6, 'question_value', bold_fmt)
                        worksheet.write(row, 7, 'answer_value', bold_fmt)
                        row+= 1
                        # worksheet.write_row(row, 1, d1[q_id][i].columns)
                        # row+=1
                        worksheet.write_column(row, 5, d2[q_id][person].index, fmt)

                        for col in d2[q_id][person].columns:
                            worksheet.write_column(row, colNum, d2[q_id][person][col], fmt)
                            colNum += 1
                        row += len(d2[q_id][person]) + 1
                        if d1[q_id].get(person) is not None:
                            if len(d1[q_id][person]) > len(d2[q_id][person]):
                                # print('\nplain is bigger')
                                # print(len(d1[q_id][i]), len(d2[q_id][i]))
                                rowSkipCount = len(d1[q_id][person]) - len(d2[q_id][person])
                                # print(rowSkipCount)
                                row += rowSkipCount
                            count += 1
                        # break
                    # break


        for c in [0, 2, 3, 4, 6, 7, 8]:
            worksheet.set_column(c, c, 35)
        for c in [1, 5]:
            worksheet.set_column(c, c, 70)

    def align_groups_of_matrix_questions(self, plain_subframe, mod_subframe):
        #######################
        # Old
        # print('\n----------\nOriginals:\n',plain_subframe)
        # print(mod_subframe)

        # outer = pd.merge(mod_subframe, plain_subframe, on=['question', 'value'], how='outer').reset_index()
        # print(outer)
        # new_df_index = new_df.sort_values(by=['group_y', 'index'], na_position='last').index.tolist()
        # print(new_df_index)
        # print(plain_subframe)
        # plain_subframe = plain_subframe.reindex(new_df_index)
        # print(plain_subframe)
        # plain_subframe.reset_index(inplace=True, drop=True)
        # plain_subframe.fillna('', inplace=True)
        # print(plain_subframe)
        # print(mod_subframe)

        ##########################################
        # Working version

        mod_subframe.reset_index(inplace=True,drop=False)
        mod_subframe = mod_subframe.rename(columns={'index': 'mod_index', 'group': 'mod_group'})
        plain_subframe.reset_index(inplace=True,drop=False)
        plain_subframe = plain_subframe.rename(columns={'index': 'plain_index', 'group': 'plain_group'})
        inner = pd.merge(mod_subframe, plain_subframe, on=['question', 'value'], how='inner')
        # print(inner)
        # print(max(inner.groupby('group_x').agg({'question': 'nunique'})['question']))
        clean_inner = pd.DataFrame()
        partial_inner = pd.DataFrame()
        for g in inner['mod_group'].unique():
            if len(inner[inner['mod_group']==g]) == max(inner.groupby('mod_group').agg({'question': 'nunique'})['question']):
                clean_inner=clean_inner.append(inner[inner['mod_group']==g])
            else:
                partial_inner=partial_inner.append(inner[inner['mod_group']==g])
        if clean_inner.empty == False:
            clean_inner = clean_inner[['question', 'value', 'mod_index']]
            clean_inner.set_index('mod_index', inplace=True)
        else:
            return plain_subframe

        
        return final

    def compare_indices(self, plain_subframe, mod_subframe):

        if len(plain_subframe) != len(mod_subframe):
            maxLen = max(len(plain_subframe), len(mod_subframe))
            if len(list(plain_subframe.index)) > len(list(mod_subframe.index)):
                mod_filled_index = list(mod_subframe.index) + [None]*(maxLen - len(list(mod_subframe.index)))
                plain_filled_index = list(plain_subframe.index)
            else:
                plain_filled_index = list(plain_subframe.index) + [None]*(maxLen - len(list(plain_subframe.index)))
                mod_filled_index = list(mod_subframe.index)
        else:
            mod_filled_index = list(mod_subframe.index)
            plain_filled_index = list(plain_subframe.index)
        # print(plain_filled_index)
        # print(mod_filled_index, '\n')
        return plain_filled_index, mod_filled_index

    def write_names_and_titles(self, worksheet, workbook):
        # Note, center_across: True, does not format within the bounds of one cell!
        # Instead, it formats the text of the cell to take up as many cells across the width of the workbook as possible!
        # This behavior is dependant on adjacent cells also having the same format
        # ... bizarre

        # bold_cntrgrey = workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'center_across': True, 'border': 1, 'top': 2, 'font_size': 12, 'text_wrap': True})
        # bold_cntrgrey.set_align('vcenter')
        bold_cntrgrey = workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'border': 1, 'top': 2, 'font_size': 12, 'text_wrap': True})
        bold_cntrgrey.set_align('vcenter')
        bold_cntrgrey.set_align('center')
        # bold_cntrgrey_titles = workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'center_across': True, 'border': 1, 'font_size': 12, 'text_wrap': True})
        # bold_cntrgrey_titles.set_align('vcenter')
        bold_cntrgrey_titles = workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'border': 1, 'font_size': 12, 'text_wrap': True})
        bold_cntrgrey_titles.set_align('vcenter')
        bold_cntrgrey_titles.set_align('center')
        right_fmt_bold_cntrgrey = workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'border': 1, 'font_size': 12, 'top': 2, 'right': 2, 'text_wrap': True})
        right_fmt_bold_cntrgrey.set_align('vcenter')
        right_fmt_bold_cntrgrey.set_align('center')
        # right_fmt_bold_cntrgrey_titles = workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'center_across': True, 'border': 1, 'font_size': 12, 'right': 2, 'text_wrap': True})
        # right_fmt_bold_cntrgrey_titles.set_align('vcenter')
        right_fmt_bold_cntrgrey_titles = workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'border': 1, 'font_size': 12, 'right': 2, 'text_wrap': True})
        right_fmt_bold_cntrgrey_titles.set_align('vcenter')
        right_fmt_bold_cntrgrey_titles.set_align('center')
        drk_gry_bar_hdr = workbook.add_format({'bg_color': '#808080', 'right': 2, 'text_wrap': True})
        # test = workbook.add_format({"font_name": "Times New Roman", 'border':1, 'text_wrap': True, 'bold': 1, 'bg_color': '#e7e6e6', 'font_size': 12})
        # test.set_align('vcenter')
        # test.set_align('center')
        # print(self.ppl_df)
        # for i in self.m_df.index.tolist():
        #     try:
        #         print(self.ppl_df.at[i, 'FirstName'])
        #     except:
        #         print('here')
        #         pass

        if all(self.ppl_df.index==self.m_df.index) == False:
            logging.warning('The indices in the modified and plain files are mismatched.')
            # Output = [(x, y) for x, y in zip(self.ppl_df.index.tolist(), self.m_df.index.tolist()) if y != x]
            for x, y in zip(self.ppl_df.index.tolist(), self.m_df.index.tolist()):
                if x not in self.m_df.index.tolist():
                    pass
                    # print('\nPlain value: {}\nNot found in modified file'.format(x))
                if y not in self.ppl_df.index.tolist():
                    pass
                    # print('\nModified value: {}\nNot found in plain file'.format(y))
            # for i in Output:
            #     print('mismatched values\nPlain:     {}\nModified:  {}'.format(i[0],i[1]))
            # print('')

        # write names
        colNum = 1
        for i in self.m_df.index.tolist():
            if self.ppl_df.at[i, 'MiddleName'] is not None and self.ppl_df.at[i, 'MiddleName'] != '':
                name = self.ppl_df.at[i, 'FirstName'].strip() + ' ' + self.ppl_df.at[i, 'MiddleName'].strip()  + ' ' + self.ppl_df.at[i, 'LastName'].strip()
            else:
                name = self.ppl_df.at[i, 'FirstName'].strip() + ' ' + self.ppl_df.at[i, 'LastName'].strip()
            # print(name)
            if i == self.m_df.index.tolist()[-1]:
                # print(MiddleName)
                worksheet.write(0, colNum, name, right_fmt_bold_cntrgrey)
            else:
                # print(self.ppl_df.at[i, 'FirstName'])
                # print(self.ppl_df.at[i, 'MiddleName'])
                worksheet.write(0, colNum, name, bold_cntrgrey)
            colNum += 1

        # print(self.ppl_df)

        # write titles
        if 'Title' in self.ppl_df.columns:
            colNum = 1
            for i in self.m_df.index.tolist():
                value = self.ppl_df.loc[i]['Title']
                if i == self.m_df.index.tolist()[-1]:
                    worksheet.write(1, colNum, value, right_fmt_bold_cntrgrey_titles)
                else:
                    worksheet.write(1, colNum, value, bold_cntrgrey_titles)
                colNum += 1
        else:
            self.write_titles = False
        if self.write_titles == True:
            worksheet.merge_range(2, 1, 2, len(self.m_df.index.tolist()), ' ', drk_gry_bar_hdr)
        else:
            worksheet.merge_range(1, 1, 1, len(self.m_df.index.tolist()), ' ', drk_gry_bar_hdr)
        return

    def write_row_index(self, worksheet, workbook):
        indexfmt = workbook.add_format({"font_name": "Times New Roman", 'bg_color': '#fce4d6', 'border': 1, 'text_wrap': True, 'font_size': 12})
        indexfmt.set_align('left')
        indexfmt.set_align('top')
        indexfmt_right_text = workbook.add_format({"font_name": "Times New Roman", 'bg_color': '#fce4d6', 'border': 1, 'text_wrap': True, 'font_size': 12})
        indexfmt_right_text.set_align('right')
        salmon_bar_hdr = workbook.add_format({'align': 'center', "font_name": "Times New Roman", 'bold': 1, 'bg_color': '#fce4d6', 'right': 2, 'font_size': 14, 'top': 1, 'bottom': 1, 'left': 1, 'right': 2, 'text_wrap': True})
        salmon_bar_hdr.set_align('vcenter')

        # The default row number is 3. But if there are no title or if print_headers is false, then bump the row number back.
        # If there are headers without header symbology, modify the header symbol column so the first if condition below is satisfied
        row = 3
        # if self.print_headers == False and self.questions_df.at[0, 'header_symbol'] is not None:
        #     print('here')
        #     row -= 1
        if self.write_titles == False:
            # print('also here')
            row -= 1
        # print('row value: ', row)
        for question_id in self.questions_df.index:
            # Debugging
            # if question_id == 0:
                # print(bool(self.questions_df.loc[question_id]['header_symbol'] is not None and self.print_headers == True))

            # This line will not produce headers for sections without header symbols.
            # Input files will need to denote sections without symbols using the header_symbol column
            if self.questions_df.loc[question_id]['header_symbol'] is not None and self.print_headers == True:
                # print(self.questions_df.loc[question_id])
                # worksheet.write(row, 0, self.questions_df.loc[question_id]['header_symbol'], )

                worksheet.merge_range(row, 1, row, len(self.ppl_df['Capacity']), self.questions_df.loc[question_id]['header'], salmon_bar_hdr)
                row+=1
            elif self.questions_df.loc[question_id]['header_symbol'] == 'cert' and self.print_headers == False:
                worksheet.merge_range(row, 1, row, len(self.ppl_df['Capacity']), self.questions_df.loc[question_id]['header'], salmon_bar_hdr)
                row+=1
            # Formatted row index values should not include values that have been inserted as placeholders
            writeFlag = True

            # print(self.questions_df.loc[question_id]['question_raw'])
            if re.search('^blank_column_\d+$', self.questions_df.loc[question_id]['question_raw']) is not None:
                # print(self.questions_df.loc[question_id]['question_raw'])
                worksheet.write(row, 0, '', indexfmt)
                row+=1
                if self.mod_dict_of_dfs.get(question_id) is None and self.plain_dict_of_dfs.get(question_id) is None:
                    continue
                else:
                    writeFlag = False
                    pass
            # Handle Repeated Questions by removing the duplicate number
            elif re.search('^.*__\d+$', self.questions_df.loc[question_id]['question_raw']) is not None:
                frmttd_row = re.sub('__\d+$', '', self.questions_df.loc[question_id]['question_raw'])
                # print(self.questions_df.loc[question_id]['question_raw'])
                worksheet.write(row, 0, frmttd_row, indexfmt)
                row+=1
                if self.mod_dict_of_dfs.get(question_id) is None and self.plain_dict_of_dfs.get(question_id) is None:
                    continue
                else:
                    writeFlag = False
                    pass

            if self.mod_dict_of_dfs.get(question_id) is not None or self.plain_dict_of_dfs.get(question_id) is not None:
                # print(self.mod_dict_of_dfs.get(question_id).get('meta_data'))
                # print('here', self.questions_df.loc(question_id))
                try:
                    if self.mod_dict_of_dfs.get(question_id).get('meta_data')[0] == 'boundMatrix':
                        pass
                        # print('\n\n-----------\nBound Type Matrix on row {}.\nResponse Data should always have a space after a semicolon'.format(row+1))
                    elif self.plain_dict_of_dfs.get(question_id).get('meta_data')[0] == 'boundMatrix':
                        pass
                        # print('\n\n-----------\nBound Type Matrix on row {}.\nResponse Data should always have a space after a semicolon'.format(row+1))
                except:
                    pass

                if writeFlag == True:
                    worksheet.write(row, 0, self.questions_df.loc[question_id]['question_raw'], indexfmt)
                    row+=1

                # Determine the length of the longest matrix question
                if self.mod_dict_of_dfs.get(question_id) is not None:
                    m_max = 0
                    key = self.mod_dict_of_dfs.get(question_id)
                    for respondent_id in self.mod_dict_of_dfs[question_id].keys():
                        if isinstance(self.mod_dict_of_dfs[question_id][respondent_id], pd.DataFrame):
                            if not self.mod_dict_of_dfs[question_id][respondent_id].empty:
                                if len(self.mod_dict_of_dfs[question_id][respondent_id]) > m_max:
                                    m_biggest_df = self.mod_dict_of_dfs[question_id][respondent_id]
                                    m_max = len(m_biggest_df)
                else:
                    m_biggest_df = None
                    m_max = None

                if self.plain_dict_of_dfs.get(question_id) is not None:
                    p_max = 0
                    key = self.plain_dict_of_dfs.get(question_id)
                    for respondent_id in self.plain_dict_of_dfs[question_id].keys():
                        if isinstance(self.plain_dict_of_dfs[question_id][respondent_id], pd.DataFrame):
                            if not self.plain_dict_of_dfs[question_id][respondent_id].empty:
                                if len(self.plain_dict_of_dfs[question_id][respondent_id]) > p_max:
                                    p_biggest_df = self.plain_dict_of_dfs[question_id][respondent_id]
                                    p_max = len(p_biggest_df)
                else:
                    p_biggest_df = None
                    p_max = None

                if p_max is not None and m_max is not None:
                    if p_max > m_max:
                        for q in p_biggest_df['question'].tolist():
                            worksheet.write(row, 0, q, indexfmt_right_text)
                            row+=1
                    else:
                        for q in m_biggest_df['question'].tolist():
                            worksheet.write(row, 0, q, indexfmt_right_text)
                            row+=1
                elif p_max is None and m_max is not None:
                    for q in m_biggest_df['question'].tolist():
                        worksheet.write(row, 0, q, indexfmt_right_text)
                        row+=1
                elif m_max is None and p_max is not None:
                    for q in p_biggest_df['question'].tolist():
                        worksheet.write(row, 0, q, indexfmt_right_text)
                        row+=1
                else:
                    logging.warning('This condition should never be satisfied')
            # If not a matrix question
            else:
                # print(question_id)
                worksheet.write(row, 0, self.questions_df.loc[question_id]['question'], indexfmt)
                row+=1

    def get_opcodes(self, a, b):
        s = difflib.SequenceMatcher(None, a, b)
        arg_list = []
        for tag, i1, i2, j1, j2 in s.get_opcodes():
            # print('{:7}   a[{}:{}] --> b[{}:{}] {!r:>8} --> {!r}'.format(tag, i1, i2, j1, j2, a[i1:i2], b[j1:j2]))
            arg_list.append((tag, i1, i2, j1, j2, a[i1:i2], b[j1:j2]))
        return arg_list

    def get_diff_with_color_2(self, ppl_id, q_id, plain_value = '', mod_value = ''):
        fmt = self.workbook.add_format({"font_name": "Times New Roman", 'font_size': 12, 'border': 1, 'text_wrap': True})
        add = self.workbook.add_format({"font_name": "Times New Roman", 'font_size': 12, 'border': 1, 'underline': True, 'font_color': '#0000ff', 'text_wrap': True})
        sub = self.workbook.add_format({"font_name": "Times New Roman", 'font_size': 12, 'border': 1, 'font_color': '#ff0000', 'font_strikeout': 1, 'text_wrap': True})
        topleft = self.workbook.add_format({'align': 'left', 'valign': 'top', 'border': 1, 'text_wrap': True})
        fmt.set_align('left')
        fmt.set_valign('top')
        add.set_align('left')
        add.set_valign('top')
        sub.set_align('left')
        sub.set_valign('top')

        # plain_comp = plain_value.lower()
        # mod_comp = mod_value.lower()
        # self.ppl_ids_set.add(ppl_id)
        # if 'Victor' in ppl_id:

        # Highlight yellow debug
        # fmt = self.workbook.add_format({'border': 1, 'bg_color': '#ffff00'})
        # add = self.workbook.add_format({'border': 1, 'bg_color': '#ffff00'})
        # sub = self.workbook.add_format({'border': 1, 'bg_color': '#ffff00'})
            # print(mod_value)

        confirmationStatement = re.compile('[\s\t]*i[\s\t]+confirm[\s\t]+that[\s\t]+the', re.IGNORECASE)
        if confirmationStatement.match(mod_value):
            return [add, mod_value]

        # plain_value = '''1. Beautiful is better than ugly. 2. Explicit is better than implicit. 3. Simple is better than complex. 4. Complex is better than complicated.'''
        # mod_value = '''1. Beautiful is better than ugly. 3. Simple is better than complex. 4. Complicated is better than complex. 5. Flat is better than nested.'''
        # d= difflib.Differ()
        # result = list(d.compare(text1, text2))
        # result = list(d.compare(plain_value, mod_value))
        # from pprint import pprint
        # pprint(result)
        # print(i in difflib.context_diff(plain_value, mod_value):
        try:
            if plain_value == '' and mod_value == '':
                return [fmt, '']
            elif plain_value == '' and mod_value != '':
                return [add, mod_value]
            elif plain_value != '' and mod_value == '':
                return [sub, plain_value]
            else:
                list_of_codes = self.get_opcodes(plain_value.split(), mod_value.split())
        except:
            pass
            # print('plain_value: ',plain_value)
            # print('mod_value: ', mod_value)

        list_of_fmts = []
        First = True
        for i in list_of_codes:
            # print(i)
            tag, i1, i2, j1, j2, before, after = i[0], i[1], i[2], i[3], i[4], i[5], i[6]
            # if ppl_id == 'John Ferron - ServiceSource' and q_id == 2:
                # print(tag, i1, i2, j1, j2, before, after)
            if First:
                if tag == 'equal':
                    list_of_fmts.extend([fmt, ' '.join(map(str, before))])
                elif tag == 'delete':
                    list_of_fmts.extend([sub, ' '.join(map(str, before))])
                elif tag == 'replace':
                    list_of_fmts.extend([sub, ' '.join(map(str, before))])
                    list_of_fmts.extend([fmt, ' '])
                    list_of_fmts.extend([add, ' '.join(map(str, after))])
                elif tag == 'insert':
                    list_of_fmts.extend([add, ' '.join(map(str, after))])
                else:
                    logging.warning('Warning! All op_code tags should be equal, delete, insert, or replace. Not:')
                    # print(tag, len(tag), type(tag))
                First = False
                # list_of_fmts[-1] = ' ' + list_of_fmts[-1]
            else:
                if tag == 'equal':
                    # list_of_fmts[-1] = ' ' + list_of_fmts[-1]
                    list_of_fmts.extend([fmt, ' '])
                    list_of_fmts.extend([fmt, ' '.join(map(str, before))])
                elif tag == 'delete':
                    # list_of_fmts[-1] = ' ' + list_of_fmts[-1]
                    list_of_fmts.extend([fmt, ' '])
                    list_of_fmts.extend([sub, ' '.join(map(str, before))])
                elif tag == 'replace':
                    # list_of_fmts[-1] = ' ' + list_of_fmts[-1]
                    list_of_fmts.extend([fmt, ' '])
                    list_of_fmts.extend([sub, ' '.join(map(str, before))])
                    # list_of_fmts[-1] = ' ' + list_of_fmts[-1]
                    list_of_fmts.extend([fmt, ' '])
                    list_of_fmts.extend([add, ' '.join(map(str, after))])
                elif tag == 'insert':
                    # list_of_fmts[-1] = ' ' + list_of_fmts[-1]
                    list_of_fmts.extend([fmt, ' '])
                    list_of_fmts.extend([add, ' '.join(map(str, after))])
                else:
                    logging.warning('Warning! All op_code tags should be equal, delete, insert, or replace. Not:')
                    # print(tag, len(tag), type(tag))
        if len(list_of_fmts) > 2:
            list_of_fmts.extend([topleft])
        # for i in list(difflib.Differ().compare(plain_value, mod_value)):
        #     list_of_tups.append((i[0], i[2]))
        # if set([i[0] for i in list_of_tups]) == set(' '):
        #     return [fmt, plain_value]
        #
        # list_of_fmts = []
        #
        # last_sign = ''
        # section = ''
        # add_section = ''
        # sub_section = ''
        # word = ''
        #
        # group = ''
        # index_nbr = 0
        # wordbreak = re.compile('[\s|\t|\n|^]?')
        # sign = list_of_tups[0][0]
        # while index_nbr < len(list_of_tups):
        #     if list_of_tups[index_nbr][0] != sign:
        #         while not wordbreak.match(list_of_tups[index_nbr - 1][1]):
        #             group = group[:-1]
        #             index_nbr -= 1
        #         # Add the trimmed group to list_of_fmts
        #         if sign == '+':
        #             list_of_fmts.extend([add, group])
        #             group = ''
        #         elif sign == '-':
        #             list_of_fmts.extend([sub, group])
        #             group = ''
        #         elif sign == ' ':
        #             list_of_fmts.extend([fmt, group])
        #             group = ''
        #         else:
        #             logging.warning('? sign encountered')
        #         sign = list_of_tups[index_nbr][0]
        #         # print(list_of_tups[:index_nbr])
        #         # while not wordbreak.match(list_of_tups[index_nbr][1]) or list_of_tups[index_nbr][0] == sign:
        #             # group += list_of_tups[index_nbr][1]
        #             # index_nbr += 1
        #     else:
        #         group += list_of_tups[index_nbr][1]
        #         index_nbr += 1
            # index_nbr += len(group)
        # for sign_and_string, index_nbr in zip(list_of_tups, range(0, len(list_of_tups))):
        #     sign = sign_and_string[0]
        #     string = sign_and_string[1]

            # if last_sign == '':
            #     last_sign = sign
            #     section += string
            #     # print(sign, string)
            # elif sign == last_sign:
            #     section += string
            #     # print(sign, string)
            #     count+=1
                # print(count)
        #     # else:
        #     #     print(last_sign, sign, string)
        #     #     print('turn index', list_of_tups.index((sign, string)))
        #     #     count += 1
        #     #     print(count
            # else:
            #     if last_sign == '+':
            #         list_of_fmts.extend([add, section])
            #         section = ''
            #     elif last_sign == '-':
            #         list_of_fmts.extend([sub, section])
            #         section = ''
            #     elif last_sign == ' ':
            #         list_of_fmts.extend([fmt, section])
            #         section = ''
            #     else:
            #         logging.warning('? sign encountered')
            #
            #     last_sign = sign
            #     section += string
        # if (len(list_of_fmts) == 0) or (section != ''):
        #     if last_sign == '+':
        #         list_of_fmts.extend([add, section])
        #     elif last_sign == '-':
        #         list_of_fmts.extend([sub, section])
        #     elif last_sign == ' ':
        #         list_of_fmts.extend([fmt, section])
        #     else:
        #         logging.warning('? sign encountered')
        # print(list_of_fmts)
        return list_of_fmts

    def write_data(self, worksheet, workbook, modified = False, plain = False):
        fmt = workbook.add_format({"font_name": "Times New Roman", 'font_size': 12, 'border': 1, 'text_wrap': True})
        last_col_fmt = workbook.add_format({"font_name": "Times New Roman", 'font_size': 12, 'top': 1, 'bottom': 1, 'left': 1, 'right': 2, 'text_wrap': True})
        bold_fmt = workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'text_wrap': True})
        blank_border = workbook.add_format({"font_name": "Times New Roman", 'border': 1, 'text_wrap': True})
        fmt.set_align('left')
        fmt.set_valign('top')
        # Pink Debug
        # blank_border = workbook.add_format({"font_name": "Times New Roman", 'border': 1, 'text_wrap': True, 'bg_color': '#ff69b4'})
        # print(self.questions_df)
        col = 1
        # print(self.plain_dict_of_dfs.keys())
        # print(self.mod_dict_of_dfs.keys())
        # print(self.questions_df)
        for ppl_id in self.m_df.index:
        # for ppl_id in self.m_df.index.tolist()[:1]:
            row = 3
            # if self.print_headers == False and self.questions_df.at[0, 'header_symbol'] is not None:
            #     row -= 1
            if self.write_titles == False:
                row -= 1
            for q_id in self.questions_df.index:
                # print('\n', ppl_id)
                # print(self.questions_df.loc[q_id]['question'])
                # print(ppl_df.index)
                # Multi Column and Row writer
                if (self.plain_dict_of_dfs.get(q_id) is not None) or (self.mod_dict_of_dfs.get(q_id) is not None):
                    # Get number of rows to skip after writing. Here, get the max number of rows for this question
                    # if ppl_id == 'DOQ - James Hearty':
                    #     print(q_id)
                    #     print(self.plain_dict_of_dfs.get(q_id).get(ppl_id))
                    #     print(self.mod_dict_of_dfs.get(q_id).get(ppl_id))
                    max_df_len = 0
                    if self.plain_dict_of_dfs.get(q_id) is not None:
                        for respondent_id in self.plain_dict_of_dfs[q_id].keys():
                            if self.plain_dict_of_dfs.get(q_id).get(respondent_id) is not None:
                                if isinstance(self.plain_dict_of_dfs[q_id][respondent_id], pd.DataFrame):
                                    if len(self.plain_dict_of_dfs[q_id][respondent_id]) > max_df_len:
                                        max_df_len = len(self.plain_dict_of_dfs[q_id][respondent_id])
                    if self.mod_dict_of_dfs.get(q_id) is not None:
                        for respondent_id in self.mod_dict_of_dfs[q_id].keys():
                            if self.mod_dict_of_dfs.get(q_id).get(respondent_id) is not None:
                                if isinstance(self.mod_dict_of_dfs[q_id][respondent_id], pd.DataFrame):
                                    if len(self.mod_dict_of_dfs[q_id][respondent_id]) > max_df_len:
                                        max_df_len = len(self.mod_dict_of_dfs[q_id][respondent_id])

                    # Automatically skip a row, and if the row begins a section, skip another row
                    if self.questions_df.loc[q_id]['header_symbol'] is not None and self.print_headers == True:
                        row += 1
                    elif self.questions_df.loc[q_id]['header_symbol'] =='cert' and self.print_headers == False:
                        row+=1
                    worksheet.write(row, col, '', blank_border)
                    row += 1

                    plain_dict_item = self.plain_dict_of_dfs.get(q_id)
                    if plain_dict_item is not None:
                        plain_subframe= plain_dict_item.get(ppl_id)
                    else:
                        plain_subframe = None

                    mod_dict_item = self.mod_dict_of_dfs.get(q_id)
                    if mod_dict_item is not None:
                        mod_subframe= mod_dict_item.get(ppl_id)
                    else:
                        mod_subframe = None
                    mod_subframe = mod_dict_item.get(ppl_id)
                    # print(plain_subframe, '\n', mod_subframe, '\n---------')

                    # If, no answer is given to any part of the row/column questions,
                    # then both subframes will be none
                    # print(ppl_id)
                    # print(row, max_df_len)
                    lastRow = row + max_df_len
                    if plain_subframe is None and mod_subframe is None:
                        # print('Both frames are None for: ', ppl_id, q_id)
                        while row < lastRow:
                            worksheet.write(row, col, '', blank_border)
                            row += 1
                        continue

                    elif plain_subframe is None and mod_subframe is not None:
                        # print('Plain Frame is None for: ', ppl_id, q_id)

                        # Fill out the plain_subframe if it's None
                        plain_subframe = pd.DataFrame(index=mod_subframe.index, columns=mod_subframe.columns)
                        plain_subframe.fillna('', inplace=True)

                        rowSkipCount = max_df_len - len(mod_subframe)

                        # Write row/column header questions
                        for p_index, m_index in zip(plain_subframe.index, mod_subframe.index):
                            p_value = plain_subframe.at[p_index, 'value']
                            m_value = mod_subframe.at[m_index, 'value']
                            # print('pvalue', p_value, len(p_value), type(p_value))
                            # print('mvalue', m_value, len(m_value), type(m_value))
                            list_of_fmts_and_values = self.get_diff_with_color_2(ppl_id, q_id, plain_value = p_value, mod_value = m_value)
                            if len(list_of_fmts_and_values)==0:
                                worksheet.write(row, col, '', blank_border)
                                row += 1
                            elif len(list_of_fmts_and_values) == 2:
                                worksheet.write(row, col, list_of_fmts_and_values[1], list_of_fmts_and_values[0])
                                row += 1
                            elif len(list_of_fmts_and_values) > 2:
                                # print(list_of_fmts_and_values)
                                worksheet.write_rich_string(row, col, *list_of_fmts_and_values)
                                row += 1
                            else:
                                logging.warning('get_diff_with_color_2 did not return a list!')

                        while rowSkipCount > 0:
                            worksheet.write(row, col, '', blank_border)
                            rowSkipCount -= 1
                            row += 1

                    elif plain_subframe is not None and mod_subframe is None:
                        # print('Mod Frame is None for: ', ppl_id, q_id)

                        # Fill out the mod_subframe if it is None
                        mod_subframe = pd.DataFrame(index=plain_subframe.index, columns=plain_subframe.columns)
                        mod_subframe.fillna('', inplace=True)

                        rowSkipCount = max_df_len - len(plain_subframe)

                        # Write row/column header questions
                        for p_index, m_index in zip(plain_subframe.index, mod_subframe.index):
                            p_value = plain_subframe.at[p_index, 'value']
                            m_value = mod_subframe.at[m_index, 'value']
                            # print('pvalue', p_value, len(p_value), type(p_value))
                            # print('mvalue', m_value, len(m_value), type(m_value))
                            list_of_fmts_and_values = self.get_diff_with_color_2(ppl_id, q_id, plain_value = p_value, mod_value = m_value)
                            if len(list_of_fmts_and_values)==0:
                                worksheet.write(row, col, '', blank_border)
                                row += 1
                            elif len(list_of_fmts_and_values) == 2:
                                worksheet.write(row, col, list_of_fmts_and_values[1], list_of_fmts_and_values[0])
                                row += 1
                            elif len(list_of_fmts_and_values) > 2:
                                # print(list_of_fmts_and_values)
                                worksheet.write_rich_string(row, col, *list_of_fmts_and_values)
                                row += 1
                            else:
                                logging.warning('get_diff_with_color_2 did not return a list!')

                        while rowSkipCount > 0:
                            worksheet.write(row, col, '', blank_border)
                            rowSkipCount -= 1
                            row += 1
                    # If both DataFrames exist
                    else:
                        # set up comparable indices. This takes into account different index sizes
                        # plain_subframe = self.align_groups_of_matrix_questions(plain_subframe, mod_subframe)
                        plain_filled_index, mod_filled_index = self.compare_indices(plain_subframe, mod_subframe)
                        # Get number of rows for this df and establish rowSkipCount

                        # DOUBLE CHECK THE BELOW LINE
                        # plain filled index is always the same as mod_filled_index
                        rowSkipCount = max_df_len - len(plain_filled_index)

                        # Write row/column header questions
                        for p_index, m_index in zip(plain_filled_index, mod_filled_index):
                            if p_index is None:
                                p_value = ''
                            else:
                                p_value = plain_subframe.at[p_index, 'value']
                            if m_index is None:
                                m_value = ''
                            else:
                                m_value = mod_subframe.at[m_index, 'value']
                            if modified == False and plain == False:
                                # print('pvalue', p_value, len(p_value), type(p_value))
                                # print('mvalue', m_value, len(m_value), type(m_value))
                                list_of_fmts_and_values = self.get_diff_with_color_2(ppl_id, q_id, plain_value = p_value, mod_value = m_value)
                                if len(list_of_fmts_and_values)==0:
                                    worksheet.write(row, col, '', blank_border)
                                    row += 1
                                elif len(list_of_fmts_and_values) == 2:
                                    worksheet.write(row, col, list_of_fmts_and_values[1], list_of_fmts_and_values[0])
                                    row += 1
                                elif len(list_of_fmts_and_values) > 2:
                                    # print(list_of_fmts_and_values)
                                    worksheet.write_rich_string(row, col, *list_of_fmts_and_values)
                                    row += 1
                                else:
                                    logging.warning('get_diff_with_color_2 did not return a list!')
                            elif modified == False and plain == True:
                                worksheet.write(row, col, p_value, fmt)
                                row += 1
                            elif modified == True and plain == False:
                                worksheet.write(row, col, m_value, fmt)
                                row += 1
                            else:
                                logging.warning('This else condition should never be satisfied')
                                row += 1

                        while rowSkipCount > 0:
                            worksheet.write(row, col, '', blank_border)
                            rowSkipCount -= 1
                            row += 1

                # If it is not a variable length question
                else:
                    if self.questions_df.loc[q_id]['header_symbol'] is not None and self.print_headers == True:
                        row += 1
                    elif self.questions_df.loc[q_id]['header_symbol'] =='cert' and self.print_headers == False:
                        row+=1
                    if modified == False and plain == False:
                        # print('pvalue', self.p_df.loc[ppl_id, self.p_df.columns[q_id]], len(self.p_df.loc[ppl_id, self.p_df.columns[q_id]]), type(self.p_df.loc[ppl_id, self.p_df.columns[q_id]]))
                        p_value = self.p_df.loc[ppl_id, self.p_df.columns[q_id]]
                        # print('p_value: ', p_value, '\nlength: ', len(p_value), type(p_value))
                        m_value = self.m_df.loc[ppl_id, self.m_df.columns[q_id]]
                        # print('m_value: ', m_value, '\nlength: ', len(m_value), type(m_value))
                        list_of_fmts_and_values = self.get_diff_with_color_2(ppl_id, q_id, plain_value = p_value, mod_value = m_value)

                        if len(list_of_fmts_and_values)==0:
                            worksheet.write(row, col, '', blank_border)
                            row += 1
                        elif len(list_of_fmts_and_values) == 2:
                            worksheet.write(row, col, list_of_fmts_and_values[1], list_of_fmts_and_values[0])
                            row += 1
                        elif len(list_of_fmts_and_values) > 2:
                            # print(list_of_fmts_and_values)
                            worksheet.write_rich_string(row, col, *list_of_fmts_and_values)
                            row += 1
                        else:
                            logging.warning('get_diff_with_color_2 did not return a list!')
                            row += 1
                    elif modified == True and plain == False:
                        worksheet.write(row, col, self.m_df.loc[ppl_id, self.m_df.columns[q_id]], fmt)
                        row += 1
                    elif modified == False and plain == True:
                        worksheet.write(row, col, self.p_df.loc[ppl_id, self.p_df.columns[q_id]], fmt)
                        row += 1
                    else:
                        logging.warning('Specify either both modified and plain should be false, or only one can be true. Do not specify both arguments as True')
            col += 1

    def fsr_handler(self):
        
        produce_plain = False

        worksheet = self.workbook.add_worksheet(self.output_name[0:31])
        if produce_plain:
            pworksheet = self.plain_workbook.add_worksheet(self.output_name[0:31])
        if self.produce_mod:
            mworksheet = self.mod_workbook.add_worksheet(self.output_name[0:31])

        self.write_names_and_titles(worksheet, self.workbook)
        if produce_plain:
            self.write_names_and_titles(pworksheet, self.plain_workbook)
        if self.produce_mod:
            self.write_names_and_titles(mworksheet, self.mod_workbook)

        self.write_row_index(worksheet, self.workbook)
        if produce_plain:
            self.write_row_index(pworksheet, self.plain_workbook)
        if self.produce_mod:
            self.write_row_index(mworksheet, self.mod_workbook)

        self.write_data(worksheet, self.workbook)
        if produce_plain:
            self.write_data(pworksheet, self.plain_workbook, plain=True)
        if self.produce_mod:
            self.write_data(mworksheet, self.mod_workbook, modified=True)

        worksheet.set_column(0, 0, 60)
        if produce_plain:
            pworksheet.set_column(0, 0, 60)
        if self.produce_mod:
            mworksheet.set_column(0, 0, 60)
        for c in range(1, len(self.m_df.index)+1):
            worksheet.set_column(c, c, 25)
            if self.produce_mod:
                mworksheet.set_column(c, c, 25)
        if produce_plain:
            for c in range(1, len(self.p_df.index)+1):
                pworksheet.set_column(c, c, 25)

        self.workbook.close()
        if self.produce_mod:
            self.mod_workbook.close()
