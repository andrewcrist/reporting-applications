def ppl_parser(df, sort=False):
    # Create dataframe of respondents
    import pandas as pd
    import logging
    ppl = dict()
    cols = ['UniqueIdentifier', 'FirstName', 'LastName', 'MiddleName', 'Capacity', 'Audit', 'Compensation', 'NomGov', 'HR', 'Risk', 'Trust', 'hsse', 'Regulatory', 'Compliance', 'Corporate', 'Title']
    for k, v in df.items():
        if k in cols:
            ppl[k] = v

    ppl_df = pd.DataFrame.from_dict(ppl)

    if  'UniqueIdentifier' in ppl_df.columns:
        ppl_df.set_index('UniqueIdentifier', inplace=True)

    if sort:
        if None in set(ppl_df['Capacity'].tolist()) or '' in set(ppl_df['Capacity'].tolist()):
            logging.warning('Null values in Capacity column.\nppl_df will not be sorted.')
            return ppl_df

        orderedCapacities = ['Both', 'Director', 'Officer']
        df_list = []
        for i in orderedCapacities:
            d = ppl_df[ppl_df['Capacity']==i]
            d.sort_values(['LastName', 'FirstName'], inplace=True)
            df_list.append(d)
        ppl_df = pd.concat(df_list)

    return ppl_df
