import pandas as pd
import numpy as np
import os
import glob
import re
from random import shuffle
from collections import Counter
import warnings
import xlsxwriter
from functools import reduce
from statistics import mean
import math
import sys
import itertools
import difflib
import logging
import time
from pandas import ExcelWriter
from decimal import Decimal, ROUND_HALF_UP
import matplotlib.pyplot as plt
plt.switch_backend('agg')
from matplotlib.patches import Circle, RegularPolygon
from matplotlib.path import Path
from matplotlib.projections.polar import PolarAxes
from matplotlib.projections import register_projection
from matplotlib.spines import Spine
from matplotlib.transforms import Affine2D
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import matplotlib.colors as mc
# import seaborn as sns
from matplotlib.ticker import *
from collections import Counter
import matplotlib.transforms

pd.options.mode.chained_assignment = None
warnings.filterwarnings("ignore",  'This pattern has match groups')

class bea_report(object):
    '''
    Pretty much everything below is deprecated at Aug. 31, 2021
    --Class variables--
        self.workbook: workbook for radar chart, hilo, aggregate section analysis, and bar chart,
        self.reportname: name of the excel Workbook,
        self.df: raw csv input,
        self.file: input CSV file,
        self.rows_of_survey_object: rows of multidimensional dataframe,
        self.shuffled_rows_of_survey_object: shuffled rows of multidimensional dataframe,
        self.number_of_respondents: number of respondents, i.e. the number of people who took the survey,
        self.survey_object: multidimensional dataframe containing all information about every question
        self.report_sheet: formatted sheet for final output
        self.averages: dictionary of averages for each section
        self.five_point_scale: boolean flag indicating whether the survey is based on a 5 point likert scale.

    --Class Methods--
        read_file: drop extraneous columns. Initialize workbook, reportname, and number_of_respondents.
        create_multi_index_rows: creating a multiindexed dataframe requires an explicit index. An iterative
            process requires that each row is assigned an index before the rows are concatenated.
            This method creates these indexed rows for shuffled and unshuffled Responses
        ToArabic: Roman numerals are difficult to sort. ToArabic and check_valid methods are a robust
            method to convert from roman numerals to arabic numerals for the purpose of sorting.
        check_valid: helper function for ToArabic.
        parse_columns: this method does all the heavy lifting in terms of the logic necesssary to parse
            columns with a sparse or spotty multiindex.
        statistics: this function takes the ouput of parse_columns and create_multiindex. It calculates
            averages for different sections, string formatting, formats report_sheet, and creates the survey_object
        heat_map: creates the heat map for the report. Currently, this heat map outputs to its own workbook.
            The reason for this is that the to_excel method automatically merges columns which is extremely
            convienent. If this method is to be outputted using self.workbook, this merging and formatting
            will need to be done using xlsxwriter.
        format_heat_map: column values need a specific formatting. This function formats every column
        radar_chart: creates the radar chart, i.e. "web".
        bar_chart: creates the bar chart.
        hi_lo: creates hilo sheet.
        aggregated_section_analysis: calculates the individual statistics for the aggregated section analysis sheet.
        eSigma: calculates the eSigma score, i.e. the sum of the averages of all sections and the average of
            the averages of each question divided by 2.
            This function assings a value to self.average_total_rating_given--necessary for the aggregated_section_analysis
        writeXLSX: calls self.workbook.close().
'''

    def __init__(self, file_name, report_type, df, questions_df, ppl_df, na_no_null_dict, app, skills_response=False, randomize = True, comparative = True, year=None):
        self.file_name = file_name
        self.report_type = report_type
        self.df = df
        self.questions_df = questions_df
        self.ppl_df = ppl_df
        self.na_no_null_dict = na_no_null_dict
        self.section_data = dict()
        self.section_avg_list = []
        self.randomize = randomize

        self.app = app

        self.rows_of_survey_object = []
        self.shuffled_rows_of_survey_object = []
        self.skill_matrix_dfs = {}
        self.five_point_scale = False
        self.eval_stats_list_of_dicts = []
        self.skills_response = skills_response
        self.year = year
        self.comparative = comparative
        self.bea_handler()

    def bea_handler(self):
        # Initialize workbook
        client_name_re = re.compile(r'(.*?)(_csvExport)?(\s*\(\d\))?\.csv$')
        client_name = client_name_re.match(self.file_name)
        self.report_name = client_name.group(1)

        self.output_filename = '{}_{}.xlsx'.format(self.report_name, self.report_type)

        self.workbook = xlsxwriter.Workbook(os.path.join(self.app.config['EXCEL_OUTPUT'], self.output_filename), {'nan_inf_to_errors': True})

        self.number_of_respondents = len(self.df)
        self.overview()

        if not self.skills_response:
            self.statistics()
            if self.report_type == 'BEA' and (self.comparative):
                self.radar_chart()
            self.bar_chart()
            self.aggregated_section_analysis()
            self.hi_lo()
            self.heat_map()
        if self.skill_matrix_dfs:
            for tup in self.skill_matrix_dfs:
                self.skills_matrix(tup)

        self.workbook.close()

    def create_multi_index_rows(self, section, number, sub_section, qtype, question, answers):
        # question = re.sub(r'\.\.\d+$', r'.', question)
        index = pd.MultiIndex.from_arrays([[section], [number], [sub_section], [qtype], [question]], \
                                          names = ['section', 'number', 'sub_section', 'question_type', 'question'])
        formatted_reportsheet_row = pd.DataFrame([tuple(answers)], index = index, columns = np.arange(1, len(self.df)+1))

        self.rows_of_survey_object.append(formatted_reportsheet_row)
        shuffle(answers)
        shuffled_row= pd.DataFrame([tuple(answers)], index = index, columns = np.arange(1, len(self.df)+1))
        self.shuffled_rows_of_survey_object.append(shuffled_row)

    def ToArabic(self, roman):
        roman = self.check_valid(roman)
        keys = ['IV', 'IX', 'XL', 'XC', 'CD', 'CM', 'I', 'V', 'X', 'L', 'C', 'D', 'M']
        to_arabic = {'IV': '4', 'IX': '9', 'XL': '40', 'XC': '90', 'CD': '400', 'CM': '900',
                'I': '1', 'V': '5', 'X': '10', 'L': '50', 'C': '100', 'D': '500', 'M': '1000'}
        for key in keys:
            if key in roman:
                roman = roman.replace(key, ' {}'.format(to_arabic.get(key)))
        arabic = sum(int(num) for num in roman.split())
        return arabic

    def check_valid(self, roman):
        roman = roman.upper()
        invalid = ['IIII', 'VV', 'XXXX', 'LL', 'CCCC', 'DD', 'MMMM']
        if any(sub in roman for sub in invalid):
            raise ValueError('Numerus invalidus est: {}\n.The number is an invalid Roman Numeral'.format(roman))
        return roman

    def format_heat_map(self, row):
        row['section'] = re.search(r'\b([IVX]+)\.?', row['section']).group(1)
        row['number'] += row['sub_section']
        # remove spaces from likert questions
        match_likert = re.search('^(\d+)\.$', row['number'])
        if match_likert is not None:
            row['number'] = match_likert.group(1)
        # match everything except 'a' so we can format correctly
        match_b_z = re.search('^\d+\.(?![a])([a-z])', row['number'])
        if match_b_z is not None:
            row['number'] = match_b_z.group(1)
        # pair 'a' with the corresponding question number without a period
        match_a = re.search('^(\d+)\.(a)\.', row['number'])
        if match_a is not None:
            row['number'] = match_a.group(1) + match_a.group(2)
        return row

    def radar_chart(self):
        worksheet = self.workbook.add_worksheet('Radar Chart')
        fmt = self.workbook.add_format({'font_name': 'Garamond',
                                                'font_size': 12})
        web = self.survey_object.copy()
        web = web[web['question_type'].isin(['likert', 'matrix'])]
        del web['sub_section']
        del web['question_type']
        del web['question']
        web.reset_index(inplace=True, drop=True)
        colList = web.columns.tolist()
        for c in colList:
            try:
                web[c].replace({'': 0}, inplace=True)
            except:
                pass
        web.loc[:, '1':] = web.loc[:, '1':].apply(pd.to_numeric)
        sums = web.iloc[:, 2:].sum(axis=0)
        counts = web.iloc[:, 2:].apply(pd.value_counts)
        if self.five_point_scale == False:
            new_index = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            counts = counts.reindex(new_index, fill_value=0)
            ten_counts = counts.loc[10]
            data = pd.concat([ten_counts, sums], axis=1, sort=True)
            data.reset_index(inplace=True)
            data.columns = ['director', 'Number of Times a Director Rated a Question at a 10', 'Individual Director Total Rating (Scaled=Total/10)']
            data.iloc[:, 2] = data.iloc[:, 2]/10
        else:
            new_index = [1, 2, 3, 4, 5]
            counts = counts.reindex(new_index, fill_value=0)
            five_counts = counts.loc[5]
            data = pd.concat([five_counts, sums], axis=1, sort=True)
            data.reset_index(inplace=True)
            data.columns = ['director', 'Number of Times a Director Rated a Question at a 5', 'Individual Director Total Rating (Scaled=Total/5)']
            data.iloc[:, 2] = data.iloc[:, 2]/5

        # Global font
        # fmt = self.workbook.add_format({"font_name": "Garamond"})
        bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': 1, 'font_size': 12})


        data = data.sort_values('director', ascending=True)

        worksheet.write_row('A1', data.columns, bold_fmt)
        worksheet.write_column('A2', data['director'], fmt)
        worksheet.write_column('B2', data.iloc[:, 1], fmt)
        worksheet.write_column('C2', data.iloc[:, 2], fmt)

        # Create a new radar chart.
        chart1 = self.workbook.add_chart({'type': 'radar'})

        chart1.add_series({
            'name':       ['Radar Chart', 0, 1],
            'categories': ['Radar Chart', 1, 0, len(data), 0],
            'values':     ['Radar Chart', 1, 1, len(data), 1],
            'line': {'color': '#2E682B'}
        })

        chart1.add_series({
            'name':       ['Radar Chart', 0, 2],
            'categories': ['Radar Chart', 1, 0, len(data), 0],
            'values':     ['Radar Chart', 1, 2, len(data), 2],
            'line': {'color': '#5E7BAD'}
        })

        chart1.set_legend({'position': 'bottom',
                            'font': {'name': "Garamond", 'size': 14, 'bold': False}
                            })
        # Add a chart title and some axis labels.
        chart1.set_title ({'name': 'Number of Directors = {}'.format(len(data)),
                            'name_font': {'name': "Garamond", 'size': 14, 'bold': False},
                            'position': 'bottom',
                            'overlay': True,
                            'layout': {
                                'x': 0.35,
                                'y': 0.85,
                            }
                        })

        chart1.set_x_axis({'name': 'Test number', 'num_font':  {'name': 'Garamond'}})
        chart1.set_y_axis({'name': 'Sample length (mm)'})
        worksheet.insert_chart('D2', chart1, {'x_offset': 25, 'y_offset': 10, 'x_scale': 1.5, 'y_scale': 2})

        ##################
        # Automated Chart
        N = len(data['director'])
        theta = self.radar_factory(N, frame='circle')
        fig, axes = plt.subplots(figsize=(9, 9), nrows=1, ncols=1, subplot_kw=dict(projection='radar'))
        data.fillna(0, inplace=True)

        re_order = [1]
        additional_numbers = range(2, len(data)+1)
        additional_numbers = list(reversed(additional_numbers))
        re_order.extend(additional_numbers)
        # print('reorder: ', re_order)
        data['director'] = re_order
        # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
        #     print(data)

        if self.five_point_scale==False:
            axes.set_rgrids(list(np.arange(10, (int(math.ceil(max(data['Individual Director Total Rating (Scaled=Total/10)']) / 10.0)) * 10) + 10, 5)))
            axes.plot(theta, data['Individual Director Total Rating (Scaled=Total/10)'], 'b')
            axes.plot(theta, data['Number of Times a Director Rated a Question at a 10'], 'g')
            axes.set_varlabels(data['director'])

            fig.text(0.5, 0.05, 'Number of Directors = {}'.format(N), horizontalalignment='center', color='black', weight='bold', size='large', **{'fontname':'Garamond', 'fontsize': 18})
            labels = ['Individual Director Total Rating (Scaled=Total/10)', 'Number of Times a Director Rated a Question at a 10']
        else:
            axes.set_rgrids(list(np.arange(5, (int(math.ceil(max(data['Individual Director Total Rating (Scaled=Total/5)']) / 5.0)) * 5) + 5, 5)))
            axes.plot(theta, data['Individual Director Total Rating (Scaled=Total/5)'], 'b')
            axes.plot(theta, data['Number of Times a Director Rated a Question at a 5'], 'g')
            axes.set_varlabels(data['director'])

            fig.text(0.5, 0.05, 'Number of Directors = {}'.format(N), horizontalalignment='center', color='black', weight='bold', size='large', **{'fontname':'Garamond', 'fontsize': 18})
            labels = ['Individual Director Total Rating (Scaled=Total/5)', 'Number of Times a Director Rated a Question at a 5']

        legend = axes.legend(labels, loc=(0, -.25), labelspacing=0.1, fontsize='large', prop={'size':20, 'family': 'Garamond'})
        plt.savefig(os.path.join(self.app.config['CHARTS'], 'radar_chart.png'), bbox_inches='tight', dpi=300)

    def radar_factory(self, num_vars, frame='circle'):
        """Create a radar chart with `num_vars` axes.

        This function creates a RadarAxes projection and registers it.

        Parameters
        ----------
        num_vars : int
            Number of variables for radar chart.
        frame : {'circle' | 'polygon'}
            Shape of frame surrounding axes.

        """
        # calculate evenly-spaced axis angles
        theta = np.linspace(0, 2*np.pi, num_vars, endpoint=False)

        class RadarAxes(PolarAxes):

            name = 'radar'
            # use 1 line segment to connect specified points
            RESOLUTION = 1

            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
                # rotate plot such that the first axis is at the top
                self.set_theta_zero_location('N')

            def fill(self, *args, closed=True, **kwargs):
                """Override fill so that line is closed by default"""
                return super().fill(closed=closed, *args, **kwargs)

            def plot(self, *args, **kwargs):
                """Override plot so that line is closed by default"""
                lines = super().plot(*args, **kwargs)
                for line in lines:
                    self._close_line(line)

            def _close_line(self, line):
                x, y = line.get_data()
                # FIXME: markers at x[0], y[0] get doubled-up
                if x[0] != x[-1]:
                    x = np.concatenate((x, [x[0]]))
                    y = np.concatenate((y, [y[0]]))
                    line.set_data(x, y)

            def set_varlabels(self, labels):
                self.set_thetagrids(np.degrees(theta), labels)

            def _gen_axes_patch(self):
                # The Axes patch must be centered at (0.5, 0.5) and of radius 0.5
                # in axes coordinates.
                if frame == 'circle':
                    return Circle((0.5, 0.5), 0.5)
                elif frame == 'polygon':
                    return RegularPolygon((0.5, 0.5), num_vars,
                                          radius=.5, edgecolor="k")
                else:
                    raise ValueError("unknown value for 'frame': %s" % frame)

            def _gen_axes_spines(self):
                if frame == 'circle':
                    return super()._gen_axes_spines()
                elif frame == 'polygon':
                    # spine_type must be 'left'/'right'/'top'/'bottom'/'circle'.
                    spine = Spine(axes=self,
                                  spine_type='circle',
                                  path=Path.unit_regular_polygon(num_vars))
                    # unit_regular_polygon gives a polygon of radius 1 centered at
                    # (0, 0) but we want a polygon of radius 0.5 centered at (0.5,
                    # 0.5) in axes coordinates.
                    spine.set_transform(Affine2D().scale(.5).translate(.5, .5)
                                        + self.transAxes)
                    return {'polar': spine}
                else:
                    raise ValueError("unknown value for 'frame': %s" % frame)

        register_projection(RadarAxes)
        return theta

    def skills_matrix_row_builder(self, section, number, skillArea, firstName, lastName, answers):
        firstName = firstName.tolist()
        lastName = lastName.tolist()
        listOfFirstIndex = []
        for f, l in zip(firstName, lastName):
            joined = f + ' ' + l
            first = joined.split()
            letters = [word[0] for word in first]
            ind = ''.join(letters)
            listOfFirstIndex.append(ind)
        uniqueID = list(np.arange(1, len(listOfFirstIndex) + 1))
        arrays = [uniqueID, listOfFirstIndex]
        mi = pd.MultiIndex.from_arrays(arrays,names=['uniqueID', 'initials'])
        if (section, number) not in self.skill_matrix_dfs:
            self.skill_matrix_dfs[(section, number)] = []
            # print(section, number, skillArea)
            # print('\n', mi)
            self.skill_matrix_dfs[(section, number)].append(pd.DataFrame(answers.tolist(), columns=[skillArea], index=mi))
        else:
            self.skill_matrix_dfs[(section, number)].append(pd.DataFrame(answers.tolist(), columns=[skillArea], index=mi))
        # self.skill_matrix_rows.append(pd.DataFrame([answers.tolist()], columns=listOfColumns, index=[skillArea]))

    def skills_matrix(self, tup):
        skills_matrix = self.skill_matrix_dfs[tup][0].join(self.skill_matrix_dfs[tup][1:])
        # print(skills_matrix)
        skills_matrix.loc[(len(skills_matrix)+1, 'board'), :] = skills_matrix.apply(lambda row: float(str(Decimal('{}'.format(mean(row))).quantize(Decimal('1e-2'), ROUND_HALF_UP))), axis = 0)
        # print(skills_matrix)
        skills_matrix.to_excel('skills_matrix.xlsx')
        skills_matrix = skills_matrix.sort_values((len(skills_matrix), 'board'), axis = 1, ascending=False)
        self.write_df(skills_matrix, 'Skills Matrix_{}_{}'.format(tup[0][:12], tup[1][:3]))

    def overview(self):
        # Identify the columns of data in an iterative manner.
        columns = list(self.df.columns)
        # Format rows one at a time and append them to a list
        formatted_reportsheet_rows = []

        # Need the ability to set and reset values for ranked choice questions
        # the process is iterative, so it is necessary to count ahead as well as behind
        # rankFlag is True as long as the question is a ranked question.
        rankFlag = False
        lengthOfRankedChoiceSection = 0
        yAxisCount = 1
        sectionNames = []
        firstColumnFound = False
        continuedSectionFlag = False

        section_set = set()

        for index, row in self.questions_df.iterrows():

            # Funnel skills_matrix questions out of the report sheet
            if row['type'] == 'skills_matrix':
                # print(self.file_name, self.report_type)
                self.skills_matrix_row_builder(row['section'], row['number'], row['sub_section'], self.ppl_df['FirstName'], self.ppl_df['LastName'], self.df[row['question_raw']])
                continue

            elif row['type'] == 'attribute_matrix':
                # print(self.file_name, self.report_type)
                self.skills_matrix_row_builder(row['section'], row['number'], row['sub_section'], self.ppl_df['FirstName'], self.ppl_df['LastName'], self.df[row['question_raw']])
                continue
            # Create an average row to appear above section header row if the
            # section does not just consist of long_form question types
            # print(row['section'], section_set, row['section'] in section_set)
            if row['section'] not in section_set:
                section_set.add(row['section'])
                new_section_flag = True

                if not set(self.questions_df[self.questions_df['section'] == row['section']]['type']).issubset(set(['long_form', 'yes/no'])):
                    # print(row['section'])
                    # print(set(self.questions_df[self.questions_df['section'] == row['section']]['type']))
                    formatted_reportsheet_rows.append((None, None, None, None, None) + tuple([None]*(len(self.df[row['question_raw']])-1)) + ('Average', 'average_value'))
            else:
                new_section_flag = False

            # Long form questions
            if row['type'] in ['long_form', 'optional_long_form']:
                # If long_form question types are unnumbered
                if row['number'] == '' or row['number'] == np.nan:
                    if new_section_flag:
                        long_form_title_row = (row['section'], row['number'], row['question'], None, None)
                    else:
                        long_form_title_row = (None, None, row['question'], None, None)

                    answers = self.df[row['question_raw']].tolist()
                    answers = [str(answer) for answer in answers]

                    # If the question is unnumbered, only display it if it is answered
                    if len(set(answers)) == 1:
                        continue
                    else:
                        #
                        self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())
                        #
                        formatted_reportsheet_rows.append(long_form_title_row)
                        if self.randomize == True:
                            shuffle(answers)
                        for optional in answers:
                            if optional != 'nan':
                                formatted_reportsheet_rows.append((None, None, optional))
                        continue

                # If it's a numbered, optional question that needs displayed
                elif row['number'] != '' and row['number'] != np.nan:
                    if new_section_flag:
                        long_form_title_row = (row['section'], row['number'], row['question'], None, None)
                    else:
                        long_form_title_row = (None, row['number'], row['question'], None, None)
                    answers = self.df[row['question_raw']].tolist()
                    #
                    self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())
                    #
                    formatted_reportsheet_rows.append(long_form_title_row)
                    if self.randomize == True:
                        shuffle(answers)

                    if set(answers) == set(['nan']) | set(answers) == set([np.nan]):
                        formatted_reportsheet_rows.append((None, None, "no answers given"))
                        continue
                    else:
                        for optional in answers:
                            if optional != 'nan':
                                formatted_reportsheet_rows.append((None, None, optional))
                        continue
                else:
                    pass
                    # print('long_form error in: ', self.file_name, self.report_type)


            # Yes/No Questions
            elif row['type'] == 'yes/no':
                if new_section_flag:
                    yes_no_title_row = (row['section'], row['number'], row['question'], None, None)
                else:
                    yes_no_title_row = (None, row['number'], row['question'], None, None)

                yesNoCheck = self.df[row['question_raw']].tolist()
                yesNoCheck = [answer.lower() for answer in yesNoCheck]
                #
                self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())
                #
                yesNoCheck = dict(Counter(yesNoCheck))

                formatted_reportsheet_rows.append(yes_no_title_row)
                formatted_reportsheet_rows.append((None, None, 'Yes', yesNoCheck.get('yes', 0)))
                formatted_reportsheet_rows.append((None, None, 'No', yesNoCheck.get('no', 0)))
                continue
            # Determine if question has multiple parts
            elif row['type'] == 'matrix':
                # Reset Flags
                rankFlag = False
                yAxisCount = 1

                if any(self.df[row['question_raw']].isna()):
                    answers = self.df[row['question_raw']].replace({np.nan: None})
                    answers.fillna('N/A', inplace=True)
                    answers = answers.tolist()
                else:
                    answers = self.df[row['question_raw']].tolist()

                if self.randomize ==True:
                    shuffle(answers)

                try:
                    sub_section_identifier = re.search('^([a-z]\.)', row['sub_section']).group(1)
                except:
                    sub_section_identifier = '!__!'
                try:
                    sub_section_value = re.search('^[a-z]\.[\s\t]*(.*)', row['sub_section']).group(1)
                except:
                    sub_section_value = '!__!'

                if re.search('(^[a-z]\.)', row['sub_section']).group(1) != 'a.':
                    self.create_multi_index_rows(row['section'], row['number'], sub_section_identifier, row['type'], row['question'] + ' ' + sub_section_value, self.df[row['question_raw']].tolist())

                    formatted_reportsheet_row = (None, None, sub_section_identifier, sub_section_value) + tuple(answers) + (str(Decimal('{}'.format(np.mean(self.df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    formatted_reportsheet_rows.append(formatted_reportsheet_row)
                    continue

                else:
                    if new_section_flag:
                        matrix_title_row = (row['section'], row['number'], row['question'], None, None)
                    else:
                        matrix_title_row = (None, row['number'], row['question'], None, None)
                    self.create_multi_index_rows(row['section'], row['number'], sub_section_identifier, row['type'], row['question'] + ' ' + sub_section_value, self.df[row['question_raw']].tolist())

                    formatted_reportsheet_row = (None, None, sub_section_identifier, sub_section_value) + tuple(answers) + (str(Decimal('{}'.format(np.mean(self.df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)

                    formatted_reportsheet_rows.append(matrix_title_row)
                    formatted_reportsheet_rows.append(formatted_reportsheet_row)
                    continue

            # Check if it's a ranked choice question
            # Ranked choice questions must contain a substring with a capital letter that follows an underscore and the entire string must end in an underscore
            elif row['type'] == 'rank':
                # Ranked questions have no sequential identification.
                # "Flip a switch" to indicate that the iterative process has encountered a ranked Choice question
                # We will switch the rankFlag value to False when the loop encounters a different type of question
                if rankFlag == False:
                    # We know that, if rankFlag is false, this is the first question.
                    # Create and format the rank choice section's titles
                    if new_section_flag:
                        rank_title_row = (row['section'], row['number'], row['question'], None, None)
                    else:
                        rank_title_row = (None, row['number'], row['question'], None, None)

                    xAxisTitle = (None, None, None, None, "Number of Responses")


                    lengthOfRankedChoiceSection = len(self.questions_df[(self.questions_df['section']==row['section']) & (self.questions_df['number'] == row['number'])])
                    xAxis = (None, None, 'Rank:', None) + tuple(np.arange(1, lengthOfRankedChoiceSection+1, 1))

                    formatted_reportsheet_rows.append(rank_title_row)
                    formatted_reportsheet_rows.append(xAxisTitle)
                    formatted_reportsheet_rows.append(xAxis)

                # Count up the respective rankings, add them to list, and insert 0s where necessary
                dictonaryOfCounts = dict(Counter(self.df[row['question_raw']].tolist()))
                listOfOrderedCounts = list()
                for value in np.arange(1, lengthOfRankedChoiceSection+1, 1):
                    listOfOrderedCounts.append(dictonaryOfCounts.get(value, 0))

                formatted_reportsheet_row = (None, None, yAxisCount, row['sub_section']) + tuple(listOfOrderedCounts)
                formatted_reportsheet_rows.append(formatted_reportsheet_row)
                # Ranked questions have a number of responses based on the number of choices for the whole section

                self.create_multi_index_rows(row['section'], row['number'], yAxisCount, row['type'], row['question'] + ' ' + row['sub_section'], self.df[row['question_raw']].tolist())
                #
                rankFlag = True
                yAxisCount+=1
                continue

            # The simple instance of a rating question
            else:
                if any(self.df[row['question_raw']].isna()):
                    answers = self.df[row['question_raw']].replace({np.nan: None})
                    answers.fillna('N/A', inplace=True)
                    answers = answers.tolist()
                else:
                    answers = self.df[row['question_raw']].tolist()

                rankFlag = False
                yAxisCount = 1
                if new_section_flag:
                    # print(str(Decimal(np.mean(self.df[row['question_raw']].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    if self.randomize:
                        shuffle(answers)
                    formatted_reportsheet_row = (row['section'], row['number'], row['question'], None) + tuple(answers) + (str(Decimal('{}'.format(np.mean(self.df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    formatted_reportsheet_rows.append(formatted_reportsheet_row)

                    self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())
                    #

                else:
                    if self.randomize:
                        shuffle(answers)

                    # if all(isinstance(answer, (int, float)) for answer in answers):

                    formatted_reportsheet_row = (None, row['number'], row['question'], None) + tuple(answers) + (str(Decimal('{}'.format(np.mean(self.df[row['question_raw']].dropna()))).quantize(Decimal('1e-2'), ROUND_HALF_UP)),)
                    formatted_reportsheet_rows.append(formatted_reportsheet_row)
                    #
                    self.create_multi_index_rows(row['section'], row['number'], '', row['type'], row['question'], self.df[row['question_raw']].tolist())
                    #
                    continue

        self.report_sheet = pd.DataFrame(formatted_reportsheet_rows)

    def statistics(self):
        df = pd.concat(self.rows_of_survey_object)
        df_shuffled = pd.concat(self.shuffled_rows_of_survey_object)

        df.reset_index(inplace=True)
        df_shuffled.reset_index(inplace=True)

        # Calculate Averages for each section
        df.columns = [str(i) for i in df.columns]
        df_shuffled.columns = [str(i) for i in df.columns]
        #
        df[df['question_type'].isin(['likert', 'matrix'])].loc[:, '1':] = df[df['question_type'].isin(['likert', 'matrix'])].loc[:, '1':].apply(pd.to_numeric)
        #
        numericAnswers = df[df['question_type'].isin(['likert', 'matrix'])]
        # Check if the report is based on a 5 point scale
        # if numericAnswers.loc[:,'1':].max(numeric_only=True) <= 5:
        if max(numericAnswers.loc[:,'1':].max()) <=5:
            self.five_point_scale = True

        averages = {}
        for s in df['section'].unique():
            try:
                sub_frame = numericAnswers[numericAnswers['section']==s]
                totalNumberOfNullReponses = sub_frame.isna().sum().sum()
                sums = []
                for index, row in sub_frame.loc[:, '1':].iterrows():
                    sums.append(row.sum())
                total = sum(sums)
                totalNumberOfResponses = len(sums)*len(sub_frame.loc[:, '1':].columns)
                if totalNumberOfResponses == 0:
                    continue
                average = total/(totalNumberOfResponses-totalNumberOfNullReponses)
                # Create Dictionary of Averages for each Section
                averages[s] = str(Decimal('{}'.format(average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                self.section_avg_list.append({s: str(Decimal(average).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
            except:
                pass
        self.report_sheet.columns = [str(i) for i in self.report_sheet.columns]
        # If a report begins with a rank choice question, sectionIndices will not populate
        sectionIndices = self.report_sheet.index[self.report_sheet[self.report_sheet.columns[-1]] == 'average_value']

        if not sectionIndices.tolist():
            warnings.warn('average_value not found in the last column of the report sheet! Iterating backwards to identify')
            for x in range(len(self.report_sheet.columns)-1, 0, -1):
                sectionIndices = self.report_sheet.index[self.report_sheet[self.report_sheet.columns[-x]] == 'average_value']
                if sectionIndices.tolist():
                    break
            if not sectionIndices.tolist():
                sectionIndices = self.report_sheet.index[self.report_sheet[self.report_sheet.columns[-1]] == 'average_value']
        sectionIndices = [i + 1 for i in sectionIndices]
        self.row_count_of_sections = sectionIndices
        for i in sectionIndices:
            section = self.report_sheet.iloc[i, 0]
            self.report_sheet.iloc[i-1, [-1]] = averages.get(section)
        df['sub_section'] = df['sub_section'].astype(str)

        df_shuffled['sub_section'] = df_shuffled['sub_section'].astype(str)

        # Save members averages and survey_object
        self.averages = averages
        self.survey_object = df.copy()
        self.shuffled_survey_object = df_shuffled.copy()

        self.sectionMinsAndMaxes = {}
        for section in self.survey_object['section'].unique():
            sub_frame = self.survey_object[(self.survey_object['section']==section) & (self.survey_object['question_type'].isin(['likert', 'matrix']))]
            sub_frame.loc[:,'1':] = sub_frame.loc[:,'1':].apply(pd.to_numeric)
            if sub_frame.empty:
                continue
            try:
                self.sectionMinsAndMaxes[section] = {'min': int(min(sub_frame.loc[:,'1':].min())), 'max': int(max(sub_frame.loc[:, '1':].max()))}
            except:
                # print('This error is being thrown most likely because a question has been assigned an incorrect question type.\nSee the dataframe below to debug:\n')
                # print(sub_frame)
                pass

        # write dataframe to existing Workbook
        self.write_df(self.report_sheet, 'Report')
        self.write_df(self.survey_object, 'Survey Object')

    def heat_map(self):
        worksheet = self.workbook.add_worksheet('Heat Map')
        worksheet.hide_gridlines(2)
        if self.randomize:
            hm = self.shuffled_survey_object.copy()
        else:
            hm = self.survey_object.copy()

        hm = hm[hm['question_type'].isin(['likert', 'matrix'])]
        hm_copy = hm.copy()
        hm = hm.apply(lambda row: self.format_heat_map(row), axis = 1)
        del hm['sub_section']
        del hm['question_type']
        del hm['question']
        hm.set_index(['section', 'number'], inplace=True)
        # Get the size of each section in order to merge cells
        indexSize = hm.groupby(level=0).size()
        # hm = hm.sample(frac=1, axis=1)
        hm = hm.T

        # Add a header format.
        header_format1 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'bg_color': '#BE6E28', 'font_size': 20, 'center_across': True, 'border': 2})
        header_format2 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 14, 'center_across': True})
        # Add fill formats
        green_format = self.workbook.add_format({'bg_color': '#008000', 'font_color': '#008000'})
        yellow_format = self.workbook.add_format({'bg_color': '#FFFF00', 'font_color': '#FFFF00'})
        red_format = self.workbook.add_format({'bg_color': '#FF0000', 'font_color': '#FF0000'})
        white_format = self.workbook.add_format({'bg_color': '#FFFFFF', 'font_color': '#FFFFFF'})

        # Conditional formatting for colors
        start_row = 3
        end_row   = start_row + hm.shape[0] - 1
        start_col = 1
        end_col   = start_col + hm.shape[1] - 1

        if self.five_point_scale == False:
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
                {'type': 'cell',
                 'criteria': 'between',
                 'minimum': '1',
                 'maximum': '3',
                 'format': red_format})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
                {'type': 'cell',
                 'criteria': 'between',
                 'minimum': '4',
                 'maximum': '7',
                 'format': yellow_format})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
                {'type': 'cell',
                 'criteria': '>',
                 'value': '7',
                 'format': green_format})
        else:
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
             'criteria': 'between',
             'minimum': '1',
             'maximum': '2',
             'format': red_format})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
             'criteria': '==',
             'value': '3',
             'format': yellow_format})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
             'criteria': '>',
             'value': '3',
             'format': green_format})

        worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
             'criteria': '==',
             'value': '0',
             'format': white_format})

        # Write the column headers with correct format.
        header_format1 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'bg_color': '#BE6E28', 'font_size': 28, 'center_across': True, 'border': 2})
        header_format2 = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 20, 'center_across': True, 'right': 1})
        header_format2_leftBorder = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 20, 'center_across': True, 'left': 2})
        header_format2_leftBorder_rightBorder = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 20, 'center_across': True, 'left': 2, 'right':1})
        header_format2_rightBorder = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 20, 'center_across': True, 'right': 2})

        indexSize = pd.DataFrame({'section':indexSize.index, 'length':indexSize.values})
        # Ensure correct ordering
        indexSize['arabic'] = indexSize.apply(lambda x: self.ToArabic(x['section']), axis = 1)
        indexSize.sort_values('arabic', inplace=True)
        indexSize.reset_index(drop=True, inplace=True)
        indexSize.set_index('section', inplace=True)

        # Y Axis: Number of directors
        directors = self.workbook.add_format({'bold': True, 'font_name': 'Garamond', 'font_size': 18, 'center_across': True})
        worksheet.write_column(3, 0, np.arange(1, self.number_of_respondents+1, 1), directors)

        # Section Headers
        col = 1
        for ind in indexSize.index:
            if indexSize.loc[ind]['length'] > 1:
                worksheet.merge_range(1, col, 1, col+indexSize.loc[ind]['length'] - 1, ind, header_format1)
                col += indexSize.loc[ind]['length']
            elif indexSize.loc[ind]['length'] == 1:
                worksheet.write(1, col, ind, header_format1)
                col += indexSize.loc[ind]['length']

        # Create a dataframe that contains the ending column count for each section.
        indexSizeTotals = indexSize.copy()
        indexSizeTotals.reset_index(inplace=True)
        for i in range(1, len(indexSize)):
            indexSizeTotals.loc[i, 'length'] += indexSizeTotals.loc[i-1, 'length']

        worksheet.write(2, 1, hm.columns.values[0][1], header_format2_leftBorder_rightBorder)

        # Question Number Headers
        col = 2
        for value in hm.columns.values[1:]:
            if col == indexSizeTotals.loc[indexSizeTotals['section'] == value[0], 'length'].item():
                worksheet.write(2, col, value[1], header_format2_rightBorder)
            else:
                worksheet.write(2, col, value[1], header_format2)
            col += 1

        # Global font
        fmt = self.workbook.add_format({"font_name": "Garamond"})

        # Adjust row and column widths
        worksheet.set_column(1, 250, 4, fmt)
        worksheet.set_default_row(20.2)
        worksheet.set_row(1, 33, fmt)
        worksheet.set_row(2, 24, fmt)
        worksheet.set_row(0, 3, fmt )
        worksheet.set_row(self.number_of_respondents+4, 3, fmt)
        worksheet.set_column(int(end_col)+1, int(end_col)+1, 8.4, fmt)

        top_thick_border = self.workbook.add_format({'top': 2})
        bottom_thick_border = self.workbook.add_format({'bottom': 2})
        left_thick_border = self.workbook.add_format({'left': 2})
        right_thick_border = self.workbook.add_format({'right': 2})
        thick = self.workbook.add_format({'top':2, 'bottom': 2, 'left': 2, 'right': 2})

        cell = self.workbook.add_format({'border': 1, "font_name": "Garamond"})
        cell_leftBorder = self.workbook.add_format({'left': 2, 'bottom': 1, 'top': 1, 'right': 1})
        cell_rightBorder = self.workbook.add_format({'right': 2, 'top': 1, 'bottom': 1, 'left': 1})

        hm.fillna(0, inplace=True)

        # Write the colored response cells
        worksheet.write_column(3, 1, hm.iloc[:, 0], cell_leftBorder)
        col = 2
        for colName in hm.columns[1:]:
            if col == indexSizeTotals.loc[indexSizeTotals['section'] == colName[0], 'length'].item():
                worksheet.write_column(3, col, hm.iloc[:, col - 1], cell_rightBorder)
            else:
                worksheet.write_column(3, col, hm.iloc[:, col - 1], cell)
            col += 1

        # Format the bottom row containing averages
        startColumnMerge = 1
        endColumnMerge = 0
        for ind, size, avg in zip(indexSize.index, indexSize['length'], self.averages.values()):
            if indexSize.loc[ind]['length'] > 1:
                endColumnMerge += size
                worksheet.merge_range(len(hm)+3, startColumnMerge, len(hm)+3, endColumnMerge, avg, header_format1)
                startColumnMerge = endColumnMerge + 1
            elif indexSize.loc[ind]['length'] == 1:
                endColumnMerge += size
                worksheet.write(len(hm)+3, startColumnMerge, avg, header_format1)
                startColumnMerge = endColumnMerge + 1

        # bottom row height
        worksheet.set_row(hm.shape[0] + 3, 33, fmt)



        ########################################################################

        # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
        #     print(hm)
        hm_sections = hm.columns.get_level_values(0)

        # section_set = set()
        # for sec_num in list(hm.columns):

        # hmT = hm.T
        # for section, new_df in hmT.groupby(level=0):
        #     count = 1
        #     for _, row in new_df.iterrows():
        #         hm_values_borders.append({_[1]: list(row)})
        #
        # print(hm_values_borders)

        hm_section_length_counts = dict()
        heat_map_averages = dict()

        one_count_sections = 0
        section_names_with_two_questions = set()
        for i in hm_sections:
            how_many_questions_in_section = list(hm_sections).count(i)
            hm_section_length_counts[i+'.'] = how_many_questions_in_section
            if how_many_questions_in_section == 2:
                self.two_questions_in_section = True
                section_names_with_two_questions.add(i)
            else:
                self.two_questions_in_section = False

            if how_many_questions_in_section == 1:
                one_count_sections += 1
        # print('one_count_sections: ', one_count_sections)

        for (k, v), (section, avg) in zip(hm_section_length_counts.items(), self.averages.items()):
            heat_map_averages[k] = (v, avg)
        hm_numbers_borders = dict()

        last_item = list(list(hm.columns)[-1])
        last_item = tuple(last_item)

        # print('last_item: ', last_item)
        # print('hm.columns: ', hm.columns)

        duplicate_count = 0
        section_set = set()
        hm_values_borders = []
        for sec_num in list(hm.columns):
            if sec_num[0] not in section_set:
                section_set.add(sec_num[0])
                if sec_num == last_item:
                    hm_numbers_borders[sec_num] = (sec_num[1], 'right')
                    hm_values_borders.append('right')
                else:
                    hm_numbers_borders[sec_num] = (sec_num[1], 'left')
                    hm_values_borders.append('left')
            elif sec_num == last_item:
                hm_numbers_borders[sec_num] = (sec_num[1], 'right')
                hm_values_borders.append('right')
            else:
                # print('here: ', sec_num)
                if sec_num in hm_numbers_borders:
                    new_tuple = list(sec_num)
                    new_tuple.append(duplicate_count)
                    sec_num = tuple(new_tuple)
                    duplicate_count += 1
                    # print('\nnew: ', sec_num)
                hm_numbers_borders[sec_num] = (sec_num[1], 'None')
                hm_values_borders.append('None')

        # print(hm)
        hm_rows_directions = []
        self.na_in_heat_map = False
        for index, row in hm.iterrows():
            # print('\n', row.tolist())
            if 0 in row.tolist():
                self.na_in_heat_map = True
            hm_rows_directions.append({index: zip(list(row), hm_values_borders)})

        # print(hm_rows_directions)
        # for row in hm_rows_directions:
        #     print(list(row.keys())[0])
        #     for index, tup in row.items():
        #         for num, direction in tup:
        #             print(num, direction)
        # print(hm_rows_directions)
        # print(type(hm_rows_directions[0]))
        self.hm_rows_directions = hm_rows_directions

        # print('\nhm_rows_directions: ', hm_rows_directions)
        # for row in self.hm_rows_directions:
        #     print(row)
        #     for k, v in row.items():
        #         print(k)
        #         for item in v:
        #             print(item)

        self.hm_values_borders = hm_values_borders
        self.hm_section_length_counts = hm_section_length_counts
        self.hm_numbers_borders = hm_numbers_borders
        self.heat_map_export = hm
        self.heat_map_index = list(hm.index)


        total_pixels_of_two_px_wide_borders = (len(hm_section_length_counts)+1)*2
        # print('total_pixels_of_two_px_wide_borders: ', total_pixels_of_two_px_wide_borders)
        total_pixels_of_one_px_wide_borders = len(hm_numbers_borders)-len(hm_section_length_counts)
        # print('total_pixels_of_one_px_wide_borders: ', total_pixels_of_one_px_wide_borders)

        if len(hm_numbers_borders) > 50:
            pixel_size = math.floor(((610 - (one_count_sections * 50))-total_pixels_of_one_px_wide_borders-total_pixels_of_two_px_wide_borders)/(len(hm_numbers_borders)))
        else:
            pixel_size = math.floor(((610 - (one_count_sections * 20))-total_pixels_of_one_px_wide_borders-total_pixels_of_two_px_wide_borders)/(len(hm_numbers_borders)))


        # print((one_count_sections * 18))
        # print((640-total_pixels_of_one_px_wide_borders-total_pixels_of_two_px_wide_borders)/(len(hm_numbers_borders)))
        # print(total_pixels_of_one_px_wide_borders)
        # print(total_pixels_of_two_px_wide_borders)
        # print(len(hm_numbers_borders))

        if pixel_size > 24:
            pixel_size = 24
        # print(hm_section_length_counts)
        # print(pixel_size)
        # print(len(hm_numbers_borders))
        # print('hm_numbers_borders: ', hm_numbers_borders)
        # print(12 + total_pixels_of_one_px_wide_borders + total_pixels_of_two_px_wide_borders + (pixel_size*len(hm_numbers_borders)))

        # print('pixel_size: ', pixel_size)
        self.percentage_size = pixel_size

        if pixel_size < 15 and self.two_questions_in_section:
            for k, v in heat_map_averages.items():
                if v[1] == '10.00' and v[0] == 2:
                    heat_map_averages[k] = (v[0], '10')

        # The following block explicitly sets the pixel size for every column
        # Any section with only 2 questions, has both columns set at 12 pixels wide
        # Then, because we are enlarging certain columns, we need to check that
        # the total width of the heat map is less than 565 pixels
        hm_pixel_size_list = [pixel_size]
        if pixel_size < 12:
            for k, v in hm_numbers_borders.items():
                if k[0] in section_names_with_two_questions:
                    hm_pixel_size_list.append(12)
                else:
                    hm_pixel_size_list.append(pixel_size)
        else:
            hm_pixel_size_list = hm_pixel_size_list * (len(hm_numbers_borders) + 1)

        # print('\nhm_pixel_size_list: ', hm_pixel_size_list)
        # print(sum(hm_pixel_size_list) + (one_count_sections * 20)+total_pixels_of_one_px_wide_borders+total_pixels_of_two_px_wide_borders)
        # print(sum(hm_pixel_size_list))
        # print(one_count_sections)
        # print(total_pixels_of_one_px_wide_borders)
        # print(total_pixels_of_two_px_wide_borders)
        #
        # print(section_names_with_two_questions)


        while (sum(hm_pixel_size_list) + (one_count_sections * 20)+total_pixels_of_one_px_wide_borders+total_pixels_of_two_px_wide_borders) > 565:
            hm_pixel_size_list = [x-1 for x in hm_pixel_size_list]

        # print('\nHERE')
        # print(len(hm_pixel_size_list), hm_pixel_size_list)
        # print(len(hm_values_borders), hm_values_borders)

        self.hm_pixel_size_list = hm_pixel_size_list
        self.heat_map_averages = heat_map_averages
        return

    def NonLinCdict(self, steps, hexcol_array):
        import matplotlib.pyplot as plt
        plt.switch_backend('agg')
        cdict = {'red': (), 'green': (), 'blue': ()}
        for s, hexcol in zip(steps, hexcol_array):
            rgb =plt.colors.hex2color(hexcol)
            cdict['red'] = cdict['red'] + ((s, rgb[0], rgb[0]),)
            cdict['green'] = cdict['green'] + ((s, rgb[1], rgb[1]),)
            cdict['blue'] = cdict['blue'] + ((s, rgb[2], rgb[2]),)
        return cdict

    def write_df(self, df, sheetName):
        worksheet = self.workbook.add_worksheet(sheetName)
        colNum = 0

        if sheetName == 'Survey Object':
            fmt = self.workbook.add_format({'font_name': 'Garamond',
                                                    'font_size': 12})
            worksheet.write_row(0, 0, df.columns, fmt)
            for col in df.columns:
                worksheet.write_column(1, colNum, df[col], fmt)
                colNum += 1

        elif 'Skills Matrix' in sheetName:
            df.fillna('', inplace=True)
            # df.to_pickle(r'C:\Users\Drew\Board-Assessment\skills_matrix.pkl')
            # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
            #     print(df)
            fmt = self.workbook.add_format({"font_name": "Garamond"})
            left_top_fmt = self.workbook.add_format({"font_name": "Garamond", 'left': 1, 'top': 1})
            top_bold_fmt_left_right = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1, 'left': 1, 'right': 1})
            top_bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1})
            top_right_bold_center = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1, 'right': 1})
            left_bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'left': 1, 'bottom': 1})
            right_bottom_bold_center_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'right': 1, 'bottom': 1})
            left_fmt = self.workbook.add_format({"font_name": "Garamond", 'left': 1})
            right_fmt = self.workbook.add_format({"font_name": "Garamond", 'right': 1})
            bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True})
            center_bold_fmt_left_right = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1, 'bottom': 1})
            center_bold_fmt_bottom = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'bottom': 1})
            center_bold = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1})
            center_bold_top = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1, 'top': 1})

            worksheet.write(1, 1, '', left_top_fmt)
            worksheet.write(1, 2, 'Board', top_bold_fmt_left_right)

            number_of_skills_matrix_respondents = df.shape[0] - 1
            number_of_skills_matrix_questions = df.shape[1]
            worksheet.merge_range(1, 3, 1, 2 + number_of_skills_matrix_respondents, 'Director Self-Ratings', top_right_bold_center)
            worksheet.write(2, 1, 'Skill Areas:', left_bold_fmt)
            worksheet.write(2, 2, 'Average', center_bold_fmt_left_right)
            initials = list(df.index.get_level_values('initials'))
            initials.remove('board')
            worksheet.write_row(2, 3, initials[:-1], center_bold_fmt_bottom)
            worksheet.write(2, 2+len(initials), initials[-1], right_bottom_bold_center_fmt)

            df.reset_index(inplace=True)

            colNum += 1
            start_row = 3
            end_row   = start_row + number_of_skills_matrix_questions
            start_col = 1
            end_col   = start_col + number_of_skills_matrix_respondents + 1

            ten_format = self.workbook.add_format({'bg_color': '#63be7b'})
            nine_format = self.workbook.add_format({'bg_color': '#86c97e'})
            eight_format = self.workbook.add_format({'bg_color': '#a9d27f'})
            seven_format = self.workbook.add_format({'bg_color': '#ccdd82'})
            six_format = self.workbook.add_format({'bg_color': '#eee683'})
            five_format = self.workbook.add_format({'bg_color': '#fedd81'})
            four_format = self.workbook.add_format({'bg_color': '#fcbf7b'})
            three_format = self.workbook.add_format({'bg_color': '#fba276'})
            two_format = self.workbook.add_format({'bg_color': '#f98570'})
            one_format = self.workbook.add_format({'bg_color': '#f8696b'})

            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '9.01',
            'maximum': '10',
            'format': ten_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '8.01',
            'maximum': '9',
            'format': nine_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '7.01',
            'maximum': '8',
            'format': eight_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '6.01',
            'maximum': '7',
            'format': seven_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '5.01',
            'maximum': '6',
            'format': six_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '4.01',
            'maximum': '5',
            'format': five_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '3.01',
            'maximum': '4',
            'format': four_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '2.01',
            'maximum': '3',
            'format': three_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '1.01',
            'maximum': '2',
            'format': two_format})
            worksheet.conditional_format(start_row, start_col, end_row, end_col,
            {'type': 'cell',
            'criteria': 'between',
            'minimum': '0.01',
            'maximum': '1',
            'format': one_format})

            worksheet.write_column(3, 1, df.columns[2:], left_fmt)
            worksheet.write_column(3, 2, df.iloc[number_of_skills_matrix_respondents, 2:], center_bold)
            colNum += 2
            for i, u in zip(initials, df['uniqueID'].tolist()):
                value = df.loc[(df['uniqueID']==u) & (df['initials']==i)].values.flatten().tolist()
                value.remove(i)
                value.remove(u)
                if i == initials[-1]:
                    worksheet.write_column(3, colNum, value, right_fmt)
                else:
                    worksheet.write_column(3, colNum, value, fmt)
                colNum += 1

            average_of_averages = df.loc[(df['initials']=='board')].values.flatten().tolist()
            average_of_averages = average_of_averages[2:]
            # print(average_of_averages)
            average_of_averages = np.mean(average_of_averages)

            bold_align_right_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'right', 'left': 1, 'top': 1})
            worksheet.write(number_of_skills_matrix_questions + 3, 1, 'Overall Average:', bold_align_right_fmt)
            worksheet.write(number_of_skills_matrix_questions+3, 2, str(Decimal('{}'.format(average_of_averages)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), center_bold_top)

            self.skills_matrix_average = str(Decimal('{}'.format(average_of_averages)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            self.number_of_skills_matrix_questions = number_of_skills_matrix_questions

            top_bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1})
            top_right_bold_center_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'right': 1, 'top': 1})

            del df['uniqueID']
            with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
                df2 = df.T
                cols = list(df2.columns)
                cols = [cols[-1]] + cols[:-1]
                df2 = df2[cols]
                df2.rename(columns={x:y for x,y in zip(df2.columns,range(0,len(df2.columns)))}, inplace=True)
                # print(df2)
                self.skills_matrix_export = df2
                df2.loc['Overall Average:'] = df2.iloc[1:,0:].mean()
                # print(df2)
                # print(type(df2.loc['Overall Average:', 4]))
                df2.loc['Overall Average:'] = df2.loc['Overall Average:'].apply(lambda x: str(Decimal(x).quantize(Decimal('1e-2'), ROUND_HALF_UP)))
                df2.iloc[1:,0] = df2.iloc[1:, 0].apply(lambda x: str(Decimal(x).quantize(Decimal('1e-2'), ROUND_HALF_UP)))
                # print(df2)

            # for _, row in df2.iloc[1:-1,:].iterrows():
            #     print('index: ', _, '\nrow:', row)
            #     print(row[0])


            del df['initials']
            df.drop(df.tail(1).index,inplace=True)

            colNum = 3

            df['respondentAverage'] = df.apply(lambda row: row.mean(), axis=1)
            # print(df)
            # writer = pd.ExcelWriter('skills.xlsx')
            # df.to_excel(writer)
            # writer.save()
            for i, r in df.iterrows():
                if i == len(df) - 1:
                    worksheet.write(number_of_skills_matrix_questions+3, colNum, str(Decimal('{}'.format(r['respondentAverage'].mean())).quantize(Decimal('1e-2'), ROUND_HALF_UP)), top_right_bold_center_fmt)
                else:
                    worksheet.write(number_of_skills_matrix_questions+3, colNum, str(Decimal('{}'.format(r['respondentAverage'].mean())).quantize(Decimal('1e-2'), ROUND_HALF_UP)), top_bold_fmt)
                colNum += 1

            blank_left_bottom = self.workbook.add_format({"font_name": "Garamond", 'left': 1, 'bottom': 1})
            blank_left_right_bottom = self.workbook.add_format({"font_name": "Garamond", 'left': 1, 'bottom': 1, 'right': 1})
            blank_bottom_right_bold = self.workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'bottom': 1, 'right': 1})
            worksheet.write(number_of_skills_matrix_questions + 4, 1, '', blank_left_bottom)
            worksheet.write(number_of_skills_matrix_questions + 4, 2, '', blank_left_right_bottom)
            worksheet.merge_range(number_of_skills_matrix_questions + 4, 3, number_of_skills_matrix_questions + 4, 2 + number_of_skills_matrix_respondents, 'Total Expertise Average by Directors', blank_bottom_right_bold)


        elif sheetName == 'Report':
            df.fillna('', inplace=True)
            fmt = self.workbook.add_format({'font_name': 'Garamond',
                                                    'font_size': 12})
            fmt_center = self.workbook.add_format({'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True})
            right_border = self.workbook.add_format({'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True,
                                                    'right': 1})
            # Identify the columns we want to center_across, and where we want the
            # right border line to go
            str_columns = [str(x) for x in range(4, 5 + self.number_of_respondents)]
            # print(df)
            for col in df.columns:
                if col == str(self.number_of_respondents + 3):
                    # print(df[col])
                    worksheet.write_column(0, colNum, df[col], right_border)
                    colNum += 1
                elif col in str_columns:
                    worksheet.write_column(0, colNum, df[col], fmt_center)
                    colNum += 1
                else:
                    worksheet.write_column(0, colNum, df[col], fmt)
                    colNum += 1

            # Get min and max average rating for each question in a section,
            # Get the lowest and highest absolute response in each section
            sectionMinsAndMaxes = {}
            questionAveragesBySectionMinsAndMaxes = {}
            for section in self.questions_df['section'].unique():
                questions_sub_frame = self.questions_df[(self.questions_df['section']==section) & (self.questions_df['type'].isin(['likert', 'matrix']))]
                raw_questions_list = questions_sub_frame['question_raw'].tolist()
                sub_frame = self.df[raw_questions_list]

                # If only long-form questions
                if sub_frame.empty:
                    # print('subframe empty')
                    self.section_data[section] = 'These open-ended questions provide Board members with an opportunity to openly and anonymously comment on the Board’s competency and overall performance.'
                    continue
                # try:
                sectionMinsAndMaxes[section] = {'min': int(np.nanmin(sub_frame.values)), 'max': int(np.nanmax(sub_frame.values))}
                # except:
                #     print('This error is being thrown most likely because a question has been assigned an incorrect question type.\nSee the dataframe below to debug:\n')
                #     print(sub_frame)
                #     pass
                means = []
                # for index, row in sub_frame.loc[:, '1':].iterrows():
                #     means.append(row.mean())
                for col in sub_frame:
                    means.append(np.mean(sub_frame[col]))


                questionAveragesBySectionMinsAndMaxes[section] = {'min': str(Decimal('{}'.format(min(means))).quantize(Decimal('1e-2'), ROUND_HALF_UP)), 'max': str(Decimal('{}'.format(max(means))).quantize(Decimal('1e-2'), ROUND_HALF_UP))}
            report_format = self.workbook.add_format({'bold': True,
                                                    'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True,
                                                    'bottom': 1,
                                                    'right': 1})
            underline = self.workbook.add_format({'bold': True,
                                                    'font_name': 'Garamond',
                                                    'font_size': 12,
                                                    'center_across': True,
                                                    'bottom': 1})
            for rowIndex, section in zip(self.row_count_of_sections, self.survey_object['section'].unique()):
                sub_frame = self.survey_object[(self.survey_object['section']==section) & (self.survey_object['question_type'].isin(['likert', 'matrix']))]
                sub_frame.loc[:,'1':] = sub_frame.loc[:,'1':].apply(pd.to_numeric)
                if sub_frame.empty:
                    continue
                worksheet.merge_range(rowIndex - 1, 4, rowIndex - 1, 3 + self.number_of_respondents, 'Individual Ratings', report_format)
                worksheet.write(rowIndex - 1, 4 + self.number_of_respondents, 'Average', underline)
                formattedText = 'The average rating for this section is {}. The average rating for each question in the section ranged from {} to {}. Individual ratings ranged from {} to {}.'
                equal_formattedText = 'The average rating for each question and this section is {}. Individual ratings ranged from {} to {}.'
                if questionAveragesBySectionMinsAndMaxes.get(section).get('max') == questionAveragesBySectionMinsAndMaxes.get(section).get('min') and (questionAveragesBySectionMinsAndMaxes.get(section).get('max') == self.averages.get(section)):
                    self.section_data[section] = equal_formattedText.format(self.averages.get(section),
                    sectionMinsAndMaxes.get(section).get('min'),
                    sectionMinsAndMaxes.get(section).get('max'))

                    worksheet.write(rowIndex - 1, 5 + self.number_of_respondents, equal_formattedText.format(self.averages.get(section),
                    sectionMinsAndMaxes.get(section).get('min'),
                    sectionMinsAndMaxes.get(section).get('max')), fmt)
                else:
                    self.section_data[section] = formattedText.format(self.averages.get(section),
                        questionAveragesBySectionMinsAndMaxes.get(section).get('min'),
                        questionAveragesBySectionMinsAndMaxes.get(section).get('max'),
                        sectionMinsAndMaxes.get(section).get('min'),
                        sectionMinsAndMaxes.get(section).get('max'))

                    worksheet.write(rowIndex - 1, 5 + self.number_of_respondents, formattedText.format(self.averages.get(section),
                        questionAveragesBySectionMinsAndMaxes.get(section).get('min'),
                        questionAveragesBySectionMinsAndMaxes.get(section).get('max'),
                        sectionMinsAndMaxes.get(section).get('min'),
                        sectionMinsAndMaxes.get(section).get('max')), fmt)

            self.questionAveragesBySectionMinsAndMaxes = questionAveragesBySectionMinsAndMaxes
            self.sectionMinsAndMaxes = sectionMinsAndMaxes
            # print(self.averages)

    def bar_chart(self):
        chart = self.survey_object.copy()
        chart = chart[chart['question_type'].isin(['likert', 'matrix'])]
        chart.loc[:,'1':] = chart.loc[:,'1':].apply(pd.to_numeric)
        worksheet = self.workbook.add_worksheet('Bar Chart')

        del chart['number']
        del chart['sub_section']
        del chart['question_type']
        del chart['question']

        min_by_user = chart.groupby('section').min()
        min_by_user = min_by_user.min(axis=1)
        min_by_user = pd.DataFrame(min_by_user)
        min_by_user.reset_index(inplace=True)
        min_by_user.columns = ['Section', 'Low']
        max_by_user = chart.groupby('section').max()
        max_by_user = max_by_user.max(axis=1)
        max_by_user = pd.DataFrame(max_by_user)
        max_by_user.reset_index(inplace=True)
        max_by_user.columns = ['Section', 'High']

        chart = pd.DataFrame(list(self.averages.items()), columns=['Section', 'Averages'])
        chart = chart[chart['Averages']!='nan']

        chart['Section'] = chart.apply(lambda row: row['Section'].replace(u'\xa0', u' '), axis=1)
        # chart['Section'] = chart.apply(lambda row: row['Section'].replace('&#8217;', '&rsquo;'), axis=1)
        chart['Averages'] = chart['Averages'].astype(float)
        min_by_user['Section'] = min_by_user.apply(lambda row: row['Section'].replace(u'\xa0', u' '), axis=1)
        max_by_user['Section'] = max_by_user.apply(lambda row: row['Section'].replace(u'\xa0', u' '), axis=1)
        chart = chart.merge(min_by_user, how='outer', on='Section')
        chart = chart.merge(max_by_user, how='outer', on='Section')

        text = 'Range of Ratings'
        chart = chart.reindex(index=chart.index[::-1])

        # Global font
        fmt = self.workbook.add_format({"font_name": "Garamond", 'font_size': 12})
        bold_fmt = self.workbook.add_format({"font_name": "Garamond", 'bold': 1, 'font_size': 12})

        worksheet.write_row('A2', chart.columns, bold_fmt)
        worksheet.write_column('A3', chart['Section'], bold_fmt)
        worksheet.write_column('B3', chart['Averages'], bold_fmt)
        worksheet.write_column('C3', chart['Low'], bold_fmt)
        worksheet.write_column('D3', chart['High'], bold_fmt)

        worksheet.write(0, 2, text)
        worksheet.merge_range('C1:D1', text, fmt)

        chart1 = self.workbook.add_chart({'type': 'bar', 'name_font': {'font_name': 'Garamond', 'bold': True}})

        if self.five_point_scale == False:
            chart1.set_x_axis({'min': 1.00, 'max': 10.00, 'major_unit': 1, 'num_font':  {'name': 'Garamond', 'size': 16}, 'major_gridlines': {'visible': True, 'line': {'width': 1, 'color': '#D9D9D9'}}})
            # chart1.set_y_axis({'num_font':  {'name': 'Garamond'}})
        else:
            chart1.set_x_axis({'min': 1.00, 'max': 5.00, 'major_unit': 1, 'num_font':  {'name': 'Garamond', 'size': 16}, 'major_gridlines': {'visible': True, 'line': {'width': 1, 'color': '#D9D9D9'}}})
            # chart1.set_y_axis({'num_font':  {'name': 'Garamond'}})
        chart1.set_y_axis({'major_gridlines': {'visible': True, 'line': {'width': 1, 'color': '#D9D9D9'}},
            'num_font': {'name': 'Garamond', 'bold': True, 'size': 24}
        })
        chart1.add_series({
            'name': ['Bar Chart', 0, 2],
            'categories': ['Bar Chart', 2, 0, len(chart)+1, 0],
            'values': ['Bar Chart', 2, 1, len(chart)+1, 1],
            'fill': {'color': '#BE6E28'},
            'data_labels': {'value': True, 'position': 'inside_end', 'font': {'name': 'Garamond', 'bold': True, 'size': 28}},
            'gap': 50,
            'name_font': {'font_name': 'Garamond', 'bold': True}
        })
        chart1.set_legend({'none': True})
        chart1.set_title({'none': True})
        chart1.set_chartarea({'border': {'none': True}})
        worksheet.insert_chart('G2', chart1, {'x_offset': 25, 'y_offset': 10, 'x_scale': 3.4, 'y_scale': 3})

        ####################
        # print(chart)
        chart_height = min([10, (2*len(chart['Section']))])
        fig = plt.figure(figsize=(16, chart_height))
        ax = fig.add_subplot(1,1,1)
        # ax.yaxis.grid(color='gray', linestyle='solid')
        ax.xaxis.grid(color='gray', linestyle='solid')
        ax.set_axisbelow(True)

        ax.set_xlim([1,10])
        plt.xticks(fontname='Garamond')
        plt.yticks(fontname='Garamond', weight='bold', size=28)

        if self.five_point_scale == False:
            ax.set_xlim([1.0,10.0])
            ax.set_xticklabels(np.arange(1.00, 11.00, 1))
        else:
            ax.set_xticklabels(np.arange(1.00, 6.00, 1))
            ax.set_xlim([1.0,5.0])

        ax.set_xticklabels(ax.get_xticks(), weight='bold', size=22, fontname='Garamond')
        chart['Section'] = chart.apply(lambda row: row['Section'].replace('&#8217;', "'"), axis=1)
        # print('\n\n', chart, '\n\n')
#        chart = chart.reindex(index=chart.index[::-1])
        # print('\n\n', chart, '\n\n')
        sections = chart['Section'].tolist()

        # print('sections: ', sections)
        averages = chart['Averages'].tolist()
        # print('averages: ', averages)


        y = np.arange(len(sections))
        ax.barh(y, averages, color='#BE6E28')
        ax.set_yticks(y)
        ax.set_yticklabels(sections)
#        chart.to_pickle("./bar_chart_section_reversed.pkl")    
        ax.barh(y, averages, color='#BE6E28')

        for a, b in enumerate(chart['Averages']):
            if b in [10.0, 10.00, 10]:
                plt.text(b-.65, a-.1, str(Decimal('{}'.format(b)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), **{'fontname':'Garamond', 'fontsize': 24, 'weight':'bold', 'size':28})
            else:
                plt.text(b-.5, a-.1, str(Decimal('{}'.format(b)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), **{'fontname':'Garamond', 'fontsize': 24, 'weight':'bold', 'size':28})

        if self.year:
            plt.savefig(os.path.join(self.app.config['CHARTS'], '{}_cgpg_bar_chart.png'.format(self.year)), bbox_inches='tight', dpi=300)
        else:
            plt.savefig(os.path.join(self.app.config['CHARTS'], 'cgpg_bar_chart.png'), bbox_inches='tight', dpi=300)
    def hi_lo(self):
        worksheet = self.workbook.add_worksheet('Hi-Lo')
        hilo = self.survey_object.copy()

        worksheet.set_column(1,1 ,6)
        worksheet.set_column(2,2 ,22)
        worksheet.set_column(3,3 ,4)
        worksheet.set_column(4,4 ,60)

        hilo = hilo[hilo['question_type'].isin(['likert', 'matrix'])]
        pd.options.mode.chained_assignment = None
        raw_answers = hilo.loc[:,'1':]
        raw_answers = raw_answers.apply(pd.to_numeric)
        hilo['question_average'] = raw_answers.apply(lambda row: row.mean(), axis=1)
        # hi = hilo.nlargest(5, 'question_average', keep='all')
        # lo = hilo.nsmallest(5, 'question_average', keep='all')
        #
        # jinja_hi = hilo.nlargest(5, 'question_average', keep='first')
        # jinja_lo = hilo.nsmallest(5, 'question_average', keep='first')

        if len(self.questions_df.drop_duplicates(['section','number']).index) < 10:
            number_for_hi_and_low = math.floor(len(self.questions_df.drop_duplicates(['section','number']).index)/2)
            hi = hilo.nlargest(number_for_hi_and_low, 'question_average', keep='all')
            lo = hilo.nsmallest(number_for_hi_and_low, 'question_average', keep='all')

            jinja_hi = hilo.nlargest(number_for_hi_and_low, 'question_average', keep='first')
            jinja_lo = hilo.nsmallest(number_for_hi_and_low, 'question_average', keep='first')
        else:
            hi = hilo.nlargest(5, 'question_average', keep='all')
            lo = hilo.nsmallest(5, 'question_average', keep='all')

            jinja_hi = hilo.nlargest(5, 'question_average', keep='first')
            jinja_lo = hilo.nsmallest(5, 'question_average', keep='first')

        hi['question_average'] = hi.apply(lambda row: str(Decimal('{}'.format(row['question_average'])).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)
        lo['question_average'] = lo.apply(lambda row: str(Decimal('{}'.format(row['question_average'])).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)
        hi['section'] = hi['section'].str.extract(r'\b([IVX]+\.)?')
        lo['section'] = lo['section'].str.extract(r'\b([IVX]+\.)?')

        jinja_hi['question_average'] = jinja_hi.apply(lambda row: str(Decimal(row['question_average']).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)
        jinja_lo['question_average'] = jinja_lo.apply(lambda row: str(Decimal(row['question_average']).quantize(Decimal('1e-2'), ROUND_HALF_UP)), axis =1)

        hi_header = self.workbook.add_format({"font_name": "Garamond",
                                         'bold': 2,
                                         'bg_color': '#008000',
                                         'font_color': '#FFFFFF',
                                         'align': 'center',
                                         'border': 1,
                                         'border_color': '#FFFFFF',
                                         'font_size': 12})
        hi_header_left_align = self.workbook.add_format({"font_name": "Garamond",
                                         'bold': 1,
                                         'bg_color': '#008000',
                                         'font_color': '#FFFFFF',
                                         'align': 'left',
                                         'border': 1,
                                         'border_color': '#FFFFFF',
                                         'font_size': 12})
        cell_fmt = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#F3F3F3',
                                           'align': 'left',
                                           'border': 1,
                                           'border_color': '#FFFFFF',
                                           'valign': 'top',
                                           'text_wrap': True,
                                           'font_size': 12})
        cell_fmt_centered = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#F3F3F3',
                                           'align': 'center',
                                           'border': 1,
                                           'border_color': '#FFFFFF',
                                           'valign': 'top',
                                           'font_size': 12})

        worksheet.merge_range('B2:E2', 'Highest Rated Questions', hi_header)
        worksheet.write('B3', 'Rating', hi_header)
        worksheet.write('C3', 'Section', hi_header)
        worksheet.merge_range('D3:E3', 'Question', hi_header_left_align)

        ###############
        hi['formatted_number'] = hi['number'] + hi['sub_section']
        jinja_hi['formatted_number'] = jinja_hi['number'] + jinja_hi['sub_section']
        ###############

        worksheet.write_column('B4', hi['question_average'], cell_fmt_centered)
        worksheet.write_column('C4', hi['section'], cell_fmt)

        ###############
        hi['formatted_number'] = hi['formatted_number'].str.replace(r'\.', r'')
        hi['formatted_number'] += '.'

        jinja_hi['formatted_number'] = jinja_hi['formatted_number'].str.replace(r'\.', r'')
        jinja_hi['formatted_number'] += '.'
        ###############

        worksheet.write_column('D4', hi['formatted_number'], cell_fmt_centered)
        worksheet.write_column('E4', hi['question'], cell_fmt)

        lo_header = self.workbook.add_format({"font_name": "Garamond",
                                         'bold': 1,
                                         'bg_color': '#FFFF00',
                                         'align': 'center',
                                         'border': 1,
                                         'border_color': '#FFFFFF',
                                         'font_size': 12})
        lo_header_left_align = self.workbook.add_format({"font_name": "Garamond",
                                         'bold': 1,
                                         'bg_color': '#FFFF00',
                                         'align': 'left',
                                         'border': 1,
                                         'border_color': '#FFFFFF',
                                         'font_size': 12})

        worksheet.merge_range('B{0}:E{0}'.format(int(4 + len(hi))), 'Lowest Rated Questions', lo_header)
        worksheet.write('B{}'.format(int(5 + len(hi))), 'Rating', lo_header)
        worksheet.write('C{}'.format(int(5 + len(hi))), 'Section', lo_header)
        worksheet.merge_range('D{0}:E{0}'.format(int(5 + len(hi))), 'Question', lo_header_left_align)

        ###############
        lo['formatted_number'] = lo['number'] + lo['sub_section']
        jinja_lo['formatted_number'] = jinja_lo['number'] + jinja_lo['sub_section']
        ###############

        worksheet.write_column('B{}'.format(int(6 + len(hi))), lo['question_average'], cell_fmt_centered)
        worksheet.write_column('C{}'.format(int(6 + len(hi))), lo['section'], cell_fmt)

        ###############
        lo['formatted_number'] = lo['formatted_number'].str.replace(r'\.', r'')
        lo['formatted_number'] += '.'

        jinja_lo['formatted_number'] = jinja_lo['formatted_number'].str.replace(r'\.', r'')
        jinja_lo['formatted_number'] += '.'
        ###############

        worksheet.write_column('D{}'.format(int(6 + len(hi))), lo['formatted_number'], cell_fmt_centered)
        worksheet.write_column('E{}'.format(int(6 + len(hi))), lo['question'], cell_fmt)

        jinja_hi['section'] = jinja_hi['section'].str.extract(r'\b([IVX]+\.)?')
        jinja_lo['section'] = jinja_lo['section'].str.extract(r'\b([IVX]+\.)?')

        self.jinja_hi = jinja_hi
        self.jinja_lo = jinja_lo
        return

    def eSigma(self):
        eS = pd.DataFrame(list(self.averages.items()), columns=['Section', 'Averages'])
        eS['Section'] = eS.apply(lambda row: row['Section'].replace(u'\xa0', u' '), axis=1)
        eS['Averages'] = eS['Averages'].astype(float)
        average_of_section_averages = eS['Averages'].mean()
        qa = self.survey_object.copy()
        qa = qa[qa['question_type'].isin(['likert', 'matrix'])]
        qa = qa.loc[:,'1':].apply(pd.to_numeric)
        qa['average'] = qa.loc[:,'1':].apply(lambda row: row.mean(), axis=1)
        average_of_question_averages = qa['average'].mean()
        # This is a convient place to calculate part of the section analysis
        self.average_total_rating_given = qa['average'].sum()
        #
        eSigma = (average_of_question_averages + average_of_section_averages) / 2
        return str(Decimal('{}'.format(eSigma)).quantize(Decimal('1e-2'), ROUND_HALF_UP))

    def aggregated_section_analysis(self):
        worksheet = self.workbook.add_worksheet('Section Analysis')

        sec = self.survey_object.copy()
        header = self.workbook.add_format({"font_name": "Garamond",
                                         'bg_color': '#c4590f',
                                         'font_color': '#FFFFFF',
                                         'align': 'center',
                                         'font_size': 12})
        header_left_align = self.workbook.add_format({"font_name": "Garamond",
                                         'bg_color': '#c4590f',
                                         'font_color': '#FFFFFF',
                                         'align': 'left',
                                         'bottom': 1,
                                         'left': 1,
                                         'top': 1,
                                         'border_color': '#c4590f',
                                         'font_size': 12})
        cell_fmt1 = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#fbe4d5',
                                           'align': 'left',
                                           'bottom': 1,
                                           'left': 1,
                                           'top': 1,
                                           'border_color': '#f5b083',
                                           'font_size': 12})
        cell_fmt2 = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#FFFFFF',
                                           'align': 'left',
                                           'bottom': 1,
                                           'left': 1,
                                           'top': 1,
                                           'border_color': '#f5b083',
                                           'font_size': 12})
        cell_fmt1_centered = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#fbe4d5',
                                           'align': 'center',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083',
                                           'font_size': 12})
        cell_fmt2_centered = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#FFFFFF',
                                           'align': 'center',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083',
                                           'font_size': 12})
        cell_fmt1_right = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#fbe4d5',
                                           'align': 'right',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083',
                                           'font_size': 12})
        cell_fmt2_right = self.workbook.add_format({"font_name": "Garamond",
                                           'bg_color': '#FFFFFF',
                                           'align': 'right',
                                           'bottom': 1,
                                           'right': 1,
                                           'top': 1,
                                           'border_color': '#f5b083',
                                           'font_size': 12})
        cell_bold = self.workbook.add_format({"font_name": "Garamond",
                                           'align': 'right',
                                           'bold': 1,
                                           'font_size': 12})
        cell_bold_left = self.workbook.add_format({"font_name": "Garamond",
                                           'align': 'left',
                                           'border': 1,
                                           'bold': 1,
                                           'border_color': '#f5b083',
                                           'font_size': 12})
        orange_border_orange_bg = self.workbook.add_format({'border': 1,
                                                'border_color': '#f5b083',
                                                "font_name": "Garamond",
                                                'bg_color': '#fbe4d5',
                                                'align': 'center',
                                                'font_size': 12
                                                })
        orange_border_white_bg = self.workbook.add_format({'border': 1,
                                                'border_color': '#f5b083',
                                                "font_name": "Garamond",
                                                'bg_color': '#FFFFFF',
                                                'align': 'center',
                                                'font_size': 12
                                                })
        orange_border = self.workbook.add_format({'border': 1, 'border_color': '#f5b083'})

        worksheet.write('A1', 'Aggregated Section Analysis', header_left_align)
        worksheet.write('B1', 'Average', header)

        averages = pd.DataFrame(list(self.averages.items()), columns=['Section', 'Averages'])
        averages = averages[averages['Averages']!='nan']
        averages['Section'] = averages.apply(lambda row: row['Section'].replace(u'\xa0', u' '), axis=1)
        currentRow = 2
        for index, row in averages.iterrows():
            if index % 2 == 0:
                worksheet.write(r'A{}'.format(index+2), row['Section'], cell_fmt1)
                worksheet.write(r'B{}'.format(index+2), str(row['Averages']), cell_fmt1_centered)
            else:
                worksheet.write(r'A{}'.format(index+2), row['Section'], cell_fmt2)
                worksheet.write(r'B{}'.format(index+2), str(row['Averages']), cell_fmt2_centered)
            currentRow += 1

        eSigma_value = self.eSigma()
        self.eSigma_value = eSigma_value

        currentRow += 2
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'Evaluation Statistics', header_left_align)
        currentRow += 1

        # Assessment Statistics
        worksheet.write('A{}'.format(currentRow), 'Number of rated questions', cell_fmt1)
        worksheet.write('B{}'.format(currentRow), len(sec[sec['question_type'].isin(['likert', 'matrix'])]), cell_fmt1_right)
        self.eval_stats_list_of_dicts.append({'Number of rated questions': len(sec[sec['question_type'].isin(['likert', 'matrix'])])})
        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Number of open-ended questions', cell_fmt2)
        worksheet.write('B{}'.format(currentRow), len(sec[sec['question_type'].isin(['long_form'])]), cell_fmt2_right)
        self.eval_stats_list_of_dicts.append({'Number of open-ended questions': len(sec[sec['question_type'].isin(['long_form'])])})
        currentRow += 1
        if self.report_type != 'BEA':
            worksheet.write('A{}'.format(currentRow), 'Number of Committee members surveyed', cell_fmt1)
            self.eval_stats_list_of_dicts.append({'Number of Committee members surveyed': self.number_of_respondents})
        else:
            worksheet.write('A{}'.format(currentRow), 'Number of Board members surveyed', cell_fmt1)
            self.eval_stats_list_of_dicts.append({'Number of Board members surveyed': self.number_of_respondents})
        worksheet.write('B{}'.format(currentRow), self.number_of_respondents, cell_fmt1_right)
        currentRow += 1

        worksheet.write('A{}'.format(currentRow), 'Percentage participation', cell_fmt2)
        # likert_matrix_questions = self.questions_df[self.questions_df['type'].isin(['likert', 'matrix'])]
        # print(self.df[likert_matrix_questions['question_raw'].tolist()].apply(pd.value_counts))

        # Calculate N/A statistics
        raw_answers = sec[sec['question_type'].isin(['likert', 'matrix'])]
        # raw_answers.to_excel('raw_answers.xlsx')
        raw_answers = raw_answers.loc[:,'1':]

        number_of_empty_strings = 0
        number_of_non_responses = 0
        for q in self.questions_df['question_raw'].tolist():
            number_of_non_responses += self.na_no_null_dict.get('na').get(q, 0)
            number_of_non_responses += self.na_no_null_dict.get('no').get(q, 0)
            number_of_empty_strings += self.na_no_null_dict.get('null').get(q, 0)



        # print('number_of_non_responses: ', number_of_non_responses)
        # print('number_of_empty_strings', number_of_empty_strings)
        len_index_times_len_columns = (len(raw_answers.index) * len(raw_answers.columns))
        # print('len_index_times_len_columns: ', len_index_times_len_columns)
        number_of_eligible_responses = len_index_times_len_columns - number_of_empty_strings
        # print('number_of_eligible_responses', number_of_eligible_responses)
        number_of_questions_answered = number_of_eligible_responses
        # number_of_questions_answered = number_of_eligible_responses - number_of_non_responses
        # print('number_of_questions_answered', number_of_questions_answered)
        percent_participation = ((number_of_questions_answered) / number_of_eligible_responses) * 100


        worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(percent_participation)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
        percent_participation_formatted = "{0:.2f}%".format(Decimal('{}'.format(percent_participation)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
        if percent_participation_formatted == '100.00%':
            self.eval_stats_list_of_dicts.append({'Percentage participation': "100%"})
        elif percent_participation_formatted == '0.00%':
            self.eval_stats_list_of_dicts.append({'Percentage participation': "0%"})
        else:
            self.eval_stats_list_of_dicts.append({'Percentage participation': percent_participation_formatted})
        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Total number of responses', cell_fmt1)
        worksheet.write('B{}'.format(currentRow), number_of_questions_answered, cell_fmt1_right)
        self.eval_stats_list_of_dicts.append({'Total number of responses': number_of_questions_answered})
        currentRow += 1
        if self.report_type != 'BEA':
            worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'Question analysis by Committee member response:', cell_bold_left)
            self.eval_stats_list_of_dicts.append({'Question analysis by Committee member response:': ''})
        else:
            worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'Question analysis by Board member response:', cell_bold_left)
            self.eval_stats_list_of_dicts.append({'Question analysis by Board member response:': ''})
        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Maximum possible total rating', cell_fmt1)
        if self.five_point_scale == False:
            worksheet.write('B{}'.format(currentRow), len(raw_answers)*10, cell_fmt1_right)
            max_rating = len(raw_answers)*10
        else:
            worksheet.write('B{}'.format(currentRow), len(raw_answers)*5, cell_fmt1_right)
            max_rating = len(raw_answers)*5
        self.eval_stats_list_of_dicts.append({'Maximum possible total rating': max_rating})
        currentRow += 1

        # print(raw_answers.sum(axis=0))
        # print(raw_answers.sum(axis=0).sum())
        # print(len(raw_answers.columns))
        # print('\n\nHERE:\n', (raw_answers.sum(axis=0).sum())/len(raw_answers.columns))


        worksheet.write('A{}'.format(currentRow), 'Average total rating given', cell_fmt2)
        worksheet.write('B{}'.format(currentRow), str(Decimal('{}'.format((raw_answers.sum(axis=0).sum())/len(raw_answers.columns))).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
        self.eval_stats_list_of_dicts.append({'Average total rating given': str(Decimal('{}'.format((raw_answers.sum(axis=0).sum())/len(raw_answers.columns))).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
        currentRow += 1



        worksheet.write('A{}'.format(currentRow), 'Highest total rating given', cell_fmt1)
        check = 0
        raw_answers = raw_answers.apply(pd.to_numeric)
        for c in raw_answers.columns:
            if raw_answers[c].sum()>check:
                check = raw_answers[c].sum()
        worksheet.write('B{}'.format(currentRow), int(check), cell_fmt1_right)
        self.eval_stats_list_of_dicts.append({'Highest total rating given': int(check)})
        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Lowest total rating given', cell_fmt2)
        check = len(sec[sec['question_type'].isin(['likert', 'matrix'])])*10
        for c in raw_answers.columns:
            if raw_answers[c].sum()<check:
                check = raw_answers[c].sum()
        worksheet.write('B{}'.format(currentRow), int(check), cell_fmt2_right)
        self.eval_stats_list_of_dicts.append({'Lowest total rating given': int(check)})
        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Maximum possible average', cell_fmt1)
        if self.five_point_scale == False:
            worksheet.write('B{}'.format(currentRow), '10.00', cell_fmt1_right)
            scale_value = '10.00'
        else:
            worksheet.write('B{}'.format(currentRow), '5.00', cell_fmt1_right)
            scale_value = '5.00'
        self.eval_stats_list_of_dicts.append({'Maximum possible average': scale_value})

        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Highest rating average given', cell_fmt2)
        check = 0
        for c in raw_answers.columns:
            if raw_answers[c].mean()>check:
                check = raw_answers[c].mean()
        worksheet.write('B{}'.format(currentRow), str(Decimal('{}'.format(check)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
        self.eval_stats_list_of_dicts.append({'Highest rating average given': str(Decimal('{}'.format(check)).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
        currentRow += 1
        worksheet.write('A{}'.format(currentRow), 'Lowest rating average given', cell_fmt1)
        check = 10
        for c in raw_answers.columns:
            if raw_answers[c].mean()<check:
                check = raw_answers[c].mean()
        worksheet.write('B{}'.format(currentRow), str(Decimal('{}'.format(check)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
        self.eval_stats_list_of_dicts.append({'Lowest rating average given': str(Decimal('{}'.format(check)).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
        currentRow += 1
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'Group response analysis:', cell_bold_left)
        self.eval_stats_list_of_dicts.append({'Group response analysis:': ''})
        # This section needs try, except clauses for the case that value counts returns a zero count for whole range of answers
        # Create one series from the raw answers to use value_counts function
        series_of_all_answers = pd.Series(dtype='float')
        for c in raw_answers.columns:
            series_of_all_answers = series_of_all_answers.append(raw_answers[c], ignore_index=True)
        counts = raw_answers.apply(pd.value_counts)
        counts.fillna(0, inplace=True)
        if self.five_point_scale == False:
            new_index = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        else:
            new_index = [1, 2, 3, 4, 5]
        counts = counts.reindex(new_index, fill_value=0)
        currentRow += 1

        if self.five_point_scale == False:
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 10', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format((counts.loc[10].sum()/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)

            percentage_of_ten_ratings_formatted = "{0:.2f}%".format(Decimal('{}'.format((counts.loc[10].sum()/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if percentage_of_ten_ratings_formatted == '100.00%':
                percentage_of_ten_ratings_formatted = '100%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 10': percentage_of_ten_ratings_formatted})
            if percentage_of_ten_ratings_formatted == '0.00%':
                percentage_of_ten_ratings_formatted = '0%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 10': percentage_of_ten_ratings_formatted})
            else:
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 10': percentage_of_ten_ratings_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 8 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            eight_or_above_formatted = "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if eight_or_above_formatted == '100.00%':
                eight_or_above_formatted = '100%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 8 or above': eight_or_above_formatted})
            elif eight_or_above_formatted == '0.00%':
                eight_or_above_formatted = '0%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 8 or above': eight_or_above_formatted})
            else:
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 8 or above': eight_or_above_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 7 or below', cell_fmt1)

            # seven_or_below = number_of_questions_answered - (counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())
            seven_or_below = int(counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum() + counts.loc[4].sum() + counts.loc[5].sum() + counts.loc[6].sum() + counts.loc[7].sum())

            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format((seven_or_below/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            percentage_seven_below_formatted = "{0:.2f}%".format(Decimal('{}'.format((seven_or_below/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if percentage_seven_below_formatted == '100.00%':
                percentage_seven_below_formatted = '100%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 7 or below': percentage_seven_below_formatted})
            elif percentage_seven_below_formatted == '0.00%':
                percentage_seven_below_formatted = '0%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 7 or below': percentage_seven_below_formatted})
            else:
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 7 or below': percentage_seven_below_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 3 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            percentage_three_below_formatted = "{0:.2f}%".format(Decimal('{}'.format(((counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())/number_of_questions_answered)*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if percentage_three_below_formatted == '100.00%':
                percentage_three_below_formatted = '100%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted})
            elif percentage_three_below_formatted == '0.00%':
                percentage_three_below_formatted = '0%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted})
            else:
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted})
            currentRow += 1
            if number_of_non_responses:
                x = cell_fmt2
                cell_fmt2 = cell_fmt1
                cell_fmt1 = x
                x = cell_fmt2_right
                cell_fmt2_right = cell_fmt1_right
                cell_fmt1_right = x
                worksheet.write('A{}'.format(currentRow), 'Percent of ratings of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((number_of_non_responses)/(number_of_questions_answered))*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
                self.eval_stats_list_of_dicts.append({'Percent of ratings of N/A': "{0:.2f}%".format(Decimal('{}'.format(((number_of_non_responses)/(number_of_questions_answered))*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
                currentRow += 1

            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 10', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), counts.loc[10].sum(), cell_fmt1_right)
            self.eval_stats_list_of_dicts.append({'Number of ratings of 10': int(counts.loc[10].sum())})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 8 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum()), cell_fmt2_right)
            self.eval_stats_list_of_dicts.append({'Number of ratings of 8 or above': int(counts.loc[8].sum() + counts.loc[9].sum() + counts.loc[10].sum())})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 7 or below', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), seven_or_below, cell_fmt1_right)
            self.eval_stats_list_of_dicts.append({'Number of ratings of 7 or below': int(seven_or_below)})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 3 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), (counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum()), cell_fmt2_right)
            self.eval_stats_list_of_dicts.append({'Number of ratings of 3 or below': int(counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())})
            currentRow += 1
            if number_of_non_responses:
                worksheet.write('A{}'.format(currentRow), 'Number of ratings of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), str(int(number_of_non_responses)), cell_fmt2_right)
                self.eval_stats_list_of_dicts.append({'Number of ratings of N/A': str(int(number_of_non_responses))})
                currentRow += 1
        else:
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 5', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal((counts.loc[5].sum()/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            percentage_five_formatted = "{0:.2f}%".format(Decimal((counts.loc[5].sum()/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if percentage_five_formatted == '100.00%':
                percentage_five_formatted = '100%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 5': percentage_five_formatted})
            elif percentage_five_formatted == '0.00%':
                percentage_five_formatted = '0%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 5': percentage_five_formatted})
            else:
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 5': percentage_five_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 4 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal(((counts.loc[4].sum() + counts.loc[5].sum())/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            percentage_four_above_formatted = "{0:.2f}%".format(Decimal(((counts.loc[4].sum() + counts.loc[5].sum())/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if percentage_four_above_formatted == '100.00%':
                percentage_four_above_formatted = '100%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 4 or above': percentage_four_above_formatted})
            elif percentage_four_above_formatted == '0.00%':
                percentage_four_above_formatted = '0%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 4 or above': percentage_four_above_formatted})
            else:
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 4 or above': percentage_four_above_formatted})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 3 or below', cell_fmt1)

            three_or_below = int(counts.loc[1].sum() + counts.loc[2].sum() + counts.loc[3].sum())

            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal((three_or_below/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt1_right)
            percentage_three_below_formatted_five_scale = "{0:.2f}%".format(Decimal((three_or_below/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if percentage_three_below_formatted_five_scale == '100.00%':
                percentage_three_below_formatted_five_scale = '100%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted_five_scale})
            elif percentage_three_below_formatted_five_scale == '0.00%':
                percentage_three_below_formatted_five_scale = '0%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted_five_scale})
            else:
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 3 or below': percentage_three_below_formatted_five_scale})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Percent of ratings of 2 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal(((counts.loc[1].sum() + counts.loc[2].sum())/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
            percentage_two_below_formatted = "{0:.2f}%".format(Decimal(((counts.loc[1].sum() + counts.loc[2].sum())/number_of_questions_answered)*100).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            if percentage_two_below_formatted == '100.00%':
                percentage_two_below_formatted = '100%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 2 or below': percentage_two_below_formatted})
            elif percentage_two_below_formatted == '0.00%':
                percentage_two_below_formatted = '0%'
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 2 or below': percentage_two_below_formatted})
            else:
                self.eval_stats_list_of_dicts.append({'Percent of ratings of 2 or below': percentage_two_below_formatted})
            currentRow += 1
            if number_of_non_responses:
                x = cell_fmt2
                cell_fmt2 = cell_fmt1
                cell_fmt1 = x
                x = cell_fmt2_right
                cell_fmt2_right = cell_fmt1_right
                cell_fmt1_right = x
                worksheet.write('A{}'.format(currentRow), 'Percent of ratings of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), "{0:.2f}%".format(Decimal('{}'.format(((number_of_non_responses)/(number_of_questions_answered))*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)), cell_fmt2_right)
                self.eval_stats_list_of_dicts.append({'Percent of ratings of N/A': "{0:.2f}%".format(Decimal('{}'.format(((number_of_non_responses)/(number_of_questions_answered))*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP))})
                currentRow += 1

                # print('number_of_non_responses: ', number_of_non_responses)
                # print('number_of_questions_answered: ', number_of_questions_answered)
                # print('Percent of ratings of N/A: ', "{0:.2f}%".format(Decimal('{}'.format(((number_of_non_responses)/(number_of_questions_answered))*100)).quantize(Decimal('1e-2'), ROUND_HALF_UP)))
                # print('unformatted: ', ((number_of_non_responses)/(number_of_questions_answered)))

            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 5', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), int(counts.loc[5].sum()), cell_fmt1_right)
            self.eval_stats_list_of_dicts.append({'Number of ratings of 5': int(counts.loc[5].sum())})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 4 or above', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), int(counts.loc[4].sum() + counts.loc[5].sum()), cell_fmt2_right)
            self.eval_stats_list_of_dicts.append({'Number of ratings of 4 or above': int(counts.loc[4].sum() + counts.loc[5].sum())})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 3 or below', cell_fmt1)
            worksheet.write('B{}'.format(currentRow), int(three_or_below), cell_fmt1_right)
            self.eval_stats_list_of_dicts.append({'Number of ratings of 3 or below': int(three_or_below)})
            currentRow += 1
            worksheet.write('A{}'.format(currentRow), 'Number of ratings of 2 or below', cell_fmt2)
            worksheet.write('B{}'.format(currentRow), int(counts.loc[1].sum() + counts.loc[2].sum()), cell_fmt2_right)
            self.eval_stats_list_of_dicts.append({'Number of ratings of 2 or below': int(counts.loc[1].sum() + counts.loc[2].sum())})
            currentRow += 1
            if number_of_non_responses:
                worksheet.write('A{}'.format(currentRow), 'Number of ratings of N/A', cell_fmt2)
                worksheet.write('B{}'.format(currentRow), str(int(number_of_non_responses)), cell_fmt2_right)
                self.eval_stats_list_of_dicts.append({'Number of ratings of N/A': str(int(number_of_non_responses))})
                currentRow += 1

        currentRow += 2
        worksheet.merge_range('A{}:B{}'.format(currentRow, currentRow), 'e∑igma = {}'.format(eSigma_value), orange_border_white_bg)
        worksheet.set_column(0,0 ,75)
        worksheet.set_column(1,1,10)
