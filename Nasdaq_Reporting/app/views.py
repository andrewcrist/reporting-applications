from app import app
from flask import render_template, request, redirect, jsonify, make_response, send_from_directory, abort, session, url_for
from jinja2 import Template, Environment, PackageLoader, select_autoescape, FileSystemLoader
from datetime import datetime
import os
from werkzeug.utils import secure_filename
import secrets
import re
import zipfile
from weasyprint import HTML, CSS
from os.path import basename
import numpy as np
from decimal import Decimal, ROUND_HALF_UP
from collections import Counter, OrderedDict
from random import shuffle
import pandas as pd
import matplotlib.pyplot as plt
plt.switch_backend('agg')
from matplotlib.patches import Circle, RegularPolygon
from matplotlib.path import Path
from matplotlib.projections.polar import PolarAxes
from matplotlib.projections import register_projection
from matplotlib.spines import Spine
from matplotlib.transforms import Affine2D
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import matplotlib.colors as mc
# import seaborn as sns
from matplotlib.ticker import *
from collections import Counter
import matplotlib.transforms
import matplotlib.ticker as ticker
import warnings
import xlsxwriter

from .ppl_parser import ppl_parser
from .csv_opener import csv_opener
from .fsr_csv_opener import fsr_csv_opener

from .peer_etl import peer_splitter, peer_etl
from .peer_reports import peer_individual_reports, peer_composite_report

from .fsr_etl import fsr_etl
from .fsr_report import fsr_report

from .bea_etl import bea_splitter, bea_etl
from .bea_report import bea_report

@app.route("/")
def index():
    return render_template('public/index.html')

@app.route("/delete", methods=['POST'])
def delete_cache():
    for file in os.listdir(app.config["ZIP_OUTPUT"]):
        if file.endswith(".zip"):
            os.remove(os.path.join(app.config["ZIP_OUTPUT"], file))
    return redirect(url_for('index'))

@app.route('/bea_uploader', methods=['GET', 'POST'])
def bea_uploader():
    if request.method == 'POST':
        if request.form['submit_button'] == 'bea':
            # if request.form['clear_cache'] == 'clear_cache':
            #     for file in os.listdir(app.config["ZIP_OUTPUT"]):
            #         if file.endswith(".zip"):
            #             os.remove(os.path.join(app.config["ZIP_OUTPUT"], file))
            #     for file in os.listdir(app.config["REPORTS"]):
            #         os.remove(os.path.join(app.config["REPORTS"], file))
            #     for file in os.listdir(app.config["CHARTS"]):
            #         os.remove(os.path.join(app.config["CHARTS"], file))

            #     return render_template('public/bea.html')

            if 'bea_export' not in request.files:
                # print('no file')
                return redirect(request.url)
            bea_file = request.files['bea_export']


            # if user does not select file, browser also
            # submit a empty part without filename
            if bea_file.filename == '':
                # print('no filename')
                return redirect(request.url)
            else:
                file_name = secure_filename(bea_file.filename)
                bea_file.save(os.path.join(app.config['CSV_UPLOADS'], file_name))

                randomize_bea = request.form['randomize_bea']
                if not randomize_bea:
                    randomize_bea=False
                else:
                    randomize_bea=True

                heatmap = request.form['create_heatmap']
                if not heatmap:
                    heatmap=False
                else:
                    heatmap=True

                cgpg = request.form['create_cgpg']
                if not cgpg:
                    cgpg=False
                else:
                    cgpg=True

                web = request.form['create_web']
                if not web:
                    web=False
                else:
                    web=True

                hilo = request.form['create_hilo']
                if not hilo:
                    hilo=False
                else:
                    hilo=True

                swot = request.form['create_swot']
                if not swot:
                    swot=False
                else:
                    swot=True

                high_act_pts = request.form['create_high_act_pts']
                if not high_act_pts:
                    high_act_pts = False
                else:
                    high_act_pts = True

                cgfr_method = request.form['create_cgfr']
                if not cgfr_method:
                    cgfr_method = False
                else:
                    cgfr_method = True

                cgfr_legend = request.form['create_cgfr_legend']
                if not cgfr_legend:
                    cgfr_legend = False
                else:
                    cgfr_legend = True

                stats = request.form['create_stat']
                if not stats:
                    stats=False
                else:
                    stats=True

                column_width = request.form['create_column_width']
                if not column_width:
                    column_width = 35
                else:
                    pass

                bea = run_bea(file_name, randomize_bea, heatmap, cgpg, web, hilo, swot, high_act_pts, cgfr_method, cgfr_legend, stats, column_width)

                report_name = re.search('^(.*).csv', file_name).group(1)

                with zipfile.ZipFile(os.path.join(app.config['ZIP_OUTPUT'], '{}.zip'.format(report_name)),'w') as zip:
                    for report in bea:
                        zip.write(os.path.join(app.config['EXCEL_OUTPUT'], report), basename(report))
                    for file in os.listdir(app.config["REPORTS"]):
                        if file.endswith(".pdf") or file.endswith(".xlsx"):
                            zip.write(os.path.join(app.config['REPORTS'], file), basename(file))

                for file in os.listdir(app.config["CSV_UPLOADS"]):
                    if file.endswith(".csv"):
                        os.remove(os.path.join(app.config['CSV_UPLOADS'], file))
                for file in os.listdir(app.config["REPORTS"]):
                    if file.endswith(".pdf") or file.endswith(".xlsx"):
                        os.remove(os.path.join(app.config['REPORTS'], file))
                for file in os.listdir(app.config["CHARTS"]):
                    if file.endswith(".png"):
                        os.remove(os.path.join(app.config['CHARTS'], file))

                delete_excel_output()

                return redirect('/downloadzip/'+ '{}.zip'.format(report_name))

        elif request.form['submit_button'] == 'comparative_bea':
            # if request.form['clear_cache'] == 'clear_cache':
            #     for file in os.listdir(app.config["ZIP_OUTPUT"]):
            #         if file.endswith(".zip"):
            #             os.remove(os.path.join(app.config["ZIP_OUTPUT"], file))
            #     for file in os.listdir(app.config["REPORTS"]):
            #         os.remove(os.path.join(app.config["REPORTS"], file))
            #     for file in os.listdir(app.config["CHARTS"]):
            #         os.remove(os.path.join(app.config["CHARTS"], file))

            #     return render_template('public/bea.html')

            if 'comparative' not in request.files or 'compared' not in request.files:
                # print('no file')
                return redirect(request.url)

            comparative_file = request.files['comparative']
            compared_file = request.files['compared']

            # if user does not select file, browser also
            # submit a empty part without filename
            if comparative_file.filename == '' or compared_file.filename == '':
                # print('no filename')
                return redirect(request.url)
            else:
                comparative_file_name = secure_filename(comparative_file.filename)
                compared_file_name = secure_filename(compared_file.filename)

                comparative_file.save(os.path.join(app.config['CSV_UPLOADS'], comparative_file_name))
                compared_file.save(os.path.join(app.config['CSV_UPLOADS'], compared_file_name))

                randomize_bea = request.form['randomize_bea']
                if not randomize_bea:
                    randomize_bea=False
                else:
                    randomize_bea=True

                heatmap = request.form['create_heatmap']
                if not heatmap:
                    heatmap=False
                else:
                    heatmap=True

                cgpg = request.form['create_cgpg']
                if not cgpg:
                    cgpg=False
                else:
                    cgpg=True

                web = request.form['create_web']
                if not web:
                    web=False
                else:
                    web=True

                hilo = request.form['create_hilo']
                if not hilo:
                    hilo=False
                else:
                    hilo=True

                swot = request.form['create_swot']
                if not swot:
                    swot=False
                else:
                    swot=True

                high_act_pts = request.form['create_high_act_pts']
                if not high_act_pts:
                    high_act_pts = False
                else:
                    high_act_pts = True

                cgfr_method = request.form['create_cgfr']
                if not cgfr_method:
                    cgfr_method = False
                else:
                    cgfr_method = True

                cgfr_legend = request.form['create_cgfr_legend']
                if not cgfr_legend:
                    cgfr_legend = False
                else:
                    cgfr_legend = True

                stats = request.form['create_stat']
                if not stats:
                    stats=False
                else:
                    stats=True

                column_width = request.form['create_column_width']
                if not column_width:
                    column_width = 35
                else:
                    pass

                files = [comparative_file_name, compared_file_name]
                years = {}
                for f in files:
                    year = f[-8:-4]
                    years[int(year)] = {'file': f}

                sorted_years = OrderedDict(sorted(years.items(), reverse=True))

                current_year = max(sorted_years.keys())

                current_year_file = sorted_years[current_year].get('file')

                for year in sorted_years.keys():
                    year_path = os.path.join(app.config['CSV_UPLOADS'], sorted_years.get(year).get('file'))
                    sorted_years[year]['raw_data_dict'] = csv_opener(year_path)

                for year in sorted_years.keys():
                    sorted_years[year]['dict_of_dfs'] = bea_splitter(sorted_years.get(year).get('file'), sorted_years.get(year).get('raw_data_dict'))
                    # Check to see if a proper split has been made across years and report types
                    # print('\n\n', sorted_years[year]['dict_of_dfs'].keys())
                for year in sorted_years.keys():
                    sorted_years[year]['ppl_df'] = ppl_parser(sorted_years.get(year).get('dict_of_dfs').get('meta'))

                for year in sorted_years.keys():
                    # print('\n\nyear: ', year)
                    for year2 in sorted_years.keys():
                        # print('year2: ', year2)
                        if set(sorted_years[year]['ppl_df'].index) != set(sorted_years[year2]['ppl_df'].index):
                            warnings.warn('UniqueIdentifier columns do not match!')



                for year in sorted_years.keys():
                    # print('year: ', year)
                    sorted_years[year]['report_types'] = dict()
                    for report_type in sorted_years.get(year).get('dict_of_dfs'):
                        # print(report_type)
                        if report_type != 'meta':
                            sorted_years[year]['report_types'][report_type] = dict()
                            sorted_years[year]['report_types'][report_type]['not_app_or_obs'] = {}
                            sorted_years[year]['report_types'][report_type]['ETLd_df'] = {}
                            sorted_years[year]['report_types'][report_type]['data_dict'] = {}
                            sorted_years[year]['report_types'][report_type]['smoothed_df'] = {}
                            
                            

                            report_to_run = sorted_years[year]['dict_of_dfs'].get(report_type)

                            questions_df, transformed_df, na_no_null_dict = bea_etl(report_to_run, sorted_years.get(year).get('ppl_df'), report_type, sorted_years.get(year).get('file'), True, year=year)

                            # response = ask_use_questionsdf_in_directory(year, report_type)
                            # response = True
                            # if response:
                            #     if os.path.isfile('{}_{}_questions_df.xlsx'.format(year, report_type)):
                            #         q_df = pd.read_excel('{}_{}_questions_df.xlsx'.format(year, report_type), index=False, converters={'number': str})
                            #         sorted_years[year]['report_types'][report_type]['questions_df'] = q_df
                            #         questions_df = q_df
                            #     else:
                            #         print('\n\nAborting... Question data frame file "{}_questions_df.xlsx" not found in directory\n'.format(report_type))
                            #         sys.exit()
                            # else:
                            #     sorted_years[year]['report_types'][report_type]['questions_df'] = questions_df

                            sorted_years[year]['report_types'][report_type]['questions_df'] = questions_df
                            sorted_years[year]['report_types'][report_type]['ETLd_df'] = transformed_df
                            sorted_years[year]['report_types'][report_type]['na_no_null_dict'] = na_no_null_dict

                            sorted_years[year]['report_types'][report_type]['smoothed_df'][year]= join_qdf_and_rawdata_into_smoothed_df(questions_df, transformed_df, sorted_years.get(year).get('file'), report_type+'{}'.format(year), False)

                # for year in sorted_years.keys():
                #     print(year)
                #     print(sorted_years[year].get('report_types'))

                reports = create_comparative_bea_jinja_dict(sorted_years, randomize_bea)

                if set(questions_df['type']).issubset(set(['skills_matrix', ''])):
                    skills_response = True
                else:
                    skills_response = False

                comparative_year_number = max(sorted_years.keys())
                comparative_year = sorted_years.pop(max(sorted_years.keys()), None)

                comparative_bea_reports_list = []
                for report_type in reports:
                    member_variable_query = dict()

                    list_of_compared_bea_objects = []

                    # print('\nCreating {} report for {}'.format(report_type, comparative_year.get('file')))

                    report_to_run = comparative_year['report_types'].get(report_type).get('ETLd_df')
                    questions_df = comparative_year['report_types'].get(report_type).get('questions_df')
                    BEA = bea_report(comparative_year.get('file'), report_type, report_to_run, questions_df, comparative_year.get('ppl_df'), comparative_year['report_types'].get(report_type).get('na_no_null_dict'), app, randomize = randomize_bea)
                    comparative_bea_reports_list.append(BEA.output_filename)

                    member_variable_query['compared_heat_map_export'] = BEA.heat_map_export
                    member_variable_query['compared_hm_section_length_counts'] = BEA.hm_section_length_counts
                    member_variable_query['compared_hm_numbers_borders'] = BEA.hm_numbers_borders
                    member_variable_query['compared_heat_map_index'] = BEA.heat_map_index
                    member_variable_query['compared_heat_map_averages'] = BEA.heat_map_averages
                    member_variable_query['compared_hm_values_borders'] = BEA.hm_values_borders
                    member_variable_query['compared_hm_rows_directions'] = BEA.hm_rows_directions
                    member_variable_query['compared_hm_pixel_size_list'] = BEA.hm_pixel_size_list
                    member_variable_query['compared_percentage_size'] = BEA.percentage_size

                    member_variable_query['header_font_size'] = 11
                    if BEA.percentage_size<13 and BEA.percentage_size>10:
                        member_variable_query['header_font_size'] = 10
                    elif BEA.percentage_size<=10:
                        member_variable_query['header_font_size'] = 9

                    member_variable_query['number_font_size'] = 10
                    if BEA.percentage_size<13 and BEA.percentage_size>10:
                        member_variable_query['number_font_size'] = 8
                    elif BEA.percentage_size<=10:
                        member_variable_query['number_font_size'] = 7

                    member_variable_query['hm_cell_height'] = hm_cell_height = 10
                    if BEA.number_of_respondents < 16 and BEA.number_of_respondents >= 10:
                        hm_cell_height = 7
                    elif BEA.number_of_respondents < 20 and BEA.number_of_respondents >= 16:
                        hm_cell_height = 5
                    elif BEA.number_of_respondents >= 20:
                        hm_cell_height = 4
                    member_variable_query['hm_cell_height'] = hm_cell_height

                    try:
                        member_variable_query['skills_matrix_df'] = BEA.skills_matrix_export
                    except:
                        pass

                    esigmas = []
                    for year in sorted_years:
                        try:
                            compared_year_report_to_run = sorted_years[year]['report_types'][report_type]['ETLd_df']
                            compared_year_questions_df = sorted_years[year]['report_types'][report_type]['questions_df']
                            b = bea_report(sorted_years.get(year).get('file'), report_type, compared_year_report_to_run, compared_year_questions_df, sorted_years.get(year).get('ppl_df'), sorted_years[year]['report_types'].get(report_type).get('na_no_null_dict'), app, randomize = randomize_bea, year=year)
                            comparative_bea_reports_list.append(b.output_filename)

                            member_variable_query['comparative_heat_map_export'] = b.heat_map_export
                            member_variable_query['comparative_hm_section_length_counts'] = b.hm_section_length_counts
                            member_variable_query['comparative_hm_numbers_borders'] = b.hm_numbers_borders
                            member_variable_query['comparative_heat_map_index'] = b.heat_map_index
                            member_variable_query['comparative_heat_map_averages'] = b.heat_map_averages
                            member_variable_query['comparative_hm_values_borders'] = b.hm_values_borders
                            member_variable_query['comparative_hm_rows_directions'] = b.hm_rows_directions
                            member_variable_query['comparative_hm_pixel_size_list'] = b.hm_pixel_size_list
                            member_variable_query['comparative_percentage_size'] = b.percentage_size

                            list_of_compared_bea_objects.append(b)

                            esigmas.append({year: b.eSigma_value})
                        except:
                            pass

                    if not list_of_compared_bea_objects:
                        # print('\n\nNo applicable report types found for type {}!'.format(report_type))
                        continue

                    try:
                        member_variable_query['skills_matrix_average'] = BEA.skills_matrix_average
                        member_variable_query['number_of_skills_matrix_questions'] = BEA.number_of_skills_matrix_questions
                        member_variable_query['skills_matrix_df_width'] = np.arange(1, BEA.skills_matrix_export.shape[1] + 1, 1)
                    except:
                        pass

                    member_variable_query['eSigma_value'] = BEA.eSigma_value
                    member_variable_query['previous_esigmas'] = esigmas
                    member_variable_query['b_esigma'] = b.eSigma_value

                    formattedText = 'The average rating for this section is {} ({} - {}). The average rating for each question in the section ranged from {} to {}. Individual ratings ranged from {} to {}.'
                    equal_formattedText = 'The average rating for each question and this section is {} ({} - {}). Individual ratings ranged from {} to {}.'
                    compared_section_data = dict()
                    for section in BEA.averages:
                        if BEA.questionAveragesBySectionMinsAndMaxes.get(section).get('max') == BEA.questionAveragesBySectionMinsAndMaxes.get(section).get('min') and (BEA.questionAveragesBySectionMinsAndMaxes.get(section).get('max') == BEA.averages.get(section)):
                            compared_section_data[section] = equal_formattedText.format(BEA.averages.get(section), b.averages.get(section), list(sorted_years.keys())[0], BEA.sectionMinsAndMaxes.get(section).get('min'), BEA.sectionMinsAndMaxes.get(section).get('max'))
                        else:
                            compared_section_data[section] = formattedText.format(BEA.averages.get(section), b.averages.get(section), list(sorted_years.keys())[0],
                                BEA.questionAveragesBySectionMinsAndMaxes.get(section).get('min'),
                                BEA.questionAveragesBySectionMinsAndMaxes.get(section).get('max'),
                                BEA.sectionMinsAndMaxes.get(section).get('min'),
                                BEA.sectionMinsAndMaxes.get(section).get('max'))
                    member_variable_query['compared_section_data'] = compared_section_data

                    member_variable_query['questions_df'] = BEA.questions_df
                    member_variable_query['jinja_for_loop_dict'] = reports[report_type]['data_dict']
                    member_variable_query['number_of_respondents'] = BEA.number_of_respondents
                    member_variable_query['section_averages'] = BEA.averages
                    member_variable_query['five_point_scale'] = BEA.five_point_scale
                    # print('Five Point Scale: ', BEA.five_point_scale)

                    member_variable_query['jinja_hi'] = BEA.jinja_hi
                    member_variable_query['jinja_lo'] = BEA.jinja_lo
                    member_variable_query['number_of_comparisons'] = reports[report_type]['number_of_comparisons']

                    ##################
                    # Old Method. It's tied to eval_stats_list_of_dicts and sometimes
                    # there will be 0s in the heatmap that do not count as N/As
                    # na_present = False
                    compared_year_na_present = False
                    comparative_year_na_present = False

                    eval_stats_list_of_dicts = BEA.eval_stats_list_of_dicts
                    for category in eval_stats_list_of_dicts:
                        category_key = list(category.keys())[0]
                        if category_key in ['Percent of ratings of N/A', 'Number of ratings of N/A']:
                            # na_present = True
                            compared_year_na_present = True

                    eval_stats_list_of_dicts = b.eval_stats_list_of_dicts
                    for category in eval_stats_list_of_dicts:
                        category_key = list(category.keys())[0]
                        if category_key in ['Percent of ratings of N/A', 'Number of ratings of N/A']:
                            # na_present = True
                            comparative_year_na_present = True

                    # print('na_present: ', na_present)
                    # print('comparative_year_na_present: ', comparative_year_na_present)
                    # print('compared_year_na_present: ', compared_year_na_present)
                    ##################
                    if b.na_in_heat_map or BEA.na_in_heat_map:
                        member_variable_query['na_in_heat_map'] = True
                    else:
                        member_variable_query['na_in_heat_map'] = False
                    ##################
                    # member_variable_query['eval_stats_list_of_dicts'] = BEA.eval_stats_list_of_dicts
                    # member_variable_query['eval_stats_list_of_dicts_last_item'] = tuple((list(BEA.eval_stats_list_of_dicts[-1].keys())[0], BEA.eval_stats_list_of_dicts[-1].get(list(BEA.eval_stats_list_of_dicts[-1].keys())[0])))
                    eval_stats_list_of_dicts = BEA.eval_stats_list_of_dicts
                    new_eval_list_of_dicts = list()
                    add = False
                    for category in eval_stats_list_of_dicts:
                        category_key = list(category.keys())[0]
                        if category_key in ['Question analysis by Board member response:', 'Group response analysis:']:
                            new_eval_list_of_dicts.append({category_key: ['']*(reports[report_type]['number_of_comparisons']+1)})
                            continue
                        score = category.get(list(category.keys())[0], '')
                        list_of_scores = list()
                        list_of_scores.append(score)
                        for item in list_of_compared_bea_objects:
                            score_list = item.eval_stats_list_of_dicts
                            for i in score_list:
                                for k, v in i.items():
                                    if k == category_key:
                                        list_of_scores.append(i.get(category_key, ''))
                                    else:
                                        pass
                        # If there are n/a in the current year, but not last year's
                        if (category_key in ['Percent of ratings of N/A', 'Number of ratings of N/A']) and (len(list_of_scores)==1):
                            if category_key == 'Percent of ratings of N/A':
                                list_of_scores.append('0%')
                            elif category_key == 'Number of ratings of N/A':
                                list_of_scores.append('0')

                        if new_eval_list_of_dicts:
                            # If there are n/a in last year but not the current year
                            if comparative_year_na_present and not compared_year_na_present and (list(new_eval_list_of_dicts[-1].keys())[0] in ['Number of ratings of 3 or below', 'Number of ratings of 2 or below']):
                                for i in score_list:
                                    for k, v in i.items():
                                        if k == 'Number of ratings of N/A':
                                            add1_list_of_scores = ['0', i.get(k)]
                                            add1_category_key = 'Number of ratings of N/A'
                                            add = True
                            elif comparative_year_na_present and not compared_year_na_present and (list(new_eval_list_of_dicts[-1].keys())[0] in ['Percent of ratings of 2 or below', 'Percent of ratings of 3 or below']):
                                for i in score_list:
                                    for k, v in i.items():
                                        if k == 'Percent of ratings of N/A':
                                            add1_list_of_scores = ['0%', i.get(k)]
                                            add1_category_key = 'Percent of ratings of N/A'
                                            add = True

                        if add:
                            new_eval_list_of_dicts.append({category_key: list_of_scores})
                            new_eval_list_of_dicts.append({add1_category_key: add1_list_of_scores})
                            add = False
                        else:
                            new_eval_list_of_dicts.append({category_key: list_of_scores})

                    member_variable_query['eval_stats_list_of_dicts'] = new_eval_list_of_dicts

                    member_variable_query['compared_years'] = reports[report_type]['compared_years']
                    member_variable_query['comparative_year_number'] = comparative_year_number


                    # member_variable_query['section_avg_list'] = BEA.section_avg_list
                    section_avg_list = BEA.section_avg_list
                    # member_variable_query['section_avg_list_last_item'] = tuple((list(BEA.section_avg_list[-1].keys())[0], BEA.section_avg_list[-1].get(list(BEA.section_avg_list[-1].keys())[0])))
                    # section_avg_list_last_item = tuple((list(BEA.section_avg_list[-1].keys())[0], BEA.section_avg_list[-1].get(list(BEA.section_avg_list[-1].keys())[0])))
                    # print('here: ', section_avg_list_last_item)
                    new_section_avg_list = list()
                    for section in section_avg_list:
                        section_key = list(section.keys())[0]
                        section_avg = section.get(list(section.keys())[0], '')
                        # new_section_avg_list.append({section_key: []})
                        list_of_section_averages = list()
                        list_of_section_averages.append(section_avg)
                        for item in list_of_compared_bea_objects:
                            avg_list = item.section_avg_list
                            for i in avg_list:
                                for k, v in i.items():
                                    if k == section_key:
                                        # if i == avg_list[-1]:
                                        #     section_avg_list_last_item[1].append(i.get(section_key, ''))
                                        # else:
                                        list_of_section_averages.append(i.get(section_key, ''))

                                    else:
                                        pass
                        new_section_avg_list.append({section_key: list_of_section_averages})


                    # print('new_section_avg_list: ', new_section_avg_list)
                    for section in new_section_avg_list:
                        if len(section.get(list(section.keys())[0], '')) == 1:
                            section.get(list(section.keys())[0], '').append('')
                    # print('new_section_avg_list: ', new_section_avg_list)
                    member_variable_query['section_avg_list'] = new_section_avg_list
                    # member_variable_query['section_avg_list_last_item'] = section_avg_list_last_item

                    bea_comparative_templater(comparative_year.get('file'), member_variable_query, report_type, report_to_run, questions_df, comparative_year.get('ppl_df'), heatmap, cgpg, web, hilo, swot, high_act_pts, cgfr_method, cgfr_legend, stats, column_width, number_of_comparisons=1, randomize = randomize_bea)


                report_name = re.search('^(.*).csv', comparative_file_name).group(1)

                # print('\ncomparative_bea_reports_list: ', comparative_bea_reports_list)
                with zipfile.ZipFile(os.path.join(app.config['ZIP_OUTPUT'], '{}.zip'.format(report_name)),'w') as zip:
                    for report in comparative_bea_reports_list:
                        # print(report)
                        zip.write(os.path.join(app.config['EXCEL_OUTPUT'], report), basename(report))
                    for file in os.listdir(app.config["REPORTS"]):
                        if file.endswith(".pdf") or file.endswith(".xlsx"):
                            zip.write(os.path.join(app.config['REPORTS'], file), basename(file))

                for file in os.listdir(app.config["CSV_UPLOADS"]):
                    if file.endswith(".csv"):
                        os.remove(os.path.join(app.config['CSV_UPLOADS'], file))
                for file in os.listdir(app.config["REPORTS"]):
                    if file.endswith(".pdf") or file.endswith(".xlsx"):
                        os.remove(os.path.join(app.config['REPORTS'], file))
                for file in os.listdir(app.config["CHARTS"]):
                    if file.endswith(".png"):
                        os.remove(os.path.join(app.config['CHARTS'], file))

                delete_excel_output()

                return redirect('/downloadzip/'+ '{}.zip'.format(report_name))


    return render_template('public/bea.html')

def bea_comparative_templater(file_name, member_variable_query, report_type, df, questions_df, ppl_df, heatmap, CGPG, relationship_web, hilo, swot, highlights_actionpoints, show_GCFR_method, show_flag_legend, stats, question_column_width, number_of_comparisons=1, randomize=True):
    # print('\nCGPG: ', CGPG, '\nrelationship_web: ', relationship_web, '\nhilo: ', hilo, '\nswot: ', swot, '\nhighlights_actionpoints: ', highlights_actionpoints, '\nshow_GCFR_method: ', show_GCFR_method, '\nshow_flag_legend: ', show_flag_legend, '\nquestion_column_width: ', question_column_width)
    if report_type != 'BEA':
        swot = False
        highlights_actionpoints = False
        relationship_web = False
        if report_type == 'NomGov':
            report_title = 'Nominating and Governance Committee Report'
            intro_title = 'Nominating and Governance Committee'
        else:
            report_title = '{} Committee Report'.format(report_type)
            intro_title = '{} Committee'.format(report_type)
        board_or_committee = 'committee'
        show_flag_legend = False
        show_GCFR_method = False

    if report_type == 'BEA':
        report_title = 'Board Evaluation Report'
        intro_title = 'Board of Directors'
        board_or_committee = 'Board'

    else:
        relationship_web = False

    now = datetime.now()
    formatted_date = str(now.strftime("%B")) + ' ' + str(now.day) + ', ' + str(now.year)

    file_loader =FileSystemLoader(app.config['REPORT_TEMPLATES'])
    env = Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)
    template = env.get_template('comparative_bea_report_template.html')

    skills_matrix_present = 'skills_matrix' in questions_df['type'].unique()


    jinja_input_dict = {'title': 'Nasdaq BEA Comparative Evaluation', 'nasdaq_logo': os.path.join(app.config['IMAGES'], 'nasdaq_logo.png'), 'heatmap': heatmap, 'CGPG': CGPG, 'report_title': report_title,
         'year': str(now.year), 'formatted_date': formatted_date, 'skills_matrix_present': skills_matrix_present,
         'questions_df': questions_df, 'swot': swot, 'highlights_actionpoints': highlights_actionpoints, 'number_of_comparisons': number_of_comparisons,
         'swot_s': os.path.join(app.config['IMAGES'], 'swot_s.png'), 'swot_w': os.path.join(app.config['IMAGES'],'swot_w.png'), 'swot_o': os.path.join(app.config['IMAGES'], 'swot_o.png'),
         'swot_t': os.path.join(app.config['IMAGES'], 'swot_t.png'), 'relationship_web': relationship_web,
         'scale_image': os.path.join(app.config['IMAGES'], 'scale.png'), 'five_scale_image': os.path.join(app.config['IMAGES'], 'five_scale.png'), 'green_flag': os.path.join(app.config['IMAGES'], 'green_flag.png'),
          'yellow_flag': os.path.join(app.config['IMAGES'], 'yellow_flag.png'), 'red_flag': os.path.join(app.config['IMAGES'], 'red_flag.png'),
         'question_column_width': question_column_width, 'show_GCFR_method': show_GCFR_method, 'show_flag_legend': show_flag_legend, 'intro_title': intro_title,
         'board_or_committee': board_or_committee, 'hilo_section_bool': hilo, 'stats': stats}

    if CGPG:
        jinja_input_dict['cgpg_bar_chart'] = os.path.join(app.config['CHARTS'], 'cgpg_bar_chart.png')
        jinja_input_dict['compared_cgpg_bar_charts'] = []
        for year in member_variable_query['compared_years']:
            jinja_input_dict['compared_cgpg_bar_charts'].append((year, os.path.join(app.config['CHARTS'], '{}_cgpg_bar_chart.png'.format(year))))
    if relationship_web:
        jinja_input_dict['radar_chart_image'] = os.path.join(app.config['CHARTS'], 'radar_chart.png')



    # Get Assessment sections labels and section names
    section_names_and_labels = {}
    label_re = re.compile(r'^[\s\t]*([IVX]+\.)[\s\t]*(.*)')
    for section in questions_df['section'].unique().tolist():
        label = label_re.match(section)
        section_names_and_labels[label.group(1)] = label.group(2)
    jinja_input_dict['section_names_and_labels'] = section_names_and_labels

    # print('\nCGPG: ', CGPG, '\nrelationship_web: ', relationship_web, '\nhilo: ', hilo, '\nswot: ', swot, '\nhighlights_actionpoints: ', highlights_actionpoints, '\nshow_GCFR_method: ', show_GCFR_method, '\nshow_flag_legend: ', show_flag_legend, '\nquestion_column_width: ', question_column_width)
    # Control which sections are in the table of contents
    jinja_input_dict['table_of_contents_titles'] = []
    if heatmap:
        jinja_input_dict['table_of_contents_titles'].append('#heat_map_title')
    if CGPG:
        jinja_input_dict['table_of_contents_titles'].append('#CGPG_title')
    if relationship_web:
        jinja_input_dict['table_of_contents_titles'].append('#radar_title')
    if hilo:
        jinja_input_dict['table_of_contents_titles'].append('#hilo_title')
    if member_variable_query.get('skills_matrix_average') is not None:
        jinja_input_dict['table_of_contents_titles'].append('#skills_matrix_title')
    if swot:
        jinja_input_dict['table_of_contents_titles'].append('#swot_title')
    if highlights_actionpoints:
        jinja_input_dict['table_of_contents_titles'].append('#highlights_actionpts_title')
    if show_GCFR_method:
        jinja_input_dict['table_of_contents_titles'].append('#CGPG_method_title')
    # if show_flag_legend:
    jinja_input_dict['table_of_contents_titles'].append('#CG_flag_title')
    if stats:
        jinja_input_dict['table_of_contents_titles'].append('#statistical_analysis_title')

    client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file_name)
    jinja_input_dict['client_name'] = client_name.group(1)

    jinja_input_dict['number_of_sections'] = len(questions_df['section'].unique().tolist())
    jinja_input_dict['number_of_long_form_questions'] = len(questions_df[questions_df['type']=='long_form'])
    jinja_input_dict['number_of_rated_questions'] = len(questions_df['question_raw']) - len(questions_df[questions_df['type']=='long_form']) - len(questions_df[questions_df['type']=='skill_matrix']) - len(questions_df[questions_df['type']=='attribute_matrix'])


    jinja_input_dict['report_type'] = report_type

    for item in member_variable_query.keys():
        jinja_input_dict['{}'.format(item)] = member_variable_query[item]

    output = template.render(jinja_input_dict)
    # with open("test.html", "w") as fh:
    #     fh.write(output)


    HTML(string=output, base_url=os.path.dirname(os.path.realpath(__file__))).write_pdf(os.path.join(app.config['REPORTS'], '{}_{}.pdf'.format(client_name.group(1), report_type)), stylesheets=[CSS(os.path.abspath(os.path.join(os.sep, app.config['CSS'], 'bea_comparative_template_style.css')))])

def create_comparative_bea_jinja_dict(sorted_years, randomize_bea):
    copied_dict = sorted_years.copy()
    comparative_year = copied_dict.pop(max(copied_dict.keys()), None)
    compared_years = copied_dict

    number_of_comparisons = len(list(copied_dict.keys()))
    # print("Number of comparisons: ", number_of_comparisons)
    # print(sorted_years.get(max(sorted_years.keys())).keys())
    # for key in sorted_years.get(max(sorted_years.keys())).keys():
    #     print('\n\n', key, '\n\n', sorted_years.get(max(sorted_years.keys())).get(key))
    # print(sorted_years.get(max(sorted_years.keys())).get('BEA'))

    reports = dict()
    for report_type in comparative_year.get('report_types'):
        # print('\nReport Type {} found in comparative year'.format(report_type))
        if report_type != 'meta':
            rankFlag = False
            randomize = randomize_bea
            # randomize = False

            # populate report_types_to_compare with similar report types of compared_years
            report_types_to_compare = []
            year_numbers_compared = []
            for year in compared_years:
                try:
                    report_types_to_compare.append(compared_years[year]['report_types'][report_type])
                    year_numbers_compared.append(year)
                    # print('found also in year: ', year)
                    # print(year, report_type, compared_years[year]['report_types'][report_type]['ETLd_df'].columns)
                except:
                    # print('not found in year: ', year)
                    pass
            # print('\nMatching report types found: ', report_types_to_compare.keys())
            data_dict = dict()
            qdf = comparative_year['report_types'][report_type].get('questions_df')
            tdf = comparative_year['report_types'][report_type].get('ETLd_df')

            for s in qdf['section'].unique():
                # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
                #     print(qdf[qdf['section']==s])
                #     print(qdf[qdf['section']==s]['type'].unique().tolist())
                if qdf[qdf['section']==s]['type'].unique().tolist() == ['skills_matrix']:
                    continue
                data_dict[s] = {}
                for q in qdf[qdf['section']==s]['question_raw']:
                    data_dict[s][q] = {}
                    data_dict[s][q]['type'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item()
                    data_dict[s][q]['number'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item()
                    data_dict[s][q]['question'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item()
                    data_dict[s][q]['sub_section'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item()
                    if qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['matrix', 'likert']:
                        rankFlag = False
                        answers = tdf[q].replace({np.nan: None})
                        answers.fillna('N/A', inplace=True)
                        answers = answers.tolist()
                        answers = [int(x) if x != 'N/A' else x for x in answers]
                        if randomize:
                            shuffle(answers)
                        data_dict[s][q]['answers'] = answers
                        data_dict[s][q]['average'] = str(Decimal(np.mean(tdf[q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP))

                        if report_types_to_compare:
                            try:
                                comp = report_types_to_compare[0]
                                # print('\n\n', q)
                                # print([r.get('ETLd_df')[q] for r in report_types_to_compare])
                                data_dict[s][q]['previous_averages'] = [str(Decimal(np.mean(r.get('ETLd_df')[q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP)) for r in report_types_to_compare]
                            except:
                                data_dict[s][q]['previous_averages'] = 'N/A'
                            # print([str(Decimal(np.mean(r.get('ETLd_dfs')[q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP)) for r in report_types_to_compare])
                            # print(q in list(comp.get('ETLd_df').columns))
                    elif qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['long_form', 'optional_long_form']:
                        rankFlag = False
                        answers = tdf[q].dropna().tolist()
                        answers = [str(a) for a in answers if a.lower() not in ['nan', '#name?', None,  'no comment', 'none', np.nan, '']]
                        if not answers:
                            answers = [None]
                        if randomize:
                            shuffle(answers)
                        data_dict[s][q]['answers'] = answers
                    elif qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['rank']:
                        # print(qdf[(qdf['section']==s)&(qdf['question_raw']==q)])
                        if rankFlag == False:
                            rankFlag = True
                            count = 1
                            data_dict[s][q]['count'] = count
                            lengthOfRankedChoiceSection = len(qdf[(qdf['section']==s) & (qdf['number']==data_dict[s][q]['number'])])
                            data_dict[s][q]['lengthOfRankedChoiceSection'] = lengthOfRankedChoiceSection
                            data_dict[s][q]['ranks'] = list(np.arange(1, lengthOfRankedChoiceSection+1, 1))
                            data_dict[s][q]['first'] = True

                            dictonaryOfCounts = dict(Counter(tdf[q].tolist()))
                            listOfOrderedCounts = list()
                            for value in np.arange(1, lengthOfRankedChoiceSection+1, 1):
                                listOfOrderedCounts.append(dictonaryOfCounts.get(value, 0))
                            data_dict[s][q]['answers'] = listOfOrderedCounts
                        else:
                            rankFlag = True
                            count += 1
                            data_dict[s][q]['count'] = count
                            data_dict[s][q]['first'] = False

                            dictonaryOfCounts = dict(Counter(tdf[q].tolist()))
                            listOfOrderedCounts = list()
                            for value in np.arange(1, lengthOfRankedChoiceSection+1, 1):
                                listOfOrderedCounts.append(dictonaryOfCounts.get(value, 0))
                            data_dict[s][q]['answers'] = listOfOrderedCounts

                    elif qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['yes/no']:
                        answers = tdf[q].dropna().tolist()
                        yesNoCheck = [answer.lower() for answer in answers]
                        yesNoCheck = dict(Counter(yesNoCheck))
                        data_dict[s][q]['answers'] = yesNoCheck

            reports[report_type] = {'data_dict': data_dict}
            reports[report_type]['number_of_comparisons'] = len(report_types_to_compare)
            reports[report_type]['compared_years'] = year_numbers_compared
            reports[report_type]['comparative_year'] = max(sorted_years.keys())
            reports[report_type]['randomized'] = randomize
            # print('compared_years: ', compared_years.keys())

    # count = 0
    # for s in data_dict:
    #     print('\n\nSection Name:  ', s)
    #     for q in data_dict[s]:
    #         print('Raw Question:  ', q)
    #         try:
    #             print('Question Average:  ', data_dict[s][q]['average'])
    #         except:
    #             pass
    #         print('Number: ', data_dict[s][q]['number'])
    #         print('Type:  ', data_dict[s][q]['type'])
    #         print('Question: ', data_dict[s][q]['question'])
    #         print('Sub:  ', data_dict[s][q]['sub_section'])
    #         print('Answers:  ', data_dict[s][q]['answers'])
    #         print('\n')
            # count+=1
            # if count == 4:
            #     break
    return reports

def run_bea(bea_filename, randomize_bea, heatmap, cgpg, web, hilo, swot, high_act_pts, cgfr_method, cgfr_legend, stats, column_width):
    bea_export_path = os.path.join(app.config['CSV_UPLOADS'], bea_filename)

    raw_data_dict = csv_opener(file = bea_export_path)
    dict_of_dfs = bea_splitter(bea_export_path, raw_data_dict)

    meta_section = dict_of_dfs.get('meta')
    ppl_df = ppl_parser(meta_section)

    not_app_or_obs = {}
    ETLd_question_dfs = {}
    ETLd_dfs = {}
    data_dicts = {}
    smoothed_dfs = {}

    for report_type in dict_of_dfs:
        if report_type != 'meta':
            data_dicts[report_type] = dict()
            report_to_run = dict_of_dfs.get(report_type)
            questions_df, transformed_df, na_no_null_dict = bea_etl(report_to_run, ppl_df, report_type, bea_export_path, False)

            ETLd_question_dfs[report_type] = questions_df

            ETLd_dfs[report_type] = transformed_df
            not_app_or_obs[report_type] = {}
            not_app_or_obs[report_type]['na_no_null_dict'] = na_no_null_dict

            jinja_data_dict = create_bea_jinja_dict(questions_df, transformed_df, True)
            data_dicts[report_type]['randomized'] = jinja_data_dict

            smoothed_df = join_qdf_and_rawdata_into_smoothed_df(questions_df, transformed_df, bea_filename, report_type, False)
            smoothed_dfs[report_type] = smoothed_df

    if set(questions_df['type']).issubset(set(['skills_matrix', ''])):
        skills_response = True
    else:
        skills_response = False


    bea_reports = []
    for report_type in dict_of_dfs:
        if report_type != 'meta':
            # na_present = bool([a for a in not_app_or_obs[report_type]['na_no_null_dict'].values() if bool(a)])
            # print('Creating {} report for {}'.format(report_type, bea_filename))

            report_to_run = ETLd_dfs.get(report_type)
            questions_df = ETLd_question_dfs.get(report_type)
            BEA = bea_report(bea_filename, report_type, report_to_run, questions_df, ppl_df, not_app_or_obs[report_type]['na_no_null_dict'], app, randomize = randomize_bea)
            bea_reports.append(BEA.output_filename)

            member_variable_query = dict()
            if BEA.na_in_heat_map:
                member_variable_query['na_in_heat_map'] = True
            else:
                member_variable_query['na_in_heat_map'] = False

            try:
                member_variable_query['skills_matrix_average'] = BEA.skills_matrix_average
                member_variable_query['number_of_skills_matrix_questions'] = BEA.number_of_skills_matrix_questions
            except:
                pass
            member_variable_query['five_point_scale'] = BEA.five_point_scale
            member_variable_query['skills_response'] = skills_response
            try:
                member_variable_query['skills_matrix_df'] = BEA.skills_matrix_export
                member_variable_query['skills_matrix_df_width'] = np.arange(1, BEA.skills_matrix_export.shape[1] + 1, 1)
            except:
                pass

            if not skills_response:
                member_variable_query['eSigma_value'] = BEA.eSigma_value
                member_variable_query['section_data'] = BEA.section_data
                member_variable_query['questions_df'] = BEA.questions_df
                if randomize_bea:
                    member_variable_query['jinja_for_loop_dict'] = data_dicts[report_type]['randomized']
                else:
                    member_variable_query['jinja_for_loop_dict'] = create_bea_jinja_dict(questions_df, report_to_run, False)

                member_variable_query['number_of_respondents'] = BEA.number_of_respondents
                member_variable_query['section_averages'] = BEA.averages
                member_variable_query['eval_stats_list_of_dicts'] = BEA.eval_stats_list_of_dicts
                member_variable_query['eval_stats_list_of_dicts_last_item'] = tuple((list(BEA.eval_stats_list_of_dicts[-1].keys())[0], BEA.eval_stats_list_of_dicts[-1].get(list(BEA.eval_stats_list_of_dicts[-1].keys())[0])))
                member_variable_query['section_avg_list'] = BEA.section_avg_list
                member_variable_query['section_avg_list_last_item'] = tuple((list(BEA.section_avg_list[-1].keys())[0], BEA.section_avg_list[-1].get(list(BEA.section_avg_list[-1].keys())[0])))
                member_variable_query['jinja_hi'] = BEA.jinja_hi
                member_variable_query['jinja_lo'] = BEA.jinja_lo
                # member_variable_query['heat_map_export'] = BEA.heat_map_export
                member_variable_query['hm_section_length_counts'] = BEA.hm_section_length_counts
                member_variable_query['hm_numbers_borders'] = BEA.hm_numbers_borders
                # member_variable_query['heat_map_index'] = BEA.heat_map_index
                member_variable_query['heat_map_averages'] = BEA.heat_map_averages
                # member_variable_query['hm_values_borders'] = BEA.hm_values_borders
                member_variable_query['hm_rows_directions'] = BEA.hm_rows_directions
                member_variable_query['percentage_size'] = BEA.percentage_size
                # print('percentage_size: ', member_variable_query['percentage_size'])

                member_variable_query['header_font_size'] = 11
                if BEA.percentage_size<13 and BEA.percentage_size>10:
                    member_variable_query['header_font_size'] = 10
                elif BEA.percentage_size<=10:
                    member_variable_query['header_font_size'] = 9

                member_variable_query['number_font_size'] = 10
                if BEA.percentage_size<13 and BEA.percentage_size>10:
                    member_variable_query['number_font_size'] = 8
                elif BEA.percentage_size<=10:
                    member_variable_query['number_font_size'] = 7

                member_variable_query['hm_cell_height'] = hm_cell_height = 10
                if BEA.number_of_respondents < 16 and BEA.number_of_respondents >= 10:
                    hm_cell_height = 8
                elif BEA.number_of_respondents < 20 and BEA.number_of_respondents >= 16:
                    hm_cell_height = 6
                elif BEA.number_of_respondents >= 20:
                    hm_cell_height = 5
                member_variable_query['hm_cell_height'] = hm_cell_height

            else:
                member_variable_query['eSigma_value'] = None
                member_variable_query['section_data'] = None
                member_variable_query['questions_df'] = None
                if randomize_bea:
                    member_variable_query['jinja_for_loop_dict'] = data_dicts[report_type]['randomized']
                else:
                    member_variable_query['jinja_for_loop_dict'] = create_bea_jinja_dict(questions_df, report_to_run, False)
                member_variable_query['number_of_respondents'] = None
                member_variable_query['section_averages'] = None
                member_variable_query['eval_stats_list_of_dicts'] = None
                member_variable_query['eval_stats_list_of_dicts_last_item'] = None
                member_variable_query['section_avg_list'] = None
                member_variable_query['section_avg_list_last_item'] = None
                member_variable_query['jinja_hi'] = None
                member_variable_query['jinja_lo'] = None

            BEA_templater(bea_filename, member_variable_query, report_type, report_to_run, questions_df, ppl_df, skills_response, heatmap, cgpg, web, hilo, swot, high_act_pts, cgfr_method, cgfr_legend, stats, column_width, randomize = randomize_bea)

    # #loading the Excel File and the sheet
    # print(bea_filename)
    # pxl_doc = openpyxl.load_workbook(os.path.join(app.config['EXCEL_OUTPUT'], 'XYZ_BEA.xlsx'))
    # sheet = pxl_doc['Heat Map']

    # #calling the image_loader
    # image_loader = SheetImageLoader(sheet)

    # #get the image (put the cell you need instead of 'A1')
    # image = image_loader.get('A1:AI8')

    # #showing the image
    # image.show()

    # #saving the image
    # image.save(os.path.join(app.config['IMAGES'], 'image_name.jpg'))

    return bea_reports

def BEA_templater(file_name, member_variable_query, report_type, df, questions_df, ppl_df, skills_response, heatmap, CGPG, relationship_web, hilo, swot, highlights_actionpoints, show_GCFR_method, show_flag_legend, stats, question_column_width, randomize = True):
    client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file_name)
    # print('\nCGPG: ', CGPG, '\nrelationship_web: ', relationship_web, '\nhilo: ', hilo, '\nswot: ', swot, '\nhighlights_actionpoints: ', highlights_actionpoints, '\nshow_GCFR_method: ', show_GCFR_method, '\nshow_flag_legend: ', show_flag_legend, '\nquestion_column_width: ', question_column_width)
    if report_type != 'BEA' and not skills_response:
        swot = False
        highlights_actionpoints = False
        relationship_web = False
        if report_type == 'NomGov':
            report_title = 'Nominating and Governance Committee Report'
            intro_title = 'Nominating and Governance Committee'
        else:
            report_title = '{} Committee Report'.format(report_type)
            intro_title = '{} Committee'.format(report_type)
        board_or_committee = 'committee'
        show_GCFR_method = False
        show_flag_legend = False

        try:
            eval_overview_html = '''
            <div style="text-align: justify;">
              The {} of {} completed a self-evaluation. All responses and comments presented throughout this report in all graphics and reporting features are anonymized and randomized to promote candid feedback and adhere to best practices in corporate governance. The goal of this report is to provide the committee with an understanding  of how the directors view the committee’s effectiveness, highlight areas of strength and areas for improvement, promote positive committee dynamics, and provide information to help improve the committee’s overall performance and effectiveness.
            </div>'''.format(intro_title, client_name.group(1))
        except:
            eval_overview_html = '''
            <div style="text-align: justify;">
              The {} in this report completed a self-evaluation. All responses and comments presented throughout this report in all graphics and reporting features are anonymized and randomized to promote candid feedback and adhere to best practices in corporate governance. The goal of this report is to provide the committee with an understanding  of how the directors view the committee’s effectiveness, highlight areas of strength and areas for improvement, promote positive committee dynamics, and provide information to help improve the committee’s overall performance and effectiveness.
            </div>'''.format(intro_title)


    if report_type == 'BEA' and not skills_response:
        report_title = 'Board Evaluation Report'
        intro_title = 'Board of Directors'
        board_or_committee = 'Board'

        eval_overview_html = '''
        <div style="text-align: justify;">
          The {} of {} completed a self-evaluation. All responses and comments presented throughout this report in all graphics and reporting features are anonymized and randomized to promote candid feedback and adhere to best practices in corporate governance. The goal of this report is to provide the Board with an understanding of how the directors view the Board’s effectiveness, highlight areas of strength and areas for improvement, promote positive Board dynamics, and provide information to help improve the Board’s overall performance.
        </div>'''.format(intro_title, client_name.group(1))
    else:
        relationship_web = False


    if skills_response:
        relationship_web = False
        highlights_actionpoints = False
        swot = False
        show_GCFR_method = False
        show_flag_legend = False
        report_title = 'Skills Matrix'
        question_column_width = 35
        intro_title = 'Board of Directors'
        board_or_committee = 'Board'

    if member_variable_query['eval_stats_list_of_dicts'][1].get('Number of Board members surveyed') == 0:
        del member_variable_query['eval_stats_list_of_dicts'][1]

    now = datetime.now()
    formatted_date = str(now.strftime("%B")) + ' ' + str(now.day) + ', ' + str(now.year)

    file_loader =FileSystemLoader(app.config['REPORT_TEMPLATES'])
    env = Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)
    template = env.get_template('bea_report_template.html')


    # print('\n\n', '-----------------------\n\n', os.path.join(app.config['IMAGES'], 'cbe_logo.png'))
    # print('C:\\Users\\Drew\\Flask-Apps\\Nasdaq_Reporting\\app\\static\\img\\cbe_logo.png')
    # print(os.path.abspath(os.path.join(os.sep, app.config['IMAGES'], 'cbe_logo.png')))

    skills_matrix_present = 'skills_matrix' in questions_df['type'].unique()


    jinja_input_dict = {'title': 'Nasdaq Board Evaluation', 'nasdaq_logo': os.path.join(app.config['IMAGES'], 'nasdaq_logo.png'), 'heatmap': heatmap, 'CGPG': CGPG, 'report_title': report_title,
         'year': str(now.year), 'formatted_date': formatted_date, 'skills_matrix_present': skills_matrix_present,
         'questions_df': questions_df, 'swot': swot, 'highlights_actionpoints': highlights_actionpoints, 'relationship_web': relationship_web,
         'swot_s': os.path.join(app.config['IMAGES'], 'swot_s.png'), 'swot_w': os.path.join(app.config['IMAGES'],'swot_w.png'), 'swot_o': os.path.join(app.config['IMAGES'], 'swot_o.png'), 'swot_t': os.path.join(app.config['IMAGES'], 'swot_t.png'),
         'scale_image': os.path.join(app.config['IMAGES'], 'scale.png'), 'five_scale_image': os.path.join(app.config['IMAGES'], 'five_scale.png'), 'green_flag': os.path.join(app.config['IMAGES'], 'green_flag.png'), 'yellow_flag': os.path.join(app.config['IMAGES'], 'yellow_flag.png'),
         'red_flag': os.path.join(app.config['IMAGES'], 'red_flag.png'), 'question_column_width': question_column_width, 'show_GCFR_method': show_GCFR_method,
          'show_flag_legend': show_flag_legend, 'intro_title': intro_title, 'board_or_committee': board_or_committee, 'hilo_section_bool': hilo, 'stats': stats}

    if report_type == 'BEA' and relationship_web:
        jinja_input_dict['radar_chart_image'] = os.path.join(app.config['CHARTS'], 'radar_chart.png')

    if CGPG:
        jinja_input_dict['cgpg_bar_chart'] = os.path.join(app.config['CHARTS'], 'cgpg_bar_chart.png')


    # Get Assessment sections labels and section names
    section_names_and_labels = {}
    label_re = re.compile(r'^[\s\t]*([IVX]+\.)[\s\t]*(.*)')
    for section in questions_df['section'].unique().tolist():
        label = label_re.match(section)
        section_names_and_labels[label.group(1)] = label.group(2)
    jinja_input_dict['section_names_and_labels'] = section_names_and_labels

    # Control which sections are in the table of contents
    
    # jinja_input_dict['table_of_contents_titles'] = ['#heat_map_title', '#hilo_title', '#CGPG_method_title', '#CG_flag_title', '#statistical_analysis_title']

    jinja_input_dict['table_of_contents_titles'] = []
    if heatmap:
        jinja_input_dict['table_of_contents_titles'].append('#heat_map_title')
    if CGPG:
        jinja_input_dict['table_of_contents_titles'].append('#CGPG_title')
    if relationship_web:
        jinja_input_dict['table_of_contents_titles'].append('#radar_title')
    if hilo:
        jinja_input_dict['table_of_contents_titles'].append('#hilo_title')
    if member_variable_query.get('skills_matrix_average') is not None:
        jinja_input_dict['table_of_contents_titles'].append('#skills_matrix_title')
    if swot:
        jinja_input_dict['table_of_contents_titles'].append('#swot_title')
    if highlights_actionpoints:
        jinja_input_dict['table_of_contents_titles'].append('#highlights_actionpts_title')
    if show_GCFR_method:
        jinja_input_dict['table_of_contents_titles'].append('#CGPG_method_title')
    # if show_flag_legend:
    jinja_input_dict['table_of_contents_titles'].append('#CG_flag_title')
    if stats:
        jinja_input_dict['table_of_contents_titles'].append('#statistical_analysis_title')


    jinja_input_dict['client_name'] = client_name.group(1)

    jinja_input_dict['number_of_sections'] = len(questions_df['section'].unique().tolist())
    jinja_input_dict['number_of_long_form_questions'] = len(questions_df[questions_df['type']=='long_form'])
    jinja_input_dict['number_of_rated_questions'] = len(questions_df['question_raw']) - len(questions_df[questions_df['type']=='long_form']) - len(questions_df[questions_df['type']=='skill_matrix']) - len(questions_df[questions_df['type']=='attribute_matrix'])

    jinja_input_dict['report_type'] = report_type
    jinja_input_dict['eval_overview_html'] = eval_overview_html


    for item in member_variable_query.keys():
        jinja_input_dict['{}'.format(item)] = member_variable_query[item]

    output = template.render(jinja_input_dict)
    # with open("test.html", "w") as fh:
    #     fh.write(output)

    # print(member_variable_query['data_dict'])
    HTML(string=output, base_url=os.path.dirname(os.path.realpath(__file__))).write_pdf(os.path.join(app.config['REPORTS'], '{}_{}.pdf'.format(client_name.group(1), report_type)), stylesheets=[CSS(os.path.abspath(os.path.join(os.sep, app.config['CSS'], 'bea_template_style.css')))])

def join_questions_df_and_raw_data_into_dict(qdf, tdf):
    data_dict = dict()
    rankFlag = False
    for s in qdf['section'].unique():
        data_dict[s] = {}
        for q in qdf[qdf['section']==s]['question_raw']:
            data_dict[s][q] = {}
            data_dict[s][q]['type'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item()
            data_dict[s][q]['number'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item()
            data_dict[s][q]['question'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item()
            data_dict[s][q]['subsection'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['subsection'].item()
            if qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['matrix', 'likert']:
                rankFlag = False
                answers = tdf[q].replace({np.nan: None})
                answers.fillna('N/A', inplace=True)
                data_dict[s][q]['answers'] = answers.tolist()
                data_dict[s][q]['average'] = str(Decimal(np.mean(tdf[q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            elif qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['long_form']:
                rankFlag = False
                answers = tdf[q].dropna().tolist()
                answers = [a for a in answers if a not in ['nan', '#NAME?']]
                data_dict[s][q]['answers'] = answers
            elif qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['rank']:
                if rankFlag == False:
                    rankFlag = True
                    count = 1
                    data_dict[s][q]['count'] = count
                    lengthOfRankedChoiceSection = len(qdf[(qdf['section']==s) & (qdf['number']==data_dict[s][q]['number'])])
                    data_dict[s][q]['lengthOfRankedChoiceSection'] = lengthOfRankedChoiceSection
                    data_dict[s][q]['ranks'] = list(np.arange(1, lengthOfRankedChoiceSection+1, 1))
                    data_dict[s][q]['first'] = True

                    dictonaryOfCounts = dict(Counter(tdf[q].tolist()))
                    listOfOrderedCounts = list()
                    for value in np.arange(1, lengthOfRankedChoiceSection+1, 1):
                        listOfOrderedCounts.append(dictonaryOfCounts.get(value, 0))
                    data_dict[s][q]['answers'] = listOfOrderedCounts
                else:
                    rankFlag = True
                    count += 1
                    data_dict[s][q]['count'] = count
                    data_dict[s][q]['first'] = False

                    dictonaryOfCounts = dict(Counter(tdf[q].tolist()))
                    listOfOrderedCounts = list()
                    for value in np.arange(1, lengthOfRankedChoiceSection+1, 1):
                        listOfOrderedCounts.append(dictonaryOfCounts.get(value, 0))
                    data_dict[s][q]['answers'] = listOfOrderedCounts

    return data_dict

def join_qdf_and_rawdata_into_smoothed_df(qdf, tdf, file, report_type, anonymize):

    ############################################################################
    # ETL
    df = pd.DataFrame(columns=['client_name', 'report_type', 'section', 'number', 'question_raw', 'question', 'sub_section', 'type', 'average', 'response', 'respondent_id', 'question_mean'])
    try:
        client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file).group(1)
    except:
        client_name = 'client'

    if anonymize ==True:
        director_encoder = dict()
        director_count = 1

    for s in qdf['section'].unique():
        for q in qdf[qdf['section']==s]['question_raw']:
            for i in tdf[q].index:
                # if (tdf.loc[i,q]==np.nan or (tdf.loc[i,q]=='nan')):
                #     print('here!', i, q)
                #     response = None
                # else:
                #     response = tdf.loc[i,q]
                if anonymize == False:
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item(), 'response': tdf.loc[i,q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}
                else:
                    sub_q = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item()
                    if sub_q not in ['', '!__!']:
                        if director_encoder.get(sub_q) is None:
                            director_encoder[sub_q] = 'Director {}'.format(director_count)
                            sub_q = 'Director {}'.format(director_count)
                            director_count += 1
                        else:
                            sub_q = director_encoder.get(sub_q)
                    else:
                        pass
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section': sub_q, 'response': tdf.loc[i,q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}

                df = df.append(row, ignore_index=True)

    question_means = []
    for section in df['section'].unique():
        for number in df[(df['section']==section)]['number'].unique():
            try:
                mean_by_question = df[(df['section']==section) & (df['number']==number)]['response'].mean()
                mean_by_question = "{0:.2f}%".format(Decimal('{}'.format(mean_by_question)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                mean_by_question = None
            mean_column_addition = [mean_by_question]*len(df[(df['section']==section) & (df['number']==number)]['response'])
            question_means.extend(mean_column_addition)

    df['question_mean'] = question_means
    df.to_excel(os.path.join(app.config['REPORTS'], 'SMOOTHED_{}_{}.xlsx'.format(client_name, report_type)))
    return df

def create_bea_jinja_dict(qdf, tdf, randomize):
    data_dict = dict()
    rankFlag = False
    for s in qdf['section'].unique():
        data_dict[s] = {}
        for q in qdf[qdf['section']==s]['question_raw']:
            if qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() == 'skills_matrix':
                del data_dict[s]
                break

            data_dict[s][q] = {}
            data_dict[s][q]['type'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item()
            data_dict[s][q]['number'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item()
            data_dict[s][q]['question'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item()
            data_dict[s][q]['sub_section'] = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item()
            if qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['matrix', 'likert']:
                rankFlag = False
                answers = tdf[q].replace({np.nan: None})
                answers.fillna('N/A', inplace=True)
                answers = answers.tolist()
                answers = [int(x) if x != 'N/A' else x for x in answers]
                if randomize:
                    shuffle(answers)
                data_dict[s][q]['answers'] = answers
                data_dict[s][q]['average'] = str(Decimal(np.mean(tdf[q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            elif qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['long_form', 'optional_long_form']:
                rankFlag = False
                answers = tdf[q].dropna().tolist()
                answers = [str(a) for a in answers if a.lower() not in ['nan', '#name?', None,  'no comment', 'none', np.nan, '']]
                if not answers:
                    answers = [None]
                if randomize:
                    shuffle(answers)
                data_dict[s][q]['answers'] = answers
            elif qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['rank']:
                if rankFlag == False:
                    rankFlag = True
                    count = 1
                    data_dict[s][q]['count'] = count
                    lengthOfRankedChoiceSection = len(qdf[(qdf['section']==s) & (qdf['number']==data_dict[s][q]['number'])])
                    data_dict[s][q]['lengthOfRankedChoiceSection'] = lengthOfRankedChoiceSection
                    data_dict[s][q]['ranks'] = list(np.arange(1, lengthOfRankedChoiceSection+1, 1))
                    data_dict[s][q]['first'] = True

                    dictonaryOfCounts = dict(Counter(tdf[q].tolist()))
                    listOfOrderedCounts = list()
                    for value in np.arange(1, lengthOfRankedChoiceSection+1, 1):
                        listOfOrderedCounts.append(dictonaryOfCounts.get(value, 0))
                    data_dict[s][q]['answers'] = listOfOrderedCounts
                else:
                    rankFlag = True
                    count += 1
                    data_dict[s][q]['count'] = count
                    data_dict[s][q]['first'] = False

                    dictonaryOfCounts = dict(Counter(tdf[q].tolist()))
                    listOfOrderedCounts = list()
                    for value in np.arange(1, lengthOfRankedChoiceSection+1, 1):
                        listOfOrderedCounts.append(dictonaryOfCounts.get(value, 0))
                    data_dict[s][q]['answers'] = listOfOrderedCounts

            elif qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item() in ['yes/no']:
                answers = tdf[q].dropna().tolist()
                yesNoCheck = [answer.lower() for answer in answers]
                yesNoCheck = dict(Counter(yesNoCheck))
                data_dict[s][q]['answers'] = yesNoCheck

    # count = 0
    # for s in data_dict:
    #     print('\n\nSection Name:  ', s)
    #     for q in data_dict[s]:
    #         print('Raw Question:  ', q)
    #         try:
    #             print('Question Average:  ', data_dict[s][q]['average'])
    #         except:
    #             pass
    #         print('Number: ', data_dict[s][q]['number'])
    #         print('Type:  ', data_dict[s][q]['type'])
    #         print('Question: ', data_dict[s][q]['question'])
    #         print('Sub:  ', data_dict[s][q]['sub_section'])
    #         print('Answers:  ', data_dict[s][q]['answers'])
    #         print('\n')
    #         count+=1
    #         if count == 14:
    #             break
    return data_dict

@app.route("/downloadfile/<filename>", methods = ['GET'])
def download_excel_output(filename):
    return send_from_directory(app.config["EXCEL_OUTPUT"], filename=filename, as_attachment=True, attachment_filename='')

@app.route("/downloadzip/<filename>", methods = ['GET'])
def download_zip_output(filename):
    return send_from_directory(app.config["ZIP_OUTPUT"], filename=filename, as_attachment=True, attachment_filename='')

@app.route('/fsr_uploader', methods=['GET', 'POST'])
def fsr_uploader():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'modified' not in request.files or 'plain' not in request.files:
            return redirect(request.url)
        modified = request.files['modified']
        plain = request.files['plain']
        create_section_headers = request.form['create_section_headers']
        create_modified_instance = request.form['create_modified_instance']
        # print('section headers', section_headers)
        # print('modified instance', modified_instance)

        # if user does not select file, browser also
        # submit a empty part without filename
        if modified.filename == '' or plain.filename == '':
            return redirect(request.url)
        else:
            modified_filename = secure_filename(modified.filename)
            plain_filename = secure_filename(plain.filename)

            modified.save(os.path.join(app.config['CSV_UPLOADS'], modified_filename))
            plain.save(os.path.join(app.config['CSV_UPLOADS'], plain_filename))

            fsr = run_fsr(plain_filename, modified_filename, create_section_headers, create_modified_instance)
            fsr_output_filename = fsr.output_filename

            for file in os.listdir(app.config["CSV_UPLOADS"]):
                if file.endswith(".csv"):
                    os.remove(os.path.join(app.config['CSV_UPLOADS'], file))

            with zipfile.ZipFile(os.path.join(app.config['ZIP_OUTPUT'], '{}.zip'.format(fsr_output_filename)),'w') as zip:
                for report in fsr.output_file_names:
                    zip.write(os.path.join(app.config['EXCEL_OUTPUT'], report), basename(report))

            delete_excel_output()
            return redirect('/downloadzip/'+ '{}.zip'.format(fsr_output_filename))

    return render_template('public/fsr.html')

def run_fsr(plain_filename, modified_filename, create_section_headers, create_modified_instance):
    plain_path = os.path.join(app.config['CSV_UPLOADS'], plain_filename)
    modified_path = os.path.join(app.config['CSV_UPLOADS'], modified_filename)

    pdf_raw = fsr_csv_opener(file = plain_path)
    mdf_raw = fsr_csv_opener(file = modified_path)

    mdf, mquestions_df, mdict_of_dfs, symbol  = fsr_etl(mdf_raw)
    pdf, pquestions_df, pdict_of_dfs, symbol = fsr_etl(pdf_raw)

    ppl_df = ppl_parser(pdf_raw)
    print_headers = create_section_headers

    fsr = fsr_report(pdf, mdf, ppl_df, mquestions_df, pdict_of_dfs, mdict_of_dfs, print_headers, app, modified_filename, create_modified_instance)

    return fsr

@app.route('/peer_uploader', methods=['GET', 'POST'])
def peer_uploader():
    if request.method == 'POST':
        if request.form['submit_button'] == 'peer':
            # if request.form['clear_cache'] == 'clear_cache':
            #     for file in os.listdir(app.config["ZIP_OUTPUT"]):
            #         if file.endswith(".zip"):
            #             os.remove(os.path.join(app.config["ZIP_OUTPUT"], file))
            #     for file in os.listdir(app.config["REPORTS"]):
            #         os.remove(os.path.join(app.config["REPORTS"], file))
            #     for file in os.listdir(app.config["CHARTS"]):
            #         os.remove(os.path.join(app.config["CHARTS"], file))

            #     return render_template('public/peer.html')

            if 'peer_export' not in request.files:
                # print('no file')
                return redirect(request.url)

            peer_file = request.files['peer_export']


            # if user does not select file, browser also
            # submit a empty part without filename
            if peer_file.filename == '':
                # print('no filename')
                return redirect(request.url)
            else:
                file_name = secure_filename(peer_file.filename)
                peer_file.save(os.path.join(app.config['CSV_UPLOADS'], file_name))

                question_column_width = request.form['create_column_width']
                if not question_column_width:
                    question_column_width = 35
                else:
                    pass

                randomize_peer = request.form['randomize_peer']
                if not randomize_peer:
                    randomize_peer=False
                else:
                    randomize_peer=True

                heat_map_section_bool = request.form['create_heatmap']
                if not heat_map_section_bool:
                    heat_map_section_bool=False
                else:
                    heat_map_section_bool=True

                section_analysis_bool = request.form['create_section_analysis']
                if not section_analysis_bool:
                    section_analysis_bool=False
                else:
                    section_analysis_bool=True

                esigma_bar_chart_bool = request.form['create_esgima_bar_chart']
                if not esigma_bar_chart_bool:
                    esigma_bar_chart_bool=False
                else:
                    esigma_bar_chart_bool=True

                hilo_section_bool = request.form['create_hilo']
                if not hilo_section_bool:
                    hilo_section_bool=False
                else:
                    hilo_section_bool=True

                peer_self_report_bool = request.form['create_cgfr']
                if not peer_self_report_bool:
                    peer_self_report_bool=False
                else:
                    peer_self_report_bool=True

                statistical_analysis_bool = request.form['create_stat']
                if not statistical_analysis_bool:
                    statistical_analysis_bool=False
                else:
                    statistical_analysis_bool=True



                ind_question_column_width = request.form['ind_create_column_width']
                if not ind_question_column_width:
                    ind_question_column_width = 35
                else:
                    pass

                ind_heat_map_section_bool = request.form['ind_create_heatmap']
                if not ind_heat_map_section_bool:
                    ind_heat_map_section_bool=False
                else:
                    ind_heat_map_section_bool=True

                ind_section_analysis_bool = request.form['ind_create_section_analysis']
                if not ind_section_analysis_bool:
                    ind_section_analysis_bool=False
                else:
                    ind_section_analysis_bool=True

                ind_hilo_section_bool = request.form['ind_create_hilo']
                if not ind_hilo_section_bool:
                    ind_hilo_section_bool=False
                else:
                    ind_hilo_section_bool=True

                ind_peer_self_report_bool = request.form['ind_create_cgfr']
                if not ind_peer_self_report_bool:
                    ind_peer_self_report_bool=False
                else:
                    ind_peer_self_report_bool=True

                ind_statistical_analysis_bool = request.form['ind_create_stat']
                if not ind_statistical_analysis_bool:
                    ind_statistical_analysis_bool=False
                else:
                    ind_statistical_analysis_bool=True
                # print('ind_statistical_analysis_bool: ', ind_statistical_analysis_bool)
                peer_filenames = run_peer(file_name, question_column_width, randomize_peer,heat_map_section_bool,section_analysis_bool,esigma_bar_chart_bool,hilo_section_bool,peer_self_report_bool,statistical_analysis_bool,ind_question_column_width,ind_heat_map_section_bool,ind_section_analysis_bool,ind_hilo_section_bool,ind_peer_self_report_bool,ind_statistical_analysis_bool)

                report_name = re.search('^(.*).csv', file_name).group(1)
                # output_filename = 'Peer_Zip_{}'.format(report_name)

                with zipfile.ZipFile(os.path.join(app.config['ZIP_OUTPUT'], '{}.zip'.format(report_name)),'w') as zip:
                    for report in peer_filenames:
                        # print('\n\n\nHERE------------------------------\nbasename: ', report)
                        zip.write(os.path.join(app.config['EXCEL_OUTPUT'], report), basename(report))
                    for file in os.listdir(app.config["REPORTS"]):
                        if file.endswith(".pdf") or file.endswith(".xlsx"):
                            zip.write(os.path.join(app.config['REPORTS'], file), basename(file))
                    for file in os.listdir(app.config["CSV_UPLOADS"]):
                        if file.endswith(".pdf"):
                            zip.write(os.path.join(app.config['CSV_UPLOADS'], file), basename(file))

                for file in os.listdir(app.config["CSV_UPLOADS"]):
                    if file.endswith(".csv"):
                        os.remove(os.path.join(app.config['CSV_UPLOADS'], file))
                for file in os.listdir(app.config["REPORTS"]):
                    if file.endswith(".pdf") or file.endswith(".xlsx"):
                        os.remove(os.path.join(app.config['REPORTS'], file))
                for file in os.listdir(app.config["CHARTS"]):
                    if file.endswith(".png"):
                        os.remove(os.path.join(app.config['CHARTS'], file))


                delete_excel_output()
                return redirect('/downloadzip/'+ '{}.zip'.format(report_name))

        elif request.form['submit_button'] == 'comparative_peer':

            if 'comparative' not in request.files or 'compared' not in request.files:
                # print('no file')
                return redirect(request.url)

            comparative_file = request.files['comparative']
            compared_file = request.files['compared']

            # if user does not select file, browser also
            # submit a empty part without filename
            if comparative_file.filename == '' or compared_file.filename == '':
                # print('no filename')
                return redirect(request.url)
            else:
                comparative_file_name = secure_filename(comparative_file.filename)
                compared_file_name = secure_filename(compared_file.filename)

                comparative_file.save(os.path.join(app.config['CSV_UPLOADS'], comparative_file_name))
                compared_file.save(os.path.join(app.config['CSV_UPLOADS'], compared_file_name))

                question_column_width = request.form['create_column_width']
                if not question_column_width:
                    question_column_width = 35
                else:
                    pass

                randomize_peer = request.form['randomize_peer']
                if not randomize_peer:
                    randomize_peer=False
                else:
                    randomize_peer=True

                heat_map_section_bool = request.form['create_heatmap']
                if not heat_map_section_bool:
                    heat_map_section_bool=False
                else:
                    heat_map_section_bool=True

                section_analysis_bool = request.form['create_section_analysis']
                if not section_analysis_bool:
                    section_analysis_bool=False
                else:
                    section_analysis_bool=True

                esigma_bar_chart_bool = request.form['create_esgima_bar_chart']
                if not esigma_bar_chart_bool:
                    esigma_bar_chart_bool=False
                else:
                    esigma_bar_chart_bool=True

                hilo_section_bool = request.form['create_hilo']
                if not hilo_section_bool:
                    hilo_section_bool=False
                else:
                    hilo_section_bool=True

                peer_self_report_bool = request.form['create_cgfr']
                if not peer_self_report_bool:
                    peer_self_report_bool=False
                else:
                    peer_self_report_bool=True

                statistical_analysis_bool = request.form['create_stat']
                if not statistical_analysis_bool:
                    statistical_analysis_bool=False
                else:
                    statistical_analysis_bool=True


                ind_question_column_width = request.form['ind_create_column_width']
                if not ind_question_column_width:
                    ind_question_column_width = 35
                else:
                    pass

                ind_heat_map_section_bool = request.form['ind_create_heatmap']
                if not ind_heat_map_section_bool:
                    ind_heat_map_section_bool=False
                else:
                    ind_heat_map_section_bool=True

                ind_section_analysis_bool = request.form['ind_create_section_analysis']
                if not ind_section_analysis_bool:
                    ind_section_analysis_bool=False
                else:
                    ind_section_analysis_bool=True

                ind_hilo_section_bool = request.form['ind_create_hilo']
                if not ind_hilo_section_bool:
                    ind_hilo_section_bool=False
                else:
                    ind_hilo_section_bool=True

                ind_peer_self_report_bool = request.form['ind_create_cgfr']
                if not ind_peer_self_report_bool:
                    ind_peer_self_report_bool=False
                else:
                    ind_peer_self_report_bool=True

                ind_statistical_analysis_bool = request.form['ind_create_stat']
                if not ind_statistical_analysis_bool:
                    ind_statistical_analysis_bool=False
                else:
                    ind_statistical_analysis_bool=True

                peer_filenames = run_comparative_peer(comparative_file_name, compared_file_name, question_column_width, randomize_peer,heat_map_section_bool,section_analysis_bool,esigma_bar_chart_bool,hilo_section_bool,peer_self_report_bool,statistical_analysis_bool,ind_question_column_width,ind_heat_map_section_bool,ind_section_analysis_bool,ind_hilo_section_bool,ind_peer_self_report_bool,ind_statistical_analysis_bool)
                # print(peer_filenames)
                report_name = re.search('^(.*).csv', comparative_file_name).group(1)

                with zipfile.ZipFile(os.path.join(app.config['ZIP_OUTPUT'], '{}.zip'.format(report_name)),'w') as zip:
                    for report in peer_filenames:
                        zip.write(os.path.join(app.config['EXCEL_OUTPUT'], report), basename(report))
                    for file in os.listdir(app.config["REPORTS"]):
                        if file.endswith(".pdf") or file.endswith(".xlsx"):
                            zip.write(os.path.join(app.config['REPORTS'], file), basename(file))
                    for file in os.listdir(app.config["CSV_UPLOADS"]):
                        if file.endswith(".pdf"):
                            zip.write(os.path.join(app.config['CSV_UPLOADS'], file), basename(file))

                for file in os.listdir(app.config["CSV_UPLOADS"]):
                    if file.endswith(".csv"):
                        os.remove(os.path.join(app.config['CSV_UPLOADS'], file))
                for file in os.listdir(app.config["REPORTS"]):
                    if file.endswith(".pdf") or file.endswith(".xlsx"):
                        os.remove(os.path.join(app.config['REPORTS'], file))
                for file in os.listdir(app.config["CHARTS"]):
                    if file.endswith(".png"):
                        os.remove(os.path.join(app.config['CHARTS'], file))

                delete_excel_output()
                return redirect('/downloadzip/'+ '{}.zip'.format(report_name))

        elif request.form['submit_button'] == 'peerskills':
            if 'peerskills' not in request.files:
                # print('no file')
                return redirect(request.url)

            file = request.files['peerskills']

            dfs = parse(file)
            skill_matrices(dfs)

            with zipfile.ZipFile(os.path.join(app.config['ZIP_OUTPUT'], '{}.zip'.format('peerSkills')),'w') as zip:
                zip.write(os.path.join(app.config['EXCEL_OUTPUT'], 'peerSkills.xlsx'), basename('peerSkills.xlsx'))

            for file in os.listdir(app.config["CSV_UPLOADS"]):
                if file.endswith(".csv"):
                    os.remove(os.path.join(app.config['CSV_UPLOADS'], file))


            delete_excel_output()
            return redirect('/downloadzip/'+ '{}.zip'.format('peerSkills'))

    return render_template('public/peer.html')

def skill_matrices(df):
    workbook = xlsxwriter.Workbook(os.path.join(app.config['EXCEL_OUTPUT'], 'peerSkills.xlsx'), {'nan_inf_to_errors': True})

    df.reset_index(inplace=True, drop=True)
    fmt = workbook.add_format({"font_name": "Garamond"})
    left_top_fmt = workbook.add_format({"font_name": "Garamond", 'left': 1, 'top': 1})
    top_bold_fmt_left_right = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1, 'left': 1, 'right': 1})
    top_bold_fmt = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1})
    top_right_bold_center = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1, 'right': 1, 'left': 1})
    left_bold_fmt = workbook.add_format({"font_name": "Garamond", 'bold': True, 'left': 1, 'bottom': 1})
    right_bottom_bold_center_fmt = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'right': 1, 'bottom': 1})
    left_fmt = workbook.add_format({"font_name": "Garamond", 'left': 1})
    left_right_fmt = workbook.add_format({"font_name": "Garamond", 'right': 1, 'left': 1})
    bold_fmt = workbook.add_format({"font_name": "Garamond", 'bold': True})
    center_bold_fmt_left_right = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1, 'bottom': 1})
    center_bold_fmt_bottom = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'bottom': 1})
    center_bold = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1})
    center_bold_top = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1, 'top': 1, 'bottom': 1})

    Frames = []

    for ratee in df['ratee'].unique():
        # if ratee in df['rater'].unique():
            # print(ratee)
        for skill in df[df['ratee']==ratee]['skill'].unique():
            # print('here')
            rateeFrame = df[(df['ratee']==ratee) & (df['skill']==skill)]
            rateeFrame['othersRatingOfMeAveraged'] = df[(df['rater']!=ratee) & (df['ratee']==ratee)  & (df['skill']==skill)]['rating'].mean()
            rateeFrame['boardAverageOfSkill'] = df[df['skill']==skill]['rating'].mean()
            # print(rateeFrame)
            Frames.append(rateeFrame)

    finalFrame = pd.concat(Frames)
    # print(finalFrame)
    for ratee in finalFrame['ratee'].unique():
        len_of_rater_df = len(finalFrame[finalFrame['ratee'] == ratee])
        ratee_df = finalFrame[(finalFrame['ratee'] == ratee) & (finalFrame['rater']==ratee)]
        ratee_df = ratee_df.sort_values(['rating', 'othersRatingOfMeAveraged', 'boardAverageOfSkill'], ascending=False)

        worksheet = workbook.add_worksheet(str(ratee[0:30]))
        worksheet.merge_range(1, 1, 1, 4, ratee, top_right_bold_center)
        worksheet.write(2, 1, 'Skill Areas:', left_bold_fmt)
        worksheet.write(2, 2, 'Self-Rating', center_bold_fmt_bottom)
        worksheet.write(2, 3, 'Peer Rating Average', center_bold_fmt_bottom)
        worksheet.write(2, 4, 'Board Overall Average.', right_bottom_bold_center_fmt)

        ten_format = workbook.add_format({'bg_color': '#63be7b'})
        nine_format = workbook.add_format({'bg_color': '#86c97e'})
        eight_format = workbook.add_format({'bg_color': '#a9d27f'})
        seven_format = workbook.add_format({'bg_color': '#ccdd82'})
        six_format = workbook.add_format({'bg_color': '#eee683'})
        five_format = workbook.add_format({'bg_color': '#fedd81'})
        four_format = workbook.add_format({'bg_color': '#fcbf7b'})
        three_format = workbook.add_format({'bg_color': '#fba276'})
        two_format = workbook.add_format({'bg_color': '#f98570'})
        one_format = workbook.add_format({'bg_color': '#f8696b'})

        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '9.01',
        'maximum': '10',
        'format': ten_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '8.01',
        'maximum': '9',
        'format': nine_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '7.01',
        'maximum': '8',
        'format': eight_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '6.01',
        'maximum': '7',
        'format': seven_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '5.01',
        'maximum': '6',
        'format': six_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '4.01',
        'maximum': '5',
        'format': five_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '3.01',
        'maximum': '4',
        'format': four_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '2.01',
        'maximum': '3',
        'format': three_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '1.01',
        'maximum': '2',
        'format': two_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '0.01',
        'maximum': '1',
        'format': one_format})

        worksheet.write_column(3, 1, ratee_df['skill'].unique().tolist(), left_fmt)
        beginRow = 3
        for skill in ratee_df['skill'].unique():
            worksheet.write(beginRow, 2, ratee_df['rating'].tolist()[beginRow-3], left_fmt)
            beginRow += 1
        beginRow = 3
        for skill in ratee_df['skill'].unique():
            worksheet.write(beginRow, 3, float('{:.2f}'.format(round(ratee_df['othersRatingOfMeAveraged'].tolist()[beginRow-3], 2))), left_fmt)
            beginRow += 1
        beginRow = 3
        for skill in ratee_df['skill'].unique():
            worksheet.write(beginRow, 4, float('{:.2f}'.format(round(ratee_df['boardAverageOfSkill'].tolist()[beginRow-3], 2))), left_right_fmt)
            beginRow += 1

        bold_align_right_fmt = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'right', 'left': 1, 'top': 1, 'bottom': 1})
        worksheet.write(len(ratee_df) + 3, 1, 'Average:', bold_align_right_fmt)
        worksheet.write(len(ratee_df)+3, 2, "{0:.2f}".format(ratee_df['rating'].mean()), center_bold_top)
        top_bold_fmt = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1, 'bottom': 1})
        top_right_bold_center_fmt = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'right': 1, 'top': 1, 'bottom': 1})

        worksheet.write(len(ratee_df)+3, 3, "{0:.2f}".format(ratee_df['othersRatingOfMeAveraged'].mean()), top_bold_fmt)
        worksheet.write(len(ratee_df)+3, 4, "{0:.2f}".format(ratee_df['boardAverageOfSkill'].mean()), top_right_bold_center_fmt)



    worksheet = workbook.add_worksheet('Summary')
    worksheet.write_row(0, 0, finalFrame.columns)
    colNum = 0
    for col in finalFrame.columns:
        worksheet.write_column(1, colNum, finalFrame[col])
        colNum += 1
    workbook.close()
    return

def parse(file):

    df = pd.read_csv(file)

    columns = list(df.columns)
    columns = [re.sub(r'\xa0', ' ', c) for c in columns]
    df.columns = columns

    names_and_skills = re.compile('.*\d+\.\s+.*_.*_$')
    names_and_skills = list(filter(names_and_skills.match, columns))

    name_and_skill = []

    if 'MiddleName' in df.columns:
        raters = df['FirstName'].astype(str) + ' ' + df['MiddleName'].fillna('').astype(str) + ' ' + df['LastName'].astype(str)
    else:
        raters = df['FirstName'].astype(str) + ' ' + df['LastName'].astype(str)
    raters = raters.apply(lambda row: re.sub('\s+', ' ', row).strip())



    dfs = []

    for n in names_and_skills:
        values = df[n]
        match = re.findall(".*\d+\.\s+(.*):\s*_(.*?)_", n)
        # match = re.search(".*\d+\.\s+(.*):\s*_(.*?)_", n)
        ratee = match[0][0]
        skill = match[0][1]
        fill = {'rater': raters.tolist(), 'ratee': [ratee] * len(raters), 'skill': [skill] * len(raters), 'rating': values}
        skill_matrix=pd.DataFrame(fill)
        dfs.append(skill_matrix)

    dfs = pd.concat(dfs)
    return dfs

def run_comparative_peer(comparative_file_name, compared_file_name, question_column_width, randomize_peer,heat_map_section_bool,section_analysis_bool,esigma_bar_chart_bool,hilo_section_bool,peer_self_report_bool,statistical_analysis_bool,ind_question_column_width,ind_heat_map_section_bool,ind_section_analysis_bool,ind_hilo_section_bool,ind_peer_self_report_bool,ind_statistical_analysis_bool):
    comparative_file = os.path.join(app.config['CSV_UPLOADS'], comparative_file_name)
    compared_file = os.path.join(app.config['CSV_UPLOADS'], compared_file_name)

    files = [comparative_file_name, compared_file_name]

    years = {}
    for f in files:
        year = f[-8:-4]
        years[int(year)] = {'file': f, 'file_path':os.path.join(app.config['CSV_UPLOADS'], f)}

    sorted_years = OrderedDict(sorted(years.items(), reverse=True))
    # print('sorted_years: ', sorted_years)

    current_year = max(sorted_years.keys())
    current_year_file = comparative_file

    # print(sorted_years)
    # print(comparative_file, comparative_file_name)
    # print(compared_file, compared_file_name)

    for year in sorted_years.keys():
        sorted_years[year]['raw_data_dict'] = csv_opener(sorted_years.get(year).get('file_path'))

    for year in sorted_years.keys():
        sorted_years[year]['dict_of_dfs'] = peer_splitter(sorted_years.get(year).get('file'), sorted_years.get(year).get('raw_data_dict'))

    for year in sorted_years.keys():
        sorted_years[year]['ppl_df'] = ppl_parser(sorted_years.get(year).get('dict_of_dfs').get('meta'))

    for year in sorted_years.keys():
        # print('\n\nyear: ', year)
        for year2 in sorted_years.keys():
            # print('year2: ', year2)
            if set(sorted_years[year]['ppl_df'].index) != set(sorted_years[year2]['ppl_df'].index):
                warnings.warn('UniqueIdentifier columns do not match!')

    for year in sorted_years.keys():
        for report_type in sorted_years.get(year).get('dict_of_dfs'):
            if report_type != 'meta':
                sorted_years[year]['not_app_or_obs'] = {}
                sorted_years[year]['ETLd_df'] = {}
                sorted_years[year]['data_dict'] = {}
                # sorted_years[year]['report_types'][report_type]['smoothed_df'] = {}
                # print(sorted_years)
                report_to_run = sorted_years[year]['dict_of_dfs'].get(report_type)

                questions_df, transformed_df, na_no_null_dict = peer_etl(report_to_run, sorted_years.get(year).get('ppl_df'), report_type, sorted_years.get(year).get('file'), True, year=year)

                # Check for no-self rating type peer reports
                ids_respondents = []
                respondent__self_rate_bool_list = []
                for respondent in questions_df[~(questions_df['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (~questions_df['sub_section'].isin(['', None, np.nan]))]['sub_section'].unique():
                    id = questions_df[~(questions_df['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (questions_df['sub_section']==respondent)]['UniqueIdentifier'].unique()[0]
                    # print('id: ', id)
                    # print('respondent: ', respondent)
                    ids_respondents.append((id, respondent))

                    raw_question_list = questions_df[(questions_df['sub_section']==respondent) & (questions_df['type'].isin(['likert', 'matrix']))]['question_raw']
                    self_ratings = transformed_df[raw_question_list].loc[id].unique().tolist()
                    # print('raw_question_list: ', raw_question_list)
                    # print('self_ratings: ', self_ratings)
                    if len(self_ratings) == 1:
                        # print('len(self_ratings) == 1: ', len(self_ratings) == 1)
                        import math
                        if math.isnan(self_ratings[0]):
                            # print('nan')
                            respondent__self_rate_bool_list.append((respondent, True))
                        else:
                            respondent__self_rate_bool_list.append((respondent, False))
                    else:
                        respondent__self_rate_bool_list.append((respondent, False))
                no_self_ratings_bool = all(item[-1] == True for item in respondent__self_rate_bool_list)
                # print('no_self_ratings_bool: ', no_self_ratings_bool)
                sorted_years[year]['no_self_ratings_bool'] = no_self_ratings_bool

                sorted_years[year]['questions_df'] = questions_df

                sorted_years[year]['ETLd_df'] = transformed_df
                sorted_years[year]['na_no_null_dict'] = na_no_null_dict

                smoothed_df = comparative_join_qdf_and_rawdata_into_smoothed_df(questions_df, transformed_df, sorted_years.get(year).get('file'), report_type, False)
                sorted_years[year]['smoothed_df'] = smoothed_df

    ###################################################################

    jinja_composite_data_dict_randomized_named = create_comparative_peer_composite_jinja_dict(sorted_years, False, randomize_peer, sorted_years[current_year]['no_self_ratings_bool'])
    sorted_years[current_year]['named'] = jinja_composite_data_dict_randomized_named

    jinja_composite_data_dict_randomized_anonymized = create_comparative_peer_composite_jinja_dict(sorted_years, True, randomize_peer, sorted_years[current_year]['no_self_ratings_bool'])
    sorted_years[current_year]['anonymized'] = jinja_composite_data_dict_randomized_anonymized

    comparative_year_number = max(sorted_years.keys())
    compared_year_number = min(sorted_years.keys())
    # print(comparative_year_number)
    # print(sorted_years.get(comparative_year_number))

    comparative_year = sorted_years.get(comparative_year_number)
    # print()
    # for year in sorted_years.keys():
    #     for report_type in sorted_years.get(year).get('dict_of_dfs'):
    #         if report_type != 'meta':
                # print('Creating {} report for {}'.format(year, report_type))

    report_to_run = comparative_year.get('ETLd_df')

    questions_df = comparative_year.get('questions_df')
    if set(questions_df['type']).issubset(set(['skills_matrix', ''])):
        skills_response = True
    else:
        skills_response = False


    ####################################################################
    peer_reports = []

    PEER_COMPOSITE = peer_composite_report(comparative_year.get('file'), 'Peer', report_to_run, questions_df, sorted_years[comparative_year_number]['ppl_df'], sorted_years[comparative_year_number]['na_no_null_dict'], skills_response, randomize_peer, app, no_self_ratings_bool=sorted_years[comparative_year_number]['no_self_ratings_bool'], year=comparative_year_number)
    peer_reports.extend(PEER_COMPOSITE.output_file_names)

    member_variable_query = dict()
    member_variable_query['comparative_year_number'] = comparative_year_number


    try:
        member_variable_query['skills_matrix_average'] = PEER_COMPOSITE.skills_matrix_average
        # member_variable_query['number_of_skills_matrix_questions'] = PEER_COMPOSITE.number_of_skills_matrix_questions
        member_variable_query['skills_matrix_df'] = PEER_COMPOSITE.skills_matrix_export
        member_variable_query['skills_matrix_df_width'] = np.arange(1, PEER_COMPOSITE.skills_matrix_export.shape[1] + 1, 1)
    except:
        pass


    member_variable_query['section_data'] = PEER_COMPOSITE.section_data
    member_variable_query['questions_df'] = PEER_COMPOSITE.questions_df

    if sorted_years[comparative_year_number]['no_self_ratings_bool']:
            member_variable_query['number_of_respondents'] = PEER_COMPOSITE.number_of_respondents - 1
    else:
        member_variable_query['number_of_respondents'] = PEER_COMPOSITE.number_of_respondents
    member_variable_query['section_averages'] = PEER_COMPOSITE.averages
    member_variable_query['five_point_scale'] = PEER_COMPOSITE.five_point_scale
    member_variable_query['eval_stats_list_of_dicts'] = PEER_COMPOSITE.eval_stats_list_of_dicts
    member_variable_query['eval_stats_list_of_dicts_last_item'] = tuple((list(PEER_COMPOSITE.eval_stats_list_of_dicts[-1].keys())[0], PEER_COMPOSITE.eval_stats_list_of_dicts[-1].get(list(PEER_COMPOSITE.eval_stats_list_of_dicts[-1].keys())[0])))
    member_variable_query['section_avg_list'] = PEER_COMPOSITE.section_avg_list
    member_variable_query['section_avg_list_last_item'] = tuple((list(PEER_COMPOSITE.section_avg_list[-1].keys())[0], PEER_COMPOSITE.section_avg_list[-1].get(list(PEER_COMPOSITE.section_avg_list[-1].keys())[0])))

    member_variable_query['jinja_hi'] = PEER_COMPOSITE.jinja_hi
    member_variable_query['jinja_lo'] = PEER_COMPOSITE.jinja_lo
    member_variable_query['number_of_comparisons'] = 1

    member_variable_query['eSigma_value'] = PEER_COMPOSITE.composite_eSigma

    member_variable_query['comparative_hm_section_length_counts'] = PEER_COMPOSITE.hm_section_length_counts
    member_variable_query['comparative_hm_numbers_borders'] = PEER_COMPOSITE.hm_numbers_borders
    member_variable_query['comparative_heat_map_averages'] = PEER_COMPOSITE.heat_map_averages
    member_variable_query['comparative_hm_rows_directions'] = PEER_COMPOSITE.hm_rows_directions
    member_variable_query['comparative_percentage_size'] = PEER_COMPOSITE.percentage_size
    member_variable_query['comparative_section_number_merge_ranges'] = PEER_COMPOSITE.section_number_merge_ranges
    member_variable_query['comparative_section_number_averages'] = PEER_COMPOSITE.section_number_averages

    member_variable_query['header_font_size'] = 11
    if PEER_COMPOSITE.percentage_size<13 and PEER_COMPOSITE.percentage_size>10:
        member_variable_query['header_font_size'] = 10
    elif PEER_COMPOSITE.percentage_size and PEER_COMPOSITE.percentage_size<=10:
        member_variable_query['header_font_size'] = 9

    member_variable_query['number_font_size'] = 10
    if PEER_COMPOSITE.percentage_size<13 and PEER_COMPOSITE.percentage_size>10:
        member_variable_query['number_font_size'] = 8
    elif PEER_COMPOSITE.percentage_size<=10:
        member_variable_query['number_font_size'] = 7

    hm_cell_height = 10
    if PEER_COMPOSITE.number_of_respondents < 16 and PEER_COMPOSITE.number_of_respondents >= 10:
        hm_cell_height = 8
    elif PEER_COMPOSITE.number_of_respondents < 20 and PEER_COMPOSITE.number_of_respondents >= 16:
        hm_cell_height = 6
    elif PEER_COMPOSITE.number_of_respondents >= 20:
        hm_cell_height = 5
    member_variable_query['hm_cell_height'] = hm_cell_height

    if PEER_COMPOSITE.percentage_size < 2:
        member_variable_query['comparative_extra_large_hm'] = True
    else:
        member_variable_query['comparative_extra_large_hm'] = False
    ############################################################################
    compared_year_report_to_run = sorted_years[compared_year_number]['ETLd_df']
    compared_year_questions_df = sorted_years[compared_year_number]['questions_df']

    # if bool([a for a in sorted_years[comparative_year_number].get('na_no_null_dict').values() if bool(a)]):
    #     if na_present == True:
    #         pass
    #     else:
    #         na_present = True
    #
    # if bool([a for a in sorted_years[compared_year_number].get('na_no_null_dict').values() if bool(a)]):
    #     if na_present == True or compared_year_na_present:
    #         pass
    #     else:
    #         na_present = True
    #         compared_year_na_present = True

    # print('na_present: ', na_present)
    # print('compared_year_na_present: ', compared_year_na_present)

    member_variable_query['compared_year_number'] = compared_year_number
    b = peer_composite_report(sorted_years.get(compared_year_number).get('file'), 'Peer', compared_year_report_to_run, compared_year_questions_df, sorted_years.get(compared_year_number).get('ppl_df'), sorted_years[compared_year_number].get('na_no_null_dict'), skills_response, randomize_peer, app, no_self_ratings_bool=sorted_years[compared_year_number]['no_self_ratings_bool'], year=compared_year_number)
    peer_reports.extend(b.output_file_names)

    try:
        # member_variable_query['b_skills_matrix_average'] = b.skills_matrix_average
        # member_variable_query['b_number_of_skills_matrix_questions'] = b.number_of_skills_matrix_questions
        member_variable_query['b_skills_matrix_df'] = b.skills_matrix_export
        member_variable_query['also_b_has_a_skills_matrix'] = True
        member_variable_query['b_skills_matrix_df_width'] = np.arange(1, b.skills_matrix_export.shape[1] + 1, 1)
    except:
        pass
    member_variable_query['compared_hm_section_length_counts'] = b.hm_section_length_counts
    member_variable_query['compared_hm_numbers_borders'] = b.hm_numbers_borders
    member_variable_query['compared_heat_map_averages'] = b.heat_map_averages
    member_variable_query['compared_hm_rows_directions'] = b.hm_rows_directions
    member_variable_query['compared_percentage_size'] = b.percentage_size

    if b.percentage_size < 2:
        member_variable_query['compared_extra_large_hm'] = True
    else:
        member_variable_query['compared_extra_large_hm'] = False

    member_variable_query['compared_section_number_merge_ranges'] = b.section_number_merge_ranges
    member_variable_query['compared_section_number_averages'] = b.section_number_averages
    member_variable_query['b_esigma'] = b.composite_eSigma

    formattedText = 'The average rating for this section is {} ({} - {}). The average rating for each question in the section ranged from {} to {}. Individual ratings ranged from {} to {}.'
    equal_formattedText = 'The average rating for each question and this section is {} ({} - {}). Individual ratings ranged from {} to {}.'
    compared_section_data = dict()

    for section in PEER_COMPOSITE.section_data:
        if section in list(PEER_COMPOSITE.averages.keys()):
            if PEER_COMPOSITE.questionAveragesBySectionMinsAndMaxes.get(section).get('max') == PEER_COMPOSITE.questionAveragesBySectionMinsAndMaxes.get(section).get('min') and (PEER_COMPOSITE.questionAveragesBySectionMinsAndMaxes.get(section).get('max') == PEER_COMPOSITE.averages.get(section)):
                compared_section_data[section] = equal_formattedText.format(PEER_COMPOSITE.averages.get(section), b.averages.get(section), compared_year_number, PEER_COMPOSITE.sectionMinsAndMaxes.get(section).get('min'), PEER_COMPOSITE.sectionMinsAndMaxes.get(section).get('max'))
            else:
                compared_section_data[section] = formattedText.format(PEER_COMPOSITE.averages.get(section), b.averages.get(section), compared_year_number,
                    PEER_COMPOSITE.questionAveragesBySectionMinsAndMaxes.get(section).get('min'),
                    PEER_COMPOSITE.questionAveragesBySectionMinsAndMaxes.get(section).get('max'),
                    PEER_COMPOSITE.sectionMinsAndMaxes.get(section).get('min'),
                    PEER_COMPOSITE.sectionMinsAndMaxes.get(section).get('max'))
        else:
            compared_section_data[section] = 'The open-ended questions provide directors with an opportunity to openly and anonymously comment on their colleagues’ competency and overall performance.'

    member_variable_query['compared_section_data'] = compared_section_data

    ############################################################################
    # na_present = False
    compared_year_na_present = False
    comparative_year_na_present = False

    eval_stats_list_of_dicts = PEER_COMPOSITE.eval_stats_list_of_dicts
    for category in eval_stats_list_of_dicts:
        category_key = list(category.keys())[0]
        if category_key in ['Percent of ratings of N/A', 'Number of ratings of N/A']:
            # na_present = True
            comparative_year_na_present = True

    eval_stats_list_of_dicts = b.eval_stats_list_of_dicts
    for category in eval_stats_list_of_dicts:
        category_key = list(category.keys())[0]
        if category_key in ['Percent of ratings of N/A', 'Number of ratings of N/A']:
            # na_present = True
            compared_year_na_present = True

    # print('na_present: ', na_present)
    # print('comparative_year_na_present: ', comparative_year_na_present)
    # print('compared_year_na_present: ', compared_year_na_present)

    if b.na_in_heat_map or PEER_COMPOSITE.na_in_heat_map:
        member_variable_query['na_in_heat_map'] = True
    else:
        member_variable_query['na_in_heat_map'] = False


    ############################################################################
    eval_stats_list_of_dicts = PEER_COMPOSITE.eval_stats_list_of_dicts
    new_eval_list_of_dicts = list()
    add = False
    for category in eval_stats_list_of_dicts:
        category_key = list(category.keys())[0]
        if category_key in ['Question analysis by Board member response:', 'Group response analysis:']:
            new_eval_list_of_dicts.append({category_key: ['']*2})
            continue
        score = category.get(list(category.keys())[0], '')
        list_of_scores = list()
        list_of_scores.append(score)
        score_list = b.eval_stats_list_of_dicts
        for i in score_list:
            for k, v in i.items():
                if k == category_key:
                    list_of_scores.append(i.get(category_key, ''))
                else:
                    pass

        # If there are n/a in the current year, but not last year's
        if (category_key in ['Percent of ratings of N/A', 'Number of ratings of N/A']) and (len(list_of_scores)==1):
            if category_key == 'Percent of ratings of N/A':
                list_of_scores.append('0%')
            elif category_key == 'Number of ratings of N/A':
                list_of_scores.append('0')

        if new_eval_list_of_dicts:
            # If there are n/a in last year but not the current year

            # Doesnt add the the last item properly
            # print(compared_year_na_present,comparative_year_na_present,list(new_eval_list_of_dicts[-1].keys())[0])
            if compared_year_na_present and not comparative_year_na_present and (category_key in ['Number of ratings of 3 or below', 'Number of ratings of 2 or below']):
                for i in score_list:
                    for k, v in i.items():
                        if k == 'Number of ratings of N/A':
                            add1_list_of_scores = ['0', i.get(k)]
                            add1_category_key = 'Number of ratings of N/A'
                            add = True
            if compared_year_na_present and not comparative_year_na_present and (category_key in ['Percent of ratings of 2 or below', 'Percent of ratings of 3 or below']):
                for i in score_list:
                    for k, v in i.items():
                        if k == 'Percent of ratings of N/A':
                            add1_list_of_scores = ['0%', i.get(k)]
                            add1_category_key = 'Percent of ratings of N/A'
                            add = True

        if add:
            new_eval_list_of_dicts.append({category_key: list_of_scores})
            new_eval_list_of_dicts.append({add1_category_key: add1_list_of_scores})
            add = False
        else:
            new_eval_list_of_dicts.append({category_key: list_of_scores})

    member_variable_query['eval_stats_list_of_dicts'] = new_eval_list_of_dicts
    ############################################################################

    # member_variable_query['section_avg_list_last_item'] = tuple((list(BEA.section_avg_list[-1].keys())[0], BEA.section_avg_list[-1].get(list(BEA.section_avg_list[-1].keys())[0])))
    # section_avg_list_last_item = tuple((list(BEA.section_avg_list[-1].keys())[0], BEA.section_avg_list[-1].get(list(BEA.section_avg_list[-1].keys())[0])))
    # print('here: ', section_avg_list_last_item)

    section_avg_list = PEER_COMPOSITE.section_avg_list
    new_section_avg_list = list()
    for section in section_avg_list:
        section_key = list(section.keys())[0]
        section_avg = section.get(list(section.keys())[0], '')
        # new_section_avg_list.append({section_key: []})
        list_of_section_averages = list()
        list_of_section_averages.append(section_avg)
        for item in [b]:
            avg_list = item.section_avg_list
            for i in avg_list:
                for k, v in i.items():
                    if k == section_key:
                        # if i == avg_list[-1]:
                        #     section_avg_list_last_item[1].append(i.get(section_key, ''))
                        # else:
                        list_of_section_averages.append(i.get(section_key, ''))

                    else:
                        pass
        new_section_avg_list.append({section_key: list_of_section_averages})

    # print('new_section_avg_list: ', new_section_avg_list)
    for section in new_section_avg_list:
        if len(section.get(list(section.keys())[0], '')) == 1:
            section.get(list(section.keys())[0], '').append('')

    # print('new_section_avg_list: ', new_section_avg_list)
    # print('section_avg_list_last_item: ', section_avg_list_last_item)
    member_variable_query['section_avg_list'] = new_section_avg_list

    member_variable_query['jinja_for_loop_dict'] = sorted_years[current_year]['named']
    comparative_peer_composite_templater(comparative_year.get('file'), member_variable_query, 'Peer', report_to_run, questions_df, sorted_years[comparative_year_number]['ppl_df'], question_column_width, randomize_peer,heat_map_section_bool,section_analysis_bool,esigma_bar_chart_bool,hilo_section_bool,peer_self_report_bool,statistical_analysis_bool, randomize = randomize_peer, anonymized=False)

    member_variable_query['jinja_for_loop_dict'] = sorted_years[current_year]['anonymized']

    member_variable_query['comparative_hm_rows_directions'] = PEER_COMPOSITE.anon_hm_rows_directions
    member_variable_query['comparative_percentage_size'] = PEER_COMPOSITE.anon_percentage_size

    member_variable_query['compared_hm_rows_directions'] = b.anon_hm_rows_directions
    member_variable_query['compared_percentage_size'] = b.anon_percentage_size

    comparative_peer_composite_templater(comparative_year.get('file'), member_variable_query, 'Peer', report_to_run, questions_df, sorted_years[comparative_year_number]['ppl_df'], question_column_width, randomize_peer,heat_map_section_bool,section_analysis_bool,esigma_bar_chart_bool,hilo_section_bool,peer_self_report_bool,statistical_analysis_bool, randomize = randomize_peer, anonymized=True)
    ###########################################################################

    # print('Creating Individual Peer Reports for {}\n\n'.format(current_year_file))

    member_variable_query = dict()

    member_variable_query['comparative_year_number'] = comparative_year_number
    member_variable_query['compared_year_number'] = compared_year_number


    PEER_INDIVIDUALS = peer_individual_reports(comparative_year.get('file'), 'Peer', report_to_run, questions_df, sorted_years[comparative_year_number]['ppl_df'], sorted_years[comparative_year_number]['na_no_null_dict'], app, randomize = randomize_peer, no_self_ratings_bool=sorted_years[comparative_year_number]['no_self_ratings_bool'], year=comparative_year_number)
    peer_reports.extend(PEER_INDIVIDUALS.output_file_names)

    compared_year_report_to_run = sorted_years[compared_year_number]['ETLd_df']
    compared_year_questions_df = sorted_years[compared_year_number]['questions_df']

    member_variable_query['compared_year_number'] = compared_year_number
    b = peer_individual_reports(sorted_years.get(compared_year_number).get('file'), 'Peer', compared_year_report_to_run, compared_year_questions_df, sorted_years.get(compared_year_number).get('ppl_df'), sorted_years[compared_year_number].get('na_no_null_dict'), app, randomize = randomize_peer, no_self_ratings_bool=sorted_years[compared_year_number]['no_self_ratings_bool'], year=compared_year_number)
    peer_reports.extend(b.output_file_names)

    smoothed_df = join_qdf_and_rawdata_into_smoothed_df(questions_df, report_to_run, comparative_year.get('file'), 'Peer', False)
    for id in sorted_years[comparative_year_number]['ppl_df'].index:
        # print('id', id)
        if id in sorted_years[compared_year_number]['ppl_df'].index:
            director_in_last_year = True
            # print('director found in last year input file.')
        else:
            director_in_last_year = False
            # print('director not found in last year input file!')
        member_variable_query['director_in_last_year'] = director_in_last_year

        director = PEER_INDIVIDUALS.questions_df[~(PEER_INDIVIDUALS.questions_df['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (PEER_INDIVIDUALS.questions_df['UniqueIdentifier']==id)]['sub_section'].unique()[0]


        jinja_for_loop_dict = create_comparative_peer_individuals_jinja_dict(id, director, sorted_years, smoothed_df, comparative_year.get('file'), 'Peer', no_self_ratings_bool, False, True)

        member_variable_query['initials'] = PEER_INDIVIDUALS.initials.get(director)

        if b.na_in_heat_maps.get(director, False) or PEER_INDIVIDUALS.na_in_heat_maps.get(director, False):
            member_variable_query['na_in_heat_map'] = True
        else:
            member_variable_query['na_in_heat_map'] = False

        try:
            member_variable_query['skills_matrix_average'] = PEER_COMPOSITE.skills_matrix_average
            member_variable_query['number_of_skills_matrix_questions'] = PEER_COMPOSITE.number_of_skills_matrix_questions
        except:
            pass

        member_variable_query['section_data'] = PEER_INDIVIDUALS.section_data[director]

        member_variable_query['questions_df'] = PEER_INDIVIDUALS.questions_df
        member_variable_query['jinja_for_loop_dict'] = jinja_for_loop_dict.get(director)

        member_variable_query['question_column_width'] = ind_question_column_width
        member_variable_query['heat_map_section_bool'] = ind_heat_map_section_bool
        member_variable_query['peer_self_comparison_bool'] = ind_section_analysis_bool
        member_variable_query['hilo_section_bool'] = ind_hilo_section_bool
        member_variable_query['peer_self_report_bool'] = ind_peer_self_report_bool
        member_variable_query['statistical_analysis_bool'] = ind_statistical_analysis_bool

        member_variable_query['number_of_respondents'] = PEER_INDIVIDUALS.number_of_respondents

        member_variable_query['section_averages'] = PEER_INDIVIDUALS.averages_data[director]
        member_variable_query['five_point_scale'] = PEER_INDIVIDUALS.five_point_scale
        member_variable_query['eval_stats_list_of_dicts'] = PEER_INDIVIDUALS.agg_section_data[director]
        member_variable_query['eval_stats_list_of_dicts_last_item'] = tuple((list(PEER_INDIVIDUALS.agg_section_data[director][-1].keys())[0], PEER_INDIVIDUALS.agg_section_data[director][-1].get(list(PEER_INDIVIDUALS.agg_section_data[director][-1].keys())[0])))
        member_variable_query['section_avg_list'] = PEER_INDIVIDUALS.section_avg_list[director]
        member_variable_query['section_avg_list_last_item'] = tuple((list(PEER_INDIVIDUALS.section_avg_list[director][-1].keys())[0], PEER_INDIVIDUALS.section_avg_list[director][-1].get(list(PEER_INDIVIDUALS.section_avg_list[director][-1].keys())[0])))
        member_variable_query['jinja_hi'] = PEER_INDIVIDUALS.hilo_data[director]['jinja_hi']
        member_variable_query['jinja_lo'] = PEER_INDIVIDUALS.hilo_data[director]['jinja_lo']
        member_variable_query['number_of_comparisons'] = 1

        ############################################################################
        member_variable_query['comparative_hm_section_length_counts'] = PEER_INDIVIDUALS.heat_map_data[director].get('hm_section_length_counts')
        member_variable_query['comparative_hm_numbers_borders'] = PEER_INDIVIDUALS.heat_map_data[director].get('hm_numbers_borders')
        member_variable_query['comparative_heat_map_averages'] = PEER_INDIVIDUALS.heat_map_data[director].get('heat_map_averages')
        member_variable_query['comparative_hm_rows_directions'] = PEER_INDIVIDUALS.heat_map_data[director].get('hm_rows_directions')
        member_variable_query['comparative_percentage_size'] = PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')
        member_variable_query['hm_cell_height'] = PEER_INDIVIDUALS.hm_cell_height

        if director_in_last_year:
            try:
                member_variable_query['compared_hm_section_length_counts'] = b.heat_map_data[director].get('hm_section_length_counts')
                member_variable_query['compared_hm_numbers_borders'] = b.heat_map_data[director].get('hm_numbers_borders')
                member_variable_query['compared_heat_map_averages'] = b.heat_map_data[director].get('heat_map_averages')
                member_variable_query['compared_hm_rows_directions'] = b.heat_map_data[director].get('hm_rows_directions')
                member_variable_query['compared_percentage_size'] = b.heat_map_data[director].get('percentage_size')
                # member_variable_query['director_in_last_year_hm'] = True
            except:
                pass
                # member_variable_query['director_in_last_year_hm'] = False

        if director_in_last_year:
            try:
                # for i in b.skills_matrix_df.keys():
                #     print(re.sub(u'\u2019', "'", i))
                #     print(b.skills_matrix_df.get(i))
                member_variable_query['last_year_skills_matrix_df'] = b.skills_matrix_df[director]
                member_variable_query['last_year_skills_matrix_info'] = b.skills_matrix_info[director]
            except:
                member_variable_query['last_year_skills_matrix_df'] = pd.DataFrame()
                member_variable_query['last_year_skills_matrix_info'] = dict()
                member_variable_query['last_year_skills_matrix_info']['Self-Average'] = ''
                member_variable_query['last_year_skills_matrix_info']['Board-Average'] = ''
            # print(member_variable_query['last_year_skills_matrix_df'])
            # print(member_variable_query['last_year_skills_matrix_info'])                

        member_variable_query['header_font_size'] = 11
        if PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')<13 and PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')>10:
            member_variable_query['header_font_size'] = 10
        elif PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')<=10:
            member_variable_query['header_font_size'] = 9

        member_variable_query['number_font_size'] = 10
        if PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')<13 and PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')>10:
            member_variable_query['number_font_size'] = 8
        elif PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')<=10:
            member_variable_query['number_font_size'] = 7

        ############################################################################
        member_variable_query['eSigma_value'] = PEER_INDIVIDUALS.esigmas[director]
        if director_in_last_year:
            member_variable_query['b_esigma'] = b.esigmas.get(director, 'N/A')
        else:
            member_variable_query['b_esigma'] = ''

        ############################################################################

        member_variable_query['no_self_ratings_bool'] = sorted_years[comparative_year_number]['no_self_ratings_bool']


        eval_stats_list_of_dicts = PEER_INDIVIDUALS.agg_section_data[director]
        new_eval_list_of_dicts = []
        add = False
        if director_in_last_year:
            # print(score_list)
            # print(comparative_year_na_present, compared_year_na_present)
            for category in eval_stats_list_of_dicts:
                category_key = list(category.keys())[0]
                # print(category)
                if category_key in ['Question analysis by Board member response:', 'Group response analysis:']:
                    new_eval_list_of_dicts.append({category_key: ['']*2})
                    continue
                score = category.get(list(category.keys())[0], '')
                list_of_scores = list()
                list_of_scores.append(score)
                for item in [b]:
                    # UPDATED
                    score_list = item.agg_section_data.get(director, '')
                    for i in score_list:
                        for k, v in i.items():
                            if k == category_key:
                                list_of_scores.append(i.get(category_key, ''))
                            else:
                                pass

                # If there are n/a in the current year, but not last year's
                if (category_key in ['Percent of ratings of N/A', 'Number of ratings of N/A']) and (len(list_of_scores)==1):
                    if category_key == 'Percent of ratings of N/A':
                        list_of_scores.append('0%')
                    elif category_key == 'Number of ratings of N/A':
                        list_of_scores.append('0')

                if new_eval_list_of_dicts:
                    # If there are n/a in last year but not the current year
                    if compared_year_na_present and not comparative_year_na_present and (category_key in ['Number of ratings of 3 or below', 'Number of ratings of 2 or below']):
                        for i in score_list:
                            for k, v in i.items():
                                if k == 'Number of ratings of N/A':
                                    add1_list_of_scores = ['0', i.get(k)]
                                    add1_category_key = 'Number of ratings of N/A'
                                    add = True

                    elif compared_year_na_present and not comparative_year_na_present and (category_key in ['Percent of ratings of 2 or below', 'Percent of ratings of 3 or below']):
                        for i in score_list:
                            for k, v in i.items():
                                if k == 'Percent of ratings of N/A':
                                    add1_list_of_scores = ['0%', i.get(k)]
                                    add1_category_key = 'Percent of ratings of N/A'
                                    add = True

                if add:
                    new_eval_list_of_dicts.append({category_key: list_of_scores})
                    new_eval_list_of_dicts.append({add1_category_key: add1_list_of_scores})
                    add = False
                else:
                    new_eval_list_of_dicts.append({category_key: list_of_scores})
        else:
            for category in eval_stats_list_of_dicts:
                category_key = list(category.keys())[0]
                if category_key in ['Question analysis by Board member response:', 'Group response analysis:']:
                    new_eval_list_of_dicts.append({category_key: ['']*2})
                    continue
                score = category.get(list(category.keys())[0], '')
                list_of_scores = list()
                list_of_scores.append(score)
                list_of_scores.append('')

                new_eval_list_of_dicts.append({category_key: list_of_scores})



        member_variable_query['eval_stats_list_of_dicts'] = new_eval_list_of_dicts
        ############################################################################
        # member_variable_query['section_avg_list_last_item'] = tuple((list(BEA.section_avg_list[-1].keys())[0], BEA.section_avg_list[-1].get(list(BEA.section_avg_list[-1].keys())[0])))
        # section_avg_list_last_item = tuple((list(BEA.section_avg_list[-1].keys())[0], BEA.section_avg_list[-1].get(list(BEA.section_avg_list[-1].keys())[0])))
        # print('here: ', section_avg_list_last_item)

        section_avg_list = PEER_INDIVIDUALS.section_avg_list
        new_section_avg_list = list()

        if director_in_last_year:
            for section in section_avg_list[director]:
                section_key = list(section.keys())[0]
                section_avg = section.get(list(section.keys())[0], '')
                # new_section_avg_list.append({section_key: []})
                list_of_section_averages = list()
                list_of_section_averages.append(section_avg)
                for item in [b]:
                    avg_list = item.section_avg_list.get(director, '')
                    for i in avg_list:
                        for k, v in i.items():
                            if k == section_key:
                                # if i == avg_list[-1]:
                                #     section_avg_list_last_item[1].append(i.get(section_key, ''))
                                # else:
                                list_of_section_averages.append(i.get(section_key, ''))

                            else:
                                pass
                new_section_avg_list.append({section_key: list_of_section_averages})
        else:
            for section in section_avg_list[director]:
                section_key = list(section.keys())[0]
                section_avg = section.get(list(section.keys())[0], '')
                # new_section_avg_list.append({section_key: []})
                list_of_section_averages = list()
                list_of_section_averages.append(section_avg)
                list_of_section_averages.append('')
                new_section_avg_list.append({section_key: list_of_section_averages})

        # print(new_section_avg_list)
        # section_avg_list_last_item = tuple((list(BEA.section_avg_list[-1].keys())[0], [a for a in new_section_avg_list[-1].get(list(new_section_avg_list[-1].keys())[0])]))


        # print('new_section_avg_list: ', new_section_avg_list)
        # print('section_avg_list_last_item: ', section_avg_list_last_item)
        member_variable_query['section_avg_list'] = new_section_avg_list
        member_variable_query['eval_stats_list_of_dicts'] = new_eval_list_of_dicts

        try:
            member_variable_query['this_year_skills_matrix_df'] = PEER_INDIVIDUALS.skills_matrix_df[director]
            member_variable_query['this_year_skills_matrix_info'] = PEER_INDIVIDUALS.skills_matrix_info[director]
            member_variable_query['self_skills_present'] = True
        except:
            member_variable_query['self_skills_present'] = False

        comparative_peer_individuals_templater(comparative_year.get('file'), member_variable_query, 'Peer Individuals', report_to_run, questions_df, sorted_years[comparative_year_number]['ppl_df'], ind_question_column_width,ind_heat_map_section_bool,ind_section_analysis_bool,ind_hilo_section_bool,ind_peer_self_report_bool,ind_statistical_analysis_bool, director_name=director, randomize = randomize_peer)

    return peer_reports

def comparative_peer_composite_templater(file_name, member_variable_query, report_type, df, questions_df, ppl_df, question_column_width, randomize_peer,heat_map_section_bool,section_analysis_bool,esigma_bar_chart_bool,hilo_section_bool,peer_self_report_bool,statistical_analysis_bool, randomize = True, anonymized=False):
    now = datetime.now()
    formatted_date = str(now.strftime("%B")) + ' ' + str(now.day) + ', ' + str(now.year)

    file_loader =FileSystemLoader(app.config['REPORT_TEMPLATES'])
    env = Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)
    template = env.get_template('comparative_peer_composite_report_template.html')

    skills_matrix_present = 'skills_matrix' in questions_df['type'].unique()

    # Get Assessment sections labels and section names
    section_names_and_labels = {}
    label_re = re.compile(r'^[\s\t]*([IVX]+\.)[\s\t]*(.*)')
    for section in questions_df['section'].unique().tolist():
        try:
            label = label_re.match(section)
            section_names_and_labels[label.group(1)] = label.group(2)
        except:
            section_names_and_labels[section] = section

    # print('\n\n\nComparative Peer Question Column Width: ', question_column_width)
    jinja_input_dict = {'nasdaq_logo': os.path.join(app.config['IMAGES'], 'nasdaq_logo.png'), 'year': str(now.year), 'formatted_date': formatted_date, 'skills_matrix_present': skills_matrix_present,
         'questions_df': questions_df, 'scale_image': os.path.join(app.config['IMAGES'], 'scale.png'), 'five_scale_image': os.path.join(app.config['IMAGES'], 'five_scale.png'), 'green_flag': os.path.join(app.config['IMAGES'], 'green_flag.png'),
         'yellow_flag': os.path.join(app.config['IMAGES'], 'yellow_flag.png'), 'red_flag': os.path.join(app.config['IMAGES'], 'red_flag.png'),
         'question_column_width': question_column_width, 'heat_map_section_bool': heat_map_section_bool,
         'section_analysis_bool': section_analysis_bool, 'esigma_bar_chart_bool': esigma_bar_chart_bool, 'hilo_section_bool': hilo_section_bool,
         'peer_self_report_bool': peer_self_report_bool, 'statistical_analysis_bool': statistical_analysis_bool, 'anonymized': anonymized}

    jinja_input_dict['section_names_and_labels'] = section_names_and_labels

    # Control which sections are in the table of contents
    
    # jinja_input_dict['table_of_contents_titles'] = ['#heat_map_title', '#section_analysis_title', '#eSigma_title_bar_chart', '#hilo_title', '#peer_and_self_report_title', '#statistical_analysis_title']

    jinja_input_dict['table_of_contents_titles'] = []
    if heat_map_section_bool:
        jinja_input_dict['table_of_contents_titles'].append('#heat_map_title')
    if section_analysis_bool:
        jinja_input_dict['table_of_contents_titles'].append('#section_analysis_title')
    if esigma_bar_chart_bool:
        jinja_input_dict['table_of_contents_titles'].append('#eSigma_title_bar_chart')
    if hilo_section_bool:
        jinja_input_dict['table_of_contents_titles'].append('#hilo_title')
    if member_variable_query.get('skills_matrix_average') is not None:
        jinja_input_dict['table_of_contents_titles'].append('#skills_matrix_title')
    if peer_self_report_bool:
        jinja_input_dict['table_of_contents_titles'].append('#peer_and_self_report_title')
    if statistical_analysis_bool:
        jinja_input_dict['table_of_contents_titles'].append('#statistical_analysis_title')


    client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file_name)

    try:
        jinja_input_dict['client_name'] = client_name.group(1)
        jinja_input_dict['test'] = client_name.group(1)
    except:
        jinja_input_dict['client_name'] = 'insert_client_name_here'
        jinja_input_dict['test'] = 'insert_file_title_here'

    jinja_input_dict['number_of_sections'] = len(questions_df['section'].unique().tolist())
    jinja_input_dict['number_of_long_form_questions'] = len(questions_df[questions_df['type']=='long_form'])
    jinja_input_dict['number_of_rated_questions'] = len(questions_df['question_raw']) - len(questions_df[questions_df['type']=='long_form']) - len(questions_df[questions_df['type']=='skill_matrix']) - len(questions_df[questions_df['type']=='attribute_matrix'])

    jinja_input_dict['report_type'] = report_type

    if anonymized == False:
        jinja_input_dict['section_avg_bar_chart'] = os.path.join(app.config['CHARTS'], '{}_section_avg_bar_chart.png'.format(member_variable_query.get('comparative_year_number')))
        jinja_input_dict['compared_section_avg_bar_chart'] = os.path.join(app.config['CHARTS'], '{}_section_avg_bar_chart.png'.format(member_variable_query.get('compared_year_number')))

        jinja_input_dict['esigma_bar_chart'] = os.path.join(app.config['CHARTS'], '{}_esigma_bar_chart.png'.format(member_variable_query.get('comparative_year_number')))
        jinja_input_dict['compared_esigma_bar_chart'] = os.path.join(app.config['CHARTS'], '{}_esigma_bar_chart.png'.format(member_variable_query.get('compared_year_number')))
    else:
        jinja_input_dict['section_avg_bar_chart'] = os.path.join(app.config['CHARTS'], '{}_anonymized_section_avg_bar_chart.png'.format(member_variable_query.get('comparative_year_number')))
        jinja_input_dict['compared_section_avg_bar_chart'] = os.path.join(app.config['CHARTS'], '{}_anonymized_section_avg_bar_chart.png'.format(member_variable_query.get('compared_year_number')))

        jinja_input_dict['esigma_bar_chart'] = os.path.join(app.config['CHARTS'], '{}_anonymized_esigma_bar_chart.png'.format(member_variable_query.get('comparative_year_number')))
        jinja_input_dict['compared_esigma_bar_chart'] = os.path.join(app.config['CHARTS'], '{}_anonymized_esigma_bar_chart.png'.format(member_variable_query.get('compared_year_number')))



    for item in member_variable_query.keys():
        jinja_input_dict['{}'.format(item)] = member_variable_query[item]

    output = template.render(jinja_input_dict)

    if anonymized == False:
        HTML(string=output, base_url='.').write_pdf(os.path.join(app.config['REPORTS'], '{}_{}.pdf'.format(client_name.group(1), report_type)), stylesheets=[CSS(os.path.abspath(os.path.join(os.sep, app.config['CSS'], 'peer_composite_template_style.css')))])
    else:
        HTML(string=output, base_url='.').write_pdf(os.path.join(app.config['REPORTS'], '{}_{}_Anonymized.pdf'.format(client_name.group(1), report_type)), stylesheets=[CSS(os.path.abspath(os.path.join(os.sep, app.config['CSS'], 'peer_composite_template_style.css')))])

def comparative_peer_individuals_templater(file_name, member_variable_query, report_type, df, questions_df, ppl_df, question_column_width,heat_map_section_bool,section_analysis_bool,hilo_section_bool,peer_self_report_bool,statistical_analysis_bool, director_name, randomize = True):
    now = datetime.now()
    formatted_date = str(now.strftime("%B")) + ' ' + str(now.day) + ', ' + str(now.year)

    file_loader =FileSystemLoader(app.config['REPORT_TEMPLATES'])
    env = Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)
    template = env.get_template('comparative_peer_individuals_report_template.html')

    skills_matrix_present = 'skills_matrix' in questions_df['type'].unique()

    jinja_input_dict = {'director_name': director_name, 'title': 'Peer Individual Report for {}'.format(director_name), 'nasdaq_logo': os.path.join(app.config['IMAGES'], 'nasdaq_logo.png'),
         'year': str(now.year), 'formatted_date': formatted_date, 'skills_matrix_present': skills_matrix_present,
         'questions_df': questions_df, 'scale_image': os.path.join(app.config['IMAGES'], 'scale.png'), 'five_scale_image': os.path.join(app.config['IMAGES'], 'five_scale.png'),
         'question_column_width': question_column_width, 'heat_map_section_bool': heat_map_section_bool,
         'section_analysis_bool': section_analysis_bool, 'hilo_section_bool': hilo_section_bool,
         'peer_self_report_bool': peer_self_report_bool, 'statistical_analysis_bool': statistical_analysis_bool}

    # Get Assessment sections labels and section names
    section_names_and_labels = {}
    label_re = re.compile(r'^[\s\t]*([IVX]+\.)[\s\t]*(.*)')
    for section in questions_df['section'].unique().tolist():
        label = label_re.match(section)
        section_names_and_labels[label.group(1)] = label.group(2)
    jinja_input_dict['section_names_and_labels'] = section_names_and_labels

    # Control which sections are in the table of contents
    # jinja_input_dict['table_of_contents_titles'] = ['#heat_map_title', '#peer_self_comparison_graph_title', '#hilo_title', '#peer_and_self_report_title', '#statistical_analysis_title']

    jinja_input_dict['table_of_contents_titles'] = []
    if heat_map_section_bool:
        jinja_input_dict['table_of_contents_titles'].append('#heat_map_title')
    if section_analysis_bool:
        jinja_input_dict['table_of_contents_titles'].append('#peer_self_comparison_graph_title')
    if hilo_section_bool:
        jinja_input_dict['table_of_contents_titles'].append('#hilo_title')
    if member_variable_query.get('self_skills_present'):
        jinja_input_dict['table_of_contents_titles'].append('#skills_matrix_title')
    if peer_self_report_bool:
        jinja_input_dict['table_of_contents_titles'].append('#peer_and_self_report_title')
    if statistical_analysis_bool:
        jinja_input_dict['table_of_contents_titles'].append('#statistical_analysis_title')


    client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file_name)
    jinja_input_dict['client_name'] = client_name.group(1)

    jinja_input_dict['number_of_sections'] = len(questions_df['section'].unique().tolist())
    jinja_input_dict['number_of_long_form_questions'] = len(questions_df[questions_df['type']=='long_form'])
    jinja_input_dict['number_of_rated_questions'] = len(questions_df['question_raw']) - len(questions_df[questions_df['type']=='long_form']) - len(questions_df[questions_df['type']=='skill_matrix']) - len(questions_df[questions_df['type']=='attribute_matrix'])

    jinja_input_dict['report_type'] = report_type

    for item in member_variable_query.keys():
        jinja_input_dict['{}'.format(item)] = member_variable_query[item]

    print_safe_respondent = re.sub('&#8217;', "", director_name)
    if member_variable_query['peer_self_comparison_bool']:
        jinja_input_dict['section_avg_bar_chart'] = os.path.join(app.config['CHARTS'], '{}_{}_peer_self.png'.format(member_variable_query.get('comparative_year_number'), print_safe_respondent))
        jinja_input_dict['compared_section_avg_bar_chart'] = os.path.join(app.config['CHARTS'], '{}_{}_peer_self.png'.format(member_variable_query.get('compared_year_number'), print_safe_respondent))

    output = template.render(jinja_input_dict)

    HTML(string=output, base_url='.').write_pdf(os.path.join(app.config['REPORTS'], '{}_{}_{}.pdf'.format(print_safe_respondent, client_name.group(1), report_type)), stylesheets=[CSS(os.path.abspath(os.path.join(os.sep, app.config['CSS'], 'peer_individuals_template_style.css')))])

def create_comparative_peer_composite_jinja_dict(sorted_years, anonymize, randomize, no_self_ratings_bool, report_type='Peer'):
    copied_dict = sorted_years.copy()
    comparative_year = copied_dict.pop(max(copied_dict.keys()), None)
    compared_year = copied_dict.pop(min(sorted_years.keys()))

    # number_of_comparisons = len(list(copied_dict.keys()))
    # print("Number of comparisons: ", number_of_comparisons)

    qdf = comparative_year['questions_df']
    tdf = comparative_year['ETLd_df']
    file = comparative_year['file']
    comparative_smoothed_df = comparative_year['smoothed_df']

    # qdf = compared_year['questions_df']
    c_tdf = compared_year['ETLd_df']
    # file = compared_year['file']
    compared_smoothed_df = compared_year['smoothed_df']

    # Get Board Averages
    # board_question_averages = dict()
    # for section in comparative_smoothed_df['section'].unique():
    #     for number in comparative_smoothed_df[comparative_smoothed_df['section']==section]['number'].unique():
    #         try:
    #             avgs = []
    #             avg = comparative_smoothed_df[(comparative_smoothed_df['section']==section) & (comparative_smoothed_df['number']==number)]['response'].mean()
    #             avgs.append(avg)
    #
    #             avgs.append(compared_smoothed_df[(compared_smoothed_df['section']==section) & (compared_smoothed_df['number']==number)]['response'].mean())
    #             avgs = ["{0:.2f}".format(Decimal('{}'.format(x)).quantize(Decimal('1e-2'), ROUND_HALF_UP)) for x in avgs]
    #             board_question_averages[(section, number)] = avgs
    #         except:
    #             board_question_averages[(section, number)] = None


    data_dict = OrderedDict()
    rankFlag = False
    if anonymize ==True:
        director_encoder = dict()
        director_count = 1
    for section in qdf['section'].unique():
        if set(qdf[qdf['section']==section]['type'].tolist()) <= set(['skills_matrix', '', 'nan']):
            continue

        data_dict[section] = OrderedDict()
        for number in qdf[qdf['section']==section]['number'].unique():
            data_dict[section][number] = OrderedDict()
            try:
                avgs = []
                question_averages = comparative_smoothed_df[(comparative_smoothed_df['section']==section) & (comparative_smoothed_df['number']==number)]['response'].mean()
                question_averages = "{0:.2f}".format(Decimal('{}'.format(question_averages)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                avgs.append(question_averages)
                avgs.append(compared_smoothed_df[(compared_smoothed_df['section']==section) & (compared_smoothed_df['number']==number)]['response'].mean())
                avgs = ["{0:.2f}".format(Decimal('{}'.format(x)).quantize(Decimal('1e-2'), ROUND_HALF_UP)) for x in avgs]
            except:
                question_averages = None

            data_dict[section][number]['question_averages'] = avgs
            data_dict[section][number]['type'] = qdf[(qdf['section']==section)&(qdf['number']==number)]['type'].unique().tolist()[0]
            data_dict[section][number]['question'] = qdf[(qdf['section']==section)&(qdf['number']==number)]['question'].unique().tolist()[0]
            # data_dict[section][number]['board_average']
            # Iterate through raw questions because sub_section is undefined when
            # improper syntax is used
            data_dict[section][number]['raw_questions'] = OrderedDict()
            for raw_q in qdf[(qdf['section']==section)&(qdf['number']==number)]['question_raw']:
                data_dict[section][number]['raw_questions'][raw_q] = {}
                sub_q = qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['sub_section'].item()

                if anonymize:
                    if sub_q not in ['', '!__!']:
                        if director_encoder.get(sub_q) is None:
                            director_encoder[sub_q] = 'Director {}'.format(director_count)
                            data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = 'Director {}'.format(director_count)
                            director_count += 1
                        else:
                            data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = director_encoder.get(sub_q)
                    else:
                        data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = sub_q
                else:
                    data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = sub_q

                if qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['type'].item() in ['matrix', 'likert']:
                    answers = tdf[raw_q].replace({np.nan: None})
                    answers.fillna('N/A', inplace=True)
                    answers = answers.tolist()
                    answers = [int(x) if x != 'N/A' else x for x in answers]
                    # print(raw_q)
                    # print(answers)
                    if no_self_ratings_bool:
                        answers.remove('N/A')
                    if randomize:
                        shuffle(answers)
                        data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers
                    else:
                        data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers
                    # raw_averages [str(Decimal(np.mean(tdf[raw_q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP))]
                    data_dict[section][number]['raw_questions'][raw_q]['averages'] = [str(Decimal(np.mean(tdf[raw_q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP))]
                    # print(data_dict[section][number]['raw_questions'][raw_q]['averages'])
                    try:
                        data_dict[section][number]['raw_questions'][raw_q]['averages'].append(str(Decimal(np.mean(compared_year['ETLd_df'][raw_q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP)))
                    except:
                        data_dict[section][number]['raw_questions'][raw_q]['averages'].append('N/A')
                    # for year in
                    # print('\n\n', section, number)
                    # print(board_question_averages.get((section, number)))
                    # data_dict[section][number]['raw_questions'][raw_q]['board_averages'] = board_question_averages.get((section, number))
                elif qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['type'].item() in ['long_form', 'optional_long_form']:
                    try:
                        if math.isnan(sub_q):
                            data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = 'None'
                    except:
                        pass

                    answers = tdf[raw_q].dropna().tolist()
                    answers = [a for a in answers if a not in ['nan', '#NAME?', None, 'None', 'No Comment', 'no comment', 'none', np.nan]]

                    if (len(answers) == 0) and (data_dict[section][number]['type'] == 'optional_long_form'):
                        del data_dict[section][number]
                        continue

                    if randomize:
                        shuffle(answers)
                    else:
                        pass

                    data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers

    # if not anonymize:
    #     count = 0
    #     for s in data_dict:
    #         print('\n\nSection Name:  ', s)
    #         for number in data_dict[s]:
    #             print('Number:  ', number)
    #             try:
    #                 print('Question Average:  ', data_dict[s][number]['question_average'])
    #             except:
    #                 pass
    #             print('Type:  ', data_dict[s][number]['type'])
    #             print('Question: ', data_dict[s][number]['question'])
    #             for raw in data_dict[s][number]['raw_questions']:
    #                 print()
    #                 print('Raw:  ', raw)
    #                 print('Sub:  ', data_dict[s][number]['raw_questions'][raw]['sub_section'])
    #                 print('Answers:  ', data_dict[s][number]['raw_questions'][raw]['answers'])
    #                 try:
    #                     print('Average:  ', data_dict[s][number]['raw_questions'][raw]['averages'])
    #                     # print('Board Average:  ', data_dict[s][number]['raw_questions'][raw]['board_average'])
    #                 except:
    #                     pass
    #                 print('\n')
    #             count+=1
    #             # if count == 4:
    #             #     break
    #     # print(data_dict[list(data_dict.keys())[0]]['1.']['Comments/Suggestions'])

    # count = 0
    # for section in data_dict:
    #     print('\n\nSection Name:  ', section)
    #     for number in data_dict[section]:
    #         print('Number:  ', number)
    #         try:
    #             print('Question Average:  ', data_dict[section][number]['question_averages'])
    #         except:
    #             pass
    #         print('Type:  ', data_dict[section][number]['type'])
    #         print('Question:  ', data_dict[section][number]['question'])
    #         # print('Answers:  ', data_dict[section][number]['answers'])
    #         for raw_q in data_dict[section][number]['raw_questions']:
    #             print('Raw Q:  ', raw_q)
    #             print('Sub:  ', data_dict[section][number]['raw_questions'][raw_q]['sub_section'])
    #             print('Answers:  ', data_dict[section][number]['raw_questions'][raw_q]['answers'])
    #             # try:
    #             if data_dict[section][number]['type'] in ['matrix', 'likert']:
    #                 print('Averages:  ', data_dict[section][number]['raw_questions'][raw_q]['averages'])
    #                 # print('Board Averages:  ', data_dict[section][number]['raw_questions'][raw_q]['board_averages'])
    #             # except:
    #             #     pass
    #         print('\n')
    #         # count+=1
    #         # if count == 4:
    #         #     break



    # return data_dict, number_of_comparisons
    return data_dict

def create_comparative_peer_individuals_jinja_dict(id, director, sorted_years, smoothed_df, file, report_type, no_self_ratings_bool, anonymize, randomize):
    copied_dict = sorted_years.copy()
    comparative_year = copied_dict.pop(max(copied_dict.keys()), None)
    compared_year = copied_dict.pop(min(sorted_years.keys()))

    qdf = comparative_year['questions_df']
    tdf = comparative_year['ETLd_df']
    file = comparative_year['file']
    comparative_smoothed_df = comparative_year['smoothed_df']
    compared_smoothed_df = compared_year['smoothed_df']

    # qdf = compared_year['questions_df']
    c_tdf = compared_year['ETLd_df']
    # file = compared_year['file']
    compared_smoothed_df = compared_year['smoothed_df']

    # Get Board Averages
    board_question_averages = dict()
    for section in comparative_smoothed_df['section'].unique():
        for number in comparative_smoothed_df[comparative_smoothed_df['section']==section]['number'].unique():
            try:
                avg = comparative_smoothed_df[(comparative_smoothed_df['section']==section) & (comparative_smoothed_df['number']==number)]['response'].mean()
                board_question_averages[(section, number)] = "{0:.2f}".format(Decimal('{}'.format(avg)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                board_question_averages[(section, number)] = 'N/A'


    data_dict = dict()
    # for director in smoothed_df['sub_section'].unique():
    #     # print('\n\n', director)
    #     row_id = qdf[~(qdf['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (qdf['sub_section']==director)]['UniqueIdentifier'].unique()[0]
    #     # print(row_id)
    if director not in ['', '!__!']:
        data_dict[director] = OrderedDict()
        for section in comparative_smoothed_df[comparative_smoothed_df['sub_section']==director]['section'].unique():
            data_dict[director][section] = OrderedDict()
            for number in comparative_smoothed_df[(comparative_smoothed_df['section']==section)&(comparative_smoothed_df['sub_section']==director)]['number'].unique():
                data_dict[director][section][number] = dict()
                # comparative_smoothed_df.to_excel('comparative_smoothed_df.xlsx')
                # print('no_self_ratings_bool: ', no_self_ratings_bool)
                if not no_self_ratings_bool:
                    try:
                        new_self_rating = comparative_smoothed_df[(comparative_smoothed_df['section']==section)&(comparative_smoothed_df['sub_section']==director)&(comparative_smoothed_df['number']==number)&(comparative_smoothed_df['respondent_id']==id)]['response'].item()
                    except:
                        new_self_rating = 'N/A'
                    try:
                        old_self_rating = compared_smoothed_df[(compared_smoothed_df['section']==section)&(compared_smoothed_df['sub_section']==director)&(comparative_smoothed_df['number']==number)&(compared_smoothed_df['respondent_id']==id)]['response'].item()
                    except:
                        old_self_rating = 'N/A'

                    if pd.isna(new_self_rating):
                        new_self_rating = 'N/A'
                    if pd.isna(old_self_rating):
                        old_self_rating = 'N/A'

                    data_dict[director][section][number]['new_self_rating'] = new_self_rating
                    data_dict[director][section][number]['old_self_rating'] = old_self_rating

                    data_dict[director][section][number]['answers'] = [new_self_rating]
                else:
                    data_dict[director][section][number]['answers'] = []

                # print(data_dict[director][section][number]['answers'])
                # print("data_dict[director][section][number]['answers']: ", data_dict[director][section][number]['answers'])

                # try:
                #     board_average = comparative_smoothed_df[(comparative_smoothed_df['section']==section) & (comparative_smoothed_df['number']==number)]['response'].mean()
                #     board_average = "{0:.2f}".format(Decimal('{}'.format(board_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                # except:
                #     board_average = 'NaN'

                try:
                    respondent_average = comparative_smoothed_df[(comparative_smoothed_df['section']==section) & (comparative_smoothed_df['number']==number) & (comparative_smoothed_df['sub_section']==director)]['response'].mean()
                    respondent_average = "{0:.2f}".format(Decimal('{}'.format(respondent_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                except:
                    question_average = None

                # data_dict[director][section][number]['board_average'] = board_average
                data_dict[director][section][number]['respondent_average'] = respondent_average
                data_dict[director][section][number]['type'] = comparative_smoothed_df[(comparative_smoothed_df['section']==section)&(comparative_smoothed_df['number']==number)]['type'].unique().tolist()[0]
                data_dict[director][section][number]['question'] = comparative_smoothed_df[(comparative_smoothed_df['section']==section)&(comparative_smoothed_df['number']==number)]['question'].unique().tolist()[0]

                if data_dict[director][section][number]['type'] in ['matrix', 'likert']:
                    # calculate average
                    answers = comparative_smoothed_df[(comparative_smoothed_df['section']==section)&(comparative_smoothed_df['number']==number)&(comparative_smoothed_df['sub_section']==director)][['respondent_id', 'response']]
                    # print('\nanswers:', answers)
                    old_answers = compared_smoothed_df[(compared_smoothed_df['section']==section)&(compared_smoothed_df['number']==number)&(compared_smoothed_df['sub_section']==director)][['respondent_id', 'response']]

                    answers.set_index('respondent_id', inplace=True)
                    # print(answers)
                    avg = np.mean(answers['response'].dropna())
                    old_avg = np.mean(old_answers['response'].dropna())

                    answers = answers.replace({np.nan: None})
                    answers.fillna('N/A', inplace=True)
                    answers.drop(index=id, inplace=True)
                    # print(answers)
                    answers = answers['response'].tolist()
                    answers = [int(x) if x != 'N/A' else x for x in answers]
                    if randomize:
                        shuffle(answers)
                    # print(answers)
                    data_dict[director][section][number]['answers'].extend(answers)

                    data_dict[director][section][number]['average'] = str(Decimal(avg).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                    data_dict[director][section][number]['old_average'] = str(Decimal(old_avg).quantize(Decimal('1e-2'), ROUND_HALF_UP))

                    # try:
                    #     board_average = comparative_smoothed_df[(comparative_smoothed_df['section']==section) & (comparative_smoothed_df['number']==number)]['response'].mean()
                    #     board_average = "{0:.2f}".format(Decimal('{}'.format(board_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                    # except:
                    #     board_average = 'NaN'
                    data_dict[director][section][number]['board_average'] = board_question_averages[(section, number)]

                elif data_dict[director][section][number]['type']in ['long_form', 'optional_long_form']:
                    data_dict[director][section][number]['answers'] = []
                    answers = comparative_smoothed_df[(comparative_smoothed_df['section']==section)&(smoothed_df['number']==number)&(smoothed_df['sub_section']==director)]['response'].tolist()
                    answers = [a for a in answers if a not in ['nan', '#NAME?', None, 'None', 'No Comment', 'no comment', 'none', np.nan]]

                    if randomize:
                        shuffle(answers)
                    # print(section, director, number, id)
                    try:
                        if comparative_smoothed_df[(comparative_smoothed_df['section']==section)&(comparative_smoothed_df['sub_section']==director)&(comparative_smoothed_df['number']==number)&(comparative_smoothed_df['respondent_id']==id)]['response'].item() not in ['nan', '#NAME?', None, 'None', 'No Comment', 'no comment', 'none', np.nan]:
                            data_dict[director][section][number]['answers'].extend(answers)
                        else:
                            data_dict[director][section][number]['answers'] = answers
                    except:
                        data_dict[director][section][number]['answers'] = answers
    else:
        pass

    # for director, o_dict in data_dict.items():
    #     print('director: ', director)
    #     # print(o_dict)
    #     for s in o_dict.keys():
    #         print('\n\nSection Name:  ', s)
    #         for number in data_dict[director][s]:
    #             print('Number:  ', number)
    #             try:
    #                 print('Question Average:  ', data_dict[director][s][number]['question_average'])
    #             except:
    #                 pass
    #             try:
    #                 print('New Self-Rating: ', data_dict[director][s][number]['new_self_rating'])
    #                 print('Old Self-Rating: ', data_dict[director][s][number]['old_self_rating'])
    #             except:
    #                 pass
    #
    #             print('Type:  ', data_dict[director][s][number]['type'])
    #             print('Question: ', data_dict[director][s][number]['question'])
    #             print('Answers:  ', data_dict[director][s][number]['answers'])
    #             try:
    #                 print('Average:  ', data_dict[director][s][number]['average'])
    #                 print('Board Average:  ', data_dict[director][s][number]['board_average'])
    #             except:
    #                 pass
    #             print('\n')

    return data_dict

def comparative_join_qdf_and_rawdata_into_smoothed_df(qdf, tdf, file, report_type, anonymize):

    ############################################################################
    # ETL
    df = pd.DataFrame(columns=['client_name', 'report_type', 'section', 'number', 'question_raw', 'question', 'sub_section', 'type', 'average', 'response', 'respondent_id', 'question_mean'])
    try:
        client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file).group(1)
    except:
        client_name = None

    if anonymize ==True:
        director_encoder = dict()
        director_count = 1
    # qdf.to_excel('qdf.xlsx')
    # tdf.to_excel('tdf.xlsx')
    for s in qdf['section'].unique():
        for q in qdf[qdf['section']==s]['question_raw']:
            for i in tdf[q].index:
                # if (tdf.loc[i,q]==np.nan or (tdf.loc[i,q]=='nan')):
                #     print('here!', i, q)
                #     response = None
                # else:
                #     response = tdf.loc[i,q]
                if anonymize == False:
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section_number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_number'].item(), 'sub_section_value': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_value'].item(),
                            'sub_section': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item(), 'response': tdf.loc[i,q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}
                else:
                    sub_q = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item()
                    if sub_q not in ['', '!__!']:
                        if director_encoder.get(sub_q) is None:
                            director_encoder[sub_q] = 'Director {}'.format(director_count)
                            sub_q = 'Director {}'.format(director_count)
                            director_count += 1
                        else:
                            sub_q = director_encoder.get(sub_q)
                    else:
                        pass
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section_number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_number'].item(), 'sub_section_value': director_encoder.get(sub_q),
                            'sub_section': sub_q, 'response': tdf.loc[i,q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}

                df = df.append(row, ignore_index=True)

    question_means = []
    for section in df['section'].unique():
        for number in df[(df['section']==section)]['number'].unique():
            try:
                mean_by_question = df[(df['section']==section) & (df['number']==number)]['response'].mean()
                mean_by_question = "{0:.2f}%".format(Decimal('{}'.format(mean_by_question)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                mean_by_question = None
            mean_column_addition = [mean_by_question]*len(df[(df['section']==section) & (df['number']==number)]['response'])
            question_means.extend(mean_column_addition)

    df['question_mean'] = question_means
    df.to_excel(os.path.join(app.config['REPORTS'], 'SMOOTHED_{}_{}.xlsx'.format(client_name, report_type)))
    return df

def run_peer(peer_filename, question_column_width,randomize,heat_map_section_bool,section_analysis_bool,esigma_bar_chart_bool,hilo_section_bool,peer_self_report_bool,statistical_analysis_bool,ind_question_column_width,ind_heat_map_section_bool,ind_section_analysis_bool,ind_hilo_section_bool,ind_peer_self_report_bool,ind_statistical_analysis_bool):
    file = os.path.join(app.config['CSV_UPLOADS'], peer_filename)

    raw_data_dict = csv_opener(file = file)
    dict_of_dfs = peer_splitter(file, raw_data_dict)

    meta_section = dict_of_dfs.get('meta')
    ppl_df = ppl_parser(meta_section)

    not_app_or_obs = {}
    ETLd_question_dfs = {}
    ETLd_dfs = {}
    data_dicts = {}
    smoothed_dfs = {}
    individuals_na_dict = {}

    for report_type in dict_of_dfs:
        if report_type != 'meta':
            data_dicts[report_type] = dict()
            report_to_run = dict_of_dfs.get(report_type)
            questions_df, transformed_df, na_no_null_dict = peer_etl(report_to_run, ppl_df, report_type, file, True)

            # Check for no-self rating type peer reports
            ids_respondents = []
            respondent__self_rate_bool_list = []
            for respondent in questions_df[~(questions_df['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (~questions_df['sub_section'].isin(['', None, np.nan]))]['sub_section'].unique():
                # print(respondent)
                id = questions_df[~(questions_df['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (questions_df['sub_section']==respondent)]['UniqueIdentifier'].unique()[0]
                # print(id)
                ids_respondents.append((id, respondent))
                # print(ids_respondents)

                raw_question_list = questions_df[(questions_df['sub_section']==respondent) & (questions_df['type'].isin(['likert', 'matrix']))]['question_raw']
                # print(raw_question_list)
                self_ratings = transformed_df[raw_question_list].loc[id].unique().tolist()
                # print(self_ratings)

                # print(raw_question_list)
                # print('self-ratings: ', self_ratings)
                # print(math.isnan(self_ratings[0]))
                if len(self_ratings) == 1:
                    import math
                    if math.isnan(self_ratings[0]):
                        respondent__self_rate_bool_list.append((respondent, True))
                    else:
                        respondent__self_rate_bool_list.append((respondent, False))
                else:
                    respondent__self_rate_bool_list.append((respondent, False))

            # print(ids_respondents)
            # print(respondent__self_rate_bool_list)
            no_self_ratings_bool = all(item[-1] == True for item in respondent__self_rate_bool_list)
            data_dicts[report_type]['no_self_ratings_bool'] = no_self_ratings_bool

            ETLd_question_dfs[report_type] = questions_df

            ETLd_dfs[report_type] = transformed_df
            not_app_or_obs[report_type] = {}
            not_app_or_obs[report_type]['na_no_null_dict'] = na_no_null_dict

            smoothed_df = join_qdf_and_rawdata_into_smoothed_df(questions_df, transformed_df, peer_filename, report_type, False)
            smoothed_dfs[report_type] = smoothed_df

            jinja_individuals_data_dict = create_peer_individuals_jinja_dict(questions_df, transformed_df, smoothed_df, file, report_type, no_self_ratings_bool, False, randomize)
            data_dicts[report_type]['individuals'], individuals_na_dict[report_type] = jinja_individuals_data_dict

            jinja_composite_data_dict_randomized_named = create_peer_composite_jinja_dict(questions_df, transformed_df, smoothed_df, file, report_type, no_self_ratings_bool, False, randomize)
            data_dicts[report_type]['named'] = jinja_composite_data_dict_randomized_named

            jinja_composite_data_dict_randomized_anonymized = create_peer_composite_jinja_dict(questions_df, transformed_df, smoothed_df, file, report_type, no_self_ratings_bool, True, randomize)
            data_dicts[report_type]['anonymized'] = jinja_composite_data_dict_randomized_anonymized


    if set(questions_df['type']).issubset(set(['skills_matrix', ''])):
        skills_response = True
    else:
        skills_response = False

    peer_reports = []
    for report_type in ETLd_dfs.keys():
        if report_type != 'meta':

            # print('Creating {} report for {}'.format(report_type, file))
            report_to_run = ETLd_dfs.get(report_type)
            questions_df = ETLd_question_dfs.get(report_type)
            no_self_ratings_bool = data_dicts[report_type]['no_self_ratings_bool']
            PEER_COMPOSITE = peer_composite_report(peer_filename, report_type, report_to_run, questions_df, ppl_df, not_app_or_obs[report_type]['na_no_null_dict'], skills_response, randomize, app, no_self_ratings_bool=no_self_ratings_bool)
            peer_reports.extend(PEER_COMPOSITE.output_file_names)
            member_variable_query = dict()

            try:
                member_variable_query['skills_matrix_average'] = PEER_COMPOSITE.skills_matrix_average
                member_variable_query['number_of_skills_matrix_questions'] = PEER_COMPOSITE.number_of_skills_matrix_questions
            except:
                pass
            try:
                member_variable_query['skills_matrix_df'] = PEER_COMPOSITE.skills_matrix_export
                # print(member_variable_query['skills_matrix_df'])
                member_variable_query['skills_matrix_df_width'] = np.arange(1, PEER_COMPOSITE.skills_matrix_export.shape[1] + 1, 1)
            except:
                pass
            # Test if there are any n/a responses in the survey
            if PEER_COMPOSITE.na_in_heat_map:
                member_variable_query['na_in_heat_map'] = True
            else:
                member_variable_query['na_in_heat_map'] = False

            member_variable_query['eSigma_value'] = PEER_COMPOSITE.composite_eSigma
            member_variable_query['section_data'] = PEER_COMPOSITE.section_data
            member_variable_query['questions_df'] = PEER_COMPOSITE.questions_df
            if not no_self_ratings_bool:
                member_variable_query['number_of_respondents'] = PEER_COMPOSITE.number_of_respondents
            else:
                member_variable_query['number_of_respondents'] = PEER_COMPOSITE.number_of_respondents - 1
            member_variable_query['section_averages'] = PEER_COMPOSITE.averages
            member_variable_query['five_point_scale'] = PEER_COMPOSITE.five_point_scale
            member_variable_query['eval_stats_list_of_dicts'] = PEER_COMPOSITE.eval_stats_list_of_dicts
            member_variable_query['eval_stats_list_of_dicts_last_item'] = tuple((list(PEER_COMPOSITE.eval_stats_list_of_dicts[-1].keys())[0], PEER_COMPOSITE.eval_stats_list_of_dicts[-1].get(list(PEER_COMPOSITE.eval_stats_list_of_dicts[-1].keys())[0])))
            member_variable_query['section_avg_list'] = PEER_COMPOSITE.section_avg_list
            member_variable_query['section_avg_list_last_item'] = tuple((list(PEER_COMPOSITE.section_avg_list[-1].keys())[0], PEER_COMPOSITE.section_avg_list[-1].get(list(PEER_COMPOSITE.section_avg_list[-1].keys())[0])))
            member_variable_query['jinja_hi'] = PEER_COMPOSITE.jinja_hi
            member_variable_query['jinja_lo'] = PEER_COMPOSITE.jinja_lo
            member_variable_query['smoothed_df'] = smoothed_dfs[report_type]

            member_variable_query['hm_section_length_counts'] = PEER_COMPOSITE.hm_section_length_counts
            member_variable_query['hm_numbers_borders'] = PEER_COMPOSITE.hm_numbers_borders
            member_variable_query['heat_map_averages'] = PEER_COMPOSITE.heat_map_averages
            member_variable_query['percentage_size'] = PEER_COMPOSITE.percentage_size

            if PEER_COMPOSITE.percentage_size < 2:
                member_variable_query['extra_large_hm'] = True
                member_variable_query['hm_rows_directions'] = PEER_COMPOSITE.anon_hm_rows_directions
            else:
                member_variable_query['extra_large_hm'] = False
                member_variable_query['hm_rows_directions'] = PEER_COMPOSITE.hm_rows_directions

            member_variable_query['header_font_size'] = 11
            if PEER_COMPOSITE.percentage_size<13 and PEER_COMPOSITE.percentage_size>10:
                member_variable_query['header_font_size'] = 10
            elif PEER_COMPOSITE.percentage_size<=10:
                member_variable_query['header_font_size'] = 9

            member_variable_query['number_font_size'] = 10
            if PEER_COMPOSITE.percentage_size<13 and PEER_COMPOSITE.percentage_size>10:
                member_variable_query['number_font_size'] = 8
            # elif PEER_COMPOSITE.percentage_size<=10 and PEER_COMPOSITE.percentage_size>5:
            elif PEER_COMPOSITE.percentage_size<=10:
                member_variable_query['number_font_size'] = 7
            # else:
            #     member_variable_query['number_font_size'] = 6

            member_variable_query['hm_cell_height'] = 10
            if PEER_COMPOSITE.number_of_respondents < 16 and PEER_COMPOSITE.number_of_respondents >= 10:
                member_variable_query['hm_cell_height'] = 8
            elif PEER_COMPOSITE.number_of_respondents < 20 and PEER_COMPOSITE.number_of_respondents >= 16:
                member_variable_query['hm_cell_height'] = 6
            elif PEER_COMPOSITE.number_of_respondents >= 20:
                member_variable_query['hm_cell_height'] = 5


            # print('header_font_size: ', member_variable_query['header_font_size'])

            member_variable_query['section_number_merge_ranges'] = PEER_COMPOSITE.section_number_merge_ranges
            member_variable_query['section_number_averages'] = PEER_COMPOSITE.section_number_averages

            member_variable_query['jinja_for_loop_dict'] = data_dicts[report_type]['named']
            member_variable_query['no_self_ratings_bool'] = no_self_ratings_bool
            peer_composite_templater(peer_filename, member_variable_query, report_type, report_to_run, questions_df, ppl_df, no_self_ratings_bool,question_column_width,heat_map_section_bool,section_analysis_bool,esigma_bar_chart_bool,hilo_section_bool,peer_self_report_bool,statistical_analysis_bool, anonymized=False, randomize = randomize)

            member_variable_query['hm_rows_directions'] = PEER_COMPOSITE.anon_hm_rows_directions
            member_variable_query['percentage_size'] = PEER_COMPOSITE.anon_percentage_size

            member_variable_query['jinja_for_loop_dict'] = data_dicts[report_type]['anonymized']
            peer_composite_templater(peer_filename, member_variable_query, report_type, report_to_run, questions_df, ppl_df, no_self_ratings_bool,question_column_width,heat_map_section_bool,section_analysis_bool,esigma_bar_chart_bool,hilo_section_bool,peer_self_report_bool,statistical_analysis_bool, anonymized=True, randomize = randomize)


    for report_type in dict_of_dfs:
        if report_type != 'meta':
            # print('\nCreating Individual {} reports for {}'.format(report_type, file))
            report_to_run = ETLd_dfs.get(report_type)
            questions_df = ETLd_question_dfs.get(report_type)
            jinja_data_dict = data_dicts[report_type]['individuals']

            PEER_INDIVIDUALS = peer_individual_reports(file, report_type, report_to_run, questions_df, ppl_df, not_app_or_obs[report_type]['na_no_null_dict'], app,randomize = True, no_self_ratings_bool=no_self_ratings_bool)
            peer_reports.extend(PEER_INDIVIDUALS.output_file_names)
            for director in jinja_data_dict.keys():
                # print('\nDirector: ', director)
                jinja_for_loop_dict = jinja_data_dict[director]
                member_variable_query = dict()
                try:
                    member_variable_query['skills_matrix_average'] = PEER_INDIVIDUALS.skills_matrix_average
                    member_variable_query['number_of_skills_matrix_questions'] = PEER_INDIVIDUALS.number_of_skills_matrix_questions
                except:
                    pass

                member_variable_query['question_column_width'] = ind_question_column_width
                member_variable_query['heat_map_section_bool'] = ind_heat_map_section_bool
                member_variable_query['peer_self_comparison_bool'] = ind_section_analysis_bool
                member_variable_query['hilo_section_bool'] = ind_hilo_section_bool
                member_variable_query['peer_self_report_bool'] = ind_peer_self_report_bool
                member_variable_query['statistical_analysis_bool'] = ind_statistical_analysis_bool

                if PEER_INDIVIDUALS.na_in_heat_maps.get(director, False):
                    member_variable_query['na_in_heat_map'] = True
                else:
                    member_variable_query['na_in_heat_map'] = False
                    
                member_variable_query['section_data'] = PEER_INDIVIDUALS.section_data[director]
                member_variable_query['questions_df'] = PEER_INDIVIDUALS.questions_df
                member_variable_query['jinja_for_loop_dict'] = jinja_for_loop_dict
                member_variable_query['number_of_respondents'] = PEER_INDIVIDUALS.number_of_respondents
                member_variable_query['section_averages'] = PEER_INDIVIDUALS.averages_data[director]
                member_variable_query['five_point_scale'] = PEER_INDIVIDUALS.five_point_scale
                member_variable_query['eval_stats_list_of_dicts'] = PEER_INDIVIDUALS.agg_section_data[director]
                member_variable_query['eval_stats_list_of_dicts_last_item'] = tuple((list(PEER_INDIVIDUALS.agg_section_data[director][-1].keys())[0], PEER_INDIVIDUALS.agg_section_data[director][-1].get(list(PEER_INDIVIDUALS.agg_section_data[director][-1].keys())[0])))
                member_variable_query['section_avg_list'] = PEER_INDIVIDUALS.section_avg_list[director]
                member_variable_query['section_avg_list_last_item'] = tuple((list(PEER_INDIVIDUALS.section_avg_list[director][-1].keys())[0], PEER_INDIVIDUALS.section_avg_list[director][-1].get(list(PEER_INDIVIDUALS.section_avg_list[director][-1].keys())[0])))
                member_variable_query['jinja_hi'] = PEER_INDIVIDUALS.hilo_data[director]['jinja_hi']
                member_variable_query['jinja_lo'] = PEER_INDIVIDUALS.hilo_data[director]['jinja_lo']
                member_variable_query['eSigma_value'] = PEER_INDIVIDUALS.esigmas[director]
                member_variable_query['initials'] = PEER_INDIVIDUALS.initials.get(director)

                member_variable_query['hm_section_length_counts'] = PEER_INDIVIDUALS.heat_map_data[director].get('hm_section_length_counts')
                member_variable_query['hm_numbers_borders'] = PEER_INDIVIDUALS.heat_map_data[director].get('hm_numbers_borders')
                member_variable_query['heat_map_averages'] = PEER_INDIVIDUALS.heat_map_data[director].get('heat_map_averages')
                member_variable_query['hm_rows_directions'] = PEER_INDIVIDUALS.heat_map_data[director].get('hm_rows_directions')
                member_variable_query['percentage_size'] = PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')
                member_variable_query['hm_cell_height'] = PEER_INDIVIDUALS.hm_cell_height

                member_variable_query['header_font_size'] = 11
                if PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')<13 and PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')>10:
                    member_variable_query['header_font_size'] = 10
                elif PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')<=10:
                    member_variable_query['header_font_size'] = 9

                member_variable_query['number_font_size'] = 10
                if PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')<13 and PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')>10:
                    member_variable_query['number_font_size'] = 8
                elif PEER_INDIVIDUALS.heat_map_data[director].get('percentage_size')<=10:
                    member_variable_query['number_font_size'] = 7

                try:
                    member_variable_query['skills_matrix_df'] = PEER_INDIVIDUALS.skills_matrix_df[director]
                    member_variable_query['skills_matrix_info'] = PEER_INDIVIDUALS.skills_matrix_info[director]
                    member_variable_query['self_skills_present'] = True
                except:
                    member_variable_query['self_skills_present'] = False

                peer_individuals_templater(peer_filename, member_variable_query, report_type, report_to_run, questions_df, ppl_df, no_self_ratings_bool, ind_question_column_width,ind_heat_map_section_bool,ind_section_analysis_bool,ind_hilo_section_bool,ind_peer_self_report_bool,ind_statistical_analysis_bool, director_name=director, randomize = randomize)

    return peer_reports

def peer_composite_templater(file_name, member_variable_query, report_type, df, questions_df, ppl_df, no_self_ratings_bool, question_column_width,heat_map_section_bool,section_analysis_bool,esigma_bar_chart_bool,hilo_section_bool,peer_self_report_bool,statistical_analysis_bool, randomize = True, anonymized=False):
    now = datetime.now()
    formatted_date = str(now.strftime("%B")) + ' ' + str(now.day) + ', ' + str(now.year)

    file_loader =FileSystemLoader(app.config['REPORT_TEMPLATES'])
    env = Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)
    template = env.get_template('peer_composite_report_template.html')

    skills_matrix_present = 'skills_matrix' in questions_df['type'].unique()
    try:
        client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file_name).group(1)
    except:
        client_name = 'client name'

    jinja_input_dict = {'title': 'Nasdaq Peer Evaluation', 'nasdaq_logo': os.path.join(app.config['IMAGES'], 'nasdaq_logo.png'),
         'client_name': client_name, 'year': str(now.year), 'formatted_date': formatted_date, 'skills_matrix_present': skills_matrix_present,
         'questions_df': questions_df, 'scale_image': os.path.join(app.config['IMAGES'], 'scale.png'), 'five_scale_image': os.path.join(app.config['IMAGES'], 'five_scale.png'),
         'question_column_width': question_column_width, 'no_self_ratings_bool': no_self_ratings_bool, 'heat_map_section_bool': heat_map_section_bool,
         'section_analysis_bool': section_analysis_bool, 'esigma_bar_chart_bool': esigma_bar_chart_bool, 'hilo_section_bool': hilo_section_bool, 'peer_self_report_bool': peer_self_report_bool, 'statistical_analysis_bool': statistical_analysis_bool, 'anonymized':anonymized}


    if anonymized == False:
        jinja_input_dict['eSigma_bar_chart'] = os.path.join(app.config['CHARTS'], 'esigma_bar_chart.png')
        jinja_input_dict['section_avg_bar_chart'] = os.path.join(app.config['CHARTS'], 'section_avg_bar_chart.png')
    else:
        jinja_input_dict['eSigma_bar_chart'] = os.path.join(app.config['CHARTS'], 'anonymized_esigma_bar_chart.png')
        jinja_input_dict['section_avg_bar_chart'] = os.path.join(app.config['CHARTS'], 'anonymized_section_avg_bar_chart.png')

    # Get Assessment sections labels and section names
    section_names_and_labels = {}
    label_re = re.compile(r'^[\s\t]*([IVX]+\.)[\s\t]*(.*)')
    for section in questions_df['section'].unique().tolist():
        label = label_re.match(section)
        # print(section)
        section_names_and_labels[label.group(1)] = label.group(2)
    jinja_input_dict['section_names_and_labels'] = section_names_and_labels

    # Control which sections are in the table of contents
    # jinja_input_dict['table_of_contents_titles'] = ['#heat_map_title', '#section_analysis_title', '#eSigma_title_bar_chart', '#hilo_title', '#peer_and_self_report_title', '#statistical_analysis_title']

    jinja_input_dict['table_of_contents_titles'] = []
    if heat_map_section_bool:
        jinja_input_dict['table_of_contents_titles'].append('#heat_map_title')
    if section_analysis_bool:
        jinja_input_dict['table_of_contents_titles'].append('#section_analysis_title')
    if esigma_bar_chart_bool:
        jinja_input_dict['table_of_contents_titles'].append('#eSigma_title_bar_chart')
    if hilo_section_bool:
        jinja_input_dict['table_of_contents_titles'].append('#hilo_title')
    if member_variable_query.get('skills_matrix_average') is not None:
        jinja_input_dict['table_of_contents_titles'].append('#skills_matrix_title')
    if peer_self_report_bool:
        jinja_input_dict['table_of_contents_titles'].append('#peer_and_self_report_title')
    if statistical_analysis_bool:
        jinja_input_dict['table_of_contents_titles'].append('#statistical_analysis_title')


    client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file_name)
    jinja_input_dict['client_name'] = client_name.group(1)

    jinja_input_dict['number_of_sections'] = len(questions_df['section'].unique().tolist())
    jinja_input_dict['number_of_long_form_questions'] = len(questions_df[questions_df['type']=='long_form'])
    jinja_input_dict['number_of_rated_questions'] = len(questions_df['question_raw']) - len(questions_df[questions_df['type']=='long_form']) - len(questions_df[questions_df['type']=='skill_matrix']) - len(questions_df[questions_df['type']=='attribute_matrix'])

    jinja_input_dict['report_type'] = report_type

    for item in member_variable_query.keys():
        jinja_input_dict['{}'.format(item)] = member_variable_query[item]

    output = template.render(jinja_input_dict)

    if anonymized == False:
        HTML(string=output, base_url=os.path.dirname(os.path.realpath(__file__))).write_pdf(os.path.join(app.config['REPORTS'], '{}_{}.pdf'.format(client_name.group(1), report_type)), stylesheets=[CSS(os.path.abspath(os.path.join(os.sep, app.config['CSS'], 'peer_composite_template_style.css')))])
    else:
        HTML(string=output, base_url=os.path.dirname(os.path.realpath(__file__))).write_pdf(os.path.join(app.config['REPORTS'], '{}_{}_Anonymized.pdf'.format(client_name.group(1), report_type)), stylesheets=[CSS(os.path.abspath(os.path.join(os.sep, app.config['CSS'], 'peer_composite_template_style.css')))])



def delete_excel_output():
    for file in os.listdir(app.config["EXCEL_OUTPUT"]):
        if file.endswith(".xlsx"):
            os.remove(os.path.join(app.config["EXCEL_OUTPUT"], file))

def peer_individuals_templater(file_name, member_variable_query, report_type, df, questions_df, ppl_df, no_self_ratings_bool, question_column_width,ind_heat_map_section_bool,ind_section_analysis_bool,ind_hilo_section_bool,ind_peer_self_report_bool,ind_statistical_analysis_bool, director_name, randomize = True):
    now = datetime.now()
    formatted_date = str(now.strftime("%B")) + ' ' + str(now.day) + ', ' + str(now.year)

    # print('question_column_width: ', question_column_width, 'no_self_ratings_bool: ',no_self_ratings_bool,
    #      'peer_self_comparison_bool: ',ind_section_analysis_bool, 'hilo_section_bool: ',ind_hilo_section_bool, 'peer_self_report_bool: ',ind_peer_self_report_bool, 'heat_map_section_bool: ',ind_heat_map_section_bool, 'statistical_analysis_bool: ',ind_statistical_analysis_bool)

    file_loader =FileSystemLoader(app.config['REPORT_TEMPLATES'])
    env = Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)
    template = env.get_template('peer_individuals_report_template.html')


    skills_matrix_present = 'skills_matrix' in questions_df['type'].unique()
    if member_variable_query['eval_stats_list_of_dicts'][1].get('Number of Board members surveyed') == 0:
        del member_variable_query['eval_stats_list_of_dicts'][1]

    jinja_input_dict = {'director_name': director_name, 'title': 'Nasdaq Peer Report', 'nasdaq_logo': os.path.join(app.config['IMAGES'], 'nasdaq_logo.png'),
         'board_name': 'XYZ', 'year': str(now.year), 'formatted_date': formatted_date, 'skills_matrix_present': skills_matrix_present,
         'questions_df': questions_df, 'scale_image': os.path.join(app.config['IMAGES'], 'scale.png'), 'five_scale_image': os.path.join(app.config['IMAGES'], 'five_scale.png'), 'question_column_width': question_column_width, 'no_self_ratings_bool': no_self_ratings_bool,
         'peer_self_comparison_bool': ind_section_analysis_bool, 'hilo_section_bool': ind_hilo_section_bool, 'peer_self_report_bool': ind_peer_self_report_bool, 'heat_map_section_bool': ind_heat_map_section_bool, 'statistical_analysis_bool': ind_statistical_analysis_bool}

    print_safe_respondent = re.sub('&#8217;', "", director_name)
    if ind_section_analysis_bool:
        jinja_input_dict['peer_self_bar_chart'] = os.path.join(app.config['CHARTS'], '{}_peer_self.png'.format(print_safe_respondent))

    # Get Assessment sections labels and section names
    section_names_and_labels = {}
    label_re = re.compile(r'^[\s\t]*([IVX]+\.)[\s\t]*(.*)')
    for section in questions_df['section'].unique().tolist():
        label = label_re.match(section)
        section_names_and_labels[label.group(1)] = label.group(2)
    jinja_input_dict['section_names_and_labels'] = section_names_and_labels


    # Control which sections are in the table of contents
    # jinja_input_dict['table_of_contents_titles'] = ['#heat_map_title', '#peer_self_comparison_graph_title', '#hilo_title', '#peer_and_self_report_title', '#statistical_analysis_title']

    jinja_input_dict['table_of_contents_titles'] = []
    if ind_heat_map_section_bool:
        jinja_input_dict['table_of_contents_titles'].append('#heat_map_title')
    if ind_section_analysis_bool:
        jinja_input_dict['table_of_contents_titles'].append('#peer_self_comparison_graph_title')
    if ind_hilo_section_bool:
        jinja_input_dict['table_of_contents_titles'].append('#hilo_title')
    if member_variable_query.get('self_skills_present'):
        jinja_input_dict['table_of_contents_titles'].append('#skills_matrix_title')
    if ind_peer_self_report_bool:
        jinja_input_dict['table_of_contents_titles'].append('#peer_and_self_report_title')
    if ind_statistical_analysis_bool:
        jinja_input_dict['table_of_contents_titles'].append('#statistical_analysis_title')


    client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file_name)

    jinja_input_dict['client_name'] = client_name.group(1)

    jinja_input_dict['number_of_sections'] = len(questions_df['section'].unique().tolist())
    jinja_input_dict['number_of_long_form_questions'] = len(questions_df[questions_df['type']=='long_form'])
    jinja_input_dict['number_of_rated_questions'] = len(questions_df['question_raw']) - len(questions_df[questions_df['type']=='long_form']) - len(questions_df[questions_df['type']=='skill_matrix']) - len(questions_df[questions_df['type']=='attribute_matrix'])

    jinja_input_dict['report_type'] = report_type

    for item in member_variable_query.keys():
        jinja_input_dict['{}'.format(item)] = member_variable_query[item]

    # print('\n\n', jinja_input_dict['jinja_for_loop_dict'])

    output = template.render(jinja_input_dict)
    # with open("test.html", "w") as fh:
    #     fh.write(output)

    # print('\n\nREPORTS: ', app.config['REPORTS'])
    # print('\n\nbase URL: ', os.path.dirname(os.path.realpath(__file__)))

    HTML(string=output, base_url=os.path.dirname(os.path.realpath(__file__))).write_pdf(os.path.join(app.config['REPORTS'], '{}_{}_{}.pdf'.format(print_safe_respondent, client_name.group(1), report_type)), stylesheets=[CSS(os.path.abspath(os.path.join(os.sep, app.config['CSS'], 'peer_individuals_template_style.css')))])
    # HTML(string=output, base_url=os.path.dirname(os.path.realpath(__file__))).write_pdf(os.path.join(app.config['REPORTS'], '{}_{}.pdf'.format(client_name.group(1), report_type)), stylesheets=[CSS(os.path.abspath(os.path.join(os.sep, app.config['CSS'], 'bea_template_style.css')))])

def peer_join_qdf_and_rawdata_into_smoothed_df(qdf, tdf, file, report_type, anonymize):

    ############################################################################
    # ETL
    df = pd.DataFrame(columns=['client_name', 'report_type', 'section', 'number', 'question_raw', 'question', 'sub_section', 'type', 'average', 'response', 'respondent_id', 'question_mean'])
    try:
        client_name = re.search('(.*?)(_csvExport)?(\s*\(\d\))?\.csv$' , file).group(1)
    except:
        client_name = None

    if anonymize ==True:
        director_encoder = dict()
        director_count = 1
    # qdf.to_excel('qdf.xlsx')
    # tdf.to_excel('tdf.xlsx')
    for s in qdf['section'].unique():
        for q in qdf[qdf['section']==s]['question_raw']:
            for i in tdf[q].index:
                # if (tdf.loc[i,q]==np.nan or (tdf.loc[i,q]=='nan')):
                #     print('here!', i, q)
                #     response = None
                # else:
                #     response = tdf.loc[i,q]
                if anonymize == False:
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section_number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_number'].item(), 'sub_section_value': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_value'].item(),
                            'sub_section': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item(), 'response': tdf.loc[i,q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}
                else:
                    sub_q = qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section'].item()
                    if sub_q not in ['', '!__!']:
                        if director_encoder.get(sub_q) is None:
                            director_encoder[sub_q] = 'Director {}'.format(director_count)
                            sub_q = 'Director {}'.format(director_count)
                            director_count += 1
                        else:
                            sub_q = director_encoder.get(sub_q)
                    else:
                        pass
                    row = {'section': s, 'question_raw': q, 'question': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['question'].item(),
                            'type': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['type'].item(), 'number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['number'].item(),
                            'sub_section_number': qdf[(qdf['section']==s)&(qdf['question_raw']==q)]['sub_section_number'].item(), 'sub_section_value': director_encoder.get(sub_q),
                            'sub_section': sub_q, 'response': tdf.loc[i,q], 'respondent_id': i,
                            'client_name': client_name, 'report_type': report_type}

                df = df.append(row, ignore_index=True)

    question_means = []
    for section in df['section'].unique():
        for number in df[(df['section']==section)]['number'].unique():
            try:
                mean_by_question = df[(df['section']==section) & (df['number']==number)]['response'].mean()
                mean_by_question = "{0:.2f}%".format(Decimal('{}'.format(mean_by_question)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                mean_by_question = None
            mean_column_addition = [mean_by_question]*len(df[(df['section']==section) & (df['number']==number)]['response'])
            question_means.extend(mean_column_addition)

    df['question_mean'] = question_means
    df.to_excel(os.path.join(app.config['REPORTS'], 'SMOOTHED_{}_{}.xlsx'.format(client_name, report_type)))
    return df

def create_peer_composite_jinja_dict(qdf, tdf, smoothed_df, file, report_type, no_self_ratings_bool, anonymize, randomize):
    # Get Board Averages
    # print('\n\nHERE!')
    board_question_averages = dict()
    for section in smoothed_df['section'].unique():
        for number in smoothed_df[smoothed_df['section']==section]['number'].unique():
            try:
                avg = smoothed_df[(smoothed_df['section']==section) & (smoothed_df['number']==number)]['response'].mean()
                board_question_averages[(section, number)] = "{0:.2f}".format(Decimal('{}'.format(avg)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                board_question_averages[(section, number)] = None

    data_dict = OrderedDict()
    rankFlag = False
    if anonymize ==True:
        director_encoder = dict()
        director_count = 1

    # with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
    #     print(qdf)
    for section in qdf['section'].unique():
        if set(qdf[qdf['section']==section]['type'].tolist()) <= set(['skills_matrix', '', 'nan']):
            continue
        # print('\n')
        # print('section: ', section)
        data_dict[section] = OrderedDict()
        for number in qdf[qdf['section']==section]['number'].unique():
            # print('\nnumber: ', number)
            data_dict[section][number] = OrderedDict()
            try:
                question_average = smoothed_df[(smoothed_df['section']==section) & (smoothed_df['number']==number)]['response'].mean()
                question_average = "{0:.2f}".format(Decimal('{}'.format(question_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                question_average = None
            data_dict[section][number]['question_average'] = question_average
            data_dict[section][number]['type'] = qdf[(qdf['section']==section)&(qdf['number']==number)]['type'].unique().tolist()[0]
            data_dict[section][number]['question'] = qdf[(qdf['section']==section)&(qdf['number']==number)]['question'].unique().tolist()[0]
            # data_dict[section][number]['board_average']
            # Iterate through raw questions because sub_section is undefined when
            # improper syntax is used
            data_dict[section][number]['raw_questions'] = OrderedDict()
            for raw_q in qdf[(qdf['section']==section)&(qdf['number']==number)]['question_raw']:
                # print('\n\n')
                # print('raw_q: ', raw_q)
                data_dict[section][number]['raw_questions'][raw_q] = {}
                # print(data_dict[section][number])
                # print(data_dict)

                sub_q = qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['sub_section'].item()

                if anonymize:
                    if sub_q not in ['', '!__!']:
                        if director_encoder.get(sub_q) is None:
                            director_encoder[sub_q] = 'Director {}'.format(director_count)
                            data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = 'Director {}'.format(director_count)
                            director_count += 1
                        else:
                            data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = director_encoder.get(sub_q)
                    else:
                        data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = sub_q
                else:
                    data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = sub_q

                if qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['type'].item() in ['matrix', 'likert']:
                    answers = tdf[raw_q].replace({np.nan: None})
                    answers.fillna('N/A', inplace=True)
                    answers = answers.tolist()
                    answers = [int(x) if x != 'N/A' else x for x in answers]
                    if no_self_ratings_bool:
                        answers.remove('N/A')
                    if randomize:
                        shuffle(answers)
                        data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers
                    else:
                        data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers

                    data_dict[section][number]['raw_questions'][raw_q]['average'] = str(Decimal(np.mean(tdf[raw_q].dropna())).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                    data_dict[section][number]['raw_questions'][raw_q]['board_average'] = board_question_averages.get((section, number))
                elif qdf[(qdf['section']==section)&(qdf['question_raw']==raw_q)]['type'].item() in ['long_form', 'optional_long_form']:
                    try:
                        if math.isnan(sub_q):
                            data_dict[section][number]['raw_questions'][raw_q]['sub_section'] = 'None'
                    except:
                        pass
                    answers = tdf[raw_q].dropna().tolist()
                    answers = [a for a in answers if a not in ['nan', '#NAME?', None, 'None', 'No Comment', 'no comment', 'none', np.nan]]

                    if (len(answers) == 0) and (data_dict[section][number]['type'] == 'optional_long_form'):
                        del data_dict[section][number]
                        continue

                    if randomize:
                        shuffle(answers)
                    else:
                        pass
                    data_dict[section][number]['raw_questions'][raw_q]['answers'] = answers

    # if not anonymize:
    #     count = 0
    #     for s in data_dict:
    #         print('\n\nSection Name:  ', s)
    #         for number in data_dict[s]:
    #             print('Number:  ', number)
    #             try:
    #                 print('Question Average:  ', data_dict[s][number]['question_average'])
    #             except:
    #                 pass
    #             print('Type:  ', data_dict[s][number]['type'])
    #             print('Question: ', data_dict[s][number]['question'])
    #             for raw in data_dict[s][number]['raw_questions']:
    #                 print('Raw:  ', raw)
    #                 print('Sub:  ', data_dict[s][number]['raw_questions'][raw]['sub_section'])
    #                 print('Answers:  ', data_dict[s][number]['raw_questions'][raw]['answers'])
    #                 try:
    #                     print('Average:  ', data_dict[s][number]['raw_questions'][raw]['average'])
    #                     print('Board Average:  ', data_dict[s][number]['raw_questions'][raw]['board_average'])
    #                 except:
    #                     pass
    #                 print('\n')
    #             count+=1
                # if count == 10:
                #     break




        # print(data_dict[list(data_dict.keys())[0]]['1.']['Comments/Suggestions'])

    return data_dict

def create_peer_individuals_jinja_dict(qdf, tdf, smoothed_df, file, report_type, no_self_ratings_bool, anonymize, randomize):
    # Get Board Averages
    board_question_averages = dict()
    for section in smoothed_df['section'].unique():
        for number in smoothed_df[smoothed_df['section']==section]['number'].unique():
            try:
                avg = smoothed_df[(smoothed_df['section']==section) & (smoothed_df['number']==number)]['response'].mean()
                board_question_averages[(section, number)] = "{0:.2f}".format(Decimal('{}'.format(avg)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
            except:
                board_question_averages[(section, number)] = None

    data_dict = dict()
    individuals_na_dict = dict()
    for director in smoothed_df[(~smoothed_df['type'].isin(['skills_matrix', 'attribute_matrix', '']))]['sub_section'].unique():
        if director not in ['', None, np.nan]:
            # print('\nDirector: ', director)
            # print('No Self-Ratings: ', no_self_ratings_bool)
            # print(qdf[~(qdf['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (qdf['sub_section']==director)]['UniqueIdentifier'].unique())
            ratee_is_also_rater = qdf[~(qdf['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (qdf['sub_section']==director)]['UniqueIdentifier'].unique()[0] not in [None, np.nan, 'nan']
            # print(ratee_is_also_rater)
            # print('ratee is also rater? ', director, '\n', ratee_is_also_rater)
            if ratee_is_also_rater:
                row_id = qdf[~(qdf['type'].isin(['skills_matrix', 'attribute_matrix', ''])) & (qdf['sub_section']==director)]['UniqueIdentifier'].unique()[0]
            else:
                row_id = director
            # print(row_id)
            if director not in ['', '!__!']:
                data_dict[director] = OrderedDict()
                for section in smoothed_df[smoothed_df['sub_section']==director]['section'].unique():
                    data_dict[director][section] = OrderedDict()
                    for number in smoothed_df[(smoothed_df['section']==section)&(smoothed_df['sub_section']==director)]['number'].unique():
                        data_dict[director][section][number] = dict()
                        if ratee_is_also_rater:
                            # self_rating = smoothed_df[(smoothed_df['section']==section)&(smoothed_df['sub_section']==director)&(smoothed_df['number']==number)&(smoothed_df['respondent_id']==row_id)]['response'].item()
                            # print('question: ', smoothed_df[(smoothed_df['section']==section)&(smoothed_df['number']==number)]['question'].unique().tolist()[0])
                            # print('Self-Rating: ', self_rating)
                            try:
                                self_rating = smoothed_df[(smoothed_df['section']==section)&(smoothed_df['sub_section']==director)&(smoothed_df['number']==number)&(smoothed_df['respondent_id']==row_id)]['response'].item()
                                data_dict[director][section][number]['self_rating'] = int(self_rating)
                                data_dict[director][section][number]['answers'] = [int(self_rating)]
                            except:
                                data_dict[director][section][number]['self_rating'] = 'N/A'
                                data_dict[director][section][number]['answers'] = ['N/A']
                        else:
                            self_rating = 'N/A'
                            data_dict[director][section][number]['self_rating'] = self_rating
                            data_dict[director][section][number]['answers'] = [self_rating]

                        # Simply overwrite the self-rating with an empty list if no_self_ratings_bool is True
                        if no_self_ratings_bool:
                            data_dict[director][section][number]['answers'] = []

                        try:
                            board_average = smoothed_df[(smoothed_df['section']==section) & (smoothed_df['number']==number)]['response'].mean()
                            board_average = "{0:.2f}".format(Decimal('{}'.format(board_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                        except:
                            board_average = None
                        try:
                            respondent_average = smoothed_df[(smoothed_df['section']==section) & (smoothed_df['number']==number) & (smoothed_df['sub_section']==director)]['response'].mean()
                            respondent_average = "{0:.2f}".format(Decimal('{}'.format(respondent_average)).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                        except:
                            question_average = None
                        data_dict[director][section][number]['board_average'] = board_average
                        data_dict[director][section][number]['respondent_average'] = respondent_average
                        data_dict[director][section][number]['type'] = smoothed_df[(smoothed_df['section']==section)&(smoothed_df['number']==number)]['type'].unique().tolist()[0]
                        data_dict[director][section][number]['question'] = smoothed_df[(smoothed_df['section']==section)&(smoothed_df['number']==number)]['question'].unique().tolist()[0]

                        if data_dict[director][section][number]['type'] in ['matrix', 'likert']:
                            # calculate average
                            answers = smoothed_df[(smoothed_df['section']==section)&(smoothed_df['number']==number)&(smoothed_df['sub_section']==director)][['respondent_id', 'response']]
                            # print(answers['response'].tolist())
                            # print(bool(all(answers['response'].tolist())))
                            # print(answers['response'].tolist()[0], type(answers['response'].tolist()[0]))

                            # print(row_id)
                            answers.set_index('respondent_id', inplace=True)
                            if np.isnan(answers['response'].tolist()).any():
                                individuals_na_dict[director] = True
                            # print(answers)
                            avg = np.mean(answers['response'].dropna())
                            answers = answers.replace({np.nan: None})
                            answers.fillna('N/A', inplace=True)
                            if ratee_is_also_rater:
                                answers.drop(index=row_id, inplace=True)
                            # print(answers)
                            answers = answers['response'].tolist()
                            answers = [int(x) if x != 'N/A' else x for x in answers]
                            if randomize:
                                shuffle(answers)

                            data_dict[director][section][number]['answers'].extend(answers)
                            # print(data_dict[director][section][number]['answers'])
                            data_dict[director][section][number]['average'] = str(Decimal(avg).quantize(Decimal('1e-2'), ROUND_HALF_UP))
                            data_dict[director][section][number]['board_average'] = board_question_averages.get((section, number))
                        elif data_dict[director][section][number]['type']in ['long_form', 'optional_long_form']:
                            answers = smoothed_df[(smoothed_df['section']==section)&(smoothed_df['number']==number)&(smoothed_df['sub_section']==director)]['response'].tolist()
                            answers = [a for a in answers if a not in ['nan', '#NAME?', None, 'None', 'No Comment', 'no comment', 'none', np.nan]]

                            if randomize:
                                shuffle(answers)
                            if self_rating not in ['nan', '#NAME?', None, 'None', 'No Comment', 'no comment', 'none', np.nan]:
                                data_dict[director][section][number]['answers'].extend(answers)
                            else:
                                data_dict[director][section][number]['answers'] = answers
            else:
                pass
    # for e, (k,v) in enumerate(data_dict.items()):
    #     print('\n\n', e)
    #     print('key: ', k)
    #     print(v)
    # print(individuals_na_dict)
    return data_dict, individuals_na_dict
